﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmEmpImprt : Form
    {
        public FrmEmpImprt()
        {
            InitializeComponent();
        }

        string connstring = string.Empty;

         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
  
        SqlCommand qur = new SqlCommand();
        private void FrmEmpImprt_Load(object sender, EventArgs e)
        {
            qur.Connection = conn;
        
        }
       

        private void BtnImport_Click(object sender, EventArgs e)
        {           
            ImportStudentClassWise(connstring, txtSearch.Text);
            //updateemp();

            //MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            grpimport.Visible = true;
            imprtgrid();
        }
        private void updateemp()
        {
            qur.Connection = conn;
            conn.Close();
            conn.Open();
            qur.CommandText = "exec sp_uploadempmast  '"+ GeneralParameters.Branch +"'";
            qur.ExecuteNonQuery();


        }
        private void imprtgrid()
        {
           
            conn.Close();
            conn.Open();
           
            string quy = "exec sp_empgird";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);

          
            Importgrid.AutoGenerateColumns = false;
            Importgrid.Refresh();
            Importgrid.DataSource = null;
            Importgrid.Rows.Clear();

            Importgrid.RowHeadersVisible = false;
            Importgrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            Importgrid.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            Importgrid.ColumnCount = 17;

            Importgrid.Columns[0].Name = "slno";
            Importgrid.Columns[0].HeaderText = "slno";
            Importgrid.Columns[0].DataPropertyName = "slno";
            Importgrid.Columns[0].Width = 50;


            Importgrid.Columns[1].Name = "empcode";
            Importgrid.Columns[1].HeaderText = "EmpCode";
            Importgrid.Columns[1].DataPropertyName = "empcode";
            Importgrid.Columns[1].Width = 100;

            Importgrid.Columns[2].Name = "EMpname";
            Importgrid.Columns[2].HeaderText = "Employee Name";
            Importgrid.Columns[2].DataPropertyName = "EMpname";
            Importgrid.Columns[2].Width = 120;


            Importgrid.Columns[3].Name = "FatherName";
            Importgrid.Columns[3].HeaderText = "FatherName";
            Importgrid.Columns[3].DataPropertyName = "FatherName";
            Importgrid.Columns[3].Width = 100;

            Importgrid.Columns[4].Name = "DOB";
            Importgrid.Columns[4].HeaderText = "DOB";
            Importgrid.Columns[4].DataPropertyName = "DOB";
            Importgrid.Columns[4].Width = 100;

            Importgrid.Columns[5].Name = "DOJ";
            Importgrid.Columns[5].HeaderText = "DOJ";
            Importgrid.Columns[5].DataPropertyName = "DOJ";
            Importgrid.Columns[5].Width = 100;

            Importgrid.Columns[6].Name = "SEX";
            Importgrid.Columns[6].HeaderText = "SEX";
            Importgrid.Columns[6].DataPropertyName = "SEX";
            Importgrid.Columns[6].Width = 60;

            Importgrid.Columns[7].Name = "Emptype";
            Importgrid.Columns[7].HeaderText = "Emptype";
            Importgrid.Columns[7].DataPropertyName = "Emptype";
            Importgrid.Columns[7].Width = 60;

            Importgrid.Columns[8].Name = "Department";
            Importgrid.Columns[8].HeaderText = "Department";
            Importgrid.Columns[8].DataPropertyName = "Department";
            Importgrid.Columns[8].Width = 100;
            Importgrid.Columns[9].Name = "Designation";
            Importgrid.Columns[9].HeaderText = "Designation";
            Importgrid.Columns[9].DataPropertyName = "Designation";
            Importgrid.Columns[9].Width = 100;
            Importgrid.Columns[10].Name = "category";
            Importgrid.Columns[10].HeaderText = "category";
            Importgrid.Columns[10].DataPropertyName = "category";
            Importgrid.Columns[10].Width = 100;
            Importgrid.Columns[11].Name = "PFESI";
            Importgrid.Columns[11].HeaderText = "PFESI";
            Importgrid.Columns[11].DataPropertyName = "PFESI";
            Importgrid.Columns[11].Width = 100;
            Importgrid.Columns[12].Name = "GrossSalary";
            Importgrid.Columns[12].HeaderText = "GrossSalary";
            Importgrid.Columns[12].DataPropertyName = "GrossSalary";
            Importgrid.Columns[12].Width = 100;
            Importgrid.Columns[13].Name = "Aadhar";
            Importgrid.Columns[13].HeaderText = "Aadhar";
            Importgrid.Columns[13].DataPropertyName = "Aadhar";
            Importgrid.Columns[13].Width = 100;
            Importgrid.Columns[14].Name = "PanNo";
            Importgrid.Columns[14].HeaderText = "PanNo";
            Importgrid.Columns[14].DataPropertyName = "PanNo";
            Importgrid.Columns[14].Width = 100;
            Importgrid.Columns[15].Name = "Aaddress";
            Importgrid.Columns[15].HeaderText = "Aaddress";
            Importgrid.Columns[15].DataPropertyName = "Aaddress";
            Importgrid.Columns[15].Width = 100;
            Importgrid.Columns[16].Name = "PunchNo";
            Importgrid.Columns[16].HeaderText = "PunchNo";
            Importgrid.Columns[16].DataPropertyName = "PunchNo";
            Importgrid.Columns[16].Width = 100;

            Importgrid.DataSource = tab;



        }
        protected void ImportStudentClassWise(string ConnectionString,string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();
                    
                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    new DataColumn("SlNo", typeof(float));
                    new DataColumn("EmpCode", typeof(float));
                    new DataColumn("EmpName", typeof(string));
                    new DataColumn("FatherName", typeof(string));
                    new DataColumn("DOB", typeof(DateTime));
                    new DataColumn("DOJ", typeof(DateTime));
                    new DataColumn("Sex", typeof(string));
                    new DataColumn("Type", typeof(string));
                    new DataColumn("Department", typeof(string));
                    new DataColumn("Desingnation", typeof(string));
                    new DataColumn("Category", typeof(string));
                    new DataColumn("esipf", typeof(string));
                    new DataColumn("GrossSalary", typeof(float));
                    new DataColumn("Aadhar", typeof(string));
                    new DataColumn("PanNo", typeof(string));
                    new DataColumn("Address", typeof(string));
                    new DataColumn("PunchNo", typeof(string));

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        string query = "truncate table  ImportEMP ";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();

                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.ImportEMP";                            
                            sqlbulkcopy.ColumnMappings.Add("SlNo", "SLNo");
                            sqlbulkcopy.ColumnMappings.Add("EmpCode", "EMpCode");
                            sqlbulkcopy.ColumnMappings.Add("EmpName", "EmpName");
                            sqlbulkcopy.ColumnMappings.Add("FatherName", "FatherName");
                            sqlbulkcopy.ColumnMappings.Add("DOB", "DOB");
                            sqlbulkcopy.ColumnMappings.Add("DOJ", "DOJ");
                            sqlbulkcopy.ColumnMappings.Add("Sex", "Sex");
                            sqlbulkcopy.ColumnMappings.Add("Type", "EMPType");
                            sqlbulkcopy.ColumnMappings.Add("Department", "Department");
                            sqlbulkcopy.ColumnMappings.Add("Desingnation", "Designation");
                            sqlbulkcopy.ColumnMappings.Add("Category", "Category");
                            sqlbulkcopy.ColumnMappings.Add("esipf", "PFESI");
                            sqlbulkcopy.ColumnMappings.Add("GrossSalary", "GrossSalary");
                            sqlbulkcopy.ColumnMappings.Add("Aadhar", "Aadhar");
                            sqlbulkcopy.ColumnMappings.Add("PanNo", "PanNo");
                            sqlbulkcopy.ColumnMappings.Add("Address", "Address");
                            sqlbulkcopy.ColumnMappings.Add("PunchNo", "PunchNo");
                            conn.Open();                            
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();                            
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            //connstring = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
            dialog.ShowDialog();
            txtSearch.Text = dialog.FileName;
            txtSearch.Visible = true;
            string extensionpath = Path.GetExtension(dialog.FileName);
            switch (extensionpath)
            {
                case ".xls":
                    connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx":
                    connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    return;
            }

        }

        private void FrmEmpImprt_Load_1(object sender, EventArgs e)
        {
            qur.Connection = conn;
            grpimport.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            grpimport.Visible = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
