﻿namespace MyEasyPay
{
    partial class FrmGeneratePay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.DataGridPay = new System.Windows.Forms.DataGridView();
            this.lblName = new System.Windows.Forms.Label();
            this.CmbDepartment = new System.Windows.Forms.ComboBox();
            this.BtnExit = new System.Windows.Forms.Button();
            this.BtnPayGenerate = new System.Windows.Forms.Button();
            this.BtnExcelExport = new System.Windows.Forms.Button();
            this.GridPay = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.BtnView = new System.Windows.Forms.Button();
            this.CmbMonth = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.GrFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPay)).BeginInit();
            this.SuspendLayout();
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.label4);
            this.GrFront.Controls.Add(this.CmbBranch);
            this.GrFront.Controls.Add(this.checkBox1);
            this.GrFront.Controls.Add(this.DataGridPay);
            this.GrFront.Controls.Add(this.lblName);
            this.GrFront.Controls.Add(this.CmbDepartment);
            this.GrFront.Controls.Add(this.BtnExit);
            this.GrFront.Controls.Add(this.BtnPayGenerate);
            this.GrFront.Controls.Add(this.BtnExcelExport);
            this.GrFront.Controls.Add(this.GridPay);
            this.GrFront.Controls.Add(this.BtnView);
            this.GrFront.Controls.Add(this.CmbMonth);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(9, 2);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(1052, 547);
            this.GrFront.TabIndex = 0;
            this.GrFront.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(175, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 240;
            this.label4.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(228, 17);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(219, 26);
            this.CmbBranch.TabIndex = 239;
            this.CmbBranch.SelectedIndexChanged += new System.EventHandler(this.CmbBranch_SelectedIndexChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(736, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(44, 22);
            this.checkBox1.TabIndex = 22;
            this.checkBox1.Text = "All";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // DataGridPay
            // 
            this.DataGridPay.BackgroundColor = System.Drawing.Color.White;
            this.DataGridPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridPay.Location = new System.Drawing.Point(7, 53);
            this.DataGridPay.Name = "DataGridPay";
            this.DataGridPay.Size = new System.Drawing.Size(1040, 457);
            this.DataGridPay.TabIndex = 21;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(454, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 18);
            this.lblName.TabIndex = 20;
            this.lblName.Text = "Dept";
            // 
            // CmbDepartment
            // 
            this.CmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDepartment.FormattingEnabled = true;
            this.CmbDepartment.Location = new System.Drawing.Point(510, 17);
            this.CmbDepartment.Name = "CmbDepartment";
            this.CmbDepartment.Size = new System.Drawing.Size(215, 26);
            this.CmbDepartment.TabIndex = 19;
            this.CmbDepartment.SelectedIndexChanged += new System.EventHandler(this.CmbDepartment_SelectedIndexChanged);
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(959, 515);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(84, 26);
            this.BtnExit.TabIndex = 7;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // BtnPayGenerate
            // 
            this.BtnPayGenerate.Location = new System.Drawing.Point(876, 21);
            this.BtnPayGenerate.Name = "BtnPayGenerate";
            this.BtnPayGenerate.Size = new System.Drawing.Size(167, 26);
            this.BtnPayGenerate.TabIndex = 6;
            this.BtnPayGenerate.Text = "Generate Payment";
            this.BtnPayGenerate.UseVisualStyleBackColor = true;
            this.BtnPayGenerate.Click += new System.EventHandler(this.BtnPayGenerate_Click);
            // 
            // BtnExcelExport
            // 
            this.BtnExcelExport.Location = new System.Drawing.Point(10, 516);
            this.BtnExcelExport.Name = "BtnExcelExport";
            this.BtnExcelExport.Size = new System.Drawing.Size(113, 26);
            this.BtnExcelExport.TabIndex = 5;
            this.BtnExcelExport.Text = "Export to Excel";
            this.BtnExcelExport.UseVisualStyleBackColor = true;
            this.BtnExcelExport.Click += new System.EventHandler(this.Button1_Click);
            // 
            // GridPay
            // 
            this.GridPay.AccessibleName = "Table";
            this.GridPay.AllowFiltering = true;
            this.GridPay.AllowResizingColumns = true;
            this.GridPay.AllowSorting = false;
            this.GridPay.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridPay.Location = new System.Drawing.Point(7, 234);
            this.GridPay.Name = "GridPay";
            this.GridPay.Size = new System.Drawing.Size(101, 130);
            this.GridPay.TabIndex = 4;
            this.GridPay.Text = "sfDataGrid1";
            this.GridPay.Click += new System.EventHandler(this.DataGridPay_Click);
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(786, 20);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(84, 26);
            this.BtnView.TabIndex = 3;
            this.BtnView.Text = "View";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // CmbMonth
            // 
            this.CmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMonth.FormattingEnabled = true;
            this.CmbMonth.Location = new System.Drawing.Point(62, 17);
            this.CmbMonth.Name = "CmbMonth";
            this.CmbMonth.Size = new System.Drawing.Size(107, 26);
            this.CmbMonth.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Month";
            // 
            // FrmGeneratePay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1068, 557);
            this.Controls.Add(this.GrFront);
            this.Name = "FrmGeneratePay";
            this.Text = "Generate Pay";
            this.Load += new System.EventHandler(this.FrmGeneratePay_Load);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.ComboBox CmbMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnView;
        private Syncfusion.WinForms.DataGrid.SfDataGrid GridPay;
        private System.Windows.Forms.Button BtnExcelExport;
        private System.Windows.Forms.Button BtnPayGenerate;
        private System.Windows.Forms.Button BtnExit;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox CmbDepartment;
        private System.Windows.Forms.DataGridView DataGridPay;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbBranch;
    }
}