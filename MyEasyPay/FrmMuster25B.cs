﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.WinForms.DataGridConverter.Events;

namespace MyEasyPay
{
    public partial class FrmMuster25B : Form
    {
        public FrmMuster25B()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        ExcelExportingOptions GridExcelExportingOptions = new ExcelExportingOptions();

        private void FrmMuster25B_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            //if (GeneralParameters.BranchId == 0)
            //{
            //    System.Data.DataRow dataRow = dataTable.NewRow();
            //    dataRow["Uid"] = 0;
            //    dataRow["BRANCHNAME"] = "All";
            //    dataTable.Rows.Add(dataRow);
            //}
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;
            Department();
        }

        public void Department()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDept_Mast", conn);
                CmbDepartment.DataSource = null;
                CmbDepartment.DisplayMember = "DeptName";
                CmbDepartment.ValueMember = "DeptId";
                CmbDepartment.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnView_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridMuster.DataSource = null;
                int Tag;
                if (GeneralParameters.MusterTag == 1)
                {
                    Tag = 1;
                }
                else
                {
                    Tag = 2;
                }

                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    new SqlParameter("@BranchID",CmbBranch.SelectedValue)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME_NEW", sqlParameters, conn);

                string Query = string.Empty;
                if (fromDate.Day < 16)
                {
                    Query = @"select Empcode,EmpName,Dept,Grade,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,CL Pre,LOP 
                            from musterroll25B ORDER BY empcode,SNO";
                }
                else
                {
                    Query = @"select Empcode,EmpName,Dept,Grade,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,EL Pre,LOP 
                            from musterroll25B ORDER BY empcode,SNO";
                }
                DataTable data = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                DatagridMuster.DataSource = data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void ExportFullMonth()
        {
            try
            {
                DatagridMuster.DataSource = null;
                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text); ;
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@MON",fromDate.Month.ToString("00")),
                    new SqlParameter("@YR",fromDate.Year),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    new SqlParameter("@TAG",1),
                            new SqlParameter("@BRANCH", CmbDepartment.Text),
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME", sqlParameters, conn);
                DatagridMuster.DataSource = dataTable;

                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Open(Application.StartupPath + "/MusterRoll.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                xlApp.DisplayAlerts = false;
                xlWb.SaveAs(Application.StartupPath + "/MusterRoll1.xls", Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, true, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing);
                Microsoft.Office.Interop.Excel.Workbook xlWbN = xlApp.Workbooks.Open(Application.StartupPath + "/MusterRoll1.xls", ReadOnly: false, Editable: true) as Microsoft.Office.Interop.Excel.Workbook;
                Microsoft.Office.Interop.Excel.Worksheet xlSht = xlWbN.Sheets[1];
                xlApp.Visible = true;

                Microsoft.Office.Interop.Excel.Range row1 = xlSht.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = xlSht.Rows.Cells[2, 1];
                Microsoft.Office.Interop.Excel.Range row3 = xlSht.Rows.Cells[3, 1];
                row1.Value = GeneralParameters.CompanyName;
                row2.Value = "Muster Roll 25B Report";
                row3.Value = "Month of " + Convert.ToDateTime(DtpFromDate.Text).ToString("MMM-yyyy");
                int j = 5;

                for (int i = 0; i < DatagridMuster.Rows.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range EmployeeCode = xlSht.Rows.Cells[j, 1];
                    Microsoft.Office.Interop.Excel.Range EmployeeName = xlSht.Rows.Cells[j, 2];
                    Microsoft.Office.Interop.Excel.Range Department = xlSht.Rows.Cells[j, 3];
                    Microsoft.Office.Interop.Excel.Range Grade = xlSht.Rows.Cells[j, 4];
                    Microsoft.Office.Interop.Excel.Range D1 = xlSht.Rows.Cells[j, 5];
                    Microsoft.Office.Interop.Excel.Range D2 = xlSht.Rows.Cells[j, 6];
                    Microsoft.Office.Interop.Excel.Range D3 = xlSht.Rows.Cells[j, 7];
                    Microsoft.Office.Interop.Excel.Range D4 = xlSht.Rows.Cells[j, 8];
                    Microsoft.Office.Interop.Excel.Range D5 = xlSht.Rows.Cells[j, 9];
                    Microsoft.Office.Interop.Excel.Range D6 = xlSht.Rows.Cells[j, 10];
                    Microsoft.Office.Interop.Excel.Range D7 = xlSht.Rows.Cells[j, 11];
                    Microsoft.Office.Interop.Excel.Range D8 = xlSht.Rows.Cells[j, 12];
                    Microsoft.Office.Interop.Excel.Range D9 = xlSht.Rows.Cells[j, 13];
                    Microsoft.Office.Interop.Excel.Range D10 = xlSht.Rows.Cells[j, 14];
                    Microsoft.Office.Interop.Excel.Range D11 = xlSht.Rows.Cells[j, 15];
                    Microsoft.Office.Interop.Excel.Range D12 = xlSht.Rows.Cells[j, 16];
                    Microsoft.Office.Interop.Excel.Range D13 = xlSht.Rows.Cells[j, 17];
                    Microsoft.Office.Interop.Excel.Range D14 = xlSht.Rows.Cells[j, 18];
                    Microsoft.Office.Interop.Excel.Range D15 = xlSht.Rows.Cells[j, 19];
                    Microsoft.Office.Interop.Excel.Range D16 = xlSht.Rows.Cells[j, 20];
                    Microsoft.Office.Interop.Excel.Range D17 = xlSht.Rows.Cells[j, 21];
                    Microsoft.Office.Interop.Excel.Range D18 = xlSht.Rows.Cells[j, 22];
                    Microsoft.Office.Interop.Excel.Range D19 = xlSht.Rows.Cells[j, 23];
                    Microsoft.Office.Interop.Excel.Range D20 = xlSht.Rows.Cells[j, 24];
                    Microsoft.Office.Interop.Excel.Range D21 = xlSht.Rows.Cells[j, 25];
                    Microsoft.Office.Interop.Excel.Range D22 = xlSht.Rows.Cells[j, 26];
                    Microsoft.Office.Interop.Excel.Range D23 = xlSht.Rows.Cells[j, 27];
                    Microsoft.Office.Interop.Excel.Range D24 = xlSht.Rows.Cells[j, 28];
                    Microsoft.Office.Interop.Excel.Range D25 = xlSht.Rows.Cells[j, 29];
                    Microsoft.Office.Interop.Excel.Range D26 = xlSht.Rows.Cells[j, 30];
                    Microsoft.Office.Interop.Excel.Range D27 = xlSht.Rows.Cells[j, 31];
                    Microsoft.Office.Interop.Excel.Range D28 = xlSht.Rows.Cells[j, 32];
                    Microsoft.Office.Interop.Excel.Range D29 = xlSht.Rows.Cells[j, 33];
                    Microsoft.Office.Interop.Excel.Range D30 = xlSht.Rows.Cells[j, 34];
                    Microsoft.Office.Interop.Excel.Range D31 = xlSht.Rows.Cells[j, 35];
                    Microsoft.Office.Interop.Excel.Range Pre = xlSht.Rows.Cells[j, 36];
                    Microsoft.Office.Interop.Excel.Range Lop = xlSht.Rows.Cells[j, 37];
                    Microsoft.Office.Interop.Excel.Range CL = xlSht.Rows.Cells[j, 38];
                    Microsoft.Office.Interop.Excel.Range EL = xlSht.Rows.Cells[j, 39];
                    EmployeeCode.Value = DatagridMuster.Rows[i].Cells[1].Value.ToString();
                    EmployeeName.Value = DatagridMuster.Rows[i].Cells[3].Value.ToString();
                    Department.Value = DatagridMuster.Rows[i].Cells[4].Value.ToString();
                    Grade.Value = DatagridMuster.Rows[i].Cells[5].Value.ToString();
                    D1.Value = DatagridMuster.Rows[i].Cells[6].Value.ToString();
                    D2.Value = DatagridMuster.Rows[i].Cells[7].Value.ToString();
                    D3.Value = DatagridMuster.Rows[i].Cells[8].Value.ToString();
                    D4.Value = DatagridMuster.Rows[i].Cells[9].Value.ToString();
                    D5.Value = DatagridMuster.Rows[i].Cells[10].Value.ToString();
                    D6.Value = DatagridMuster.Rows[i].Cells[11].Value.ToString();
                    D7.Value = DatagridMuster.Rows[i].Cells[12].Value.ToString();
                    D8.Value = DatagridMuster.Rows[i].Cells[13].Value.ToString();
                    D9.Value = DatagridMuster.Rows[i].Cells[14].Value.ToString();
                    D10.Value = DatagridMuster.Rows[i].Cells[15].Value.ToString();
                    D11.Value = DatagridMuster.Rows[i].Cells[16].Value.ToString();
                    D12.Value = DatagridMuster.Rows[i].Cells[17].Value.ToString();
                    D13.Value = DatagridMuster.Rows[i].Cells[18].Value.ToString();
                    D14.Value = DatagridMuster.Rows[i].Cells[19].Value.ToString();
                    D15.Value = DatagridMuster.Rows[i].Cells[20].Value.ToString();
                    D16.Value = DatagridMuster.Rows[i].Cells[21].Value.ToString();
                    D17.Value = DatagridMuster.Rows[i].Cells[22].Value.ToString();
                    D18.Value = DatagridMuster.Rows[i].Cells[23].Value.ToString();
                    D19.Value = DatagridMuster.Rows[i].Cells[24].Value.ToString();
                    D20.Value = DatagridMuster.Rows[i].Cells[25].Value.ToString();
                    D21.Value = DatagridMuster.Rows[i].Cells[26].Value.ToString();
                    D22.Value = DatagridMuster.Rows[i].Cells[27].Value.ToString();
                    D23.Value = DatagridMuster.Rows[i].Cells[28].Value.ToString();
                    D24.Value = DatagridMuster.Rows[i].Cells[29].Value.ToString();
                    D25.Value = DatagridMuster.Rows[i].Cells[30].Value.ToString();
                    D26.Value = DatagridMuster.Rows[i].Cells[31].Value.ToString();
                    D27.Value = DatagridMuster.Rows[i].Cells[32].Value.ToString();
                    D28.Value = DatagridMuster.Rows[i].Cells[33].Value.ToString();
                    D29.Value = DatagridMuster.Rows[i].Cells[34].Value.ToString();
                    D30.Value = DatagridMuster.Rows[i].Cells[35].Value.ToString();
                    D31.Value = DatagridMuster.Rows[i].Cells[36].Value.ToString();

                    Pre.Value = DatagridMuster.Rows[i].Cells[37].Value.ToString();
                    Lop.Value = DatagridMuster.Rows[i].Cells[38].Value.ToString();
                    CL.Value = DatagridMuster.Rows[i].Cells[39].Value.ToString();
                    EL.Value = DatagridMuster.Rows[i].Cells[40].Value.ToString();
                    j += 1;
                }
                MessageBox.Show("Exported Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void BtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChckMonthly.Checked == true)
                {
                    ExportFullMonth();
                }
                else
                {
                    ExportData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void GridExcelExportingOptions_CellExporting1(object sender, DataGridCellExcelExportingEventArgs e)
        {
            e.Range.CellStyle.Font.Size = 12;
            e.Range.CellStyle.Font.FontName = "Segoe UI";
            if (e.CellType == ExportCellType.RecordCell)
            {
                if (e.ColumnName == "NET")
                {
                    if (double.TryParse(e.CellValue.ToString(), out double value))
                        e.Range.Number = value;
                    e.Handled = true;
                }
            }
        }

        private void GridExcelExportingOptions_Exporting1(object sender, DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Size = 12;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.BackGroundColor = Color.LightGray;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.CellStyle.FontInfo.Bold = true;
                e.Handled = true;
            }
            else if (e.CellType == ExportCellType.GroupCaptionCell)
            {
                e.CellStyle.FontInfo.Size = 12;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSeaGreen;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.Handled = true;
            }
        }

        private ExcelExportingOptions ExcelExportingOptions1()
        {
            GridExcelExportingOptions.Exporting += GridExcelExportingOptions_Exporting1;
            GridExcelExportingOptions.CellExporting += GridExcelExportingOptions_CellExporting1;
            return GridExcelExportingOptions;
        }
        protected void ExportData()
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet = workbook.ActiveSheet;
            worksheet.Name = "Muster Export";

            for (int x = 1; x < DatagridMuster.Columns.Count + 1; x++)
            {
                worksheet.Cells[1, x] = DatagridMuster.Columns[x - 1].HeaderText;
            }
            for (int i = 0; i < DatagridMuster.Rows.Count - 1; i++)
            {
                for (int j = 0; j < DatagridMuster.Columns.Count; j++)
                {
                    worksheet.Cells[i + 2, j + 1] = DatagridMuster.Rows[i].Cells[j].Value.ToString();
                }
            }
            MessageBox.Show("Export Complted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void BtnWithTime_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridMuster.DataSource = null;

                DateTime fromDate = Convert.ToDateTime(DtpFromDate.Text);
                string fdate = fromDate.Year + "-" + fromDate.Month.ToString("00") + "-" + 01;
                DateTime toDate = Convert.ToDateTime(DtpToDate.Text);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", Convert.ToDateTime(fdate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    new SqlParameter("@BranchID",CmbBranch.SelectedValue)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POSTMUSTERWITHTIME_NEW", sqlParameters, conn);
                DatagridMuster.DataSource = dataTable;
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Muster Export";
                for (int i = 1; i < DatagridMuster.Columns.Count; i++)
                {
                    worksheet.Cells[1, i] = DatagridMuster.Columns[i].HeaderText;
                }
                for (int i = 0; i < DatagridMuster.Rows.Count - 1; i++)
                {
                    for (int j = 0; j < DatagridMuster.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = DatagridMuster.Rows[i].Cells[j].Value.ToString();
                    }
                }
                MessageBox.Show("Export Complted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

