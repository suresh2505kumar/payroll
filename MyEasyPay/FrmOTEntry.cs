﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmOTEntry : Form
    {
        public FrmOTEntry()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();

        BindingSource bsp = new BindingSource();
        DataTable dtOt = new DataTable();
        public int SelectId = 0;

        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbBranch.Text == "All")
                {
                    MessageBox.Show("Select Branch", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (TxtEmpCode.Text != string.Empty && txtEmpName.Text != string.Empty && TxtActualOT.Text != string.Empty && txtApprovedOT.Text != string.Empty)
                {
                    //SqlParameter[] parameters = {
                    //    new SqlParameter("@Uid",TxtEmpCode.Tag),
                    //    new SqlParameter("@OTDt",Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd")),
                    //    new SqlParameter("@Empid",txtEmpName.Tag),
                    //    new SqlParameter("@WHour",TxtActualOT.Text),
                    //    new SqlParameter("@OAmt",txtApprovedOT.Text),
                    //    new SqlParameter("@BranchId",CmbBranch.SelectedValue)
                    //};
                    //db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OT_Entry", parameters, conn);
                    string check = "Select * from Att_reg" + Convert.ToDateTime(DtpDate.Text).Month.ToString("00") + " Where Dt='" + Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd") + "' and EmpCode ='" + TxtEmpCode.Text + "'";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, check, conn);
                    if(dataTable.Rows.Count > 0)
                    {
                        string Query = "Update Att_reg" + Convert.ToDateTime(DtpDate.Text).Month.ToString("00") + " Set Aot =" + txtApprovedOT.Text + " Where Dt='" + Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd") + "' and EmpCode ='" + TxtEmpCode.Text + "'";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        TxtEmpCode.Text = string.Empty;
                        txtEmpName.Text = string.Empty;
                        TxtActualOT.Text = string.Empty;
                        txtEmpName.Tag = "0";
                        GetOtEntry();
                        TxtEmpCode.Focus();
                        bsOTEntry.DataSource = dtOt;
                        LoadOtGrid();
                    }
                    else
                    {
                        MessageBox.Show("Attendance Not Processed fot this date !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtApprovedOT.Focus();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void LoadOtGrid()
        {
            try
            {
                DataGridOT.DataSource = null;
                DataGridOT.AutoGenerateColumns = false;
                DataGridOT.ColumnCount = 5;
                DataGridOT.Columns[0].Name = "Uid";
                DataGridOT.Columns[0].HeaderText = "Uid";
                DataGridOT.Columns[0].DataPropertyName = "Uid";
                DataGridOT.Columns[0].Visible = false;

                DataGridOT.Columns[1].Name = "EmpId";
                DataGridOT.Columns[1].HeaderText = "EmpCode";
                DataGridOT.Columns[1].DataPropertyName = "EmpId";
                DataGridOT.Columns[1].Width = 100;

                DataGridOT.Columns[2].Name = "EmpName";
                DataGridOT.Columns[2].HeaderText = "EmpName";
                DataGridOT.Columns[2].DataPropertyName = "EmpName";
                DataGridOT.Columns[2].Width = 250;

                DataGridOT.Columns[3].Name = "WHour";
                DataGridOT.Columns[3].HeaderText = "ActualOT";
                DataGridOT.Columns[3].DataPropertyName = "WHour";
                DataGridOT.Columns[3].DefaultCellStyle.Format = "N2";
                DataGridOT.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridOT.Columns[4].Name = "Oamt";
                DataGridOT.Columns[4].HeaderText = "Approved OT";
                DataGridOT.Columns[4].DataPropertyName = "Oamt";
                DataGridOT.Columns[4].DefaultCellStyle.Format = "N2";
                DataGridOT.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridOT.DataSource = bsOTEntry;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }

        private void TxtEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    TxtEmpCode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        string Q = string.Empty;
        private void FrmOTEntry_Load(object sender, EventArgs e)
        {
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            //DataTable dataTable = dtBranch;
            //if (GeneralParameters.BranchId == 0)
            //{
            //    DataRow dataRow = dataTable.NewRow();
            //    dataRow["Uid"] = 0;
            //    dataRow["BRANCHNAME"] = "All";
            //    dataTable.Rows.Add(dataRow);
            //}
            //CmbBranch.DisplayMember = "BRANCHNAME";
            //CmbBranch.ValueMember = "Uid";
            //CmbBranch.DataSource = dataTable;

            TxtEmpCode.Tag = "0";
            GetOtEntry();
            DataTable selectedRows = new DataTable();
            bsOTEntry.DataSource = dtOt;
            LoadOtGrid();
        }

        protected void GetOtEntry()
        {
            try
            {
                string Query = @"Select 0 Uid,a.DT,b.EmpCode EMpId,Cast((a.OT/60) as decimal(18,2)) Whour,IsNUll(Aot,0) as OAMT,b.EmpName from ATT_reg" + Convert.ToDateTime(DtpDate.Text).Month.ToString("00") + " a Inner join Emp_mast b on a.EmpId = b.Empid " +
                          "Where a.DT = '" + Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MMM-dd") + "' and a.OT is not null order by b.EmpCode";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                Q = Query;
                dtOt = dataTable;
                bsOTEntry.DataSource = dtOt;
                LoadOtGrid();
                //SqlParameter[] sqlParameters = {
                //    new SqlParameter("@BranchId", CmbBranch.SelectedValue.ToString()),
                //    new SqlParameter("@Date", Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd"))
                //};
                //dtOt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_Gte_OT_Mast", sqlParameters, conn);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridOT_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                int idenx = DataGridOT.SelectedCells[0].RowIndex;
                TxtEmpCode.Text = DataGridOT.Rows[idenx].Cells[1].Value.ToString();
                txtEmpName.Text = DataGridOT.Rows[idenx].Cells[2].Value.ToString();
                TxtActualOT.Text = DataGridOT.Rows[idenx].Cells[3].Value.ToString();
                txtEmpName.Tag = DataGridOT.Rows[idenx].Cells[0].Value.ToString();
                txtApprovedOT.Text = DataGridOT.Rows[idenx].Cells[4].Value.ToString();
                DataGridOT.Rows.RemoveAt(idenx);
                DataGridOT.ClearSelection();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                string Query = @"Select 0 Uid,a.DT,b.EmpCode EMpId,Cast((a.OT/60) as decimal(18,2)) Whour,IsNUll(Aot,0) as OAMT,b.EmpName from ATT_reg" + Convert.ToDateTime(DtpDate.Text).Month.ToString("00") + " a Inner join Emp_mast b on a.EmpId = b.Empid " +
                         "Where a.DT = '" + Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MMM-dd") + "' and a.OT is not null order by b.EmpCode";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                dtOt = dataTable;
                bsOTEntry.DataSource = dtOt;
                LoadOtGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridOT_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int index = DataGridOT.SelectedCells[0].RowIndex;
                    int uid = Convert.ToInt32(DataGridOT.Rows[index].Cells[0].Value.ToString());
                    DialogResult res = MessageBox.Show("Are you sure want to delete OT Entry for this Employee", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.Yes)
                    {
                        string Query = "delete from OT_Mast Where Uid =" + uid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetOtEntry();
                        bsOTEntry.DataSource = dtOt;
                        LoadOtGrid();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChckImport_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ChckImport.Checked == true)
                //{
                //    DialogResult result = MessageBox.Show("Do you want to Import the OT data for this date ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                //    if (result == DialogResult.Yes)
                //    {
                //        string Query = @"Select 0 Uid,a.DT,b.EmpCode EMpId,Cast((a.OT/60) as decimal(18,2)) Whour,0 as OAMT,b.EmpName from ATT_reg" + Convert.ToDateTime(DtpDate.Text).Month.ToString("00") + " a Inner join Emp_mast b on a.EmpId = b.Empid " +
                //            "Where a.DT = '" + Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MMM-dd") + "' and a.OT is not null order by b.EmpCode";
                //        DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                //        dtOt = dataTable;
                //        bsOTEntry.DataSource = dtOt;
                //        LoadOtGrid();
                //        //DialogResult result1 = MessageBox.Show("Do you want to Import the OT data for this date ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                //        //if (result1 == DialogResult.Yes)
                //        //{
                //        //    for (int i = 0; i < dataTable.Rows.Count; i++)
                //        //    {
                //        //        SqlParameter[] parameters = {
                //        //            new SqlParameter("@Uid","0"),
                //        //            new SqlParameter("@OTDt",Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd")),
                //        //            new SqlParameter("@Empid",dataTable.Rows[i]["EMpId"].ToString()),
                //        //            new SqlParameter("@WHour",dataTable.Rows[i]["Whour"].ToString()),
                //        //            new SqlParameter("@OAmt","0"),
                //        //            new SqlParameter("@BranchId",CmbBranch.SelectedValue)
                //        //         };
                //        //        db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_OT_Entry", parameters, conn);
                //        //    }
                //        //}
                //    }
                //}
                //else
                //{

                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnMaster_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
            }
        }

        private void TxtEmpCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", TxtEmpCode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtEmpName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtEmpName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridOT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TxtEmpCode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtEmpCode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtEmpName_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtEmpName);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtEmpName_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    TxtEmpCode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
                dialog.ShowDialog();
                lblFileName.Text = dialog.FileName;
                lblFileName.Visible = true;
                string extensionpath = Path.GetExtension(dialog.FileName);
                switch (extensionpath)
                {
                    case ".xls":
                        connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx":
                        connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                        break;
                }
                ImportOT(connstring, lblFileName.Text);
                //GetEDuctEntry();
                //LoadEarnDuctGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void ImportOT(string connstring, string text)
        {
            try
            {
                connstring = string.Format(connstring, text);
                if (text != string.Empty)
                {
                    using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                    {
                        excel_Conn.Open();

                        string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                        DataTable dtexcelData = new DataTable();
                        new DataColumn("EmpCode", typeof(string));
                        new DataColumn("EmpName", typeof(string));
                        new DataColumn("DepartmentName", typeof(string));
                        new DataColumn("IN1", typeof(string));
                        new DataColumn("OUT2", typeof(string));
                        new DataColumn("TotalHours", typeof(string));
                        new DataColumn("ShitHours", typeof(string));
                        new DataColumn("WorkedHours", typeof(string));
                        new DataColumn("OTMinutes", typeof(string));

                        using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                        {
                            oda.Fill(dtexcelData);
                        }
                        excel_Conn.Close();
                        DateTime dateTime = Convert.ToDateTime(DtpDate.Text);
                        for (int i = 0; i < dtexcelData.Rows.Count; i++)
                        {
                            int OTHr = Convert.ToInt32(dtexcelData.Rows[i]["OTMinutes"].ToString());
                            string empcode = dtexcelData.Rows[i]["EmpCode"].ToString();
                            DateTime date = Convert.ToDateTime(dtexcelData.Rows[i]["IN1"].ToString());
                            string Query = "Update Att_reg" + dateTime.Month.ToString("00") + " Set OTHr=" + OTHr + " Where EmpCode = '" + empcode + "' and Dt ='" + date.ToString("yyyy-MM-dd") + "'";
                            db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        }
                    }
                    //updateAmtdetails();
                    MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                button18_Click(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
