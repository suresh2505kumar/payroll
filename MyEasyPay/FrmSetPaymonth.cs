﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmSetPaymonth : Form
    {
        public FrmSetPaymonth()
        {
            InitializeComponent();
        }

        SQLDBHelper db = new SQLDBHelper();
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        private void FrmSetPaymonth_Load(object sender, EventArgs e)
        {
            DteMonth.Format = DateTimePickerFormat.Custom;
            DteMonth.CustomFormat = "MMM-yyyy";
            LoadButton(0);
            LoadPaymonth();
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DteFromDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime dtefrom = Convert.ToDateTime(DteFromDate.Text);
                DateTime dteTo = Convert.ToDateTime(DteToDate.Text);
                string diff2 = (dteTo - dtefrom).TotalDays.ToString();
                txtNoOfDays.Text = diff2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void LoadPaymonth()
        {
            try
            {
                DataTable table = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetPayMonth", connection);
                DataGridSeyPayMonth.AutoGenerateColumns = false;
                DataGridSeyPayMonth.DataSource = null;
                DataGridSeyPayMonth.ColumnCount = 10;

                DataGridSeyPayMonth.Columns[0].Name = "Monid";
                DataGridSeyPayMonth.Columns[0].HeaderText = "Monid";
                DataGridSeyPayMonth.Columns[0].DataPropertyName = "Monid";
                DataGridSeyPayMonth.Columns[0].Visible = false;

                DataGridSeyPayMonth.Columns[1].Name = "Month";
                DataGridSeyPayMonth.Columns[1].HeaderText = "Month";
                DataGridSeyPayMonth.Columns[1].DataPropertyName = "Month";

                DataGridSeyPayMonth.Columns[2].Name = "Nfd";
                DataGridSeyPayMonth.Columns[2].HeaderText = "Nfd";
                DataGridSeyPayMonth.Columns[2].DataPropertyName = "Nfd";
                DataGridSeyPayMonth.Columns[2].Visible = false;

                DataGridSeyPayMonth.Columns[3].Name = "Nod";
                DataGridSeyPayMonth.Columns[3].HeaderText = "No Of days";
                DataGridSeyPayMonth.Columns[3].DataPropertyName = "Nod";
                DataGridSeyPayMonth.Columns[2].Visible = false;

                DataGridSeyPayMonth.Columns[4].Name = "MSDt";
                DataGridSeyPayMonth.Columns[4].HeaderText = "Start Dtae";
                DataGridSeyPayMonth.Columns[4].DataPropertyName = "MSDt";

                DataGridSeyPayMonth.Columns[5].Name = "MEDt";
                DataGridSeyPayMonth.Columns[5].HeaderText = "End Date";
                DataGridSeyPayMonth.Columns[5].DataPropertyName = "MEDt";

                DataGridSeyPayMonth.Columns[6].Name = "PDt";
                DataGridSeyPayMonth.Columns[6].HeaderText = "Pay Date";
                DataGridSeyPayMonth.Columns[6].DataPropertyName = "PDt";

                DataGridSeyPayMonth.Columns[7].Name = "Swf";
                DataGridSeyPayMonth.Columns[7].HeaderText = "Swf";
                DataGridSeyPayMonth.Columns[7].DataPropertyName = "Swf";
                DataGridSeyPayMonth.Columns[7].Visible = false;

                DataGridSeyPayMonth.Columns[8].Name = "Tht";
                DataGridSeyPayMonth.Columns[8].HeaderText = "Tht";
                DataGridSeyPayMonth.Columns[8].DataPropertyName = "Tht";
                DataGridSeyPayMonth.Columns[8].Visible = false;

                DataGridSeyPayMonth.Columns[9].Name = "Incentive";
                DataGridSeyPayMonth.Columns[9].HeaderText = "Incentive";
                DataGridSeyPayMonth.Columns[9].DataPropertyName = "Incentive";
                DataGridSeyPayMonth.Columns[9].Visible = false;
                DataGridSeyPayMonth.DataSource = table;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtNoOfDays.Tag = "0";
                LoadButton(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            try
            {
                LoadButton(3);
                LoadPaymonth();
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                txtNoOfDays.Text = string.Empty;
                txtIncentive.Text = string.Empty;
                int Index = DataGridSeyPayMonth.SelectedCells[0].RowIndex;
                DteMonth.Text = DataGridSeyPayMonth.Rows[Index].Cells[1].Value.ToString();
                DtePayDate.Text = DataGridSeyPayMonth.Rows[Index].Cells[6].Value.ToString();
                DteFromDate.Text = DataGridSeyPayMonth.Rows[Index].Cells[4].Value.ToString();
                DteToDate.Text = DataGridSeyPayMonth.Rows[Index].Cells[5].Value.ToString();
                txtNoOfDays.Text = DataGridSeyPayMonth.Rows[Index].Cells[3].Value.ToString();
                txtIncentive.Text = DataGridSeyPayMonth.Rows[Index].Cells[9].Value.ToString();
                txtNoOfDays.Tag = DataGridSeyPayMonth.Rows[Index].Cells[0].Value.ToString();
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtIncentive.Text == string.Empty)
                {
                    txtIncentive.Text = "0";
                }
                SqlParameter[] parameters = {
                    new SqlParameter("@MonId",txtNoOfDays.Tag),
                    new SqlParameter("@PDt",Convert.ToDateTime(DtePayDate.Text)),
                    new SqlParameter("@Month",DteMonth.Text),
                    new SqlParameter("@Nod",txtNoOfDays.Text),
                    new SqlParameter("@MDt",Convert.ToDateTime(DtePayDate.Text)),
                    new SqlParameter("@Nfd",txtIncentive.Text),
                    new SqlParameter("@MSDt",Convert.ToDateTime(DteFromDate.Text)),
                    new SqlParameter("@MEDt",Convert.ToDateTime(DteToDate.Text)),
                    new SqlParameter("@Swf",txtIncentive.Text),
                    new SqlParameter("@Tht",txtIncentive.Text),
                    new SqlParameter("@Incentive",txtIncentive.Text),
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_PayMonth", parameters, connection);
                txtIncentive.Text = string.Empty;
                txtNoOfDays.Text = string.Empty;
                LoadButton(3);
                LoadPaymonth();
       
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridSeyPayMonth.SelectedCells[0].RowIndex;
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Monid",DataGridSeyPayMonth.Rows[Index].Cells[0].Value.ToString()),
                    new SqlParameter("@RTN",SqlDbType.Int)
                };
                sqlParameters[1].Direction = ParameterDirection.Output;
                int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeletePayMonth", sqlParameters, connection);
                if(id == 0)
                {
                    MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadPaymonth();
                }
                else
                {
                    MessageBox.Show("Payment already Generated can't delete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DteMonth_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
