﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MyEasyPay
{
    public partial class FrmMasters : Form
    {
        public FrmMasters()
        {
            InitializeComponent();
        }
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        int EditId = 0;
        
        private void FrmMasters_Load(object sender, EventArgs e)
        {
            LoadButton(3);
            if (GeneralParametrs.MenyKey == 1)
            {
                this.Text = "Department";
                lblName.Text = "Department Name";
                lblShortName.Text = "Department Short Name";
                cmbType.Visible = false;
                txtShortName.Visible = true;
                DataTable dt = GetData("SP_GetDept_Mast");
                FillDepartment(dt);
            }
            else if (GeneralParametrs.MenyKey == 2)
            {
                this.Text = "Desingnation";
                lblName.Text = "Desingnation Name";
                lblShortName.Text = "Desingnation Short Name";
                cmbType.Visible = false;
                txtShortName.Visible = true;
                DataTable dt = GetData("SP_GetDes_Mast");
                FillDesingnation(dt);
            }
            else if (GeneralParametrs.MenyKey == 3)
            {
                this.Text = "Grade";
                lblName.Text = "Grade Name";
                lblShortName.Text = "Grade Short Name";
                cmbType.Visible = false;
                txtShortName.Visible = true;
                DataTable dt = GetData("SP_GetGrade_Mast");
                FillGrade(dt);
            }
            else if (GeneralParametrs.MenyKey == 4)
            {
                this.Text = "Type";
                lblName.Text = "Type Name";
                lblShortName.Text = "Type";
                cmbType.Visible = true;
                txtShortName.Visible = false;
                DataTable dt = GetData("SP_GetEMP_TYPE");
                FillType(dt);
            }
        }

        public DataTable GetData(string Procedurename)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void FillDepartment(DataTable dataTable)
        {
            try
            {
                DataGridMaster.DataSource = null;
                DataGridMaster.AutoGenerateColumns = false;
                DataGridMaster.ColumnCount = 3;
                DataGridMaster.Columns[0].Name = "ID";
                DataGridMaster.Columns[0].HeaderText = "ID";
                DataGridMaster.Columns[0].DataPropertyName = "DeptId";
                DataGridMaster.Columns[0].Visible = false;

                DataGridMaster.Columns[1].Name = "DepartmentName";
                DataGridMaster.Columns[1].HeaderText = "Department Name";
                DataGridMaster.Columns[1].DataPropertyName = "DeptName";
                DataGridMaster.Columns[1].Width = 270;

                DataGridMaster.Columns[2].Name = "ShortName";
                DataGridMaster.Columns[2].HeaderText = "Department ShortName";
                DataGridMaster.Columns[2].DataPropertyName = "DpSName";
                DataGridMaster.Columns[2].Width = 250;
                DataGridMaster.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FillDesingnation(DataTable dataTable)
        {
            try
            {
                DataGridMaster.DataSource = null;
                DataGridMaster.AutoGenerateColumns = false;
                DataGridMaster.ColumnCount = 3;
                DataGridMaster.Columns[0].Name = "ID";
                DataGridMaster.Columns[0].HeaderText = "ID";
                DataGridMaster.Columns[0].DataPropertyName = "DesId";
                DataGridMaster.Columns[0].Visible = false;

                DataGridMaster.Columns[1].Name = "DesingnationName";
                DataGridMaster.Columns[1].HeaderText = "Desingnation Name";
                DataGridMaster.Columns[1].DataPropertyName = "DesName";
                DataGridMaster.Columns[1].Width = 270;

                DataGridMaster.Columns[2].Name = "ShortName";
                DataGridMaster.Columns[2].HeaderText = "Desingnation ShortName";
                DataGridMaster.Columns[2].DataPropertyName = "DSName";
                DataGridMaster.Columns[2].Width = 250;
                DataGridMaster.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FillGrade(DataTable dataTable)
        {
            try
            {
                DataGridMaster.DataSource = null;
                DataGridMaster.AutoGenerateColumns = false;
                DataGridMaster.ColumnCount = 3;
                DataGridMaster.Columns[0].Name = "ID";
                DataGridMaster.Columns[0].HeaderText = "ID";
                DataGridMaster.Columns[0].DataPropertyName = "GrId";
                DataGridMaster.Columns[0].Visible = false;

                DataGridMaster.Columns[1].Name = "GradeName";
                DataGridMaster.Columns[1].HeaderText = "Grade Name";
                DataGridMaster.Columns[1].DataPropertyName = "GrName";
                DataGridMaster.Columns[1].Width = 270;

                DataGridMaster.Columns[2].Name = "ShortName";
                DataGridMaster.Columns[2].HeaderText = "Grade ShortName";
                DataGridMaster.Columns[2].DataPropertyName = "GrSName";
                DataGridMaster.Columns[2].Width = 250;
                DataGridMaster.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FillType(DataTable dataTable)
        {
            try
            {
                DataGridMaster.DataSource = null;
                DataGridMaster.AutoGenerateColumns = false;
                DataGridMaster.ColumnCount = 4;
                DataGridMaster.Columns[0].Name = "ID";
                DataGridMaster.Columns[0].HeaderText = "ID";
                DataGridMaster.Columns[0].DataPropertyName = "TId";
                DataGridMaster.Columns[0].Visible = false;

                DataGridMaster.Columns[1].Name = "Description";
                DataGridMaster.Columns[1].HeaderText = "Description";
                DataGridMaster.Columns[1].DataPropertyName = "TName";
                DataGridMaster.Columns[1].Width = 170;

                DataGridMaster.Columns[2].Name = "Type";
                DataGridMaster.Columns[2].HeaderText = "Type";
                DataGridMaster.Columns[2].DataPropertyName = "Type";
                DataGridMaster.Columns[2].Width = 150;

                DataGridMaster.Columns[3].Name = "EmployeeType";
                DataGridMaster.Columns[3].HeaderText = "Employee Type";
                DataGridMaster.Columns[3].DataPropertyName = "TSName";
                DataGridMaster.Columns[3].Width = 200;

                DataGridMaster.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            EditId = 0;
            try
            {
                txtName.Text = string.Empty;
                txtShortName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if(GeneralParametrs.MenyKey == 4)
                {
                    int Id = DataGridMaster.SelectedCells[0].RowIndex;
                    EditId = Convert.ToInt32(DataGridMaster.Rows[Id].Cells[0].Value.ToString());
                    txtName.Text = DataGridMaster.Rows[Id].Cells[1].Value.ToString();
                    cmbType.Text = DataGridMaster.Rows[Id].Cells[2].Value.ToString();
                }
                else
                {
                    int Id = DataGridMaster.SelectedCells[0].RowIndex;
                    EditId = Convert.ToInt32(DataGridMaster.Rows[Id].Cells[0].Value.ToString());
                    txtName.Text = DataGridMaster.Rows[Id].Cells[1].Value.ToString();
                    txtShortName.Text = DataGridMaster.Rows[Id].Cells[2].Value.ToString();
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if(GeneralParametrs.MenyKey == 1)
                {
                    DataTable dt = GetData("SP_GetDept_Mast");
                    FillDepartment(dt);
                }
                else if (GeneralParametrs.MenyKey == 2)
                {
                    DataTable dt = GetData("SP_GetDes_Mast");
                    FillDesingnation(dt);
                }
                else if (GeneralParametrs.MenyKey == 3)
                {
                    DataTable dt = GetData("SP_GetGrade_Mast");
                    FillGrade(dt);
                }
                else if (GeneralParametrs.MenyKey == 4)
                {
                    DataTable dt = GetData("SP_GetEMP_TYPE");
                    FillType(dt);
                }
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(GeneralParametrs.MenyKey == 1)
                {
                    Savedepartment(EditId, txtName.Text, txtShortName.Text);
                }
                else if (GeneralParametrs.MenyKey == 2)
                {
                    SaveDesingnation(EditId, txtName.Text, txtShortName.Text);
                }
                else if (GeneralParametrs.MenyKey == 3)
                {
                    SaveGrade(EditId, txtName.Text, txtShortName.Text);
                }
                else if (GeneralParametrs.MenyKey == 4)
                {
                    SaveType(EditId, txtName.Text, cmbType.Text);
                }
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtName.Text = string.Empty;
                txtShortName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void Savedepartment(int Id,string DepartmentName,string DepartmentShortName)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@DeptName",DepartmentName),
                    new SqlParameter("@DeptShortName",DepartmentShortName),
                    new SqlParameter("@DeptId",Id)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Dept_Mast", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void SaveDesingnation(int Id, string DesName, string DesShortName)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@DesName",DesName),
                    new SqlParameter("@DesShortName",DesShortName),
                    new SqlParameter("@DesId",Id)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Des_Mast", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void SaveGrade(int Id, string GradeName, string GradeShortName)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@GrName",GradeName),
                    new SqlParameter("@GrSName",GradeShortName),
                    new SqlParameter("@GrId",Id)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Grade_Mast", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void SaveType(int Id, string TypeName, string Type)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@TName",TypeName),
                    new SqlParameter("@Type",Type),
                    new SqlParameter("@TypeId",Id)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_EMP_TYPE", parameters, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int ReturnMessage = 0;
                int Id = DataGridMaster.SelectedCells[0].RowIndex;
                EditId = Convert.ToInt32(DataGridMaster.Rows[Id].Cells[0].Value.ToString());
                if (GeneralParametrs.MenyKey == 1)
                {
                    SqlParameter[] parameter = { new SqlParameter("@DeptId", EditId), new SqlParameter("@ReturnMsg", SqlDbType.Int) };
                    parameter[1].Direction = ParameterDirection.Output;
                    ReturnMessage = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteDept_Mast", parameter, 1, conn);
                    DataTable dt = GetData("SP_GetDept_Mast");
                    FillDepartment(dt);
                }
                else if (GeneralParametrs.MenyKey == 2)
                {
                    SqlParameter[] parameter = { new SqlParameter("@DesId", EditId), new SqlParameter("@ReturnMsg", SqlDbType.Int) };
                    parameter[1].Direction = ParameterDirection.Output;
                    ReturnMessage = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteDes_Mast", parameter, 1, conn);
                    DataTable dt = GetData("SP_GetDes_Mast");
                    FillDesingnation(dt);
                }
                else if (GeneralParametrs.MenyKey == 3)
                {
                    SqlParameter[] parameter = { new SqlParameter("@GrId", EditId), new SqlParameter("@ReturnMsg", SqlDbType.Int) };
                    parameter[1].Direction = ParameterDirection.Output;
                    ReturnMessage = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteGrade_Mast", parameter, 1, conn);
                    DataTable dt = GetData("SP_GetGrade_Mast");
                    FillGrade(dt);
                }
                else if (GeneralParametrs.MenyKey == 4)
                {
                    SqlParameter[] parameter = { new SqlParameter("@EmpTypId", EditId), new SqlParameter("@ReturnMsg", SqlDbType.Int) };
                    parameter[1].Direction = ParameterDirection.Output;
                    ReturnMessage = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_DeleteEMP_TYPE", parameter, 1, conn);
                    DataTable dt = GetData("SP_GetEMP_TYPE");
                    FillType(dt);
                }
                if(ReturnMessage == 1)
                {
                    MessageBox.Show("Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Record already found,Can't Delete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
