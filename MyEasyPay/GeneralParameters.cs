﻿namespace MyEasyPay
{
    public class GeneralParameters
    {
        public static int UserdId;
        public static string LoginUserName = string.Empty;
        public static string ServerName = string.Empty;
        public static string DbUserID = string.Empty;
        public static string Password = string.Empty;
        public static string DbName = string.Empty;
        public static string CompanyId = string.Empty;
        public static int IsVerify = 0;
        public static string Department = string.Empty;
        public static string Desingnation = string.Empty;
        public static string Shift = string.Empty;
        public static int ReportId = 0;
        public static string TablName = string.Empty;
        public static string ReportDate = null;
        public static string LVTAG = string.Empty;
        public static string CompanyName = string.Empty;
        public static string Branch = string.Empty;
        public static int BranchId = 0;
        public static string EmpId = string.Empty;
        public static int PayMonthId = 0;
        public static string PaMonth = string.Empty;
        public static int MusterTag = 0;
        public static int ReportBranchId = 0;
        public static int DeptId = 0;
    }
}
