﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmShift : Form
    {
        public FrmShift()
        {
            InitializeComponent();
        }
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        int EditId = 0;

        private void FrmShift_Load(object sender, EventArgs e)
        {
            chckNightShift.Checked = false;
            Loadshift();
            LoadButton(3);
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            chckNightShift.Checked = false;
            LoadButton(1);
            try
            {
                Clearcontrol();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridShift.SelectedCells[0].RowIndex;
                EditId = Convert.ToInt32(DataGridShift.Rows[Index].Cells[0].Value);
                txtShiftName.Text = DataGridShift.Rows[Index].Cells[1].Value.ToString();
                DateTime InTime = new DateTime();
                DateTime OutTime = new DateTime();
                InTime = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[2].Value.ToString());
                OutTime = Convert.ToDateTime(DataGridShift.Rows[Index].Cells[3].Value.ToString());
                string InTimeS = InTime.Hour.ToString("00") + ":" + InTime.Minute.ToString("00");
                string OutTimeS = OutTime.Hour.ToString("00") + ":" + OutTime.Minute.ToString("00");
                txtInTime.Text = InTimeS;
                txtOutTime.Text = OutTimeS;
                txtTotalHours.Text = DataGridShift.Rows[Index].Cells[5].Value.ToString();
                if (DataGridShift.Rows[Index].Cells[4].Value.ToString() == "True")
                {
                    chckNightShift.Checked = true;
                }
                else
                {
                    chckNightShift.Checked = false;
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtShiftName.Text != string.Empty && txtInTime.Text != string.Empty && txtOutTime.Text != string.Empty)
                {
                    int NightShift = 0;
                    if(chckNightShift.Checked == true)
                    {
                        NightShift = 1;
                    }

                    SqlParameter[] parameters = {
                        new SqlParameter("@ShiftId",EditId),
                        new SqlParameter("@ShiftName",txtShiftName.Text),
                        new SqlParameter("@InTime",txtInTime.Text),
                        new SqlParameter("@OutTime",txtOutTime.Text),
                        new SqlParameter("@NS",NightShift),
                        new SqlParameter("@BRANCH",GeneralParameters.Branch),
                        new SqlParameter("@THOURS",txtTotalHours.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Shift_Mast", parameters, conn);
                    MessageBox.Show("Record has been saved sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Clearcontrol();
                    LoadButton(3);
                    Loadshift();
                
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        public void Clearcontrol()
        {
            txtShiftName.Text = string.Empty;
            txtInTime.Text = string.Empty;
            txtOutTime.Text = string.Empty;
            txtTotalHours.Text = string.Empty;
        }

        protected void Loadshift()
        {
            try
            {
                DataTable dataTable = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetSHift_mast", conn);
                //DataTable dt = dataTable.Select("BRANCH ='" + GeneralParameters.Branch + "'", null).CopyToDataTable();
                DataGridShift.DataSource = null;
                DataGridShift.AutoGenerateColumns = false;
                DataGridShift.ColumnCount = 6;

                DataGridShift.Columns[0].Name = "ID";
                DataGridShift.Columns[0].HeaderText = "ID";
                DataGridShift.Columns[0].DataPropertyName = "ShiftID";
                DataGridShift.Columns[0].Visible = false;

                DataGridShift.Columns[1].Name = "ShiftName";
                DataGridShift.Columns[1].HeaderText = "Shift Name";
                DataGridShift.Columns[1].DataPropertyName = "ShiftName";
                DataGridShift.Columns[1].Width = 300;

                DataGridShift.Columns[2].Name = "InTime";
                DataGridShift.Columns[2].HeaderText = "In Time";
                DataGridShift.Columns[2].DataPropertyName = "InTime";
                DataGridShift.Columns[2].DefaultCellStyle.Format = "HH:mm";
                DataGridShift.Columns[2].Width = 120;

                DataGridShift.Columns[3].Name = "OutTime";
                DataGridShift.Columns[3].HeaderText = "Out Time";
                DataGridShift.Columns[3].DataPropertyName = "OutTime";
                DataGridShift.Columns[3].DefaultCellStyle.Format = "HH:mm";
                DataGridShift.Columns[3].Width = 120;

                DataGridShift.Columns[4].Name = "NS";
                DataGridShift.Columns[4].HeaderText = "NS";
                DataGridShift.Columns[4].DataPropertyName = "NS";
                DataGridShift.Columns[4].Visible = false;

                DataGridShift.Columns[5].Name = "THOURS";
                DataGridShift.Columns[5].HeaderText = "THOURS";
                DataGridShift.Columns[5].DataPropertyName = "THOURS";
                DataGridShift.Columns[5].Visible = false;
                DataGridShift.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Loadshift();
                LoadButton(3);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridShift.SelectedCells[0].RowIndex;
                EditId = Convert.ToInt32(DataGridShift.Rows[Index].Cells[0].Value);
                SqlParameter[] parameters = {
                    new SqlParameter("@ShiftId",EditId),
                    new SqlParameter("@RTN",SqlDbType.Int)
                };
                parameters[1].Direction = ParameterDirection.Output;
                int id = db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_DeleteSHift_mast", parameters, 1, conn);
                if(id == 0)
                {
                    MessageBox.Show("Shift Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Shift refred to employees can't Delete", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Loadshift();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtOutTime_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtInTime_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
