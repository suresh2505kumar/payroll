﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;


namespace MyEasyPay
{
    public partial class FrmEmpMast : Form
    {
        public FrmEmpMast()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsp = new BindingSource();
        int EmpId = 0;
        int LoadId = 0;
        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void FrmEmpMast_Load(object sender, EventArgs e)
        {
            //string Branch = ConfigurationManager.AppSettings["Branch"].ToString();
            //if (Branch == "All")
            //{
            //    var BranchAll = new List<string>(ConfigurationManager.AppSettings["MultiBranch"].Split(new char[] { ';' }));
            //    CmbBranch.DataSource = BranchAll;
            //}
            //else
            //{
            //    CmbBranch.Items.Clear();
            //    CmbBranch.Items.Add(Branch);
            //}
            //CmbBranch.SelectedIndex = 0;

            LoadId = 1;
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);

            //string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            //var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            DataTable dataTable = dtBranch;
            if (GeneralParameters.BranchId == 0)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["Uid"] = 0;
                dataRow["BRANCHNAME"] = "All";
                dataTable.Rows.Add(dataRow);
            }
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            CmbBranchA.DisplayMember = "BRANCHNAME";
            CmbBranchA.ValueMember = "Uid";
            CmbBranchA.DataSource = dtBranch;

            DataGridEmployee.RowHeadersVisible = false;
            this.DataGridEmployee.DefaultCellStyle.Font = new Font("calibri", 10);
            this.DataGridEmployee.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
            DataGridEmployee.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridEmployee.EnableHeadersVisualStyles = false;
            DataGridEmployee.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.GradientActiveCaption;
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            bracnhtranser.Visible = false;
            Loadapp();
            grSearch.Visible = false;
            var theDate = DateTime.Now.AddMonths(-1);
            theDate.ToString("MM/yyyy");
            LoadMasters();
            LoadId = 0;
        }

        private void Loadapp()
        {
            conn.Close();
            conn.Open();
            string qur = "select Appid,Appraiser from Appraiser order by Appid";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cmbapp.DataSource = null;
            cmbapp.DataSource = tab;
            cmbapp.DisplayMember = "Appraiser";
            cmbapp.ValueMember = "Appid";
            conn.Close();
        }

        private void Loadgrid()

        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Branch",GeneralParameters.Branch)
                };
                //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mastAll", sqlParameters, conn);
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetEmp_mastAll", sqlParameters);
                load(dt);
            }
            catch (Exception)
            {

                throw;
            }


        }

        private void load(DataTable dt)
        {
            DataGridEmployee.DataSource = null;
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            DataGridEmployee.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            DataGridEmployee.ColumnCount = 40;
            DataGridEmployee.Columns[0].Name = "EmpId";
            DataGridEmployee.Columns[0].HeaderText = "EmpId";
            DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
            DataGridEmployee.Columns[0].Visible = false;

            DataGridEmployee.Columns[1].Name = "EmpCode";
            DataGridEmployee.Columns[1].HeaderText = "EmpCode";
            DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
            DataGridEmployee.Columns[1].Width = 100;

            DataGridEmployee.Columns[2].Name = "EmpName";
            DataGridEmployee.Columns[2].HeaderText = "Employe Name";
            DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
            DataGridEmployee.Columns[2].Width = 200;

            DataGridEmployee.Columns[3].Name = "ShiftName";
            DataGridEmployee.Columns[3].HeaderText = "ShiftName";
            DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
            DataGridEmployee.Columns[3].Visible = false;

            DataGridEmployee.Columns[4].Name = "DeptId";
            DataGridEmployee.Columns[4].HeaderText = "DeptId";
            DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
            DataGridEmployee.Columns[4].Visible = false;

            DataGridEmployee.Columns[5].Name = "DesId";
            DataGridEmployee.Columns[5].HeaderText = "DesId";
            DataGridEmployee.Columns[5].DataPropertyName = "DesId";
            DataGridEmployee.Columns[5].Visible = false;

            DataGridEmployee.Columns[6].Name = "Grid";
            DataGridEmployee.Columns[6].HeaderText = "Grid";
            DataGridEmployee.Columns[6].DataPropertyName = "Grid";
            DataGridEmployee.Columns[6].Visible = false;

            DataGridEmployee.Columns[7].Name = "GrName";
            DataGridEmployee.Columns[7].HeaderText = "GrName";
            DataGridEmployee.Columns[7].DataPropertyName = "GrName";
            DataGridEmployee.Columns[7].Visible = false;

            DataGridEmployee.Columns[8].Name = "ShiftId";
            DataGridEmployee.Columns[8].HeaderText = "ShiftId";
            DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
            DataGridEmployee.Columns[8].Visible = false;

            DataGridEmployee.Columns[9].Name = "EmpTypeId";
            DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
            DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
            DataGridEmployee.Columns[9].Visible = false;

            DataGridEmployee.Columns[10].Name = "FName";
            DataGridEmployee.Columns[10].HeaderText = "FatherName";
            DataGridEmployee.Columns[10].DataPropertyName = "FName";
            DataGridEmployee.Columns[10].Width = 160;

            DataGridEmployee.Columns[11].Name = "DOB";
            DataGridEmployee.Columns[11].HeaderText = "DOB";
            DataGridEmployee.Columns[11].DataPropertyName = "DOB";
            DataGridEmployee.Columns[11].Visible = false;

            DataGridEmployee.Columns[12].Name = "DOJ";
            DataGridEmployee.Columns[12].HeaderText = "DOJ";
            DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
            DataGridEmployee.Columns[12].Visible = false;

            DataGridEmployee.Columns[13].Name = "Sex";
            DataGridEmployee.Columns[13].HeaderText = "Sex";
            DataGridEmployee.Columns[13].DataPropertyName = "Sex";
            DataGridEmployee.Columns[13].Visible = false;

            DataGridEmployee.Columns[14].Name = "EmpAddress";
            DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
            DataGridEmployee.Columns[14].DataPropertyName = "Street";
            DataGridEmployee.Columns[14].Visible = false;

            DataGridEmployee.Columns[15].Name = "City";
            DataGridEmployee.Columns[15].HeaderText = "City";
            DataGridEmployee.Columns[15].DataPropertyName = "City";
            DataGridEmployee.Columns[15].Visible = false;

            DataGridEmployee.Columns[16].Name = "Phone";
            DataGridEmployee.Columns[16].HeaderText = "Phone";
            DataGridEmployee.Columns[16].DataPropertyName = "Phone";
            DataGridEmployee.Columns[16].Visible = false;

            DataGridEmployee.Columns[17].Name = "Information";
            DataGridEmployee.Columns[17].HeaderText = "Information";
            DataGridEmployee.Columns[17].DataPropertyName = "Information";
            DataGridEmployee.Columns[17].Visible = false;
            DataGridEmployee.Columns[18].Name = "Dispensary";
            DataGridEmployee.Columns[18].HeaderText = "Dispensary";
            DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
            DataGridEmployee.Columns[18].Visible = false;

            DataGridEmployee.Columns[19].Name = "WeekOff";
            DataGridEmployee.Columns[19].HeaderText = "WeekOff";
            DataGridEmployee.Columns[19].DataPropertyName = "woff";
            DataGridEmployee.Columns[19].Visible = false;

            DataGridEmployee.Columns[20].Name = "CL";
            DataGridEmployee.Columns[20].HeaderText = "CL";
            DataGridEmployee.Columns[20].DataPropertyName = "CL";
            DataGridEmployee.Columns[20].Visible = false;
            DataGridEmployee.Columns[21].Name = "EL";
            DataGridEmployee.Columns[21].HeaderText = "EL";
            DataGridEmployee.Columns[21].DataPropertyName = "EL";
            DataGridEmployee.Columns[21].Visible = false;

            DataGridEmployee.Columns[22].Name = "ESINo";
            DataGridEmployee.Columns[22].HeaderText = "ESINo";
            DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
            DataGridEmployee.Columns[22].Visible = false;

            DataGridEmployee.Columns[23].Name = "PFNo";
            DataGridEmployee.Columns[23].HeaderText = "PFNo";
            DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
            DataGridEmployee.Columns[23].Visible = false;

            DataGridEmployee.Columns[24].Name = "UANNo";
            DataGridEmployee.Columns[24].HeaderText = "UANNo";
            DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
            DataGridEmployee.Columns[24].Visible = false;

            DataGridEmployee.Columns[25].Name = "PANNo";
            DataGridEmployee.Columns[25].HeaderText = "PANNo";
            DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
            DataGridEmployee.Columns[25].Visible = false;

            DataGridEmployee.Columns[26].Name = "AadharNo";
            DataGridEmployee.Columns[26].HeaderText = "AadharNo";
            DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
            DataGridEmployee.Columns[26].Visible = false;

            DataGridEmployee.Columns[27].Name = "empphoto";
            DataGridEmployee.Columns[27].HeaderText = "empphoto";
            DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
            DataGridEmployee.Columns[27].Visible = false;

            DataGridEmployee.Columns[28].Name = "DesName";
            DataGridEmployee.Columns[28].HeaderText = "DesName";
            DataGridEmployee.Columns[28].DataPropertyName = "DesName";
            DataGridEmployee.Columns[28].Visible = false;


            DataGridEmployee.Columns[29].Name = "DeptName";
            DataGridEmployee.Columns[29].HeaderText = "DeptName";
            DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
            DataGridEmployee.Columns[29].Width = 150;


            DataGridEmployee.Columns[30].Name = "Grade";
            DataGridEmployee.Columns[30].HeaderText = "Grade";
            DataGridEmployee.Columns[30].DataPropertyName = "Grade";
            DataGridEmployee.Columns[30].Width = 150;


            DataGridEmployee.Columns[31].Name = "TName";
            DataGridEmployee.Columns[31].HeaderText = "TName";
            DataGridEmployee.Columns[31].DataPropertyName = "TName";
            DataGridEmployee.Columns[31].Visible = false;

            DataGridEmployee.Columns[32].Name = "CardNo";
            DataGridEmployee.Columns[32].HeaderText = "Unicode";
            DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
            DataGridEmployee.Columns[32].Width = 100;


            DataGridEmployee.Columns[33].Name = "emialtag";
            DataGridEmployee.Columns[33].HeaderText = "emialtag";
            DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
            DataGridEmployee.Columns[33].Visible = false;


            DataGridEmployee.Columns[34].Name = "isappriser";
            DataGridEmployee.Columns[34].HeaderText = "isappriser";
            DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
            DataGridEmployee.Columns[34].Visible = false;

            DataGridEmployee.Columns[35].Name = "appid";
            DataGridEmployee.Columns[35].HeaderText = "appid";
            DataGridEmployee.Columns[35].DataPropertyName = "appid";
            DataGridEmployee.Columns[35].Visible = false;


            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;

            DataGridEmployee.Columns[37].Name = "acno";
            DataGridEmployee.Columns[37].HeaderText = "acno";
            DataGridEmployee.Columns[37].DataPropertyName = "acno";
            DataGridEmployee.Columns[37].Visible = false;


            DataGridEmployee.Columns[38].Name = "pmode";
            DataGridEmployee.Columns[38].HeaderText = "pmode";
            DataGridEmployee.Columns[38].DataPropertyName = "pmode";
            DataGridEmployee.Columns[38].Visible = false;



            DataGridEmployee.Columns[39].Name = "ptype";
            DataGridEmployee.Columns[39].HeaderText = "ptype";
            DataGridEmployee.Columns[39].DataPropertyName = "ptype";
            DataGridEmployee.Columns[39].Visible = false;

            bs.DataSource = dt;

            DataGridEmployee.DataSource = bs;

        }


        private void Loadgrid2()
        {
            try
            {
                if (cmgfntbox.SelectedValue != null)
                {
                    SqlParameter[] sqlParameters = {
                     new SqlParameter("@BranchId",GeneralParameters.BranchId),
                     //new SqlParameter("@deptid", cmgfntbox.SelectedValue),
                    };
                    //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mastAllDeptid", sqlParameters, conn);
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mastAll", sqlParameters, conn);
                    DataTable dt = new DataTable();
                    if (cmgfntbox.Text == "All")
                    {
                        dt = dataTable;
                    }
                    else
                    {
                        DataRow[] dataRows = dataTable.Select("Deptid =" + cmgfntbox.SelectedValue + "");
                        if(dataRows.Length > 0)
                        {
                            dt = dataTable.Select("Deptid =" + cmgfntbox.SelectedValue + "").CopyToDataTable();
                        }
                        else
                        {
                            dt = dataTable.Clone(); ;
                        }
                        
                    }

                    bs.DataSource = dt;
                    DataGridEmployee.DataSource = null;
                    DataGridEmployee.AutoGenerateColumns = false;
                    DataGridEmployee.ColumnCount = 43;
                    DataGridEmployee.Columns[0].Name = "EmpId";
                    DataGridEmployee.Columns[0].HeaderText = "EmpId";
                    DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
                    DataGridEmployee.Columns[0].Visible = false;

                    DataGridEmployee.Columns[1].Name = "EmpCode";
                    DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                    DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
                    DataGridEmployee.Columns[1].Width = 100;

                    DataGridEmployee.Columns[2].Name = "EmpName";
                    DataGridEmployee.Columns[2].HeaderText = "Employe Name";
                    DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
                    DataGridEmployee.Columns[2].Width = 200;

                    DataGridEmployee.Columns[3].Name = "ShiftName";
                    DataGridEmployee.Columns[3].HeaderText = "ShiftName";
                    DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
                    DataGridEmployee.Columns[3].Visible = false;

                    DataGridEmployee.Columns[4].Name = "DeptId";
                    DataGridEmployee.Columns[4].HeaderText = "DeptId";
                    DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
                    DataGridEmployee.Columns[4].Visible = false;

                    DataGridEmployee.Columns[5].Name = "DesId";
                    DataGridEmployee.Columns[5].HeaderText = "DesId";
                    DataGridEmployee.Columns[5].DataPropertyName = "DesId";
                    DataGridEmployee.Columns[5].Visible = false;

                    DataGridEmployee.Columns[6].Name = "Grid";
                    DataGridEmployee.Columns[6].HeaderText = "Grid";
                    DataGridEmployee.Columns[6].DataPropertyName = "Grid";
                    DataGridEmployee.Columns[6].Visible = false;

                    DataGridEmployee.Columns[7].Name = "GrName";
                    DataGridEmployee.Columns[7].HeaderText = "GrName";
                    DataGridEmployee.Columns[7].DataPropertyName = "GrName";
                    DataGridEmployee.Columns[7].Visible = false;

                    DataGridEmployee.Columns[8].Name = "ShiftId";
                    DataGridEmployee.Columns[8].HeaderText = "ShiftId";
                    DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
                    DataGridEmployee.Columns[8].Visible = false;

                    DataGridEmployee.Columns[9].Name = "EmpTypeId";
                    DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
                    DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
                    DataGridEmployee.Columns[9].Visible = false;

                    DataGridEmployee.Columns[10].Name = "FName";
                    DataGridEmployee.Columns[10].HeaderText = "FatherName";
                    DataGridEmployee.Columns[10].DataPropertyName = "FName";
                    DataGridEmployee.Columns[10].Width = 160;

                    DataGridEmployee.Columns[11].Name = "DOB";
                    DataGridEmployee.Columns[11].HeaderText = "DOB";
                    DataGridEmployee.Columns[11].DataPropertyName = "DOB";
                    DataGridEmployee.Columns[11].Visible = false;

                    DataGridEmployee.Columns[12].Name = "DOJ";
                    DataGridEmployee.Columns[12].HeaderText = "DOJ";
                    DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
                    DataGridEmployee.Columns[12].Visible = false;

                    DataGridEmployee.Columns[13].Name = "Sex";
                    DataGridEmployee.Columns[13].HeaderText = "Sex";
                    DataGridEmployee.Columns[13].DataPropertyName = "Sex";
                    DataGridEmployee.Columns[13].Visible = false;

                    DataGridEmployee.Columns[14].Name = "EmpAddress";
                    DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
                    DataGridEmployee.Columns[14].DataPropertyName = "Street";
                    DataGridEmployee.Columns[14].Visible = false;

                    DataGridEmployee.Columns[15].Name = "City";
                    DataGridEmployee.Columns[15].HeaderText = "City";
                    DataGridEmployee.Columns[15].DataPropertyName = "City";
                    DataGridEmployee.Columns[15].Visible = false;

                    DataGridEmployee.Columns[16].Name = "Phone";
                    DataGridEmployee.Columns[16].HeaderText = "Phone";
                    DataGridEmployee.Columns[16].DataPropertyName = "Phone";
                    DataGridEmployee.Columns[16].Visible = false;

                    DataGridEmployee.Columns[17].Name = "Information";
                    DataGridEmployee.Columns[17].HeaderText = "Information";
                    DataGridEmployee.Columns[17].DataPropertyName = "Information";
                    DataGridEmployee.Columns[17].Visible = false;

                    DataGridEmployee.Columns[18].Name = "Dispensary";
                    DataGridEmployee.Columns[18].HeaderText = "Dispensary";
                    DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
                    DataGridEmployee.Columns[18].Visible = false;

                    DataGridEmployee.Columns[19].Name = "WeekOff";
                    DataGridEmployee.Columns[19].HeaderText = "WeekOff";
                    DataGridEmployee.Columns[19].DataPropertyName = "woff";
                    DataGridEmployee.Columns[19].Visible = false;

                    DataGridEmployee.Columns[20].Name = "CL";
                    DataGridEmployee.Columns[20].HeaderText = "CL";
                    DataGridEmployee.Columns[20].DataPropertyName = "CL";
                    DataGridEmployee.Columns[20].Visible = false;

                    DataGridEmployee.Columns[21].Name = "EL";
                    DataGridEmployee.Columns[21].HeaderText = "EL";
                    DataGridEmployee.Columns[21].DataPropertyName = "EL";
                    DataGridEmployee.Columns[21].Visible = false;

                    DataGridEmployee.Columns[22].Name = "ESINo";
                    DataGridEmployee.Columns[22].HeaderText = "ESINo";
                    DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
                    DataGridEmployee.Columns[22].Visible = false;

                    DataGridEmployee.Columns[23].Name = "PFNo";
                    DataGridEmployee.Columns[23].HeaderText = "PFNo";
                    DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
                    DataGridEmployee.Columns[23].Visible = false;

                    DataGridEmployee.Columns[24].Name = "UANNo";
                    DataGridEmployee.Columns[24].HeaderText = "UANNo";
                    DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
                    DataGridEmployee.Columns[24].Visible = false;

                    DataGridEmployee.Columns[25].Name = "PANNo";
                    DataGridEmployee.Columns[25].HeaderText = "PANNo";
                    DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
                    DataGridEmployee.Columns[25].Visible = false;

                    DataGridEmployee.Columns[26].Name = "AadharNo";
                    DataGridEmployee.Columns[26].HeaderText = "AadharNo";
                    DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
                    DataGridEmployee.Columns[26].Visible = false;

                    DataGridEmployee.Columns[27].Name = "empphoto";
                    DataGridEmployee.Columns[27].HeaderText = "empphoto";
                    DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
                    DataGridEmployee.Columns[27].Visible = false;

                    DataGridEmployee.Columns[28].Name = "DesName";
                    DataGridEmployee.Columns[28].HeaderText = "DesName";
                    DataGridEmployee.Columns[28].DataPropertyName = "DesName";
                    DataGridEmployee.Columns[28].Visible = false;


                    DataGridEmployee.Columns[29].Name = "DeptName";
                    DataGridEmployee.Columns[29].HeaderText = "DeptName";
                    DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
                    DataGridEmployee.Columns[29].Width = 150;

                    DataGridEmployee.Columns[30].Name = "Grade";
                    DataGridEmployee.Columns[30].HeaderText = "Grade";
                    DataGridEmployee.Columns[30].DataPropertyName = "GrName";
                    DataGridEmployee.Columns[30].Width = 150;

                    DataGridEmployee.Columns[31].Name = "TName";
                    DataGridEmployee.Columns[31].HeaderText = "TName";
                    DataGridEmployee.Columns[31].DataPropertyName = "TName";
                    DataGridEmployee.Columns[31].Visible = false;

                    DataGridEmployee.Columns[32].Name = "CardNo";
                    DataGridEmployee.Columns[32].HeaderText = "Unicode";
                    DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
                    DataGridEmployee.Columns[32].Width = 100;

                    DataGridEmployee.Columns[33].Name = "emialtag";
                    DataGridEmployee.Columns[33].HeaderText = "emialtag";
                    DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
                    DataGridEmployee.Columns[33].Visible = false;

                    DataGridEmployee.Columns[34].Name = "isappriser";
                    DataGridEmployee.Columns[34].HeaderText = "isappriser";
                    DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
                    DataGridEmployee.Columns[34].Visible = false;

                    DataGridEmployee.Columns[35].Name = "appid";
                    DataGridEmployee.Columns[35].HeaderText = "appid";
                    DataGridEmployee.Columns[35].DataPropertyName = "appid";
                    DataGridEmployee.Columns[35].Visible = false;

                    DataGridEmployee.Columns[36].Name = "kname";
                    DataGridEmployee.Columns[36].HeaderText = "kname";
                    DataGridEmployee.Columns[36].DataPropertyName = "kname";
                    DataGridEmployee.Columns[36].Visible = false;

                    DataGridEmployee.Columns[37].Name = "acno";
                    DataGridEmployee.Columns[37].HeaderText = "acno";
                    DataGridEmployee.Columns[37].DataPropertyName = "acno";
                    DataGridEmployee.Columns[37].Visible = false;

                    DataGridEmployee.Columns[38].Name = "pmode";
                    DataGridEmployee.Columns[38].HeaderText = "pmode";
                    DataGridEmployee.Columns[38].DataPropertyName = "pmode";
                    DataGridEmployee.Columns[38].Visible = false;

                    DataGridEmployee.Columns[39].Name = "ptype";
                    DataGridEmployee.Columns[39].HeaderText = "ptype";
                    DataGridEmployee.Columns[39].DataPropertyName = "ptype";
                    DataGridEmployee.Columns[39].Visible = false;

                    DataGridEmployee.Columns[40].Name = "BllodGroup";
                    DataGridEmployee.Columns[40].HeaderText = "BllodGroup";
                    DataGridEmployee.Columns[40].DataPropertyName = "BllodGroup";
                    DataGridEmployee.Columns[40].Visible = false;

                    DataGridEmployee.Columns[41].Name = "Marritalstatus";
                    DataGridEmployee.Columns[41].HeaderText = "Marrital status";
                    DataGridEmployee.Columns[41].DataPropertyName = "Marritalstatus";
                    DataGridEmployee.Columns[41].Visible = false;

                    DataGridEmployee.Columns[42].Name = "Grosssalary";
                    DataGridEmployee.Columns[42].HeaderText = "Grosssalary";
                    DataGridEmployee.Columns[42].DataPropertyName = "Grosssalary";
                    DataGridEmployee.Columns[42].Visible = false;

                    bsp.DataSource = bs;

                    DataGridEmployee.DataSource = bsp;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Loadgrid1()
        {
            conn.Close();
            conn.Open();
            string quy = "exec SP_GetEmp_mastdelete";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter apt = new SqlDataAdapter(Genclass.cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.Refresh();
            DataGridEmployee.DataSource = null;
            DataGridEmployee.Rows.Clear();
            DataGridEmployee.ColumnCount = tab.Columns.Count;
            Genclass.i = 0;

            foreach (DataColumn column in tab.Columns)
            {
                DataGridEmployee.Columns[Genclass.i].Name = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].HeaderText = column.ColumnName;
                DataGridEmployee.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            DataGridEmployee.DataSource = null;
            DataGridEmployee.AutoGenerateColumns = false;
            DataGridEmployee.ColumnCount = 38;

            DataGridEmployee.Columns[0].Name = "EmpId";
            DataGridEmployee.Columns[0].HeaderText = "EmpId";
            DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
            DataGridEmployee.Columns[0].Visible = false;

            DataGridEmployee.Columns[1].Name = "EmpCode";
            DataGridEmployee.Columns[1].HeaderText = "EmpCode";
            DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
            DataGridEmployee.Columns[1].Width = 100;

            DataGridEmployee.Columns[2].Name = "EmpName";
            DataGridEmployee.Columns[2].HeaderText = "Employe Name";
            DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
            DataGridEmployee.Columns[2].Width = 200;

            DataGridEmployee.Columns[3].Name = "ShiftName";
            DataGridEmployee.Columns[3].HeaderText = "ShiftName";
            DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
            DataGridEmployee.Columns[3].Visible = false;

            DataGridEmployee.Columns[4].Name = "DeptId";
            DataGridEmployee.Columns[4].HeaderText = "DeptId";
            DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
            DataGridEmployee.Columns[4].Visible = false;

            DataGridEmployee.Columns[5].Name = "DesId";
            DataGridEmployee.Columns[5].HeaderText = "DesId";
            DataGridEmployee.Columns[5].DataPropertyName = "DesId";
            DataGridEmployee.Columns[5].Visible = false;

            DataGridEmployee.Columns[6].Name = "Grid";
            DataGridEmployee.Columns[6].HeaderText = "Grid";
            DataGridEmployee.Columns[6].DataPropertyName = "Grid";
            DataGridEmployee.Columns[6].Visible = false;

            DataGridEmployee.Columns[7].Name = "GrName";
            DataGridEmployee.Columns[7].HeaderText = "GrName";
            DataGridEmployee.Columns[7].DataPropertyName = "GrName";
            DataGridEmployee.Columns[7].Visible = false;

            DataGridEmployee.Columns[8].Name = "ShiftId";
            DataGridEmployee.Columns[8].HeaderText = "ShiftId";
            DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
            DataGridEmployee.Columns[8].Visible = false;

            DataGridEmployee.Columns[9].Name = "EmpTypeId";
            DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
            DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
            DataGridEmployee.Columns[9].Visible = false;

            DataGridEmployee.Columns[10].Name = "FName";
            DataGridEmployee.Columns[10].HeaderText = "FatherName";
            DataGridEmployee.Columns[10].DataPropertyName = "FName";
            DataGridEmployee.Columns[10].Width = 160;

            DataGridEmployee.Columns[11].Name = "DOB";
            DataGridEmployee.Columns[11].HeaderText = "DOB";
            DataGridEmployee.Columns[11].DataPropertyName = "DOB";
            DataGridEmployee.Columns[11].Visible = false;

            DataGridEmployee.Columns[12].Name = "DOJ";
            DataGridEmployee.Columns[12].HeaderText = "DOJ";
            DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
            DataGridEmployee.Columns[12].Visible = false;

            DataGridEmployee.Columns[13].Name = "Sex";
            DataGridEmployee.Columns[13].HeaderText = "Sex";
            DataGridEmployee.Columns[13].DataPropertyName = "Sex";
            DataGridEmployee.Columns[13].Visible = false;

            DataGridEmployee.Columns[14].Name = "EmpAddress";
            DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
            DataGridEmployee.Columns[14].DataPropertyName = "Street";
            DataGridEmployee.Columns[14].Visible = false;

            DataGridEmployee.Columns[15].Name = "City";
            DataGridEmployee.Columns[15].HeaderText = "City";
            DataGridEmployee.Columns[15].DataPropertyName = "City";
            DataGridEmployee.Columns[15].Visible = false;

            DataGridEmployee.Columns[16].Name = "Phone";
            DataGridEmployee.Columns[16].HeaderText = "Phone";
            DataGridEmployee.Columns[16].DataPropertyName = "Phone";
            DataGridEmployee.Columns[16].Visible = false;

            DataGridEmployee.Columns[17].Name = "Information";
            DataGridEmployee.Columns[17].HeaderText = "Information";
            DataGridEmployee.Columns[17].DataPropertyName = "Information";
            DataGridEmployee.Columns[17].Visible = false;

            DataGridEmployee.Columns[18].Name = "Dispensary";
            DataGridEmployee.Columns[18].HeaderText = "Dispensary";
            DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
            DataGridEmployee.Columns[18].Visible = false;

            DataGridEmployee.Columns[19].Name = "WeekOff";
            DataGridEmployee.Columns[19].HeaderText = "WeekOff";
            DataGridEmployee.Columns[19].DataPropertyName = "woff";
            DataGridEmployee.Columns[19].Visible = false;

            DataGridEmployee.Columns[20].Name = "CL";
            DataGridEmployee.Columns[20].HeaderText = "CL";
            DataGridEmployee.Columns[20].DataPropertyName = "CL";
            DataGridEmployee.Columns[20].Visible = false;

            DataGridEmployee.Columns[21].Name = "EL";
            DataGridEmployee.Columns[21].HeaderText = "EL";
            DataGridEmployee.Columns[21].DataPropertyName = "EL";
            DataGridEmployee.Columns[21].Visible = false;

            DataGridEmployee.Columns[22].Name = "ESINo";
            DataGridEmployee.Columns[22].HeaderText = "ESINo";
            DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
            DataGridEmployee.Columns[22].Visible = false;

            DataGridEmployee.Columns[23].Name = "PFNo";
            DataGridEmployee.Columns[23].HeaderText = "PFNo";
            DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
            DataGridEmployee.Columns[23].Visible = false;

            DataGridEmployee.Columns[24].Name = "UANNo";
            DataGridEmployee.Columns[24].HeaderText = "UANNo";
            DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
            DataGridEmployee.Columns[24].Visible = false;

            DataGridEmployee.Columns[25].Name = "PANNo";
            DataGridEmployee.Columns[25].HeaderText = "PANNo";
            DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
            DataGridEmployee.Columns[25].Visible = false;

            DataGridEmployee.Columns[26].Name = "AadharNo";
            DataGridEmployee.Columns[26].HeaderText = "AadharNo";
            DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
            DataGridEmployee.Columns[26].Visible = false;

            DataGridEmployee.Columns[27].Name = "empphoto";
            DataGridEmployee.Columns[27].HeaderText = "empphoto";
            DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
            DataGridEmployee.Columns[27].Visible = false;

            DataGridEmployee.Columns[28].Name = "DesName";
            DataGridEmployee.Columns[28].HeaderText = "DesName";
            DataGridEmployee.Columns[28].DataPropertyName = "DesName";
            DataGridEmployee.Columns[28].Visible = false;


            DataGridEmployee.Columns[29].Name = "DeptName";
            DataGridEmployee.Columns[29].HeaderText = "DeptName";
            DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
            DataGridEmployee.Columns[29].Width = 150;

            DataGridEmployee.Columns[30].Name = "Grade";
            DataGridEmployee.Columns[30].HeaderText = "Grade";
            DataGridEmployee.Columns[30].DataPropertyName = "Grade";
            DataGridEmployee.Columns[30].Width = 150;

            DataGridEmployee.Columns[31].Name = "TName";
            DataGridEmployee.Columns[31].HeaderText = "TName";
            DataGridEmployee.Columns[31].DataPropertyName = "TName";
            DataGridEmployee.Columns[31].Visible = false;

            DataGridEmployee.Columns[32].Name = "CardNo";
            DataGridEmployee.Columns[32].HeaderText = "CardNo";
            DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
            DataGridEmployee.Columns[32].Width = 100;

            DataGridEmployee.Columns[33].Name = "emialtag";
            DataGridEmployee.Columns[33].HeaderText = "emialtag";
            DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
            DataGridEmployee.Columns[33].Visible = false;

            DataGridEmployee.Columns[34].Name = "isappriser";
            DataGridEmployee.Columns[34].HeaderText = "isappriser";
            DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
            DataGridEmployee.Columns[34].Visible = false;

            DataGridEmployee.Columns[35].Name = "appid";
            DataGridEmployee.Columns[35].HeaderText = "appid";
            DataGridEmployee.Columns[35].DataPropertyName = "appid";
            DataGridEmployee.Columns[35].Visible = false;

            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;

            DataGridEmployee.Columns[36].Name = "kname";
            DataGridEmployee.Columns[36].HeaderText = "kname";
            DataGridEmployee.Columns[36].DataPropertyName = "kname";
            DataGridEmployee.Columns[36].Visible = false;

            DataGridEmployee.Columns[38].Name = "pmode";
            DataGridEmployee.Columns[38].HeaderText = "pmode";
            DataGridEmployee.Columns[38].DataPropertyName = "pmode";
            DataGridEmployee.Columns[38].Visible = false;
            DataGridEmployee.DataSource = tab;
        }
        public DataTable GetData(string Procedurename)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void FillEMp(DataTable dataTable)
        {
            try
            {


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                DataTable dtDes = ds.Tables[1];
                DataTable DtEmpType = ds.Tables[2];
                DataTable dtGrade = ds.Tables[3];
                DataTable dtShift = ds.Tables[4];
                DataTable dtpaygrp = ds.Tables[5];
                cmbDepartment.DataSource = null;
                cmbDepartment.DisplayMember = "DeptName";
                cmbDepartment.ValueMember = "DeptId";
                cmbDepartment.DataSource = dtDept;
                DataTable dt = dtDept;

                DataRow dataRow = dt.NewRow();
                dataRow["DeptName"] = "All";
                dataRow["DeptId"] = "0";
                dt.Rows.Add(dataRow);

                if(GeneralParameters.BranchId == 0)
                {
                    cmgfntbox.DataSource = null;
                    cmgfntbox.DisplayMember = "DeptName";
                    cmgfntbox.ValueMember = "DeptId";
                    cmgfntbox.DataSource = dt;
                }
                else
                {
                    string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    cmgfntbox.DataSource = null;
                    cmgfntbox.DisplayMember = "DeptName";
                    cmgfntbox.ValueMember = "DeptId";
                    cmgfntbox.DataSource = dataTable;
                }
                cmbDesinganation.DataSource = null;
                cmbDesinganation.DisplayMember = "DesName";
                cmbDesinganation.ValueMember = "DesId";
                cmbDesinganation.DataSource = dtDes;

                cmbCategory.DataSource = null;
                cmbCategory.DisplayMember = "GrName";
                cmbCategory.ValueMember = "GrId";
                cmbCategory.DataSource = dtGrade;

                cmbEmpType.DataSource = null;
                cmbEmpType.DisplayMember = "TSName";
                cmbEmpType.ValueMember = "TId";
                cmbEmpType.DataSource = DtEmpType;

                cmbShiftName.DataSource = null;
                cmbShiftName.DisplayMember = "ShiftName";
                cmbShiftName.ValueMember = "ShiftId";
                cmbShiftName.DataSource = dtShift;

                cmbpaygrp.DataSource = null;
                cmbpaygrp.DisplayMember = "PGName";
                cmbpaygrp.ValueMember = "uid";
                cmbpaygrp.DataSource = dtpaygrp;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadMasters1()
        {
            try
            {
                conn.Close();
                conn.Open();
                string qur = "select DISTINCT  1 AS ID,DISPENSARY from EMP_MAST order by DISPENSARY";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                cbotrdept.DataSource = null;
                cbotrdept.DataSource = tab;
                cbotrdept.DisplayMember = "DISPENSARY";
                cbotrdept.ValueMember = "ID";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    button7.Visible = false;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    button7.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                    button7.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Clextxt();
            ClearTextBox(this, grBack);
            LoadMasters();
            LoadButton(1);
            Loadapp();
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            EmpId = 0;

            //picEmpPhoto.Image = null;
        }

        protected void ClearTextBox(Form Frmname, GroupBox whatfldone)
        {
            Genclass.Parent = whatfldone;
            foreach (Control ccontrol in Frmname.Controls)
            {

                if (ccontrol is Panel)
                {

                    foreach (Control c in Genclass.Parent.Controls)
                    {
                        if (c is TextBox || c is RichTextBox)
                        {
                            c.Text = string.Empty;
                        }
                    }
                }
            }

        }

        private void Clextxt()
        {

            txtEmpCardNo.Text = string.Empty;
            txtEmpcode.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtFatherName.Text = string.Empty;
            txtAadharNo.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtInfo.Text = string.Empty;
            txtBloodGroup.Text = string.Empty;
            txtGrossSal.Text = string.Empty;
            txtmailid.Text = string.Empty;
            txtCL.Text = string.Empty;
            txtEL.Text = string.Empty;
            txtDispensary.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtPFNo.Text = string.Empty;
            txtaccno.Text = string.Empty;
            txtESINo.Text = string.Empty;
            txtmailid.Text = string.Empty;
            txtUANNo.Text = string.Empty;
        }

        private void PicEmpPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog
                {
                    Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*"
                };
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (cmbDepartment.Text == "")
                {
                    MessageBox.Show("Select The Department");
                    cmbDepartment.Focus();
                    return;
                }
                if (cmbDesinganation.Text == "")
                {
                    MessageBox.Show("Select The Desinganation");
                    cmbDesinganation.Focus();
                    return;
                }
                if (cmbCategory.Text == "")
                {
                    MessageBox.Show("Select The Category");
                    cmbCategory.Focus();
                    return;
                }
                if (cmbEmpType.Text == "")
                {
                    MessageBox.Show("Select The Employee Type");
                    cmbEmpType.Focus();
                    return;
                }
                if (cmbShiftName.Text == "")
                {
                    MessageBox.Show("Select The ShiftName");
                    cmbShiftName.Focus();
                    return;
                }
                if (cmbGroup.Text == "")
                {
                    MessageBox.Show("Select The Group");
                    cmbGroup.Focus();
                    return;
                }
                if (cmbWeekOff.Text == "")
                {
                    MessageBox.Show("Select The WeekOff");
                    cmbWeekOff.Focus();
                    return;
                }
                if (cmbpaygrp.Text == "")
                {
                    MessageBox.Show("Select The paygrp");
                    cmbpaygrp.Focus();
                    return;
                }
                if (cbomode.Text == "")
                {
                    MessageBox.Show("Select The Mode");
                    cbomode.Focus();
                    return;
                }

                if (txtEmpName.Text == "")
                {
                    MessageBox.Show("Enter The Employee");
                    txtEmpName.Focus();
                    return;
                }
                if (txtEmpCardNo.Text == "")
                {
                    MessageBox.Show("Enter The CardNo");
                    txtEmpCardNo.Focus();
                    return;
                }
                if (txtEmpcode.Text == "")
                {
                    MessageBox.Show("Select The Empcode");
                    txtEmpcode.Focus();
                    return;
                }
                if(picEmpPhoto.Image == null)
                {
                    MessageBox.Show("Select The Employee Photo");
                    picEmpPhoto.Focus();
                    return;
                }
                string sex = string.Empty;
                if (radioMale.Checked == true)
                {
                    sex = "M";
                }
                else
                {
                    sex = "F";
                }

                int chk = 0;
                if (IsAppriser.Checked == true)
                {
                    chk = 1;
                }
                else
                {
                    chk = 0;
                }
                int ll = 0;
                if (cmbapp.SelectedValue == null)

                {
                    ll = 0;

                }
                else
                {

                    ll = Convert.ToInt16(cmbapp.SelectedValue);
                }
                int pp;
                if (cmbpaygrp.SelectedValue == null)

                {
                    pp = 0;

                }
                else
                {
                    pp = Convert.ToInt16(cmbpaygrp.SelectedValue);
                }
                byte[] photo_aray = null;
                if (picEmpPhoto.Image != null)
                {
                    MemoryStream ms = new MemoryStream();
                    picEmpPhoto.Image.Save(ms, ImageFormat.Jpeg);
                    photo_aray = new byte[ms.Length];
                    ms.Position = 0;
                    ms.Read(photo_aray, 0, photo_aray.Length);
                }
                SqlParameter[] parameters = {
                    new SqlParameter("@EmpId",EmpId),
                    new SqlParameter("@EmpName",txtEmpName.Text),
                    new SqlParameter("@EmpCode",txtEmpcode.Text),
                    new SqlParameter("@EmpCardNo",Convert.ToInt32(txtEmpCardNo.Text)),
                    new SqlParameter("@Deptid",Convert.ToInt32(cmbDepartment.SelectedValue)),
                    new SqlParameter("@DesId",Convert.ToInt32(cmbDesinganation.SelectedValue)),
                    new SqlParameter("@CtgyId",cmbCategory.SelectedValue),
                    new SqlParameter("@ShiftId",cmbShiftName.SelectedValue),
                    new SqlParameter("@EmpTypeId",cmbEmpType.SelectedValue),
                    new SqlParameter("@FatherName",txtFatherName.Text),
                    new SqlParameter("@DOB",Convert.ToDateTime(dtpDOB.Text)),
                    new SqlParameter("@DOJ",Convert.ToDateTime(dtpDOJ.Text)),
                    new SqlParameter("@Sex",sex),
                    new SqlParameter("@EmpAddress",txtAddress.Text),
                    new SqlParameter("@City",txtCity.Text),
                    new SqlParameter("@Phone",txtPhone.Text),
                    new SqlParameter("@Information",txtInfo.Text),
                    new SqlParameter("@Dispensary",GeneralParameters.BranchId),
                    new SqlParameter("@WeekOff",cmbWeekOff.Text),
                    new SqlParameter("@CL",txtCL.Text),
                    new SqlParameter("@EL",txtEL.Text),
                    new SqlParameter("@ESINo",txtESINo.Text),
                    new SqlParameter("@PFNo",txtPFNo.Text),
                    new SqlParameter("@UANNo",txtUANNo.Text),
                    new SqlParameter("@PANNo",txtPanNo.Text),
                    new SqlParameter("@AadharNo",txtAadharNo.Text),
                    new SqlParameter("@Image",photo_aray),
                    new SqlParameter("@Eg",cmbGroup.Text),
                    new SqlParameter("@emailtag",txtmailid.Text),
                    new SqlParameter("@isappriser",chk),
                    new SqlParameter("@appid",ll),
                    new SqlParameter("@pgid",pp),
                    new SqlParameter("@acno",txtaccno.Text),
                    new SqlParameter("@RtnMsg",SqlDbType.Int),
                    new SqlParameter("@pmode",cbomode.Text),
                    new SqlParameter("@ptype",txtifsc.Text),
                    new SqlParameter("@BllodGroup",txtBloodGroup.Text),
                    new SqlParameter("@Marritalstatus",CmbMaritalSts.Text),
                    new SqlParameter("@Grosssalary",txtGrossSal.Text),
                };
                parameters[33].Direction = ParameterDirection.Output;
                EmpId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Emp_Mast", parameters, 33, conn);
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Clextxt();
                ClearTextBox(this, grBack);
                LoadButton(3);
                Cmgfntbox_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                picEmpPhoto.Image = null;
                Clextxt();
                ClearTextBox(this, grBack);
                int Id = DataGridEmployee.SelectedCells[0].RowIndex;
                EmpId = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
                txtEmpName.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
                txtEmpcode.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
                txtEmpCardNo.Text = DataGridEmployee.Rows[Id].Cells[32].Value.ToString();
                cmbDepartment.SelectedValue = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
                cmbDesinganation.SelectedValue = DataGridEmployee.Rows[Id].Cells[5].Value.ToString();
                cmbCategory.SelectedValue = DataGridEmployee.Rows[Id].Cells[6].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[30].Value.ToString();
                cmbShiftName.SelectedValue = DataGridEmployee.Rows[Id].Cells[8].Value.ToString();
                cmbEmpType.SelectedValue = DataGridEmployee.Rows[Id].Cells[9].Value.ToString();
                txtFatherName.Text = DataGridEmployee.Rows[Id].Cells[10].Value.ToString();
                dtpDOB.Text = DataGridEmployee.Rows[Id].Cells[11].Value.ToString();
                dtpDOJ.Text = DataGridEmployee.Rows[Id].Cells[12].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[13].Value.ToString() == "M")
                {
                    radioMale.Checked = true;
                }
                else
                {
                    RadioFemale.Checked = true;
                }
                txtAddress.Text = DataGridEmployee.Rows[Id].Cells[14].Value.ToString();
                txtCity.Text = DataGridEmployee.Rows[Id].Cells[15].Value.ToString();
                txtPhone.Text = DataGridEmployee.Rows[Id].Cells[16].Value.ToString();
                txtInfo.Text = DataGridEmployee.Rows[Id].Cells[17].Value.ToString();
                //txtDispensary.Text = DataGridEmployee.CurrentRow.Cells[18].Value.ToString();
                cmbWeekOff.Text = DataGridEmployee.Rows[Id].Cells[19].Value.ToString();
                txtCL.Text = DataGridEmployee.Rows[Id].Cells[20].Value.ToString();
                txtEL.Text = DataGridEmployee.Rows[Id].Cells[21].Value.ToString();
                txtESINo.Text = DataGridEmployee.Rows[Id].Cells[22].Value.ToString();
                txtPFNo.Text = DataGridEmployee.Rows[Id].Cells[23].Value.ToString();
                txtUANNo.Text = DataGridEmployee.Rows[Id].Cells[24].Value.ToString();
                txtPanNo.Text = DataGridEmployee.Rows[Id].Cells[25].Value.ToString();
                txtAadharNo.Text = DataGridEmployee.Rows[Id].Cells[26].Value.ToString();
                cmbDepartment.Text = DataGridEmployee.Rows[Id].Cells[28].Value.ToString();
                cmbDesinganation.Text = DataGridEmployee.Rows[Id].Cells[29].Value.ToString();
                cmbCategory.Text = DataGridEmployee.Rows[Id].Cells[7].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[31].Value.ToString();
                txtaccno.Text = DataGridEmployee.Rows[Id].Cells[37].Value.ToString();
                cbomode.Text = DataGridEmployee.Rows[Id].Cells[38].Value.ToString();
                txtifsc.Text = DataGridEmployee.Rows[Id].Cells[39].Value.ToString();
                CmbBranchA.Text = DataGridEmployee.Rows[Id].Cells[18].Value.ToString();
                txtBloodGroup.Text = DataGridEmployee.Rows[Id].Cells[40].Value.ToString();
                txtGrossSal.Text = DataGridEmployee.Rows[Id].Cells[42].Value.ToString();
                CmbMaritalSts.Text = DataGridEmployee.Rows[Id].Cells[41].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[33].Value == null)
                {
                    txtmailid.Text = string.Empty;
                }
                else
                {
                    txtmailid.Text = DataGridEmployee.Rows[Id].Cells[33].Value.ToString();
                }
                if (DataGridEmployee.Rows[Id].Cells[34].Value.ToString() == "1")
                {
                    IsAppriser.Checked = true;
                }
                else
                {
                    IsAppriser.Checked = false;
                    if (DataGridEmployee.Rows[Id].Cells[33].Value != null)
                    {
                        cmbapp.SelectedValue = DataGridEmployee.Rows[Id].Cells[35].Value.ToString();
                    }
                    cmbapp.Text = DataGridEmployee.Rows[Id].Cells[36].Value.ToString();
                }

                if (DataGridEmployee.Rows[Id].Cells[34].Value.ToString() == "1")
                {
                    string qur = "select Appid, Appraiser from Appraiser WHERE APPID=" + DataGridEmployee.Rows[Id].Cells[34].Value.ToString() + "  order by Appid";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    cmbapp.DataSource = null;
                    cmbapp.DataSource = tab;
                    cmbapp.DisplayMember = "Appraiser";
                    cmbapp.ValueMember = "Appid";
                    //cmbapp.SelectedIndex = -1;
                    conn.Close();
                }

                cmbShiftName.Text = DataGridEmployee.Rows[Id].Cells[3].Value.ToString();

                string qur1 = "Select * from emp_mast where empid=" + EmpId + " and Empphoto is not Null";
                Genclass.cmd = new SqlCommand(qur1, conn);

                SqlDataAdapter adpt1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                adpt1.Fill(tap1);

                if(tap1.Rows.Count > 0)
                {
                    if (tap1.Rows[0]["Empphoto"].ToString() != string.Empty)
                    {
                        byte[] photo_aray;
                        photo_aray = (byte[])tap1.Rows[0]["Empphoto"];
                        MemoryStream ms = new MemoryStream(photo_aray);
                        picEmpPhoto.Image = Image.FromStream(ms);
                        picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
                }
                LoadButton(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog
                {
                    Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*"
                };
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            try
            {
                picEmpPhoto.Image = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            LoadButton(3);
            button7.Visible = true;

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            LoadId = 1;
            Loadgrid();
            Loadapp();
            LoadId = 0;
            LoadMasters();
        }

        protected DataTable GetEmployee()
        {
            DataTable dt = new DataTable();
            string qur = "Select a.EmpId,a.EmpCode,a.EmpName,f.ShiftName,a.DeptId,a.DesId,a.Grid,d.GrName,a.ShiftId,a.EmpTyp,a.FName,a.DOB,a.doj," +
                "a.Sex,a.street,a.City,a.phone,a.Information,a.Dispensary,a.woff,a.CL,a.EL,a.ESINo,a.EPFNo,a.UANNo,a.PANNo,a.AadharNo,a.empphoto," +
                "b.DesName,c.DeptName,a.EG as grade,e.TName,a.CardNo,a.emailtag,a.isappriser,a.appid,k.empname as kname " +
                "from Emp_Mast a " +
                "inner join Des_Mast b on a.DesId = b.DesId   " +
                "inner  join Dept_Mast c on a.DeptId = c.DeptId " +
                "inner join Gr_Mast d on a.GrId = d.GrId  " +
                "inner  join EMP_TYPE e on a.EmpTyp = e.TId  " +
                "inner join Shift_Mast f on a.ShiftId = f.ShiftId  " +
                "left  join emp_mast k on a.appid = k.EmpId " +
                "where a.Dispensary = '" + GeneralParameters.Branch + "'";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }


        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtSearch.Text != string.Empty)
                {
                    bs.Filter = string.Format("EmpCode Like '%{0}%' or EmpName Like '%{1}%' ", txtSearch.Text, txtSearch.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            panFooter.Visible = true;
            Loadgrid1();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Cmgfntbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmgfntbox.SelectedValue != null)
            {
                GeneralParameters.BranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                Loadgrid2();
            }
        }

        private void PicQEmpPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog
                {
                    Filter = "jpeg|*.jpg|bmp|*.bmp|all files|*.*"
                };
                DialogResult res = fileDialog.ShowDialog();
                if (res == DialogResult.OK)
                {
                    picEmpPhoto.Image = Image.FromFile(fileDialog.FileName);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {

            textBox2.Text = "";
            textBox1.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            grSearch.Visible = true;
            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            textBox2.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
            textBox1.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
            textBox3.Text = DataGridEmployee.Rows[Id].Cells[29].Value.ToString();
            textBox4.Text = DataGridEmployee.Rows[Id].Cells[28].Value.ToString();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            string query = "Update Emp_Mast Set Active =0,DOL='" + Convert.ToDateTime(dol.Text).ToString("yyyy-MM-dd") + "' Where  Empcode='" + textBox1.Text + "' and BranchId='" + GeneralParameters.BranchId + "'";
            SqlCommand cmd = new SqlCommand(query, conn);

            cmd.ExecuteNonQuery();

            conn.Close();
            grSearch.Visible = false;

            //qur.CommandText = "delete  from  JOKNITtingREC  where headid=" + HFGP.Rows[i].Cells[0].Value.ToString() + "";
            //qur.ExecuteNonQuery();
            MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            CmbBranch_SelectedIndexChanged(sender, e);

        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //picEmpPhoto.Image = null;
            bracnhtranser.Visible = true;
            textBox5.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";

            LoadMasters1();
            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            EmpId = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
            textBox8.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
            textBox7.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
            textBox5.Text = DataGridEmployee.Rows[Id].Cells[29].Value.ToString();
            cbotrdept.SelectedValue = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
            cbotrdept.Text = DataGridEmployee.Rows[Id].Cells[28].Value.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            EmpId = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());

            conn.Close();
            string query = "Update Emp_Mast Set Active =0,DOL='" + Convert.ToDateTime(cbodol.Text).ToString("yyyy-MM-dd") + "' Where  empid=" + EmpId + "";
            SqlCommand cmd = new SqlCommand(query, conn);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            string query1 = "select * from emp_mast where empid=" + EmpId + "";
            SqlCommand cmd1 = new SqlCommand(query1, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd1);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            byte[] photo_aray = null;
            if (tab.Rows[0]["Empphoto"].ToString() != string.Empty)
            {

                photo_aray = (byte[])tab.Rows[0]["Empphoto"];
                MemoryStream ms = new MemoryStream(photo_aray);
                picEmpPhoto.Image = Image.FromStream(ms);
                picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
            }

            if (tab.Rows.Count > 0)
            {
                EmpId = 0;
                SqlParameter[] parameters = {
                    new SqlParameter("@EmpId",EmpId),
                    new SqlParameter("@EmpName",tab.Rows[0]["EmpName"].ToString()),
                    new SqlParameter("@EmpCode",textBox7.Text),
                    new SqlParameter("@EmpCardNo",textBox7.Text),
                    new SqlParameter("@Deptid",cbotrdept.SelectedValue),
                    new SqlParameter("@DesId",tab.Rows[0]["DesId"].ToString()),
                    new SqlParameter("@CtgyId",tab.Rows[0]["grid"].ToString()),
                    new SqlParameter("@ShiftId",tab.Rows[0]["ShiftId"].ToString()),
                    new SqlParameter("@EmpTypeId",tab.Rows[0]["EmpTyp"].ToString()),
                    new SqlParameter("@FatherName",tab.Rows[0]["FName"].ToString()),
                    new SqlParameter("@DOB",Convert.ToDateTime(tab.Rows[0]["DOB"].ToString())),
                    new SqlParameter("@DOJ",Convert.ToDateTime(tab.Rows[0]["DOJ"].ToString())),
                    new SqlParameter("@Sex",tab.Rows[0]["Sex"].ToString()),
                    new SqlParameter("@EmpAddress",tab.Rows[0]["street"].ToString()),
                    new SqlParameter("@City",tab.Rows[0]["City"].ToString()),
                    new SqlParameter("@Phone",tab.Rows[0]["Phone"].ToString()),
                    new SqlParameter("@Information",tab.Rows[0]["Information"].ToString()),
                    new SqlParameter("@Dispensary",CmbBranchA.Text),
                    new SqlParameter("@WeekOff",tab.Rows[0]["woff"].ToString()),
                    new SqlParameter("@CL",tab.Rows[0]["CL"].ToString()),
                    new SqlParameter("@EL",tab.Rows[0]["EL"].ToString()),
                    new SqlParameter("@ESINo",tab.Rows[0]["ESINo"].ToString()),
                    new SqlParameter("@PFNo",tab.Rows[0]["EPFNo"].ToString()),
                    new SqlParameter("@UANNo",tab.Rows[0]["UANNo"].ToString()),
                    new SqlParameter("@PANNo",tab.Rows[0]["PANNo"].ToString()),
                    new SqlParameter("@AadharNo",tab.Rows[0]["AadharNo"].ToString()),
                    new SqlParameter("@Image",photo_aray),
                    new SqlParameter("@Eg",tab.Rows[0]["Eg"].ToString()),
                    new SqlParameter("@emailtag",tab.Rows[0]["emailtag"].ToString()),
                    new SqlParameter("@isappriser",tab.Rows[0]["isappriser"].ToString()),
                    new SqlParameter("@appid",tab.Rows[0]["appid"].ToString()),
                    new SqlParameter("@pgid",tab.Rows[0]["pgid"].ToString()),
                    new SqlParameter("@acno",tab.Rows[0]["acno"].ToString()),
                    new SqlParameter("@RtnMsg",SqlDbType.Int),
                    new SqlParameter("@pmode",tab.Rows[0]["PMode"].ToString())
                };
                parameters[33].Direction = ParameterDirection.Output;
                EmpId = db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Emp_Mast", parameters, 33, conn);
                MessageBox.Show("Record has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            bracnhtranser.Visible = false;
            grSearch.Visible = false;
            MessageBox.Show("record inserted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Loadgrid();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            bracnhtranser.Visible = false;
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            bracnhtranser.Visible = false;
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            button5_Click(sender, e);
        }

        private void btnHide_Click_1(object sender, EventArgs e)
        {
            btnHide_Click(sender, e);
        }

        private void BtnOk_Click_1(object sender, EventArgs e)
        {
            BtnOk_Click(sender, e);
        }

        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Genclass.cat = Convert.ToInt16(DataGridEmployee.CurrentRow.Cells[0].Value.ToString());

            GeneralParameters.ReportId = 23;

            FrmReportviewer crv = new FrmReportviewer
            {
                StartPosition = FormStartPosition.CenterScreen
            };
            crv.ShowDialog();


        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(LoadId == 0)
                {
                    if (CmbBranch.Text != "All")
                    {
                        string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                        DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = "0";
                        dataRow[1] = "All";
                        dataTable.Rows.Add(dataRow);
                        cmgfntbox.DataSource = null;
                        cmgfntbox.DisplayMember = "DeptName";
                        cmgfntbox.ValueMember = "DeptId";
                        cmgfntbox.DataSource = dataTable;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
