﻿namespace MyEasyPay
{
    partial class FrmOCC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnExt = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.DataGridHoliday = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.DtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtoccname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.GrBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridHoliday)).BeginInit();
            this.SuspendLayout();
            // 
            // GrBack
            // 
            this.GrBack.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GrBack.Controls.Add(this.BtnDelete);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.BtnExt);
            this.GrBack.Controls.Add(this.BtnOk);
            this.GrBack.Controls.Add(this.DataGridHoliday);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.DtpDate);
            this.GrBack.Controls.Add(this.txtoccname);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(0, 0);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(460, 510);
            this.GrBack.TabIndex = 1;
            this.GrBack.TabStop = false;
            this.GrBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "Holidays";
            // 
            // BtnExt
            // 
            this.BtnExt.Location = new System.Drawing.Point(370, 466);
            this.BtnExt.Name = "BtnExt";
            this.BtnExt.Size = new System.Drawing.Size(75, 26);
            this.BtnExt.TabIndex = 12;
            this.BtnExt.Text = "Exit";
            this.BtnExt.UseVisualStyleBackColor = true;
            this.BtnExt.Click += new System.EventHandler(this.BtnExt_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(403, 76);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(42, 28);
            this.BtnOk.TabIndex = 11;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // DataGridHoliday
            // 
            this.DataGridHoliday.AllowUserToAddRows = false;
            this.DataGridHoliday.BackgroundColor = System.Drawing.Color.White;
            this.DataGridHoliday.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridHoliday.Location = new System.Drawing.Point(10, 105);
            this.DataGridHoliday.Name = "DataGridHoliday";
            this.DataGridHoliday.ReadOnly = true;
            this.DataGridHoliday.RowHeadersVisible = false;
            this.DataGridHoliday.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridHoliday.Size = new System.Drawing.Size(435, 360);
            this.DataGridHoliday.TabIndex = 9;
            this.DataGridHoliday.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridHoliday_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Occasion";
            // 
            // DtpDate
            // 
            this.DtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpDate.Location = new System.Drawing.Point(10, 76);
            this.DtpDate.Name = "DtpDate";
            this.DtpDate.Size = new System.Drawing.Size(110, 26);
            this.DtpDate.TabIndex = 5;
            // 
            // txtoccname
            // 
            this.txtoccname.Location = new System.Drawing.Point(126, 76);
            this.txtoccname.Name = "txtoccname";
            this.txtoccname.Size = new System.Drawing.Size(271, 26);
            this.txtoccname.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(10, 466);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 26);
            this.BtnDelete.TabIndex = 14;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // FrmOCC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 510);
            this.Controls.Add(this.GrBack);
            this.Name = "FrmOCC";
            this.Text = "Holidays";
            this.Load += new System.EventHandler(this.FrmOCC_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridHoliday)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.Button BtnExt;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.DataGridView DataGridHoliday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DtpDate;
        private System.Windows.Forms.TextBox txtoccname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnDelete;
    }
}