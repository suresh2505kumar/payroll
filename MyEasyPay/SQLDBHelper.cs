﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public DataTable GetDataWithParam(CommandType cmdType, string ProcedureName, SqlParameter[] para,SqlConnection sqlConnection)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = sqlConnection;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetDataWithParamMultiple(CommandType cmdType, string ProcedureName, SqlParameter[] para, SqlConnection sqlConnection)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = sqlConnection;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetData(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = ProcedureName;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable getDataWithParameterCmdType(CommandType cmdType, string CommandText, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {


                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = cmdType;
                cmd.CommandText = CommandText;
                cmd.Connection = conn;
                cmd.Parameters.AddRange(para);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetDataWithoutParam(CommandType cmdType, string ProcedureName, SqlConnection sqlConnection)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = sqlConnection
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetDataWithoutParamMultiple(CommandType cmdType, string ProcedureName, SqlConnection sqlConnection)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = sqlConnection
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return dt;
        }

        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, int Outpara, SqlConnection sqlConnection)
        {
            int Id = 0;
            try
            {
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = sqlConnection;
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(para[Outpara].Value.ToString());//Solved
                sqlConnection.Close();
            }
            catch (SqlException ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para, SqlConnection sqlConnection)
        {
            int Id = 0;
            try
            {
                if(sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = sqlConnection;
                Id = cmd.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (SqlException ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlConnection sqlConnection)
        {
            int Id = 0;
            try
            {
                if (sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                sqlConnection.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Connection = sqlConnection;
                Id = cmd.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (SqlException ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            return Id;
        }
    }
}
