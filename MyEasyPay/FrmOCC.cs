﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmOCC : Form
    {
        public FrmOCC()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtOt = new DataTable();
        BindingSource bs = new BindingSource();
        int Uid = 0;
        DataTable dtmon = new DataTable();
        private void BtnExt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtoccname.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid",Uid),
                        new SqlParameter("@date",Convert.ToDateTime(DtpDate.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@occasion",txtoccname.Text),

                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Save_NFH", parameters, conn);
                    MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtoccname.Text = string.Empty;

                    Uid = 0;
                    GetEDuctEntry();
                    LoadEarnDuctGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void GetEDuctEntry()
        {
            try
            {
                //SqlParameter[] para = { new SqlParameter("@month", Convert.ToString(CmbMonth.Text)), new SqlParameter("@comname", Convert.ToString(cmbcom.Text)) };
                //dtmon = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_Get_Earn_Duct", para);
                dtmon = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_Get_NFH", conn);
                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadEarnDuctGrid()
        {
            try
            {
                DataGridHoliday.DataSource = null;
                DataGridHoliday.AutoGenerateColumns = false;
                DataGridHoliday.ColumnCount = 3;
                this.DataGridHoliday.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.DataGridHoliday.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                DataGridHoliday.Columns[0].Name = "Uid";
                DataGridHoliday.Columns[0].HeaderText = "Uid";
                DataGridHoliday.Columns[0].DataPropertyName = "Uid";
                DataGridHoliday.Columns[0].Visible = false;

                DataGridHoliday.Columns[1].Name = "Date";
                DataGridHoliday.Columns[1].HeaderText = "Date";
                DataGridHoliday.Columns[1].DataPropertyName = "Date";
                DataGridHoliday.Columns[1].Width = 100;

                DataGridHoliday.Columns[2].Name = "occasion";
                DataGridHoliday.Columns[2].HeaderText = "Occasion";
                DataGridHoliday.Columns[2].DataPropertyName = "occasion";
                DataGridHoliday.Columns[2].Width = 300;

                DataGridHoliday.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }

        private void GrBack_Enter(object sender, EventArgs e)
        {

        }

        private void FrmOCC_Load(object sender, EventArgs e)
        {
            DataGridHoliday.RowHeadersVisible = false;
            qur.Connection = conn;
            Uid = 0;

            GetEDuctEntry();
            LoadEarnDuctGrid();

        }

        private void DataGridHoliday_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int index = DataGridHoliday.SelectedCells[0].RowIndex;
                    int uid = Convert.ToInt32(DataGridHoliday.Rows[index].Cells[0].Value.ToString());
                    DialogResult res = MessageBox.Show("Are you sure want to delete this occasion", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.Yes)
                    {
                        string Query = "delete from NFH Where Uid =" + uid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetEDuctEntry();
                        LoadEarnDuctGrid();
                    }
                    else
                    {
                        DataGridHoliday.ClearSelection();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int index = DataGridHoliday.SelectedCells[0].RowIndex;
                int uid = Convert.ToInt32(DataGridHoliday.Rows[index].Cells[0].Value.ToString());
                DialogResult res = MessageBox.Show("Are you sure want to delete this occasion", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                if (res == DialogResult.Yes)
                {
                    string Query = "delete from NFH Where Uid =" + uid + "";
                    db.ExecuteNonQuery(CommandType.Text, Query, conn);
                    MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetEDuctEntry();
                    LoadEarnDuctGrid();
                }
                else
                {
                    DataGridHoliday.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
    }
}
