﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public partial class FrmEarnDuct : Form
    {

        public FrmEarnDuct()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();


        DataTable Docno = new DataTable();
        int Loadid = 0;
        int Uid = 0;

        BindingSource bsp = new BindingSource();
        public int SelectId = 0;

        private void btnExit_Click(object sender, EventArgs e)
        {

        }
        private void FrmEarnDuct_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DataSource = null;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;
            DataGridPunch.RowHeadersVisible = false;
            qur.Connection = conn;
            Uid = 0;
            LoadPaymonth();
            LoadComponents();
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }

        protected void LoadPaymonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtmon = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadComponents()
        {
            try
            {
                int CType = 0;
                if (this.Text == "Deductions")
                {
                    CType = 1;
                }
                else
                {
                    CType = 0;
                }
                SqlParameter[] sqlParameters = { new SqlParameter("@CType", CType) };
                DataTable ds = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getcomponenttype", sqlParameters, conn);
                DataTable dtCom = ds;
                cmbcom.DataSource = null;
                cmbcom.DisplayMember = "componentname";
                cmbcom.ValueMember = "uid";
                cmbcom.DataSource = dtCom;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "")
            {
                if (DataGridPunch.CurrentCell.ColumnIndex == 4)
                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[4];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }
            }
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.RowCount > 1)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "0")
                    {
                        if (DataGridPunch.CurrentCell.ColumnIndex == 4)
                        {
                            conn.Close();
                            conn.Open();

                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@UID",DataGridPunch.CurrentRow.Cells[0].Value.ToString()),
                                new SqlParameter("@AMOUNT",DataGridPunch.CurrentRow.Cells[4].Value.ToString())
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_EARNCOMUPDATE", sqlParameters, conn);
                            conn.Close();
                        }
                    }
                }
            }
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private DataTable Ctype()
        {
            DataTable tab = new DataTable();
            if (Loadid == 0)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    string qur = "select sname,lvid from leave Order by LvId";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    apt.Fill(tab);
                }
            }
            else
            {
                string qur = "select sname,lvid from leave Order by LvId";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                apt.Fill(tab);
                tab = tab.Select("sname in ('Ir','Ab','Wo')", null).CopyToDataTable();
            }
            return tab;


        }

        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //this.DataGridPunch.CurrentRow.Cells[7].Value = this.cbxRow5.Text;
        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellClick(sender, e);
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellValueChanged(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
            dialog.ShowDialog();
            lblFileName.Text = dialog.FileName;
            lblFileName.Visible = true;
            string extensionpath = Path.GetExtension(dialog.FileName);
            switch (extensionpath)
            {
                case ".xls":
                    connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx":
                    connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    break;
            }

            ImportEarndeduction(connstring, lblFileName.Text);
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }

        private void updateAmtdetails()
        {
            SqlParameter[] sqlParameters = {
                new SqlParameter("@monid",CmbMonth.SelectedValue),
                new SqlParameter("@comid",cmbcom.SelectedValue),
                new SqlParameter("@ctype",txtctype.Text),
                new SqlParameter("@BranchId",CmbBranch.SelectedValue),
            };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_uploadearndeduction", sqlParameters, conn);
        }

        protected void ImportEarndeduction(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                if (Path != string.Empty)
                {
                    using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                    {
                        excel_Conn.Open();

                        string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                        DataTable dtexcelData = new DataTable();
                        new DataColumn("EmpCode", typeof(string));
                        new DataColumn("Amount", typeof(float));


                        using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                        {
                            oda.Fill(dtexcelData);
                        }
                        excel_Conn.Close();
                        DialogResult result = MessageBox.Show("All data will be Replaced Do you want to Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                        if (result == DialogResult.Yes)
                        {
                            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString))
                            {
                                string query = "truncate table  importearndeduction ";
                                conn.Open();
                                SqlCommand cmd = new SqlCommand(query, conn);
                                cmd.ExecuteNonQuery();
                                cmd.Dispose();
                                conn.Close();

                                using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                                {
                                    sqlbulkcopy.DestinationTableName = "dbo.importearndeduction";
                                    sqlbulkcopy.ColumnMappings.Add("EmpCode", "empcode");
                                    sqlbulkcopy.ColumnMappings.Add("Amount", "amount");
                                    conn.Open();
                                    sqlbulkcopy.BulkCopyTimeout = 600;
                                    sqlbulkcopy.WriteToServer(dtexcelData);
                                    conn.Close();
                                }
                            }
                        }
                    }
                    updateAmtdetails();
                    MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtempname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

            try
            {
                if (CmbMonth.Text != string.Empty && cmbcom.Text != string.Empty && txtempcode.Text != string.Empty && txtempname.Text != string.Empty && txtamount.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid",Uid),
                        new SqlParameter("@Empid",txtempname.Tag),
                        new SqlParameter("@monid",CmbMonth.SelectedValue),
                        new SqlParameter("@componentid",cmbcom.SelectedValue),
                        new SqlParameter("@ctype",txtctype.Text),
                        new SqlParameter("@Amount",txtamount.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Save_Earn_Duct", parameters, conn);
                    MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtempcode.Text = string.Empty;
                    txtempname.Text = string.Empty;
                    txtamount.Text = string.Empty;
                    Uid = 0;
                    GetEDuctEntry();
                    LoadEarnDuctGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void GetEDuctEntry()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@month", Convert.ToString(CmbMonth.SelectedValue)),
                    new SqlParameter("@Compontid", Convert.ToString(cmbcom.SelectedValue)),
                    new SqlParameter("@BranchID", CmbBranch.SelectedValue) };
                dtmon = db.GetDataWithParam(CommandType.StoredProcedure, "SP_Get_Earn_Duct", para, conn);
                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadEarnDuctGrid()
        {
            try
            {
                DataGridPunch.DataSource = null;
                DataGridPunch.AutoGenerateColumns = false;
                DataGridPunch.ColumnCount = 8;
                this.DataGridPunch.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.DataGridPunch.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                DataGridPunch.Columns[0].Name = "Uid";
                DataGridPunch.Columns[0].HeaderText = "Uid";
                DataGridPunch.Columns[0].DataPropertyName = "Uid";
                DataGridPunch.Columns[0].Visible = false;

                DataGridPunch.Columns[1].Name = "EmpId";
                DataGridPunch.Columns[1].HeaderText = "EmpId";
                DataGridPunch.Columns[1].DataPropertyName = "EmpId";
                DataGridPunch.Columns[1].Visible = false;

                DataGridPunch.Columns[2].Name = "EmpCode";
                DataGridPunch.Columns[2].HeaderText = "EmpCode";
                DataGridPunch.Columns[2].DataPropertyName = "EmpCode";
                DataGridPunch.Columns[2].Width = 100;

                DataGridPunch.Columns[3].Name = "EmpName";
                DataGridPunch.Columns[3].HeaderText = "EmpName";
                DataGridPunch.Columns[3].DataPropertyName = "EmpName";
                DataGridPunch.Columns[3].Width = 340;

                DataGridPunch.Columns[4].Name = "Amount";
                DataGridPunch.Columns[4].HeaderText = "Amount";
                DataGridPunch.Columns[4].DataPropertyName = "Amount";
                DataGridPunch.Columns[4].Width = 100;
                DataGridPunch.Columns[4].DefaultCellStyle.Format = "N2";
                DataGridPunch.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridPunch.Columns[5].Name = "monid";
                DataGridPunch.Columns[5].HeaderText = "monid";
                DataGridPunch.Columns[5].DataPropertyName = "monid";
                DataGridPunch.Columns[5].Visible = false;

                DataGridPunch.Columns[6].Name = "componentid";
                DataGridPunch.Columns[6].HeaderText = "componentid";
                DataGridPunch.Columns[6].DataPropertyName = "componentid";
                DataGridPunch.Columns[6].Visible = false;

                DataGridPunch.Columns[7].Name = "ctype";
                DataGridPunch.Columns[7].HeaderText = "ctype";
                DataGridPunch.Columns[7].DataPropertyName = "ctype";
                DataGridPunch.Columns[7].Visible = false;
                DataGridPunch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void cmbcom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbcom.ValueMember != null && cmbcom.Text != "")
            {
                string qur = "sp_getcomptype  " + cmbcom.SelectedValue + "";
                SqlCommand cmd1 = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd1);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    txtctype.Text = Convert.ToString(tab.Rows[0]["Ctype"].ToString());
                }
                GetEDuctEntry();
                LoadEarnDuctGrid();
            }

        }

        private void DataGridPunch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int index = DataGridPunch.SelectedCells[0].RowIndex;
                    int uid = Convert.ToInt32(DataGridPunch.Rows[index].Cells[0].Value.ToString());
                    DialogResult res = MessageBox.Show("Are you sure want to delete Earn and deduction entry for this Employee", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.Yes)
                    {
                        string Query = "delete from paymonthcomponents Where Uid =" + uid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetEDuctEntry();
                        LoadEarnDuctGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void LinklblSample_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Earnings and Deduction";

                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                row1.Value = "EmpCode";
                row2.Value = "Amount";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbMonth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
