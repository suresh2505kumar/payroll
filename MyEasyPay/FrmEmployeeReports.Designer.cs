﻿namespace MyEasyPay
{
    partial class FrmEmployeeReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployeeReports));
            this.GrpboxFront = new System.Windows.Forms.GroupBox();
            this.BtnImport = new System.Windows.Forms.Button();
            this.DataGridDepartment = new System.Windows.Forms.DataGridView();
            this.BtnShow = new System.Windows.Forms.Button();
            this.ChckShift = new System.Windows.Forms.CheckBox();
            this.DataGridShift = new System.Windows.Forms.DataGridView();
            this.s = new System.Windows.Forms.CheckBox();
            this.chckDepartment = new System.Windows.Forms.CheckBox();
            this.buttnext1 = new System.Windows.Forms.Button();
            this.DatagridCategory = new System.Windows.Forms.DataGridView();
            this.BtnAttendanceProcess = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnPrint = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.Frmdt = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.HFGP = new System.Windows.Forms.DataGridView();
            this.datas = new System.Windows.Forms.DataGridView();
            this.data1 = new System.Windows.Forms.DataGridView();
            this.CmbTag = new System.Windows.Forms.ComboBox();
            this.cboop = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DtpToTate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.GrpboxFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.data1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpboxFront
            // 
            this.GrpboxFront.Controls.Add(this.groupBox1);
            this.GrpboxFront.Controls.Add(this.BtnImport);
            this.GrpboxFront.Controls.Add(this.label4);
            this.GrpboxFront.Controls.Add(this.DataGridDepartment);
            this.GrpboxFront.Controls.Add(this.CmbBranch);
            this.GrpboxFront.Controls.Add(this.BtnShow);
            this.GrpboxFront.Controls.Add(this.ChckShift);
            this.GrpboxFront.Controls.Add(this.DataGridShift);
            this.GrpboxFront.Controls.Add(this.s);
            this.GrpboxFront.Controls.Add(this.chckDepartment);
            this.GrpboxFront.Controls.Add(this.buttnext1);
            this.GrpboxFront.Controls.Add(this.DatagridCategory);
            this.GrpboxFront.Controls.Add(this.BtnAttendanceProcess);
            this.GrpboxFront.Controls.Add(this.label5);
            this.GrpboxFront.Controls.Add(this.BtnPrint);
            this.GrpboxFront.Controls.Add(this.label2);
            this.GrpboxFront.Controls.Add(this.Frmdt);
            this.GrpboxFront.Controls.Add(this.Label1);
            this.GrpboxFront.Controls.Add(this.HFGP);
            this.GrpboxFront.Controls.Add(this.datas);
            this.GrpboxFront.Controls.Add(this.data1);
            this.GrpboxFront.Controls.Add(this.CmbTag);
            this.GrpboxFront.Controls.Add(this.cboop);
            this.GrpboxFront.Location = new System.Drawing.Point(7, 3);
            this.GrpboxFront.Name = "GrpboxFront";
            this.GrpboxFront.Size = new System.Drawing.Size(993, 484);
            this.GrpboxFront.TabIndex = 0;
            this.GrpboxFront.TabStop = false;
            this.GrpboxFront.Enter += new System.EventHandler(this.GrpboxFront_Enter);
            // 
            // BtnImport
            // 
            this.BtnImport.BackColor = System.Drawing.Color.White;
            this.BtnImport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnImport.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImport.Location = new System.Drawing.Point(746, 114);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(191, 47);
            this.BtnImport.TabIndex = 243;
            this.BtnImport.Text = "Import Attendance  and Process ";
            this.BtnImport.UseVisualStyleBackColor = false;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // DataGridDepartment
            // 
            this.DataGridDepartment.AllowUserToAddRows = false;
            this.DataGridDepartment.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridDepartment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDepartment.EnableHeadersVisualStyles = false;
            this.DataGridDepartment.Location = new System.Drawing.Point(10, 59);
            this.DataGridDepartment.Name = "DataGridDepartment";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridDepartment.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridDepartment.Size = new System.Drawing.Size(257, 406);
            this.DataGridDepartment.TabIndex = 0;
            // 
            // BtnShow
            // 
            this.BtnShow.Location = new System.Drawing.Point(106, 36);
            this.BtnShow.Name = "BtnShow";
            this.BtnShow.Size = new System.Drawing.Size(87, 23);
            this.BtnShow.TabIndex = 233;
            this.BtnShow.Text = "Show Category";
            this.BtnShow.UseVisualStyleBackColor = true;
            this.BtnShow.Click += new System.EventHandler(this.BtnShow_Click);
            // 
            // ChckShift
            // 
            this.ChckShift.AutoSize = true;
            this.ChckShift.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckShift.Location = new System.Drawing.Point(538, 36);
            this.ChckShift.Name = "ChckShift";
            this.ChckShift.Size = new System.Drawing.Size(85, 22);
            this.ChckShift.TabIndex = 232;
            this.ChckShift.Text = "Select All";
            this.ChckShift.UseVisualStyleBackColor = true;
            this.ChckShift.CheckedChanged += new System.EventHandler(this.CheckBox5_CheckedChanged);
            // 
            // DataGridShift
            // 
            this.DataGridShift.AllowUserToAddRows = false;
            this.DataGridShift.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridShift.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridShift.EnableHeadersVisualStyles = false;
            this.DataGridShift.Location = new System.Drawing.Point(538, 59);
            this.DataGridShift.Name = "DataGridShift";
            this.DataGridShift.RowHeadersVisible = false;
            this.DataGridShift.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridShift.Size = new System.Drawing.Size(195, 406);
            this.DataGridShift.TabIndex = 231;
            // 
            // s
            // 
            this.s.AutoSize = true;
            this.s.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.s.Location = new System.Drawing.Point(273, 36);
            this.s.Name = "s";
            this.s.Size = new System.Drawing.Size(85, 22);
            this.s.TabIndex = 224;
            this.s.Text = "Select All";
            this.s.UseVisualStyleBackColor = true;
            this.s.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // chckDepartment
            // 
            this.chckDepartment.AutoSize = true;
            this.chckDepartment.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckDepartment.Location = new System.Drawing.Point(10, 36);
            this.chckDepartment.Name = "chckDepartment";
            this.chckDepartment.Size = new System.Drawing.Size(85, 22);
            this.chckDepartment.TabIndex = 223;
            this.chckDepartment.Text = "Select All";
            this.chckDepartment.UseVisualStyleBackColor = true;
            this.chckDepartment.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // buttnext1
            // 
            this.buttnext1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttnext1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttnext1.Image = ((System.Drawing.Image)(resources.GetObject("buttnext1.Image")));
            this.buttnext1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttnext1.Location = new System.Drawing.Point(814, 379);
            this.buttnext1.Name = "buttnext1";
            this.buttnext1.Size = new System.Drawing.Size(57, 30);
            this.buttnext1.TabIndex = 222;
            this.buttnext1.Text = "Exit";
            this.buttnext1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttnext1.UseVisualStyleBackColor = false;
            this.buttnext1.Click += new System.EventHandler(this.Buttnext1_Click);
            // 
            // DatagridCategory
            // 
            this.DatagridCategory.AllowUserToAddRows = false;
            this.DatagridCategory.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DatagridCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.DatagridCategory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatagridCategory.EnableHeadersVisualStyles = false;
            this.DatagridCategory.Location = new System.Drawing.Point(273, 59);
            this.DatagridCategory.Name = "DatagridCategory";
            this.DatagridCategory.Size = new System.Drawing.Size(260, 406);
            this.DatagridCategory.TabIndex = 1;
            this.DatagridCategory.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatagridCategory_CellClick);
            this.DatagridCategory.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DatagridCategory_CellContentClick);
            // 
            // BtnAttendanceProcess
            // 
            this.BtnAttendanceProcess.BackColor = System.Drawing.Color.White;
            this.BtnAttendanceProcess.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnAttendanceProcess.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAttendanceProcess.Location = new System.Drawing.Point(749, 192);
            this.BtnAttendanceProcess.Name = "BtnAttendanceProcess";
            this.BtnAttendanceProcess.Size = new System.Drawing.Size(191, 31);
            this.BtnAttendanceProcess.TabIndex = 230;
            this.BtnAttendanceProcess.Text = "ATTENDANCE PROCESS";
            this.BtnAttendanceProcess.UseVisualStyleBackColor = false;
            this.BtnAttendanceProcess.Visible = false;
            this.BtnAttendanceProcess.Click += new System.EventHandler(this.BtnAttendanceProcess_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(746, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 18);
            this.label5.TabIndex = 221;
            this.label5.Text = "Type";
            // 
            // BtnPrint
            // 
            this.BtnPrint.BackColor = System.Drawing.Color.White;
            this.BtnPrint.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Image = ((System.Drawing.Image)(resources.GetObject("BtnPrint.Image")));
            this.BtnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPrint.Location = new System.Drawing.Point(746, 379);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(65, 30);
            this.BtnPrint.TabIndex = 217;
            this.BtnPrint.Text = "Print";
            this.BtnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnPrint.UseVisualStyleBackColor = false;
            this.BtnPrint.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(746, 316);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 18);
            this.label2.TabIndex = 216;
            this.label2.Text = "Type";
            // 
            // Frmdt
            // 
            this.Frmdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frmdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Frmdt.Location = new System.Drawing.Point(749, 277);
            this.Frmdt.Name = "Frmdt";
            this.Frmdt.Size = new System.Drawing.Size(115, 26);
            this.Frmdt.TabIndex = 207;
            this.Frmdt.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(749, 260);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(37, 18);
            this.Label1.TabIndex = 204;
            this.Label1.Text = "Date";
            // 
            // HFGP
            // 
            this.HFGP.AllowUserToDeleteRows = false;
            this.HFGP.AllowUserToOrderColumns = true;
            this.HFGP.BackgroundColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.HFGP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.HFGP.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.HFGP.Location = new System.Drawing.Point(58, 192);
            this.HFGP.Name = "HFGP";
            this.HFGP.Size = new System.Drawing.Size(37, 15);
            this.HFGP.TabIndex = 234;
            // 
            // datas
            // 
            this.datas.AllowUserToAddRows = false;
            this.datas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.datas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.datas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datas.EnableHeadersVisualStyles = false;
            this.datas.Location = new System.Drawing.Point(101, 162);
            this.datas.Name = "datas";
            this.datas.RowHeadersVisible = false;
            this.datas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.datas.Size = new System.Drawing.Size(35, 33);
            this.datas.TabIndex = 235;
            // 
            // data1
            // 
            this.data1.AllowUserToAddRows = false;
            this.data1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.data1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.data1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.data1.EnableHeadersVisualStyles = false;
            this.data1.Location = new System.Drawing.Point(101, 230);
            this.data1.Name = "data1";
            this.data1.RowHeadersVisible = false;
            this.data1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.data1.Size = new System.Drawing.Size(35, 33);
            this.data1.TabIndex = 236;
            // 
            // CmbTag
            // 
            this.CmbTag.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.CmbTag.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.CmbTag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbTag.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbTag.FormattingEnabled = true;
            this.CmbTag.Items.AddRange(new object[] {
            "Strength Report",
            "In Report",
            "Daily Punch Report",
            "Irregular Punch Report",
            "Worked Hour Report (< 8 Hrs)",
            "Late In Report",
            "Permission Report",
            "Lunch Break Report",
            "Early Out Report",
            "Absent List",
            "Over Time Report",
            "Muster Roll Report",
            "MusterRoll (1 to 15 days)",
            "MusterRoll (16 to 31 days)",
            "Over Time Muster Roll Report"});
            this.CmbTag.Location = new System.Drawing.Point(746, 337);
            this.CmbTag.Name = "CmbTag";
            this.CmbTag.Size = new System.Drawing.Size(241, 26);
            this.CmbTag.TabIndex = 220;
            this.CmbTag.SelectedIndexChanged += new System.EventHandler(this.CmbTag_SelectedIndexChanged);
            // 
            // cboop
            // 
            this.cboop.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cboop.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboop.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboop.FormattingEnabled = true;
            this.cboop.Items.AddRange(new object[] {
            "Employee List",
            "Employee 3 Month Joining List",
            "Employee 6 Month Joining List",
            "Employee PF/ESI Details",
            "Employee Bank Account Details",
            "Employee Contact Details",
            "Employee Data Sheet(CSV format)"});
            this.cboop.Location = new System.Drawing.Point(746, 337);
            this.cboop.Name = "cboop";
            this.cboop.Size = new System.Drawing.Size(241, 26);
            this.cboop.TabIndex = 215;
            this.cboop.SelectedIndexChanged += new System.EventHandler(this.cboop_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.DtpToTate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dtpFromDate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(745, 167);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 212);
            this.groupBox1.TabIndex = 244;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // DtpToTate
            // 
            this.DtpToTate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DtpToTate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToTate.Location = new System.Drawing.Point(128, 50);
            this.DtpToTate.Name = "DtpToTate";
            this.DtpToTate.Size = new System.Drawing.Size(102, 26);
            this.DtpToTate.TabIndex = 246;
            this.DtpToTate.Value = new System.DateTime(2019, 10, 24, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(128, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 18);
            this.label6.TabIndex = 245;
            this.label6.Text = "To Date";
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFromDate.Location = new System.Drawing.Point(8, 50);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(111, 26);
            this.dtpFromDate.TabIndex = 244;
            this.dtpFromDate.Value = new System.DateTime(2019, 10, 24, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 18);
            this.label3.TabIndex = 243;
            this.label3.Text = "From Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(746, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 248;
            this.label4.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(746, 82);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(219, 26);
            this.CmbBranch.TabIndex = 247;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(158, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 30);
            this.button1.TabIndex = 246;
            this.button1.Text = "Close";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(14, 90);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(213, 58);
            this.button2.TabIndex = 245;
            this.button2.Text = "Import Attendace";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // FrmEmployeeReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1008, 493);
            this.Controls.Add(this.GrpboxFront);
            this.MaximizeBox = false;
            this.Name = "FrmEmployeeReports";
            this.Text = "Reports";
            this.Load += new System.EventHandler(this.FrmEmployeeReports_Load);
            this.GrpboxFront.ResumeLayout(false);
            this.GrpboxFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HFGP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.data1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpboxFront;
        private System.Windows.Forms.DataGridView DataGridDepartment;
        private System.Windows.Forms.DataGridView DatagridCategory;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.DateTimePicker Frmdt;
        private System.Windows.Forms.ComboBox cboop;
        private System.Windows.Forms.Button BtnPrint;
        private System.Windows.Forms.ComboBox CmbTag;
        private System.Windows.Forms.Button buttnext1;
        private System.Windows.Forms.CheckBox s;
        private System.Windows.Forms.CheckBox chckDepartment;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnAttendanceProcess;
        private System.Windows.Forms.CheckBox ChckShift;
        private System.Windows.Forms.DataGridView DataGridShift;
        private System.Windows.Forms.Button BtnShow;
        private System.Windows.Forms.DataGridView HFGP;
        private System.Windows.Forms.DataGridView datas;
        private System.Windows.Forms.DataGridView data1;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbBranch;
        private System.Windows.Forms.DateTimePicker DtpToTate;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}