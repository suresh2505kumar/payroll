﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MyEasyPay
{
    public partial class FrmLeaveEntry : Form
    {
        public FrmLeaveEntry()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        int EditId = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        BindingSource bs = new BindingSource();
        string str1key;
        public int SelectId = 0;

        private void FrmLeaveEntry_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            //if (GeneralParameters.BranchId == 0)
            //{
            //    System.Data.DataRow dataRow = dataTable.NewRow();
            //    dataRow["Uid"] = 0;
            //    dataRow["BRANCHNAME"] = "All";
            //    dataTable.Rows.Add(dataRow);
            //}
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            DataTable dt = new DataTable();
            dt = dataTable;
            CmbFBranch.DisplayMember = "BRANCHNAME";
            CmbFBranch.ValueMember = "Uid";
            CmbFBranch.DataSource = dt;

            Frmdt.Text = DateTime.Now.Date.ToString();
            StartPosition = FormStartPosition.CenterScreen;
            grFront.Visible = true;
            grBack.Visible = false;
            Loadftag();
            Loadatag();
            LoadLVTAG();
            Loadgrid1();
            LoadButton(3);
            grSearch.Visible = false;
        }

        protected void ClearControl()
        {
            TxtEL.Text = string.Empty;
            TxtCL.Text = string.Empty;
            TxtBalCL.Text = string.Empty;
            TxtBalEL.Text = string.Empty;
            TxtEmpCode.Text = string.Empty;
            TxtEmpName.Text = string.Empty;
            txtreason.Text = string.Empty;
        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Save";
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                    btnSave.Text = "Update";
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            grFront.Visible = false;
            grBack.Visible = true;
            LoadButton(1);
            ClearControl();
        }

        private void Loadftag()
        {
            try
            {
                string qur = "Select SName,LvType from Leavetype Order by LvOrd";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, qur, conn);
                CmbForeNoonType.DataSource = null;
                CmbForeNoonType.DisplayMember = "LvType";
                CmbForeNoonType.ValueMember = "SName";
                CmbForeNoonType.DataSource = dt;
                CmbForeNoonType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
                return;
            }
        }

        private void LoadLVTAG()
        {
            string qur = "select distinct sname,0 AS UID  from leave  WHERE  SNAME NOT IN ('LW')";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            txtlvtag.DataSource = null;
            txtlvtag.DataSource = tab;
            txtlvtag.DisplayMember = "sname";
            txtlvtag.ValueMember = "UID";
            txtlvtag.SelectedIndex = -1;
            conn.Close();
        }

        private void Loadatag()
        {
            try
            {
                string qurer = "Select SName,LvType from Leavetype Order by LvOrd";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, qurer, conn);
                CmbAfterNoonType.DataSource = null;
                CmbAfterNoonType.DisplayMember = "LvType";
                CmbAfterNoonType.ValueMember = "SName";
                CmbAfterNoonType.DataSource = dt;
                CmbAfterNoonType.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
                return;
            }
        }

        private void CmbType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            grBack.Visible = false;
            grFront.Visible = true;
            LoadButton(3);
        }

        private void Loadgrid1()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@BranchId",CmbFBranch.SelectedValue)
                };
                DataTable tab = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GETLEAVEMASTLOAD", sqlParameters, conn);
                bs.DataSource = tab;
                DataGridEmployee.AutoGenerateColumns = false;
                DataGridEmployee.Refresh();
                DataGridEmployee.DataSource = null;
                DataGridEmployee.Rows.Clear();
                DataGridEmployee.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridEmployee.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                DataGridEmployee.ColumnCount = 10;

                DataGridEmployee.Columns[0].Name = "uid";
                DataGridEmployee.Columns[0].HeaderText = "uid";
                DataGridEmployee.Columns[0].DataPropertyName = "uid";
                DataGridEmployee.Columns[0].Visible = false;

                DataGridEmployee.Columns[1].Name = "empcode";
                DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                DataGridEmployee.Columns[1].DataPropertyName = "empcode";
                DataGridEmployee.Columns[1].Width = 100;

                DataGridEmployee.Columns[2].Name = "EMpname";
                DataGridEmployee.Columns[2].HeaderText = "Employee Name";
                DataGridEmployee.Columns[2].DataPropertyName = "EMpname";
                DataGridEmployee.Columns[2].Width = 120;

                DataGridEmployee.Columns[3].Name = "LVDT";
                DataGridEmployee.Columns[3].HeaderText = "Date";
                DataGridEmployee.Columns[3].DataPropertyName = "LVDT";
                DataGridEmployee.Columns[3].Width = 100;

                DataGridEmployee.Columns[4].Name = "FTAG";
                DataGridEmployee.Columns[4].HeaderText = "FTag";
                DataGridEmployee.Columns[4].DataPropertyName = "FTAG";
                DataGridEmployee.Columns[4].Visible = false;

                DataGridEmployee.Columns[5].Name = "ATAG";
                DataGridEmployee.Columns[5].HeaderText = "ATag";
                DataGridEmployee.Columns[5].DataPropertyName = "ATAG";
                DataGridEmployee.Columns[5].Visible = false;

                DataGridEmployee.Columns[6].Name = "LVTAG";
                DataGridEmployee.Columns[6].HeaderText = "LVTag";
                DataGridEmployee.Columns[6].DataPropertyName = "LVTAG";
                DataGridEmployee.Columns[6].Width = 60;

                DataGridEmployee.Columns[7].Name = "Reason";
                DataGridEmployee.Columns[7].HeaderText = "Reason";
                DataGridEmployee.Columns[7].DataPropertyName = "Reason";
                DataGridEmployee.Columns[7].Width = 130;

                DataGridEmployee.Columns[8].Name = "empid";
                DataGridEmployee.Columns[8].HeaderText = "empid";
                DataGridEmployee.Columns[8].DataPropertyName = "empid";
                DataGridEmployee.Columns[8].Visible = false;

                DataGridEmployee.Columns[9].Name = "AppNo";
                DataGridEmployee.Columns[9].HeaderText = "AppNo";
                DataGridEmployee.Columns[9].DataPropertyName = "AppNo";
                DataGridEmployee.Columns[9].Visible = false;

                DataGridEmployee.DataSource = tab;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message);
                return;
            }
            
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dateTime = Convert.ToDateTime(Frmdt.Text);
                if (btnSave.Text == "Save")
                {
                    string qur = "select * from LEAVE_MAST  where    empid=" + TxtEmpName.Tag + " and LVDT='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
                    DataTable tab = db.GetDataWithoutParam(CommandType.Text, qur, conn);
                    if (tab.Rows.Count > 0)
                    {
                        MessageBox.Show("Already exist");
                        return;
                    }
                    else
                    {
                        SqlParameter[] parameters = {
                            new SqlParameter("@LVDT", dateTime.Date.ToString("yyyy-MM-dd")),
                            new SqlParameter("@FATG", CmbForeNoonType.SelectedValue.ToString()),
                            new SqlParameter("@ATAG", CmbAfterNoonType.SelectedValue.ToString()),
                            new SqlParameter("@LVTAG", CmbForeNoonType.SelectedValue.ToString() + CmbAfterNoonType.SelectedValue.ToString()),
                            new SqlParameter("@REASON", txtreason.Text),
                            new SqlParameter("@empid",TxtEmpName.Tag),
                            new SqlParameter("@BranchId",CmbBranch.SelectedValue),
                            new SqlParameter("@AppNo",txtAppNumber.Text)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETLEAVEMAST", parameters, conn);
                    }
                }
                else
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@LVDT", dateTime.Date.ToString("yyyy-MM-dd")),
                        new SqlParameter("@FATG", CmbForeNoonType.SelectedValue.ToString()),
                        new SqlParameter("@ATAG", CmbAfterNoonType.SelectedValue.ToString()),
                        new SqlParameter("@LVTAG", CmbForeNoonType.SelectedValue.ToString() + CmbAfterNoonType.SelectedValue.ToString()),
                        new SqlParameter("@REASON", txtreason.Text),
                        new SqlParameter("@uid", Genclass.h),
                        new SqlParameter("@empid",TxtEmpName.Tag),
                        new SqlParameter("@BranchId",CmbBranch.SelectedValue),
                        new SqlParameter("@AppNo",txtAppNumber.Text)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETLEAVEMASTupdate", parameters, conn);
                }
                //string query = "update Att_Reg" + dateTime.Month.ToString("00") + " set lvtag='" + CmbForeNoonType.SelectedValue.ToString() + CmbAfterNoonType.SelectedValue.ToString() + "'  Where empid=" + TxtEmpName.Tag + " and  dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
                //db.ExecuteNonQuery(CommandType.Text, query, conn);
                ClearControl();
                LoadButton(3);
                Loadgrid1();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            ClearControl();
            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            Genclass.h = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
            TxtEmpCode.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
            TxtEmpName.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
            Frmdt.Text = DataGridEmployee.Rows[Id].Cells[3].Value.ToString();
            Loadatag();
            Loadftag();

            CmbForeNoonType.SelectedValue = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
            CmbAfterNoonType.SelectedValue = DataGridEmployee.Rows[Id].Cells[5].Value.ToString();
            txtlvtag.Text = DataGridEmployee.Rows[Id].Cells[6].Value.ToString();
            txtreason.Text = DataGridEmployee.Rows[Id].Cells[7].Value.ToString();
            TxtEmpName.Tag = Convert.ToInt16(DataGridEmployee.Rows[Id].Cells[8].Value.ToString());
            txtAppNumber.Text = DataGridEmployee.Rows[Id].Cells[9].Value.ToString();

            LoadButton(2);
            grSearch.Visible = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public DataTable GetData(string Procedurename)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetDataWithoutParam(CommandType.StoredProcedure, Procedurename, conn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            Genclass.h = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
            DialogResult result = MessageBox.Show("Do you want to delete this employee", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                conn.Close();
                conn.Open();
                string query = "delete from leave_mast  Where uid=" + Genclass.h + "";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Loadgrid1();
            }
            else
            {
                return;
            }
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void Txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", TxtEmpCode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtEmpCode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 5;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";

                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;

                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;

                DataGridCommon.Columns[3].Name = "CL";
                DataGridCommon.Columns[3].HeaderText = "CL";
                DataGridCommon.Columns[3].DataPropertyName = "CL";
                DataGridCommon.Columns[3].Visible = false;

                DataGridCommon.Columns[4].Name = "EL";
                DataGridCommon.Columns[4].HeaderText = "EL";
                DataGridCommon.Columns[4].DataPropertyName = "EL";
                DataGridCommon.Columns[4].Visible = false;

                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                bsp.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void Txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", TxtEmpName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtEmpName);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void Button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    TxtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    TxtCL.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    TxtEL.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    SqlParameter[] sqlParameters = {
                        new SqlParameter("@EmpCode",TxtEmpCode.Text),
                        //new SqlParameter("@DT",Convert.ToDateTime(Frmdt.Text))
                    };
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetLeaveBal", sqlParameters, conn);
                    TxtBalCL.Text = (Convert.ToDecimal(dataTable.Rows[0]["ACL"].ToString()) - Convert.ToDecimal(DataGridCommon.Rows[Index].Cells[3].Value.ToString())).ToString("0.0");
                    TxtBalEL.Text = (Convert.ToDecimal(dataTable.Rows[0]["AEL"].ToString()) - Convert.ToDecimal(DataGridCommon.Rows[Index].Cells[4].Value.ToString())).ToString("0.0");
                    CmbForeNoonType.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    TxtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    TxtCL.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    TxtEL.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    CmbForeNoonType.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpCode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    TxtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    TxtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    TxtCL.Text = DataGridCommon.Rows[Index].Cells[3].Value.ToString();
                    TxtEL.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    CmbForeNoonType.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtSearch.Text != string.Empty)
                {
                    bs.Filter = string.Format("EmpCode Like '%{0}%' or EmpName Like '%{1}%' or reason Like '%{2}%' or lvtag Like '%{2}%'", txtSearch.Text, txtSearch.Text, txtSearch.Text, txtSearch.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Button18_Click(sender, e);
        }

        private void CmbFBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            Loadgrid1();
        }
    }
}
