﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Collections.Generic;

namespace MyEasyPay
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        string EnValue;
        private string key = "o7x8y6";
        string LVerifyDtae;

        SqlCommand qur = new SqlCommand();
        
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            //ReadConfig();
            //CheckRegistration();
            //string Branch = ConfigurationManager.AppSettings["Branch"].ToString();
            //if (Branch == "All")
            //{
                //var BranchAll = new List<string>(ConfigurationManager.AppSettings["MultiBranch"].Split(new char[] { ';' }));
                var BranchAll = ConfigurationManager.AppSettings["BranchId"].ToString();
                GeneralParameters.BranchId = Convert.ToInt32(BranchAll);
            //}
            //else
            //{
            //    CmbBranch.Items.Clear();
            //    CmbBranch.Items.Add(Branch);
            //}
            //CmbBranch.SelectedIndex = 0;

            Genclass.data1 = Convert.ToInt16(Decrypt(ConfigurationManager.AppSettings["Data1"]));
            Genclass.data2 = Decrypt(ConfigurationManager.AppSettings["Data2"]);
            Genclass.data3 = Decrypt(ConfigurationManager.AppSettings["Data3"]);
            Genclass.data4 = Decrypt(ConfigurationManager.AppSettings["Data4"]);
            Genclass.data5 = Decrypt(ConfigurationManager.AppSettings["Data5"]);
            Genclass.data6 = Decrypt(ConfigurationManager.AppSettings["Data6"]);
            SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        }

        protected void CheckRegistration()
        {
            try
            {
                string path;
                path = Path.Combine(Application.StartupPath, "Register.Reg");
                if (!File.Exists(path))
                {
                    MessageBox.Show("Register the Company", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                     SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                    string qur = "select * from LicenseInfo";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    if (tab.Rows.Count > 0)
                    {
                        EnValue = tab.Rows[0]["F1"].ToString();
                        GeneralParameters.CompanyName = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        LVerifyDtae = tab.Rows[0]["F5"].ToString();
                        label3.Text = GeneralParameters.CompanyName;

                    }
                    btnLogin.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void ReadConfig()
        {
            try
            {
                string path;
                path = Path.Combine(Application.StartupPath, "config.LCG");

                string[] readText = File.ReadAllLines(path);
                for (int i = 0; i < readText.Length; i++)
                {
                    if (i == 0)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        GeneralParameters.ServerName = txt;
                    }
                    if (i == 1)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        GeneralParameters.DbUserID = txt;
                    }
                    if (i == 2)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        GeneralParameters.Password = txt;
                    }
                    if (i == 3)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        GeneralParameters.DbName = txt;
                    }
                    if (i == 4)
                    {
                        EnValue = readText[i].ToString();
                        string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                        GeneralParameters.CompanyId = txt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //EnValue = string.Empty;
                //EnValue = LVerifyDtae;
                //string dte = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                //DateTime dateTime = Convert.ToDateTime(dte);
                //var Days = (DateTime.Now.Date - dateTime.Date).TotalDays;

                //if (Days > 46)
                //{
                //    DialogResult dialogResult = MessageBox.Show("Updates are available please verify and get Updates", "Updates", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                //    if (dialogResult == DialogResult.Yes)
                //    {
                //        FrmConfiguration frmConfiguration = new FrmConfiguration();
                //        frmConfiguration.StartPosition = FormStartPosition.CenterScreen;
                //        frmConfiguration.ShowDialog();
                //        GeneralParameters.IsVerify = 1;
                //        return;
                //    }
                //}

                if (txtUserName.Text == string.Empty && txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtUserName.Text == string.Empty)
                {
                    MessageBox.Show("Enter user name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtUserName.Focus();
                    return;
                }
                else if (txtPassword.Text == string.Empty)
                {
                    MessageBox.Show("Enter Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtPassword.Focus();
                    return;
                }
                //else if (CmbBranch.Text == string.Empty)
                //{
                //    MessageBox.Show("Select Branch", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    CmbBranch.Focus();
                //    return;
                //}
                else
                {
                    SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                    SQLDBHelper db = new SQLDBHelper();
                    SqlParameter[] para = {
                        new SqlParameter("@UserName",txtUserName.Text),
                        new SqlParameter("@Password",txtPassword.Text)
                    };
                    DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_UserLogin", para, conn);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Enter the correct user name and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtUserName.Focus();
                        return;
                    }
                    else
                    {
                        if(GeneralParameters.BranchId == 0)
                        {
                            GeneralParameters.Branch = "All";
                        }
                        else
                        {
                            string Query = "Select * from BranchM Where UId=" + GeneralParameters.BranchId + "";
                            DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                            GeneralParameters.Branch = dataTable.Rows[0]["BRANCHNAME"].ToString();
                        }
                        GeneralParameters.UserdId = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                        GeneralParameters.LoginUserName = dt.Rows[0]["UserName"].ToString();
                        this.Hide();
                        FrmMain main = new FrmMain();
                        main.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void LinkLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FrmConfiguration frmConfiguration = new FrmConfiguration();
            frmConfiguration.StartPosition = FormStartPosition.CenterScreen;
            frmConfiguration.ShowDialog();
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "myla123";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
