﻿namespace MyEasyPay
{
    partial class FrmConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnGetDetails = new System.Windows.Forms.Button();
            this.GrFront = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtPincode = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtProductKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLicenseKey = new System.Windows.Forms.TextBox();
            this.GrFront.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product Key";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "City";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "Company Name";
            // 
            // BtnGetDetails
            // 
            this.BtnGetDetails.Location = new System.Drawing.Point(343, 35);
            this.BtnGetDetails.Name = "BtnGetDetails";
            this.BtnGetDetails.Size = new System.Drawing.Size(136, 28);
            this.BtnGetDetails.TabIndex = 3;
            this.BtnGetDetails.Text = "Generate License";
            this.BtnGetDetails.UseVisualStyleBackColor = true;
            this.BtnGetDetails.Click += new System.EventHandler(this.BtnGetDetails_Click);
            // 
            // GrFront
            // 
            this.GrFront.Controls.Add(this.txtLicenseKey);
            this.GrFront.Controls.Add(this.label5);
            this.GrFront.Controls.Add(this.button1);
            this.GrFront.Controls.Add(this.txtPincode);
            this.GrFront.Controls.Add(this.txtCity);
            this.GrFront.Controls.Add(this.txtCompanyName);
            this.GrFront.Controls.Add(this.label4);
            this.GrFront.Controls.Add(this.txtProductKey);
            this.GrFront.Controls.Add(this.BtnGetDetails);
            this.GrFront.Controls.Add(this.label3);
            this.GrFront.Controls.Add(this.label1);
            this.GrFront.Controls.Add(this.label2);
            this.GrFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrFront.Location = new System.Drawing.Point(11, 1);
            this.GrFront.Name = "GrFront";
            this.GrFront.Size = new System.Drawing.Size(513, 288);
            this.GrFront.TabIndex = 4;
            this.GrFront.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(116, 254);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 28);
            this.button1.TabIndex = 9;
            this.button1.Text = "Register";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtPincode
            // 
            this.txtPincode.Enabled = false;
            this.txtPincode.Location = new System.Drawing.Point(116, 151);
            this.txtPincode.Name = "txtPincode";
            this.txtPincode.Size = new System.Drawing.Size(221, 26);
            this.txtPincode.TabIndex = 8;
            // 
            // txtCity
            // 
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(116, 111);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(221, 26);
            this.txtCity.TabIndex = 7;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Enabled = false;
            this.txtCompanyName.Location = new System.Drawing.Point(116, 72);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(390, 26);
            this.txtCompanyName.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 5;
            this.label4.Text = "Pin Code";
            // 
            // txtProductKey
            // 
            this.txtProductKey.Location = new System.Drawing.Point(116, 35);
            this.txtProductKey.Name = "txtProductKey";
            this.txtProductKey.Size = new System.Drawing.Size(221, 26);
            this.txtProductKey.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 18);
            this.label5.TabIndex = 10;
            this.label5.Text = "License Key";
            // 
            // txtLicenseKey
            // 
            this.txtLicenseKey.Enabled = false;
            this.txtLicenseKey.Location = new System.Drawing.Point(116, 194);
            this.txtLicenseKey.Multiline = true;
            this.txtLicenseKey.Name = "txtLicenseKey";
            this.txtLicenseKey.Size = new System.Drawing.Size(390, 54);
            this.txtLicenseKey.TabIndex = 11;
            // 
            // FrmConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 301);
            this.Controls.Add(this.GrFront);
            this.MaximizeBox = false;
            this.Name = "FrmConfiguration";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.FrmConfiguration_Load);
            this.GrFront.ResumeLayout(false);
            this.GrFront.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnGetDetails;
        private System.Windows.Forms.GroupBox GrFront;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPincode;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtProductKey;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLicenseKey;
    }
}