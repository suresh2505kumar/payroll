﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace MyEasyPay
{
    public partial class FrmEmpAsset : Form
    {

        public FrmEmpAsset()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();
        int EmpId = 0;
        private DataRow doc1;
        DataTable Docno = new DataTable();
        int Loadid = 0;
        int Uid = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            
        }
       private void FrmEmpAsset_Load(object sender, EventArgs e)
        {
            DataGridPunch.RowHeadersVisible = false;
            qur.Connection = conn;
            Uid = 0;
            issdt.Value = DateTime.Now;
            recdt.Value = DateTime.Now;
            //LoadPaymonth();
            //LoadComponents();
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }
        protected void LoadPaymonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtmon = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadComponents()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getcomponenttype", conn);
                DataTable dtCom = ds.Tables[0];
                cmbcom.DataSource = null;
                cmbcom.DisplayMember = "componentname";
                cmbcom.ValueMember = "uid";
                cmbcom.DataSource = dtCom;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "")
            {


                if (DataGridPunch.CurrentCell.ColumnIndex == 4)

                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[4];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }

            }
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.RowCount > 1)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "0")
                    {
                        if (DataGridPunch.CurrentCell.ColumnIndex == 4)
                        {
                            conn.Close();
                            conn.Open();
                                           
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@UID",DataGridPunch.CurrentRow.Cells[0].Value.ToString()),
                                new SqlParameter("@AMOUNT",DataGridPunch.CurrentRow.Cells[4].Value.ToString())
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_EARNCOMUPDATE", sqlParameters, conn);
                            conn.Close();
                        }
                    }
                }
            }
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private DataTable Ctype()
        {
            DataTable tab = new DataTable();
            if (Loadid == 0)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    string qur = "select sname,lvid from leave Order by LvId";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    apt.Fill(tab);
                }
            }
            else
            {
                string qur = "select sname,lvid from leave Order by LvId";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                apt.Fill(tab);
                tab = tab.Select("sname in ('Ir','Ab','Wo')", null).CopyToDataTable();
            }
            return tab;


        }
        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }
        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {


            this.DataGridPunch.CurrentRow.Cells[7].Value = this.cbxRow5.Text;

        }

        private void HFIT_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellClick(sender, e);
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellValueChanged(sender, e);
        }

        private void HFIT_EditingControlShowing_2(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
           
        }

        private void CmbLvTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
            dialog.ShowDialog();
            lblFileName.Text = dialog.FileName;
            lblFileName.Visible = true;
            string extensionpath = Path.GetExtension(dialog.FileName);
            switch (extensionpath)
            {
                case ".xls":
                    connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx":
                    connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    return;
            }

            ImportEarndeduction(connstring, lblFileName.Text);
            updateAmtdetails();

            MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            GetEDuctEntry();
            LoadEarnDuctGrid();
        }
       
        private void updateAmtdetails()
        {
            qur.Connection = conn;
            conn.Close();
            conn.Open();
            qur.CommandText = "exec sp_uploadearndeduction  '" + CmbMonth.SelectedValue + "','" + cmbcom.SelectedValue + "','" + txtctype.Text + "'";
            qur.ExecuteNonQuery();
           

        }
        protected void ImportEarndeduction(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();

                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    new DataColumn("EmpCode", typeof(float));
                    new DataColumn("Amount", typeof(float));
                   

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        string query = "truncate table  importearndeduction ";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();

                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.importearndeduction";
                           
                            sqlbulkcopy.ColumnMappings.Add("EmpCode", "empcode");
                            sqlbulkcopy.ColumnMappings.Add("Amount", "amount");
                          
                            conn.Open();
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtempname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

            try
            {
                if ( txtempcode.Text != string.Empty && txtempname.Text != string.Empty && txtqty.Text != string.Empty && txtassetname.Text != string.Empty)
                {

                    string ss;
                    DateTime fromdate = DateTime.Parse(Convert.ToDateTime(issdt.Value).ToShortDateString());
                    DateTime todate = DateTime.Parse(Convert.ToDateTime(recdt.Value).ToShortDateString());

             
                    int pp = issdt.Value.Day;
                    int yy= recdt.Value.Day;
                    if (ChckShift.Checked==false)
                    {
                        ss = null;
                    }
                    else
                    {

                        ss = recdt.Text;
                    }
                    SqlParameter[] parameters = {
                        new SqlParameter("@Uid",Uid),
                        new SqlParameter("@Empid",txtempname.Tag),
                        new SqlParameter("@assetname", txtassetname.Text),
                        new SqlParameter("@qty", txtqty.Text),
                        new SqlParameter("@issuedate", issdt.Text),
                        new SqlParameter("@receiptdate",ss),
                        new SqlParameter("@remarks", txtremarks.Text),
                        new SqlParameter("@branch", GeneralParameters.Branch)
                      
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Save_empassetM", parameters, conn);
                    MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtempcode.Text = string.Empty;
                    txtempname.Text = string.Empty;
                    txtqty.Text = string.Empty;
                    txtassetname.Text = string.Empty;
                    txtremarks.Text = string.Empty;
                    ChckShift.Visible = true;
                    label8.Visible = false;
                    recdt.Visible = false;
                    issdt.Value = DateTime.Now;
                    recdt.Value = DateTime.Now;
                    Uid = 0;
                    GetEDuctEntry();
                   LoadEarnDuctGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void GetEDuctEntry()
        {
            try
            {
                SqlParameter[] para = {  new SqlParameter("@branch",GeneralParameters.Branch) };
                dtmon = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_Get_empassetM", para);

                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void GetEDuctEntry1()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch), new SqlParameter("@EMPID", txtempname.Tag) };
                dtmon = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_Get_empassetMEMP", para);

                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadEarnDuctGrid()
        {
            try
            {
                DataGridPunch.DataSource = null;
                DataGridPunch.AutoGenerateColumns = false;
                DataGridPunch.ColumnCount = 9;
                this.DataGridPunch.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.DataGridPunch.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                DataGridPunch.Columns[0].Name = "Uid";
                DataGridPunch.Columns[0].HeaderText = "Uid";
                DataGridPunch.Columns[0].DataPropertyName = "Uid";
                DataGridPunch.Columns[0].Visible = false;

                DataGridPunch.Columns[1].Name = "EmpId";
                DataGridPunch.Columns[1].HeaderText = "EmpId";
                DataGridPunch.Columns[1].DataPropertyName = "EmpId";
                DataGridPunch.Columns[1].Visible = false;

                DataGridPunch.Columns[2].Name = "EmpCode";
                DataGridPunch.Columns[2].HeaderText = "EmpCode";
                DataGridPunch.Columns[2].DataPropertyName = "EmpCode";
                DataGridPunch.Columns[2].Width = 100;

                DataGridPunch.Columns[3].Name = "EmpName";
                DataGridPunch.Columns[3].HeaderText = "EmpName";
                DataGridPunch.Columns[3].DataPropertyName = "EmpName";
                DataGridPunch.Columns[3].Width = 150;

                DataGridPunch.Columns[4].Name = "AssetName";
                DataGridPunch.Columns[4].HeaderText = "AssetName";
                DataGridPunch.Columns[4].DataPropertyName = "AssetName";
                DataGridPunch.Columns[4].Width = 150;
                //DataGridPunch.Columns[4].DefaultCellStyle.Format = "N2";
                //DataGridPunch.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridPunch.Columns[5].Name = "Qty";
                DataGridPunch.Columns[5].HeaderText = "Qty";
                DataGridPunch.Columns[5].DataPropertyName = "Qty";
                DataGridPunch.Columns[5].Width = 100;

                DataGridPunch.Columns[6].Name = "Issuedate";
                DataGridPunch.Columns[6].HeaderText = "Issuedate";
                DataGridPunch.Columns[6].DataPropertyName = "Issuedate";
                DataGridPunch.Columns[6].Width = 100;
                DataGridPunch.Columns[7].Name = "Receiptdate";
                DataGridPunch.Columns[7].HeaderText = "Receiptdate";
                DataGridPunch.Columns[7].DataPropertyName = "Receiptdate";
                DataGridPunch.Columns[7].Width = 100;

                DataGridPunch.Columns[8].Name = "Remarks";
                DataGridPunch.Columns[8].HeaderText = "Remarks";
                DataGridPunch.Columns[8].DataPropertyName = "Remarks";
                DataGridPunch.Columns[8].Width = 100;
                DataGridPunch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    GetEDuctEntry1();
                    LoadEarnDuctGrid();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    GetEDuctEntry1();
                    LoadEarnDuctGrid();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    GetEDuctEntry1();
                    LoadEarnDuctGrid();
                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void cmbcom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbcom.ValueMember!=null && cmbcom.Text!="")
            {
                string qur = "sp_getcomptype  "+ cmbcom.SelectedValue + "";
                SqlCommand cmd1 = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd1);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {

                    txtctype.Text =Convert.ToString(tab.Rows[0]["Ctype"].ToString());

                }

                GetEDuctEntry();
                LoadEarnDuctGrid();

            }

        }

        private void DataGridPunch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int index = DataGridPunch.SelectedCells[0].RowIndex;
                    int uid = Convert.ToInt32(DataGridPunch.Rows[index].Cells[0].Value.ToString());
                    DialogResult res = MessageBox.Show("Are you sure want to delete Asset entry for this Employee", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.Yes)
                    {
                        string Query = "delete from empassetM Where Uid =" + uid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetEDuctEntry();
                                 
                        LoadEarnDuctGrid();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridPunch_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void DataGridPunch_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
        }

        private void DataGridPunch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ChckShift_CheckedChanged(object sender, EventArgs e)
        {
            if(ChckShift.Checked==true)
            {
                ChckShift.Visible = false;
                label8.Visible = true;
                recdt.Visible = true;
            }
            else
            {
                ChckShift.Visible = true;
                label8.Visible = false;
                recdt.Visible = false;

            }
        }

        private void DataGridPunch_DoubleClick(object sender, EventArgs e)
        {
            int i = DataGridPunch.SelectedCells[0].RowIndex;
            Uid = Convert.ToInt16(DataGridPunch.Rows[i].Cells[0].Value.ToString());
            Genclass.strsql = "Select a.uid,a.empid,b.Empcode,b.EmpName,a.AssetName,a.Qty,a.Issuedate,a.Receiptdate,Remarks  from empassetM a 	 inner join Emp_Mast b on a.Empid = b.empid   	 Where  b.Dispensary='"+ GeneralParameters.Branch +"'   and a.uid=" + Uid +" order by a.Uid desc";
            Genclass.cmd = new SqlCommand(Genclass.strsql, conn);
            SqlDataAdapter aptr6 = new SqlDataAdapter(Genclass.cmd);
            DataTable tap6 = new DataTable();
            aptr6.Fill(tap6);

            txtempcode.Tag = tap6.Rows[0]["uid"].ToString();
            txtempname.Tag = tap6.Rows[0]["empid"].ToString();
            txtempcode.Text = tap6.Rows[0]["Empcode"].ToString();
            txtempname.Text = tap6.Rows[0]["EmpName"].ToString();
            txtassetname.Text = tap6.Rows[0]["AssetName"].ToString();
            txtqty.Text = tap6.Rows[0]["Qty"].ToString();
            issdt.Text = tap6.Rows[0]["Issuedate"].ToString();
            recdt.Text = tap6.Rows[0]["Receiptdate"].ToString();
            txtremarks.Text = tap6.Rows[0]["Remarks"].ToString();
        }

        private void BtnExcelExport_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;


            worksheet = workbook.Sheets["Sheet1"];


            worksheet.Name = "Employee Asset Report";

            worksheet.Cells[1, 10] = "Employee Asset Report";
            worksheet.Cells[3, 1] = "uid";
            worksheet.Cells[3, 2] = "Empid";
            worksheet.Cells[3, 3] = "EMPCODE";

            worksheet.Cells[3, 4] = "EMPNAME";
            worksheet.Cells[3, 5] = "Asset";
            worksheet.Cells[3, 6] = "Qty";
            worksheet.Cells[3, 7] = "Issuedate";

            worksheet.Cells[3, 8] = "ReceiptDate";

            worksheet.Cells[3, 9] = "Remarks";
            Genclass.sum = 0; Genclass.sum1 = 0; Genclass.sum2 = 0; Genclass.sum3 = 0; Genclass.sum4 = 0;
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            for (int i = 0; i < DataGridPunch.Rows.Count - 1; i++)
            {
                for (int j = 0; j < DataGridPunch.Columns.Count; j++)
                {

                    //if(DataGridPunch.Rows[i].Cells[j].Value.ToString()=="6")
                    worksheet.Cells[i + 4, j + 1] = DataGridPunch.Rows[i].Cells[j].Value.ToString();
                }
            }
        }
    }
}
