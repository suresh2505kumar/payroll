﻿namespace MyEasyPay
{
    partial class FrmMuster25B
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grFront = new System.Windows.Forms.GroupBox();
            this.ChckMonthly = new System.Windows.Forms.CheckBox();
            this.CmbDepartment = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnView = new System.Windows.Forms.Button();
            this.DtpToDate = new System.Windows.Forms.DateTimePicker();
            this.DtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DatagridMuster = new System.Windows.Forms.DataGridView();
            this.BtnExportExcel = new System.Windows.Forms.Button();
            this.BtnWithTime = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridMuster)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.label11);
            this.grFront.Controls.Add(this.CmbBranch);
            this.grFront.Controls.Add(this.ChckMonthly);
            this.grFront.Controls.Add(this.CmbDepartment);
            this.grFront.Controls.Add(this.label3);
            this.grFront.Controls.Add(this.BtnView);
            this.grFront.Controls.Add(this.DtpToDate);
            this.grFront.Controls.Add(this.DtpFromDate);
            this.grFront.Controls.Add(this.label2);
            this.grFront.Controls.Add(this.label1);
            this.grFront.Controls.Add(this.DatagridMuster);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(11, 4);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1089, 499);
            this.grFront.TabIndex = 0;
            this.grFront.TabStop = false;
            // 
            // ChckMonthly
            // 
            this.ChckMonthly.AutoSize = true;
            this.ChckMonthly.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckMonthly.Location = new System.Drawing.Point(995, 24);
            this.ChckMonthly.Name = "ChckMonthly";
            this.ChckMonthly.Size = new System.Drawing.Size(94, 22);
            this.ChckMonthly.TabIndex = 10;
            this.ChckMonthly.Text = "Full Month";
            this.ChckMonthly.UseVisualStyleBackColor = true;
            // 
            // CmbDepartment
            // 
            this.CmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDepartment.FormattingEnabled = true;
            this.CmbDepartment.Location = new System.Drawing.Point(273, 22);
            this.CmbDepartment.Name = "CmbDepartment";
            this.CmbDepartment.Size = new System.Drawing.Size(234, 26);
            this.CmbDepartment.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Dept";
            // 
            // BtnView
            // 
            this.BtnView.Location = new System.Drawing.Point(913, 20);
            this.BtnView.Name = "BtnView";
            this.BtnView.Size = new System.Drawing.Size(80, 30);
            this.BtnView.TabIndex = 5;
            this.BtnView.Text = "View";
            this.BtnView.UseVisualStyleBackColor = true;
            this.BtnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // DtpToDate
            // 
            this.DtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpToDate.Location = new System.Drawing.Point(788, 22);
            this.DtpToDate.Name = "DtpToDate";
            this.DtpToDate.Size = new System.Drawing.Size(119, 26);
            this.DtpToDate.TabIndex = 3;
            // 
            // DtpFromDate
            // 
            this.DtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpFromDate.Location = new System.Drawing.Point(591, 22);
            this.DtpFromDate.Name = "DtpFromDate";
            this.DtpFromDate.Size = new System.Drawing.Size(129, 26);
            this.DtpFromDate.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(728, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "To Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(513, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "From Date";
            // 
            // DatagridMuster
            // 
            this.DatagridMuster.AllowUserToAddRows = false;
            this.DatagridMuster.BackgroundColor = System.Drawing.Color.White;
            this.DatagridMuster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DatagridMuster.Location = new System.Drawing.Point(6, 53);
            this.DatagridMuster.Name = "DatagridMuster";
            this.DatagridMuster.RowHeadersVisible = false;
            this.DatagridMuster.Size = new System.Drawing.Size(1077, 440);
            this.DatagridMuster.TabIndex = 8;
            // 
            // BtnExportExcel
            // 
            this.BtnExportExcel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExportExcel.Location = new System.Drawing.Point(970, 503);
            this.BtnExportExcel.Name = "BtnExportExcel";
            this.BtnExportExcel.Size = new System.Drawing.Size(130, 30);
            this.BtnExportExcel.TabIndex = 8;
            this.BtnExportExcel.Text = "Export Excel";
            this.BtnExportExcel.UseVisualStyleBackColor = true;
            this.BtnExportExcel.Click += new System.EventHandler(this.BtnExportExcel_Click);
            // 
            // BtnWithTime
            // 
            this.BtnWithTime.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnWithTime.Location = new System.Drawing.Point(12, 505);
            this.BtnWithTime.Name = "BtnWithTime";
            this.BtnWithTime.Size = new System.Drawing.Size(292, 30);
            this.BtnWithTime.TabIndex = 11;
            this.BtnWithTime.Text = "Generate Muster With Time for Month";
            this.BtnWithTime.UseVisualStyleBackColor = true;
            this.BtnWithTime.Click += new System.EventHandler(this.BtnWithTime_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 18);
            this.label11.TabIndex = 411;
            this.label11.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Items.AddRange(new object[] {
            "Casual Leave",
            "CL AfterNoon",
            "CL ForeNoon",
            "Earned Leave",
            "Leave with Wage",
            ""});
            this.CmbBranch.Location = new System.Drawing.Point(64, 22);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(162, 26);
            this.CmbBranch.TabIndex = 410;
            // 
            // FrmMuster25B
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1112, 538);
            this.Controls.Add(this.BtnWithTime);
            this.Controls.Add(this.BtnExportExcel);
            this.Controls.Add(this.grFront);
            this.Name = "FrmMuster25B";
            this.Text = "Muster 25B";
            this.Load += new System.EventHandler(this.FrmMuster25B_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DatagridMuster)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.DateTimePicker DtpToDate;
        private System.Windows.Forms.DateTimePicker DtpFromDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbDepartment;
        private System.Windows.Forms.Button BtnExportExcel;
        private System.Windows.Forms.DataGridView DatagridMuster;
        private System.Windows.Forms.Button BtnWithTime;
        private System.Windows.Forms.CheckBox ChckMonthly;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CmbBranch;
    }
}