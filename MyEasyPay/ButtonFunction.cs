﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace MyEasyPay
{
    public class ButtonFunction
    {
        public static Control Parent;
        public static void buttonstyleform(Form Frmname)
        {
            foreach (Control ct in Frmname.Controls)
            {
                if (ct is Button)
                {
                    ct.TabStop = false;
                    (ct as Button).FlatStyle = FlatStyle.Flat;
                    (ct as Button).FlatAppearance.BorderSize = 0;
                    (ct as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }
        }
        public static void buttonstylepanel(Panel whatfldone)
        {
            Parent = whatfldone;
            foreach (Control bt in Parent.Controls)
            {
                if (bt is Button)
                {

                    bt.TabStop = false;
                    (bt as Button).FlatStyle = FlatStyle.Flat;
                    (bt as Button).FlatAppearance.BorderSize = 0;
                    (bt as Button).FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
                }
            }

        }
    }

    public class GeneralParametrs
    {
        public static int MenyKey = 0;
    }
}

