﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace MyEasyPay
{
    public partial class FrmPermissionEntry : Form
    {
        public FrmPermissionEntry()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        int EditId = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        BindingSource bs = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void FrmPermissionEntry_Load(object sender, EventArgs e)
        {
            DataTable dtBranch = new DataTable();
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            DataTable dt = new DataTable();
            dt = dataTable;
            CmbFBranch.DisplayMember = "BRANCHNAME";
            CmbFBranch.ValueMember = "Uid";
            CmbFBranch.DataSource = dt;
            StartPosition = FormStartPosition.CenterScreen;
            grBack.Visible = true;

            DtpPermissionDate.Text = DateTime.Now.Date.ToString();
            Loadgrid1();
            LoadButton(3);
            grSearch.Visible = false;

        }

        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    BtnSave.Visible = true;
                    btnBack.Visible = true;
                    BtnSave.Text = "Save";
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                    grBack.Visible = true;
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    BtnSave.Visible = true;
                    btnBack.Visible = true;
                    BtnSave.Text = "Update";
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                    grBack.Visible = false;
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    BtnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Loadgrid1()
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", CmbFBranch.SelectedValue) };
                DataTable tab = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getpermissionload", sqlParameters, conn);
                DataTable dataTable = new DataTable();
                DataRow[] dataRows;
                if (this.Text == "Permission Entry")
                {
                    dataRows = tab.Select("type='Permission'", string.Empty);
                    if (dataRows.Length > 0)
                    {
                        dataTable = tab.Select("type='Permission'", string.Empty).CopyToDataTable();
                    }
                    else
                    {
                        dataTable = tab.Clone();
                    }
                }
                else
                {
                    dataRows = tab.Select("type='On Duty'", string.Empty);
                    if (dataRows.Length > 0)
                    {
                        dataTable = tab.Select("type='On Duty'", string.Empty).CopyToDataTable();
                    }
                    else
                    {
                        dataTable = tab.Clone();
                    }
                }
                bs.DataSource = dataTable;

                DataGridEmployee.AutoGenerateColumns = false;
                DataGridEmployee.Refresh();
                DataGridEmployee.DataSource = null;
                DataGridEmployee.Rows.Clear();
                DataGridEmployee.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridEmployee.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                DataGridEmployee.ColumnCount = 10;

                DataGridEmployee.Columns[0].Name = "uid";
                DataGridEmployee.Columns[0].HeaderText = "uid";
                DataGridEmployee.Columns[0].DataPropertyName = "uid";
                DataGridEmployee.Columns[0].Visible = false;

                DataGridEmployee.Columns[1].Name = "empcode";
                DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                DataGridEmployee.Columns[1].DataPropertyName = "empcode";
                DataGridEmployee.Columns[1].Width = 100;

                DataGridEmployee.Columns[2].Name = "EMpname";
                DataGridEmployee.Columns[2].HeaderText = "Employee Name";
                DataGridEmployee.Columns[2].DataPropertyName = "EMpname";
                DataGridEmployee.Columns[2].Width = 150;

                DataGridEmployee.Columns[3].Name = "perdt";
                DataGridEmployee.Columns[3].HeaderText = "Date";
                DataGridEmployee.Columns[3].DataPropertyName = "perdt";
                DataGridEmployee.Columns[3].Width = 100;

                DataGridEmployee.Columns[4].Name = "from1";
                DataGridEmployee.Columns[4].HeaderText = "from1";
                DataGridEmployee.Columns[4].DataPropertyName = "from1";
                DataGridEmployee.Columns[4].Visible = false;

                DataGridEmployee.Columns[5].Name = "to1";
                DataGridEmployee.Columns[5].HeaderText = "to1";
                DataGridEmployee.Columns[5].DataPropertyName = "to1";
                DataGridEmployee.Columns[5].Visible = false;

                DataGridEmployee.Columns[6].Name = "reason";
                DataGridEmployee.Columns[6].HeaderText = "reason";
                DataGridEmployee.Columns[6].DataPropertyName = "reason";
                DataGridEmployee.Columns[6].Width = 120;

                DataGridEmployee.Columns[7].Name = "type";
                DataGridEmployee.Columns[7].HeaderText = "type";
                DataGridEmployee.Columns[7].DataPropertyName = "type";
                DataGridEmployee.Columns[7].Width = 100;

                DataGridEmployee.Columns[8].Name = "empid";
                DataGridEmployee.Columns[8].HeaderText = "empid";
                DataGridEmployee.Columns[8].DataPropertyName = "empid";
                DataGridEmployee.Columns[8].Visible = false;

                DataGridEmployee.Columns[9].Name = "AppNo";
                DataGridEmployee.Columns[9].HeaderText = "AppNo";
                DataGridEmployee.Columns[9].DataPropertyName = "AppNo";
                DataGridEmployee.Columns[9].Visible = false;

                DataGridEmployee.DataSource = dataTable;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void BtnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(TxtEmpcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                bsp.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtEmpName);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", TxtEmpcode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtEmpName.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    CmbPermissionType.Focus();


                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    CmbPermissionType.Focus();


                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }


        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    TxtEmpcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtEmpName.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtEmpName.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    CmbPermissionType.Focus();


                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            string type = string.Empty;
            DateTime dateTime = Convert.ToDateTime(DtpPermissionDate.Text);
            SqlParameter[] parameters = {
                    new SqlParameter("@perdt", dateTime.Date.ToString("yyyy-MM-dd")),
                    new SqlParameter("@from1", DtpFromTime.Text),
                    new SqlParameter("@to1",  dtpTotime.Text),
                    new SqlParameter("@empid", txtEmpName.Tag),
                    new SqlParameter("@REASON", TxtReason.Text),
                    new SqlParameter("@type", this)
                };
            db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETLEAVEMASTsave", parameters, conn);
            ClearControl();
            Loadgrid1();

        }

        private void DataGridEmployee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {

                string message = "Are You Sure to Delete this item ?";
                string caption = "Dilama";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                result = MessageBox.Show(message, caption, buttons);


                if (result == System.Windows.Forms.DialogResult.Yes)
                {

                    conn.Close();
                    conn.Open();
                    int Id = DataGridEmployee.SelectedCells[0].RowIndex;
                    Genclass.h = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
                    string query = "delete from permisson_Entry  Where uid=" + Genclass.h + "";
                    SqlCommand cmd = new SqlCommand(query, conn);

                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Loadgrid1();
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != string.Empty)
                {
                    bs.Filter = string.Format("EmpCode Like '%{0}%' or EmpName Like '%{1}%' or reason Like '%{2}%' or type Like '%{2}%'", textBox1.Text, textBox1.Text, textBox1.Text, textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            grFront.Visible = false;
            grBack.Visible = true;
            LoadButton(1);
            ClearControl();
        }

        private void ClearControl()
        {
            TxtEmpcode.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            TxtReason.Text = string.Empty;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            grBack.Visible = false;
            grFront.Visible = true;
            LoadButton(3);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string type = string.Empty;
                if (this.Text == "Permission Entry")
                {
                    type = "Permission";
                }
                else
                {
                    type = "On Duty";
                }

                if (BtnSave.Text == "Save")
                {
                    if (TxtEmpcode.Text != string.Empty && txtEmpName.Text != string.Empty)
                    {
                        DateTime dateTime = Convert.ToDateTime(DtpPermissionDate.Text);
                        SqlParameter[] parameters = {
                            new SqlParameter("@perdt", dateTime.Date.ToString("yyyy-MM-dd")),
                            new SqlParameter("@from1", DtpFromTime.Text),
                            new SqlParameter("@to1",  dtpTotime.Text),
                            new SqlParameter("@empid", txtEmpName.Tag),
                            new SqlParameter("@REASON", TxtReason.Text),
                            new SqlParameter("@type", type),
                            new SqlParameter("@BranchId", CmbBranch.SelectedValue),
                            new SqlParameter("@AppNo",txtAppNumber.Text)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETLEAVEMASTsave", parameters, conn);
                    }
                }
                else
                {
                    if (TxtEmpcode.Text != string.Empty && txtEmpName.Text != string.Empty)
                    {
                        DateTime dateTime = Convert.ToDateTime(DtpPermissionDate.Text);
                        SqlParameter[] parameters = {
                            new SqlParameter("@perdt", dateTime.Date.ToString("yyyy-MM-dd")),
                            new SqlParameter("@from1", DtpFromTime.Text),
                            new SqlParameter("@to1",  dtpTotime.Text),
                            new SqlParameter("@empid", txtEmpName.Tag),
                            new SqlParameter("@REASON", TxtReason.Text),
                            new SqlParameter("@type", type),
                            new SqlParameter("@BranchId", CmbBranch.SelectedValue),
                            new SqlParameter("@AppNo",txtAppNumber.Text)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_GETLEAVEMASTUpdatePer", parameters, conn);
                    }
                }
                //if (type == "On Duty")
                //{
                //    string Update = @"Update Att_reg" + Convert.ToDateTime(DtpPermissionDate.Text).Month.ToString("00") + " Set LvTag='OO' Where EmpId =" + txtEmpName.Tag + " and Dt=" + Convert.ToDateTime(DtpPermissionDate.Text).ToString("yyyy-MM-dd") + "";
                //    db.ExecuteNonQuery(CommandType.Text, Update, conn);
                //}
                MessageBox.Show("Record Updated Scuccessfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearControl();
                Loadgrid1();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Message);
                return;
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
                int Id = DataGridEmployee.SelectedCells[0].RowIndex;
                Genclass.h = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
                TxtEmpcode.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
                txtEmpName.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
                DtpPermissionDate.Text = DataGridEmployee.Rows[Id].Cells[3].Value.ToString();
                DtpFromTime.Text = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
                dtpTotime.Text = DataGridEmployee.Rows[Id].Cells[5].Value.ToString();
                TxtReason.Text = DataGridEmployee.Rows[Id].Cells[6].Value.ToString();
                txtEmpName.Tag = Convert.ToInt16(DataGridEmployee.Rows[Id].Cells[8].Value.ToString());
                CmbPermissionType.Text = DataGridEmployee.Rows[Id].Cells[7].Value.ToString();
                txtAppNumber.Text = DataGridEmployee.Rows[Id].Cells[9].Value.ToString();
                LoadButton(2);
                grSearch.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Message);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int Id = DataGridEmployee.SelectedCells[0].RowIndex;
            Genclass.h = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
            DialogResult result = MessageBox.Show("Do you want to delete this employee Permission for this date", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (result == DialogResult.Yes)
            {
                string query = "delete from permisson_Entry  Where uid=" + Genclass.h + "";
                db.ExecuteNonQuery(CommandType.Text, query, conn);
                MessageBox.Show("Deleted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Loadgrid1();
            }
            else
            {
                return;
            }
        }

        private void CmbFBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Loadgrid1();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Message);
                return;
            }
        }
    }
}

