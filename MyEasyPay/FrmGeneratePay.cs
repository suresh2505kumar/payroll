﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syncfusion.Data;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.WinForms.DataGridConverter.Events;
using Syncfusion.XlsIO;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace MyEasyPay
{
    public partial class FrmGeneratePay : Form
    {
        public FrmGeneratePay()
        {
            InitializeComponent();
            this.GridPay.Style.HeaderStyle.Font.FontStyle = FontStyle.Bold;
            this.GridPay.Style.HeaderStyle.BackColor = Color.DeepSkyBlue;
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();
        ExcelExportingOptions GridExcelExportingOptions = new ExcelExportingOptions();
        BindingSource bssalary = new BindingSource();
        int SelectId = 0;

        private void FrmGeneratePay_Load(object sender, EventArgs e)
        {
            SelectId = 1;
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);

            //string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            //var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            DataTable dataTable = dtBranch;
            if (GeneralParameters.BranchId == 0)
            {
                System.Data.DataRow dataRow = dataTable.NewRow();
                dataRow["Uid"] = 0;
                dataRow["BRANCHNAME"] = "All";
                dataTable.Rows.Add(dataRow);
            }
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            Loadmonth();
            LoadBracnh();
            DataGridPay.RowHeadersVisible = false;
            SelectId = 0;
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadBracnh()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getbranch", conn);
                DataTable dtDept = ds.Tables[0];
                CmbDepartment.DataSource = null;
                CmbDepartment.DisplayMember = "Deptname";
                CmbDepartment.ValueMember = "deptid";
                CmbDepartment.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void BtnView_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbMonth.SelectedIndex != -1 && CmbDepartment.SelectedIndex != -1 && CmbMonth.SelectedIndex != -1)
                {
                    SqlParameter[] sqlParameters = {
                          new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                          new SqlParameter("@Monid",CmbMonth.SelectedValue),
                    };
                    //DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GETSALARYREG", sqlParameters, conn);
                    DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_SALARYREGISTER", sqlParameters, conn);
                    if (data.Rows.Count > 0)
                    {
                        //SqlParameter[] Parameters = {
                        //    new SqlParameter("@Monid",CmbMonth.SelectedValue),
                        //    new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                        //};

                        //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetComponentType", Parameters, conn);
                        //System.Data.DataRow row = data.NewRow();

                        //for (int i = 0; i < data.Columns.Count; i++)
                        //{
                        //    if (data.Columns[i].ColumnName == "NET")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            Net = Convert.ToDecimal(data.Rows[j]["NET"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["NET"] = Total;
                        //        row["EMPNAME"] = "Total";
                        //    }
                        //    if (data.Columns[i].ColumnName == "GROSS")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            Net = Convert.ToDecimal(data.Rows[j]["GROSS"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["GROSS"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "Basic")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            Net = Convert.ToDecimal(data.Rows[j]["Basic"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["Basic"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "DA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["DA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["DA"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["DA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["DA"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "HRA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["HRA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["HRA"] = "0";

                        //            }


                        //            Net = Convert.ToDecimal(data.Rows[j]["HRA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["HRA"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "ESI")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            Net = Convert.ToDecimal(data.Rows[j]["ESI"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["ESI"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "EPF")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            Net = Convert.ToDecimal(data.Rows[j]["EPF"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["EPF"] = Total;
                        //    }

                        //    if (data.Columns[i].ColumnName == "ADVANCE")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {

                        //            if (data.Rows[j]["ADVANCE"].ToString() == "")
                        //            {
                        //                data.Rows[j]["ADVANCE"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["ADVANCE"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["ADVANCE"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "LOAN")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["LOAN"].ToString() == "")
                        //            {
                        //                data.Rows[j]["LOAN"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["LOAN"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["LOAN"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "TOTALDED")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {

                        //            if (data.Rows[j]["TOTALDED"].ToString() == "")
                        //            {
                        //                data.Rows[j]["TOTALDED"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["TOTALDED"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["TOTALDED"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "CPT")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {

                        //            if (data.Rows[j]["CPT"].ToString() == "")
                        //            {
                        //                data.Rows[j]["CPT"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["CPT"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["CPT"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "LWF")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["LWF"].ToString() == "")
                        //            {
                        //                data.Rows[j]["LWF"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["LWF"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["LWF"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "IT")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["IT"].ToString() == "")
                        //            {
                        //                data.Rows[j]["IT"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["IT"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["IT"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "UNIA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["UNIA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["UNIA"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["UNIA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["UNIA"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "TA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["TA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["TA"] = "0";

                        //            }
                        //            Net = Convert.ToDecimal(data.Rows[j]["TA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["TA"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "MA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {

                        //            if (data.Rows[j]["MA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["MA"] = "0";

                        //            }
                        //            Net = Convert.ToDecimal(data.Rows[j]["MA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["MA"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "EDUA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["EDUA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["EDUA"] = "0";

                        //            }

                        //            Net = Convert.ToDecimal(data.Rows[j]["EDUA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["EDUA"] = Total;
                        //    }
                        //    if (data.Columns[i].ColumnName == "CA")
                        //    {
                        //        decimal Net = 0;
                        //        decimal Total = 0;
                        //        for (int j = 0; j < data.Rows.Count; j++)
                        //        {
                        //            if (data.Rows[j]["CA"].ToString() == "")
                        //            {
                        //                data.Rows[j]["CA"] = "0";

                        //            }
                        //            Net = Convert.ToDecimal(data.Rows[j]["CA"].ToString());
                        //            Total += Net;
                        //        }
                        //        row["CA"] = Total;
                        //    }
                        //}
                        //data.Rows.Add(row);
                        DataGridPay.DataSource = null;
                        bssalary.DataSource = null;
                        bssalary.DataSource = data;
                        DataGridPay.DataSource = bssalary;
                        DataGridPay.Columns[0].Visible = false;
                        DataGridPay.Columns[1].Visible = false;
                    }
                    else
                    {
                        MessageBox.Show("Payment not generated for this " + CmbMonth.Text + " ?", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void loadexcel()
        {

            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet.Name = "Salary";
            worksheet.Cells[1, 29] = "" + GeneralParameters.Branch + " PAYROLL FOR THE MONTH OF   " + CmbMonth.Text + "  ";
            int k = 5;
            for (k = 1; k < DataGridPay.Columns.Count + 1; k++)
            {
                string str = DataGridPay.Columns[k - 1].HeaderText;
                worksheet.Cells[3, k] = str;
            }
            k = 5;
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "AC1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "AC2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A3", "AC3");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            worksheet.get_Range("A1", "AC1").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            worksheet.get_Range("A2", "AC2").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
            worksheet.Range["A1:AC1"].MergeCells = true;
            worksheet.Range["A2:AC2"].MergeCells = true;
            worksheet.Range["A3"].ColumnWidth = 6;
            worksheet.Range["B3"].ColumnWidth = 10;
            worksheet.Range["C3"].ColumnWidth = 18;
            worksheet.Range["D3"].ColumnWidth = 15;
            worksheet.Range["E3"].ColumnWidth = 10;
            worksheet.Range["F3"].ColumnWidth = 7;
            worksheet.Range["G3"].ColumnWidth = 5;
            worksheet.Range["H3"].ColumnWidth = 5;
            worksheet.Range["I3"].ColumnWidth = 7;
            worksheet.Range["J3"].ColumnWidth = 7;
            worksheet.Range["K3"].ColumnWidth = 9;
            worksheet.Range["L3"].ColumnWidth = 9;
            worksheet.Range["M3"].ColumnWidth = 9;
            worksheet.Range["N3"].ColumnWidth = 9;
            worksheet.Range["O3"].ColumnWidth = 9;
            worksheet.Range["P3"].ColumnWidth = 9;
            worksheet.Range["Q3"].ColumnWidth = 9;
            worksheet.Range["R3"].ColumnWidth = 9;
            worksheet.Range["S3"].ColumnWidth = 9;
            worksheet.Range["T3"].ColumnWidth = 9;
            worksheet.Range["U3"].ColumnWidth = 9;
            worksheet.Range["V3"].ColumnWidth = 9;
            worksheet.Range["W3"].ColumnWidth = 9;
            worksheet.Range["X3"].ColumnWidth = 9;
            worksheet.Range["Y3"].ColumnWidth = 14;
            worksheet.Range["Z3"].ColumnWidth = 9;
            worksheet.Range["AA3"].ColumnWidth = 9;
            worksheet.Range["AB3"].ColumnWidth = 9;
            worksheet.Range["AC3"].ColumnWidth = 9;
            for (int i = 0; i < DataGridPay.Rows.Count - 1; i++)
            {
                for (int j = 0; j < DataGridPay.Columns.Count; j++)
                {
                    if (DataGridPay.Rows[i].Cells[j].Value.ToString() != null)
                    {
                        worksheet.Cells[i + 5, j + 1] = DataGridPay.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            loadexcel();
        }

        private ExcelExportingOptions ExcelExportingOptions1()
        {
            GridExcelExportingOptions.Exporting += GridExcelExportingOptions_Exporting1;
            GridExcelExportingOptions.CellExporting += GridExcelExportingOptions_CellExporting1;
            return GridExcelExportingOptions;
        }

        private void GridExcelExportingOptions_Exporting1(object sender, DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Size = 9;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.BackGroundColor = Color.LightGray;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.CellStyle.FontInfo.Bold = true;
                e.Handled = true;
            }
            else if (e.CellType == ExportCellType.GroupCaptionCell)
            {
                e.CellStyle.FontInfo.Size = 9;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSeaGreen;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.Handled = true;
            }
        }

        private void GridExcelExportingOptions_CellExporting1(object sender, DataGridCellExcelExportingEventArgs e)
        {
            e.Range.CellStyle.Font.Size = 9;
            e.Range.CellStyle.Font.FontName = "Segoe UI";
            if (e.CellType == ExportCellType.RecordCell)
            {
                if (e.ColumnName == "NET")
                {
                    if (double.TryParse(e.CellValue.ToString(), out double value))
                        e.Range.Number = value;
                    e.Handled = true;
                }
            }
        }

        private void GroupTotal()
        {
            this.GridPay.TableSummaryRows.Add(new GridTableSummaryRow()
            {
                ShowSummaryInRow = false,
                SummaryColumns = new ObservableCollection<ISummaryColumn>()
                {
                    new GridSummaryColumn()
                    {
                        Name="ProductCount",
                        MappingName="NET",
                        SummaryType=SummaryType.DoubleAggregate,
                        Format="{Sum}"
                    },
                }
            });
        }

        private void BtnPayGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (CmbMonth.SelectedIndex != 1 && CmbBranch.SelectedIndex != -1)
                {
                    string query = "select * from PayrollM Where Monid = " + CmbMonth.SelectedValue + " and BranchId = " + CmbBranch.SelectedValue + "";
                    DataTable data = db.GetDataWithoutParam(CommandType.Text, query, conn);
                    if (data.Rows.Count == 0)
                    {
                        SqlParameter[] PayParameters = {
                        new SqlParameter("@MonID",CmbMonth.SelectedValue),
                        new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                        new SqlParameter("@CId",1)
                        };
                        db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_PAYCALCULATION", PayParameters, conn);
                        MessageBox.Show("Payment Generated Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        DialogResult dialogResult = MessageBox.Show("Payment already Generated do yo want to reverse the payment for this month of " + CmbMonth.Text + " !", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dialogResult == DialogResult.Yes)
                        {
                            SqlParameter[] PayParameters = {
                                new SqlParameter("@MonID",CmbMonth.SelectedValue),
                                new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                                new SqlParameter("@CId",1)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_PAYCALCULATION", PayParameters, conn);
                            MessageBox.Show("Payment Re Generated Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {   

                        }
                    }
                }
                else
                {
                    MessageBox.Show("Select Branch and Month", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridPay_Click(object sender, EventArgs e)
        {

        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CmbDepartment.ValueMember != "" && CmbDepartment.DisplayMember != "")
                {
                    if (SelectId == 0)
                    {
                        SelectId = 1;
                        if (CmbBranch.Text != "All")
                        {
                            string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                            DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                            System.Data.DataRow dataRow = dataTable.NewRow();
                            dataRow[0] = "0";
                            dataRow[1] = "All";
                            dataTable.Rows.Add(dataRow);
                            CmbDepartment.DataSource = null;
                            CmbDepartment.DisplayMember = "DeptName";
                            CmbDepartment.ValueMember = "DeptId";
                            CmbDepartment.DataSource = dataTable;
                        }
                        SelectId = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (CmbDepartment.Text == "All")
                    {
                        bssalary.RemoveFilter();
                        bssalary.ResetBindings(true);
                    }
                    else
                    {
                        bssalary.Filter = string.Format("Deptid=" + CmbDepartment.SelectedValue + "");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
