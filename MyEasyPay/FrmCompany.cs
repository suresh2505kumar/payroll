﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmCompany : Form
    {
        public FrmCompany()
        {
            InitializeComponent();
        }
        int CID = 0;
        SQLDBHelper db = new SQLDBHelper();
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtCompanyName.Text != string.Empty || txtShortName.Text != string.Empty || txtCity.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@CompanyName",txtCompanyName.Text),
                        new SqlParameter("@ShortName",txtShortName.Text),
                        new SqlParameter("@CAddress",txtAddress.Text),
                        new SqlParameter("@City",txtCity.Text),
                        new SqlParameter("@Phone",txtPhone.Text),
                        new SqlParameter("@PFNumber",txtPFNumber.Text),
                        new SqlParameter("@RegNumber",txtRegNumber.Text),
                        new SqlParameter("@GSTNumber",txtGSTNumber.Text),
                        new SqlParameter("@Pincode",txtPincode.Text),
                        new SqlParameter("@Branch",GeneralParameters.Branch),
                        new SqlParameter("@CId",CID)
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Company", parameters,conn);
                    MessageBox.Show("Saved Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Company name and City Required", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCompanyName.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void GetCompany()
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@Branch", GeneralParameters.Branch) };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetCompany", sqlParameters, conn);
                if(dt.Rows.Count != 0)
                {
                    txtCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
                    txtShortName.Text = dt.Rows[0]["ShortName"].ToString();
                    txtAddress.Text = dt.Rows[0]["CAddress"].ToString();
                    txtCity.Text = dt.Rows[0]["City"].ToString();
                    txtPincode.Text = dt.Rows[0]["Pincode"].ToString();
                    txtPhone.Text = dt.Rows[0]["Phone"].ToString();
                    txtPFNumber.Text = dt.Rows[0]["PFNumber"].ToString();
                    txtRegNumber.Text = dt.Rows[0]["RegNumber"].ToString();
                    txtGSTNumber.Text = dt.Rows[0]["GSTNumber"].ToString();
                    CID = Convert.ToInt32(dt.Rows[0]["CID"].ToString());
                }
                else
                {
                    CID = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmCompany_Load(object sender, EventArgs e)
        {
            GetCompany();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
