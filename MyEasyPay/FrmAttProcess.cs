﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace MyEasyPay
{
    public partial class FrmAttProcess : Form
    {
        public FrmAttProcess()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        
        private void Button1_Click(object sender, EventArgs e)
        {
            int Process = 0;
            DateTime dateTime = Convert.ToDateTime(Frmdt.Text);
            string check = "Select *from Att_Reg" + dateTime.Month.ToString("00") + " Where Dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
            DataTable dt = db.GetDataWithoutParam(CommandType.Text, check, conn);
            if (dt.Rows.Count == 0)
            {
                Process = 1;
            }
            else
            {
                DialogResult result = MessageBox.Show("Already Attendance Processed for this date Do you Want Re process this date?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    Process = 1;
                }
                else
                {
                    Process = 0;
                }
            }
            if(Process == 1)
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@Dt",dateTime.Date),
                    new SqlParameter("@Mon",dateTime.Month.ToString("00"))
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "INOUTPosting", parameters, conn);
            }
        }

        private void FrmAttProcess_Load(object sender, EventArgs e)
        {
            Frmdt.Value = DateTime.Now;
            qur.Connection = conn;
        }

        private void Frmdt_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
