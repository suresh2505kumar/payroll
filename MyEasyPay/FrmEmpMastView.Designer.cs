﻿namespace MyEasyPay
{
    partial class FrmEmpMastView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.label55 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.cmgfntbox = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridEmployee = new System.Windows.Forms.DataGridView();
            this.panFooter = new System.Windows.Forms.Panel();
            this.btnView = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grBack = new System.Windows.Forms.GroupBox();
            this.CmbMaritalSts = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtBloodGroup = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtGrossSal = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtBank = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.CmbBranchA = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtifsc = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.cbomode = new System.Windows.Forms.ComboBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtaccno = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.cmbpaygrp = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.cmbapp = new System.Windows.Forms.ComboBox();
            this.IsAppriser = new System.Windows.Forms.CheckBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtmailid = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.picEmpPhoto = new System.Windows.Forms.PictureBox();
            this.txtAadharNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPanNo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtUANNo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtPFNo = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtESINo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtEL = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtCL = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDispensary = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cmbShiftName = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbWeekOff = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbEmpType = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtInfo = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.RadioFemale = new System.Windows.Forms.RadioButton();
            this.radioMale = new System.Windows.Forms.RadioButton();
            this.dtpDOJ = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbDesinganation = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEmpCardNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEmpcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).BeginInit();
            this.panFooter.SuspendLayout();
            this.grBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.label55);
            this.grFront.Controls.Add(this.CmbBranch);
            this.grFront.Controls.Add(this.lblName);
            this.grFront.Controls.Add(this.cmgfntbox);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridEmployee);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, 1);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(926, 491);
            this.grFront.TabIndex = 1;
            this.grFront.TabStop = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(143, 23);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(50, 18);
            this.label55.TabIndex = 441;
            this.label55.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(217, 19);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(178, 26);
            this.CmbBranch.TabIndex = 440;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(402, 23);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(83, 18);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "Department";
            // 
            // cmgfntbox
            // 
            this.cmgfntbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmgfntbox.FormattingEnabled = true;
            this.cmgfntbox.Location = new System.Drawing.Point(505, 19);
            this.cmgfntbox.Name = "cmgfntbox";
            this.cmgfntbox.Size = new System.Drawing.Size(278, 26);
            this.cmgfntbox.TabIndex = 17;
            this.cmgfntbox.SelectedIndexChanged += new System.EventHandler(this.cmgfntbox_SelectedIndexChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(5, 58);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(906, 26);
            this.txtSearch.TabIndex = 1;
            // 
            // DataGridEmployee
            // 
            this.DataGridEmployee.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridEmployee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DataGridEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmployee.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.DataGridEmployee.Location = new System.Drawing.Point(5, 86);
            this.DataGridEmployee.Name = "DataGridEmployee";
            this.DataGridEmployee.ReadOnly = true;
            this.DataGridEmployee.RowHeadersVisible = false;
            this.DataGridEmployee.Size = new System.Drawing.Size(906, 396);
            this.DataGridEmployee.TabIndex = 0;
            // 
            // panFooter
            // 
            this.panFooter.BackColor = System.Drawing.Color.White;
            this.panFooter.Controls.Add(this.btnView);
            this.panFooter.Controls.Add(this.btnBack);
            this.panFooter.Controls.Add(this.btnExit);
            this.panFooter.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panFooter.Location = new System.Drawing.Point(6, 489);
            this.panFooter.Name = "panFooter";
            this.panFooter.Size = new System.Drawing.Size(926, 40);
            this.panFooter.TabIndex = 4;
            // 
            // btnView
            // 
            this.btnView.BackColor = System.Drawing.Color.White;
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnView.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Image = global::MyEasyPay.Properties.Resources.edit;
            this.btnView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnView.Location = new System.Drawing.Point(743, 7);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(76, 28);
            this.btnView.TabIndex = 3;
            this.btnView.Text = "View";
            this.btnView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnView.UseVisualStyleBackColor = false;
            this.btnView.Click += new System.EventHandler(this.BtnView_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Image = global::MyEasyPay.Properties.Resources.eee;
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(820, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grBack
            // 
            this.grBack.Controls.Add(this.CmbMaritalSts);
            this.grBack.Controls.Add(this.label32);
            this.grBack.Controls.Add(this.txtBloodGroup);
            this.grBack.Controls.Add(this.label31);
            this.grBack.Controls.Add(this.txtGrossSal);
            this.grBack.Controls.Add(this.label30);
            this.grBack.Controls.Add(this.txtBank);
            this.grBack.Controls.Add(this.label28);
            this.grBack.Controls.Add(this.CmbBranchA);
            this.grBack.Controls.Add(this.label26);
            this.grBack.Controls.Add(this.txtifsc);
            this.grBack.Controls.Add(this.label54);
            this.grBack.Controls.Add(this.cbomode);
            this.grBack.Controls.Add(this.label53);
            this.grBack.Controls.Add(this.txtaccno);
            this.grBack.Controls.Add(this.label42);
            this.grBack.Controls.Add(this.cmbpaygrp);
            this.grBack.Controls.Add(this.label41);
            this.grBack.Controls.Add(this.cmbapp);
            this.grBack.Controls.Add(this.IsAppriser);
            this.grBack.Controls.Add(this.label29);
            this.grBack.Controls.Add(this.txtmailid);
            this.grBack.Controls.Add(this.label27);
            this.grBack.Controls.Add(this.btnClear);
            this.grBack.Controls.Add(this.btnBrowse);
            this.grBack.Controls.Add(this.picEmpPhoto);
            this.grBack.Controls.Add(this.txtAadharNo);
            this.grBack.Controls.Add(this.label25);
            this.grBack.Controls.Add(this.txtPanNo);
            this.grBack.Controls.Add(this.label24);
            this.grBack.Controls.Add(this.txtUANNo);
            this.grBack.Controls.Add(this.label23);
            this.grBack.Controls.Add(this.txtPFNo);
            this.grBack.Controls.Add(this.label22);
            this.grBack.Controls.Add(this.txtESINo);
            this.grBack.Controls.Add(this.label21);
            this.grBack.Controls.Add(this.txtEL);
            this.grBack.Controls.Add(this.label20);
            this.grBack.Controls.Add(this.txtCL);
            this.grBack.Controls.Add(this.label19);
            this.grBack.Controls.Add(this.txtDispensary);
            this.grBack.Controls.Add(this.label18);
            this.grBack.Controls.Add(this.cmbShiftName);
            this.grBack.Controls.Add(this.label17);
            this.grBack.Controls.Add(this.cmbWeekOff);
            this.grBack.Controls.Add(this.label16);
            this.grBack.Controls.Add(this.cmbEmpType);
            this.grBack.Controls.Add(this.label15);
            this.grBack.Controls.Add(this.cmbGroup);
            this.grBack.Controls.Add(this.label14);
            this.grBack.Controls.Add(this.cmbCategory);
            this.grBack.Controls.Add(this.label13);
            this.grBack.Controls.Add(this.txtPhone);
            this.grBack.Controls.Add(this.label12);
            this.grBack.Controls.Add(this.txtCity);
            this.grBack.Controls.Add(this.label11);
            this.grBack.Controls.Add(this.txtInfo);
            this.grBack.Controls.Add(this.label10);
            this.grBack.Controls.Add(this.txtAddress);
            this.grBack.Controls.Add(this.label9);
            this.grBack.Controls.Add(this.RadioFemale);
            this.grBack.Controls.Add(this.radioMale);
            this.grBack.Controls.Add(this.dtpDOJ);
            this.grBack.Controls.Add(this.label8);
            this.grBack.Controls.Add(this.dtpDOB);
            this.grBack.Controls.Add(this.label7);
            this.grBack.Controls.Add(this.txtFatherName);
            this.grBack.Controls.Add(this.label6);
            this.grBack.Controls.Add(this.cmbDesinganation);
            this.grBack.Controls.Add(this.label5);
            this.grBack.Controls.Add(this.txtEmpCardNo);
            this.grBack.Controls.Add(this.label4);
            this.grBack.Controls.Add(this.txtEmpcode);
            this.grBack.Controls.Add(this.label3);
            this.grBack.Controls.Add(this.txtEmpName);
            this.grBack.Controls.Add(this.label2);
            this.grBack.Controls.Add(this.cmbDepartment);
            this.grBack.Controls.Add(this.label1);
            this.grBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grBack.Location = new System.Drawing.Point(6, 1);
            this.grBack.Name = "grBack";
            this.grBack.Size = new System.Drawing.Size(926, 491);
            this.grBack.TabIndex = 442;
            this.grBack.TabStop = false;
            // 
            // CmbMaritalSts
            // 
            this.CmbMaritalSts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMaritalSts.FormattingEnabled = true;
            this.CmbMaritalSts.Items.AddRange(new object[] {
            "Married",
            "UnMarried"});
            this.CmbMaritalSts.Location = new System.Drawing.Point(649, 419);
            this.CmbMaritalSts.Name = "CmbMaritalSts";
            this.CmbMaritalSts.Size = new System.Drawing.Size(111, 26);
            this.CmbMaritalSts.TabIndex = 28;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(552, 423);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(97, 18);
            this.label32.TabIndex = 79;
            this.label32.Text = "Marrital status";
            // 
            // txtBloodGroup
            // 
            this.txtBloodGroup.Location = new System.Drawing.Point(474, 419);
            this.txtBloodGroup.Name = "txtBloodGroup";
            this.txtBloodGroup.Size = new System.Drawing.Size(74, 26);
            this.txtBloodGroup.TabIndex = 27;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(383, 422);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 18);
            this.label31.TabIndex = 77;
            this.label31.Text = "Blood Group";
            // 
            // txtGrossSal
            // 
            this.txtGrossSal.Location = new System.Drawing.Point(728, 193);
            this.txtGrossSal.Name = "txtGrossSal";
            this.txtGrossSal.Size = new System.Drawing.Size(180, 26);
            this.txtGrossSal.TabIndex = 35;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(680, 196);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 18);
            this.label30.TabIndex = 75;
            this.label30.Text = "Salary";
            // 
            // txtBank
            // 
            this.txtBank.Location = new System.Drawing.Point(731, 87);
            this.txtBank.Name = "txtBank";
            this.txtBank.Size = new System.Drawing.Size(177, 26);
            this.txtBank.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(687, 90);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(38, 18);
            this.label28.TabIndex = 73;
            this.label28.Text = "Bank";
            // 
            // CmbBranchA
            // 
            this.CmbBranchA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranchA.FormattingEnabled = true;
            this.CmbBranchA.Location = new System.Drawing.Point(463, 18);
            this.CmbBranchA.Name = "CmbBranchA";
            this.CmbBranchA.Size = new System.Drawing.Size(191, 26);
            this.CmbBranchA.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(409, 22);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(50, 18);
            this.label26.TabIndex = 71;
            this.label26.Text = "Branch";
            // 
            // txtifsc
            // 
            this.txtifsc.Location = new System.Drawing.Point(731, 157);
            this.txtifsc.Name = "txtifsc";
            this.txtifsc.Size = new System.Drawing.Size(180, 26);
            this.txtifsc.TabIndex = 34;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(691, 160);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 18);
            this.label54.TabIndex = 69;
            this.label54.Text = "IFSC";
            // 
            // cbomode
            // 
            this.cbomode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbomode.FormattingEnabled = true;
            this.cbomode.Items.AddRange(new object[] {
            "Bank",
            "Cash"});
            this.cbomode.Location = new System.Drawing.Point(731, 53);
            this.cbomode.Name = "cbomode";
            this.cbomode.Size = new System.Drawing.Size(177, 26);
            this.cbomode.TabIndex = 31;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(660, 57);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(65, 18);
            this.label53.TabIndex = 66;
            this.label53.Text = "Sal Mode";
            // 
            // txtaccno
            // 
            this.txtaccno.Location = new System.Drawing.Point(731, 122);
            this.txtaccno.Name = "txtaccno";
            this.txtaccno.Size = new System.Drawing.Size(177, 26);
            this.txtaccno.TabIndex = 33;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(678, 125);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(47, 18);
            this.label42.TabIndex = 65;
            this.label42.Text = "AccNo";
            // 
            // cmbpaygrp
            // 
            this.cmbpaygrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbpaygrp.FormattingEnabled = true;
            this.cmbpaygrp.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednsday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbpaygrp.Location = new System.Drawing.Point(731, 18);
            this.cmbpaygrp.Name = "cmbpaygrp";
            this.cmbpaygrp.Size = new System.Drawing.Size(177, 26);
            this.cmbpaygrp.TabIndex = 30;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(657, 22);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(68, 18);
            this.label41.TabIndex = 62;
            this.label41.Text = "PayGroup";
            // 
            // cmbapp
            // 
            this.cmbapp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbapp.FormattingEnabled = true;
            this.cmbapp.Location = new System.Drawing.Point(464, 386);
            this.cmbapp.Name = "cmbapp";
            this.cmbapp.Size = new System.Drawing.Size(183, 26);
            this.cmbapp.TabIndex = 26;
            // 
            // IsAppriser
            // 
            this.IsAppriser.AutoSize = true;
            this.IsAppriser.Location = new System.Drawing.Point(653, 390);
            this.IsAppriser.Name = "IsAppriser";
            this.IsAppriser.Size = new System.Drawing.Size(90, 22);
            this.IsAppriser.TabIndex = 59;
            this.IsAppriser.Text = "IsAppriser";
            this.IsAppriser.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(395, 389);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 18);
            this.label29.TabIndex = 58;
            this.label29.Text = "Appriser";
            // 
            // txtmailid
            // 
            this.txtmailid.Location = new System.Drawing.Point(463, 355);
            this.txtmailid.Name = "txtmailid";
            this.txtmailid.Size = new System.Drawing.Size(208, 26);
            this.txtmailid.TabIndex = 25;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(403, 358);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 18);
            this.label27.TabIndex = 56;
            this.label27.Text = "EmailId";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(846, 453);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 32);
            this.btnClear.TabIndex = 54;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(762, 453);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 32);
            this.btnBrowse.TabIndex = 53;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            // 
            // picEmpPhoto
            // 
            this.picEmpPhoto.BackColor = System.Drawing.Color.White;
            this.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picEmpPhoto.Image = global::MyEasyPay.Properties.Resources.MEMP;
            this.picEmpPhoto.Location = new System.Drawing.Point(762, 265);
            this.picEmpPhoto.Name = "picEmpPhoto";
            this.picEmpPhoto.Size = new System.Drawing.Size(159, 184);
            this.picEmpPhoto.TabIndex = 52;
            this.picEmpPhoto.TabStop = false;
            // 
            // txtAadharNo
            // 
            this.txtAadharNo.Location = new System.Drawing.Point(463, 322);
            this.txtAadharNo.Name = "txtAadharNo";
            this.txtAadharNo.Size = new System.Drawing.Size(208, 26);
            this.txtAadharNo.TabIndex = 24;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(387, 325);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 18);
            this.label25.TabIndex = 50;
            this.label25.Text = "Aadhar No";
            // 
            // txtPanNo
            // 
            this.txtPanNo.Location = new System.Drawing.Point(463, 290);
            this.txtPanNo.Name = "txtPanNo";
            this.txtPanNo.Size = new System.Drawing.Size(208, 26);
            this.txtPanNo.TabIndex = 23;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(405, 293);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 18);
            this.label24.TabIndex = 48;
            this.label24.Text = "PAN No";
            // 
            // txtUANNo
            // 
            this.txtUANNo.Location = new System.Drawing.Point(463, 257);
            this.txtUANNo.Name = "txtUANNo";
            this.txtUANNo.Size = new System.Drawing.Size(208, 26);
            this.txtUANNo.TabIndex = 22;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(403, 260);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 18);
            this.label23.TabIndex = 46;
            this.label23.Text = "UAN No";
            // 
            // txtPFNo
            // 
            this.txtPFNo.Location = new System.Drawing.Point(463, 224);
            this.txtPFNo.Name = "txtPFNo";
            this.txtPFNo.Size = new System.Drawing.Size(208, 26);
            this.txtPFNo.TabIndex = 21;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(409, 227);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 18);
            this.label22.TabIndex = 44;
            this.label22.Text = "EPF No";
            // 
            // txtESINo
            // 
            this.txtESINo.Location = new System.Drawing.Point(463, 190);
            this.txtESINo.Name = "txtESINo";
            this.txtESINo.Size = new System.Drawing.Size(208, 26);
            this.txtESINo.TabIndex = 20;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(413, 193);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 18);
            this.label21.TabIndex = 42;
            this.label21.Text = "ESI No";
            // 
            // txtEL
            // 
            this.txtEL.Location = new System.Drawing.Point(566, 158);
            this.txtEL.Name = "txtEL";
            this.txtEL.Size = new System.Drawing.Size(74, 26);
            this.txtEL.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(542, 162);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 18);
            this.label20.TabIndex = 40;
            this.label20.Text = "EL";
            // 
            // txtCL
            // 
            this.txtCL.Location = new System.Drawing.Point(463, 158);
            this.txtCL.Name = "txtCL";
            this.txtCL.Size = new System.Drawing.Size(74, 26);
            this.txtCL.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(438, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(22, 18);
            this.label19.TabIndex = 38;
            this.label19.Text = "CL";
            // 
            // txtDispensary
            // 
            this.txtDispensary.Location = new System.Drawing.Point(731, 18);
            this.txtDispensary.Name = "txtDispensary";
            this.txtDispensary.Size = new System.Drawing.Size(177, 26);
            this.txtDispensary.TabIndex = 19;
            this.txtDispensary.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(649, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 18);
            this.label18.TabIndex = 36;
            this.label18.Text = "Dispensary";
            this.label18.Visible = false;
            // 
            // cmbShiftName
            // 
            this.cmbShiftName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbShiftName.FormattingEnabled = true;
            this.cmbShiftName.Location = new System.Drawing.Point(463, 126);
            this.cmbShiftName.Name = "cmbShiftName";
            this.cmbShiftName.Size = new System.Drawing.Size(191, 26);
            this.cmbShiftName.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(424, 132);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 18);
            this.label17.TabIndex = 34;
            this.label17.Text = "Shift";
            // 
            // cmbWeekOff
            // 
            this.cmbWeekOff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWeekOff.FormattingEnabled = true;
            this.cmbWeekOff.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednsday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.cmbWeekOff.Location = new System.Drawing.Point(463, 453);
            this.cmbWeekOff.Name = "cmbWeekOff";
            this.cmbWeekOff.Size = new System.Drawing.Size(177, 26);
            this.cmbWeekOff.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(395, 457);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 18);
            this.label16.TabIndex = 32;
            this.label16.Text = "Week Off";
            // 
            // cmbEmpType
            // 
            this.cmbEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEmpType.FormattingEnabled = true;
            this.cmbEmpType.Location = new System.Drawing.Point(463, 88);
            this.cmbEmpType.Name = "cmbEmpType";
            this.cmbEmpType.Size = new System.Drawing.Size(191, 26);
            this.cmbEmpType.TabIndex = 16;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(358, 92);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 18);
            this.label15.TabIndex = 30;
            this.label15.Text = "Employee Type";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Items.AddRange(new object[] {
            "Staff",
            "Worker",
            "Executive",
            "Casuals",
            "Probation"});
            this.cmbGroup.Location = new System.Drawing.Point(728, 227);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(177, 26);
            this.cmbGroup.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(679, 231);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 18);
            this.label14.TabIndex = 28;
            this.label14.Text = "Group";
            // 
            // cmbCategory
            // 
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(463, 53);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(191, 26);
            this.cmbCategory.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(397, 57);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 18);
            this.label13.TabIndex = 26;
            this.label13.Text = "Category";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(121, 404);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(262, 26);
            this.txtPhone.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(67, 408);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "Phone";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(121, 369);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(262, 26);
            this.txtCity.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(83, 373);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 18);
            this.label11.TabIndex = 22;
            this.label11.Text = "City";
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(121, 436);
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.Size = new System.Drawing.Size(262, 46);
            this.txtInfo.TabIndex = 13;
            this.txtInfo.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(82, 436);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 18);
            this.label10.TabIndex = 20;
            this.label10.Text = "Info";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(121, 294);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(262, 68);
            this.txtAddress.TabIndex = 10;
            this.txtAddress.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(57, 294);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 18);
            this.label9.TabIndex = 18;
            this.label9.Text = "Address";
            // 
            // RadioFemale
            // 
            this.RadioFemale.AutoSize = true;
            this.RadioFemale.Location = new System.Drawing.Point(262, 263);
            this.RadioFemale.Name = "RadioFemale";
            this.RadioFemale.Size = new System.Drawing.Size(72, 22);
            this.RadioFemale.TabIndex = 9;
            this.RadioFemale.TabStop = true;
            this.RadioFemale.Text = "FeMale";
            this.RadioFemale.UseVisualStyleBackColor = true;
            // 
            // radioMale
            // 
            this.radioMale.AutoSize = true;
            this.radioMale.Location = new System.Drawing.Point(262, 225);
            this.radioMale.Name = "radioMale";
            this.radioMale.Size = new System.Drawing.Size(57, 22);
            this.radioMale.TabIndex = 8;
            this.radioMale.TabStop = true;
            this.radioMale.Text = "Male";
            this.radioMale.UseVisualStyleBackColor = true;
            // 
            // dtpDOJ
            // 
            this.dtpDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOJ.Location = new System.Drawing.Point(125, 261);
            this.dtpDOJ.Name = "dtpDOJ";
            this.dtpDOJ.Size = new System.Drawing.Size(110, 26);
            this.dtpDOJ.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 265);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "Date of Joining";
            // 
            // dtpDOB
            // 
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(125, 223);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(111, 26);
            this.dtpDOB.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 18);
            this.label7.TabIndex = 12;
            this.label7.Text = "Date of Birth";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(125, 187);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(262, 26);
            this.txtFatherName.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Father\'s Name";
            // 
            // cmbDesinganation
            // 
            this.cmbDesinganation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDesinganation.FormattingEnabled = true;
            this.cmbDesinganation.Location = new System.Drawing.Point(125, 155);
            this.cmbDesinganation.Name = "cmbDesinganation";
            this.cmbDesinganation.Size = new System.Drawing.Size(262, 26);
            this.cmbDesinganation.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Desingnation";
            // 
            // txtEmpCardNo
            // 
            this.txtEmpCardNo.Location = new System.Drawing.Point(125, 86);
            this.txtEmpCardNo.Name = "txtEmpCardNo";
            this.txtEmpCardNo.Size = new System.Drawing.Size(135, 26);
            this.txtEmpCardNo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Unicode";
            // 
            // txtEmpcode
            // 
            this.txtEmpcode.Location = new System.Drawing.Point(125, 53);
            this.txtEmpcode.Name = "txtEmpcode";
            this.txtEmpcode.Size = new System.Drawing.Size(135, 26);
            this.txtEmpcode.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Employee Code";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(125, 19);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(255, 26);
            this.txtEmpName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Employee Name";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(125, 123);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(262, 26);
            this.cmbDepartment.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Department";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.White;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Image = global::MyEasyPay.Properties.Resources.cancel;
            this.btnBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBack.Location = new System.Drawing.Point(820, 7);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(76, 28);
            this.btnBack.TabIndex = 443;
            this.btnBack.Text = "Back";
            this.btnBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // FrmEmpMastView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(952, 531);
            this.Controls.Add(this.panFooter);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grBack);
            this.Name = "FrmEmpMastView";
            this.Text = "FrmEmpMastView";
            this.Load += new System.EventHandler(this.FrmEmpMastView_Load);
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).EndInit();
            this.panFooter.ResumeLayout(false);
            this.grBack.ResumeLayout(false);
            this.grBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmpPhoto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ComboBox CmbBranch;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cmgfntbox;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.DataGridView DataGridEmployee;
        private System.Windows.Forms.Panel panFooter;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox grBack;
        private System.Windows.Forms.ComboBox CmbMaritalSts;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtBloodGroup;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtGrossSal;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtBank;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox CmbBranchA;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtifsc;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox cbomode;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtaccno;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox cmbpaygrp;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cmbapp;
        private System.Windows.Forms.CheckBox IsAppriser;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtmailid;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox picEmpPhoto;
        private System.Windows.Forms.TextBox txtAadharNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPanNo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtUANNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtPFNo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtESINo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtEL;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCL;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDispensary;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbShiftName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbWeekOff;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbEmpType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox txtInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox txtAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton RadioFemale;
        private System.Windows.Forms.RadioButton radioMale;
        private System.Windows.Forms.DateTimePicker dtpDOJ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbDesinganation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEmpCardNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEmpcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBack;
    }
}