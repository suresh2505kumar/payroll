﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmEmpMastView : Form
    {
        public FrmEmpMastView()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        BindingSource bsp = new BindingSource();
        int EmpId = 0;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();

        private void BtnView_Click(object sender, EventArgs e)
        {
            try
            {
                picEmpPhoto.Image = null;
                Clextxt();
                ClearTextBox(this, grBack);
                int Id = DataGridEmployee.SelectedCells[0].RowIndex;
                EmpId = Convert.ToInt32(DataGridEmployee.Rows[Id].Cells[0].Value.ToString());
                txtEmpName.Text = DataGridEmployee.Rows[Id].Cells[2].Value.ToString();
                txtEmpcode.Text = DataGridEmployee.Rows[Id].Cells[1].Value.ToString();
                txtEmpCardNo.Text = DataGridEmployee.Rows[Id].Cells[32].Value.ToString();
                cmbDepartment.SelectedValue = DataGridEmployee.Rows[Id].Cells[4].Value.ToString();
                cmbDesinganation.SelectedValue = DataGridEmployee.Rows[Id].Cells[5].Value.ToString();
                cmbCategory.SelectedValue = DataGridEmployee.Rows[Id].Cells[6].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[30].Value.ToString();
                cmbShiftName.SelectedValue = DataGridEmployee.Rows[Id].Cells[8].Value.ToString();
                cmbEmpType.SelectedValue = DataGridEmployee.Rows[Id].Cells[9].Value.ToString();
                txtFatherName.Text = DataGridEmployee.Rows[Id].Cells[10].Value.ToString();
                dtpDOB.Text = DataGridEmployee.Rows[Id].Cells[11].Value.ToString();
                dtpDOJ.Text = DataGridEmployee.Rows[Id].Cells[12].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[13].Value.ToString() == "M")
                {
                    radioMale.Checked = true;
                }
                else
                {
                    RadioFemale.Checked = true;
                }
                txtAddress.Text = DataGridEmployee.Rows[Id].Cells[14].Value.ToString();
                txtCity.Text = DataGridEmployee.Rows[Id].Cells[15].Value.ToString();
                txtPhone.Text = DataGridEmployee.Rows[Id].Cells[16].Value.ToString();
                txtInfo.Text = DataGridEmployee.Rows[Id].Cells[17].Value.ToString();
                cmbWeekOff.Text = DataGridEmployee.Rows[Id].Cells[19].Value.ToString();
                txtCL.Text = DataGridEmployee.Rows[Id].Cells[20].Value.ToString();
                txtEL.Text = DataGridEmployee.Rows[Id].Cells[21].Value.ToString();
                txtESINo.Text = DataGridEmployee.Rows[Id].Cells[22].Value.ToString();
                txtPFNo.Text = DataGridEmployee.Rows[Id].Cells[23].Value.ToString();
                txtUANNo.Text = DataGridEmployee.Rows[Id].Cells[24].Value.ToString();
                txtPanNo.Text = DataGridEmployee.Rows[Id].Cells[25].Value.ToString();
                txtAadharNo.Text = DataGridEmployee.Rows[Id].Cells[26].Value.ToString();
                cmbDepartment.Text = DataGridEmployee.Rows[Id].Cells[28].Value.ToString();
                cmbDesinganation.Text = DataGridEmployee.Rows[Id].Cells[29].Value.ToString();
                cmbCategory.Text = DataGridEmployee.Rows[Id].Cells[7].Value.ToString();
                cmbGroup.Text = DataGridEmployee.Rows[Id].Cells[31].Value.ToString();
                txtaccno.Text = DataGridEmployee.Rows[Id].Cells[37].Value.ToString();
                cbomode.Text = DataGridEmployee.Rows[Id].Cells[38].Value.ToString();
                txtifsc.Text = DataGridEmployee.Rows[Id].Cells[39].Value.ToString();
                CmbBranchA.Text = DataGridEmployee.Rows[Id].Cells[18].Value.ToString();
                txtBloodGroup.Text = DataGridEmployee.Rows[Id].Cells[40].Value.ToString();
                txtGrossSal.Text = DataGridEmployee.Rows[Id].Cells[42].Value.ToString();
                CmbMaritalSts.Text = DataGridEmployee.Rows[Id].Cells[41].Value.ToString();
                if (DataGridEmployee.Rows[Id].Cells[33].Value == null)
                {
                    txtmailid.Text = string.Empty;
                }
                else
                {
                    txtmailid.Text = DataGridEmployee.Rows[Id].Cells[33].Value.ToString();
                }
                if (DataGridEmployee.Rows[Id].Cells[34].Value.ToString() == "1")
                {
                    IsAppriser.Checked = true;
                }
                else
                {
                    IsAppriser.Checked = false;
                    if (DataGridEmployee.Rows[Id].Cells[33].Value != null)
                    {
                        cmbapp.SelectedValue = DataGridEmployee.Rows[Id].Cells[35].Value.ToString();
                    }
                    cmbapp.Text = DataGridEmployee.Rows[Id].Cells[36].Value.ToString();
                }

                if (DataGridEmployee.Rows[Id].Cells[34].Value.ToString() == "1")
                {
                    string qur = "select Appid, Appraiser from Appraiser WHERE APPID=" + DataGridEmployee.Rows[Id].Cells[34].Value.ToString() + "  order by Appid";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);
                    cmbapp.DataSource = null;
                    cmbapp.DataSource = tab;
                    cmbapp.DisplayMember = "Appraiser";
                    cmbapp.ValueMember = "Appid";
                    //cmbapp.SelectedIndex = -1;
                    conn.Close();
                }

                cmbShiftName.Text = DataGridEmployee.Rows[Id].Cells[3].Value.ToString();

                string qur1 = "Select * from emp_mast where empid=" + EmpId + "";
                Genclass.cmd = new SqlCommand(qur1, conn);

                SqlDataAdapter adpt1 = new SqlDataAdapter(Genclass.cmd);
                DataTable tap1 = new DataTable();
                adpt1.Fill(tap1);


                if (tap1.Rows[0]["Empphoto"].ToString() != string.Empty)
                {
                    byte[] photo_aray;
                    photo_aray = (byte[])tap1.Rows[0]["Empphoto"];
                    MemoryStream ms = new MemoryStream(photo_aray);
                    picEmpPhoto.Image = Image.FromStream(ms);
                    picEmpPhoto.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                grFront.Visible = false;
                grBack.Visible = true;
                btnBack.Visible = true;
                btnView.Visible = false;
                btnExit.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearTextBox(Form Frmname, GroupBox whatfldone)
        {
            Genclass.Parent = whatfldone;
            foreach (Control ccontrol in Frmname.Controls)
            {

                if (ccontrol is Panel)
                {

                    foreach (Control c in Genclass.Parent.Controls)
                    {
                        if (c is TextBox || c is RichTextBox)
                        {
                            c.Text = string.Empty;
                        }
                    }
                }
            }
        }

        private void Clextxt()
        {
            txtEmpCardNo.Text = string.Empty;
            txtEmpcode.Text = string.Empty;
            txtEmpName.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtFatherName.Text = string.Empty;
            txtAadharNo.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtInfo.Text = string.Empty;
            txtBloodGroup.Text = string.Empty;
            txtGrossSal.Text = string.Empty;
            txtmailid.Text = string.Empty;
            txtCL.Text = string.Empty;
            txtEL.Text = string.Empty;
            txtDispensary.Text = string.Empty;
            txtPanNo.Text = string.Empty;
            txtPFNo.Text = string.Empty;
            txtaccno.Text = string.Empty;
            txtESINo.Text = string.Empty;
            txtmailid.Text = string.Empty;
            txtUANNo.Text = string.Empty;
        }

        private void FrmEmpMastView_Load(object sender, EventArgs e)
        {
            string Branch = ConfigurationManager.AppSettings["Branch"].ToString();
            if (Branch == "All")
            {
                var BranchAll = new List<string>(ConfigurationManager.AppSettings["MultiBranch"].Split(new char[] { ';' }));
                CmbBranch.DataSource = BranchAll;
            }
            else
            {
                CmbBranch.Items.Clear();
                CmbBranch.Items.Add(Branch);
            }
            CmbBranch.SelectedIndex = 0;

            string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            CmbBranchA.DataSource = BranchA1;
            CmbBranchA.SelectedIndex = 0;
            grFront.Visible = true;
            grBack.Visible = false;
            btnBack.Visible = false;
            Loadapp();
            LoadMasters();
        }

        private void Loadapp()
        {
            string qur = "select Appid,Appraiser from Appraiser order by Appid";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            cmbapp.DataSource = null;
            cmbapp.DataSource = tab;
            cmbapp.DisplayMember = "Appraiser";
            cmbapp.ValueMember = "Appid";
            conn.Close();
        }

        private void cmgfntbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmgfntbox.SelectedValue != null)
            {
                GeneralParameters.Branch = CmbBranch.Text;
                Loadgrid2();
            }
        }

        private void Loadgrid2()
        {
            try
            {
                if (cmgfntbox.SelectedValue != null)
                {
                    SqlParameter[] sqlParameters = {
                     new SqlParameter("@Branch",GeneralParameters.Branch),
                     //new SqlParameter("@deptid", cmgfntbox.SelectedValue),
                    };
                    //DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mastAllDeptid", sqlParameters, conn);
                    DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mastAll", sqlParameters, conn);
                    DataTable dt = new DataTable();
                    if (cmgfntbox.Text == "All")
                    {
                        dt = dataTable;
                    }
                    else
                    {
                        dt = dataTable.Select("Deptid =" + cmgfntbox.SelectedValue + "").CopyToDataTable();
                    }

                    bs.DataSource = dt;
                    DataGridEmployee.DataSource = null;
                    DataGridEmployee.AutoGenerateColumns = false;
                    DataGridEmployee.ColumnCount = 43;
                    DataGridEmployee.Columns[0].Name = "EmpId";
                    DataGridEmployee.Columns[0].HeaderText = "EmpId";
                    DataGridEmployee.Columns[0].DataPropertyName = "EmpId";
                    DataGridEmployee.Columns[0].Visible = false;

                    DataGridEmployee.Columns[1].Name = "EmpCode";
                    DataGridEmployee.Columns[1].HeaderText = "EmpCode";
                    DataGridEmployee.Columns[1].DataPropertyName = "EmpCode";
                    DataGridEmployee.Columns[1].Width = 100;

                    DataGridEmployee.Columns[2].Name = "EmpName";
                    DataGridEmployee.Columns[2].HeaderText = "Employe Name";
                    DataGridEmployee.Columns[2].DataPropertyName = "EmpName";
                    DataGridEmployee.Columns[2].Width = 200;

                    DataGridEmployee.Columns[3].Name = "ShiftName";
                    DataGridEmployee.Columns[3].HeaderText = "ShiftName";
                    DataGridEmployee.Columns[3].DataPropertyName = "ShiftName";
                    DataGridEmployee.Columns[3].Visible = false;

                    DataGridEmployee.Columns[4].Name = "DeptId";
                    DataGridEmployee.Columns[4].HeaderText = "DeptId";
                    DataGridEmployee.Columns[4].DataPropertyName = "DeptId";
                    DataGridEmployee.Columns[4].Visible = false;

                    DataGridEmployee.Columns[5].Name = "DesId";
                    DataGridEmployee.Columns[5].HeaderText = "DesId";
                    DataGridEmployee.Columns[5].DataPropertyName = "DesId";
                    DataGridEmployee.Columns[5].Visible = false;

                    DataGridEmployee.Columns[6].Name = "Grid";
                    DataGridEmployee.Columns[6].HeaderText = "Grid";
                    DataGridEmployee.Columns[6].DataPropertyName = "Grid";
                    DataGridEmployee.Columns[6].Visible = false;

                    DataGridEmployee.Columns[7].Name = "GrName";
                    DataGridEmployee.Columns[7].HeaderText = "GrName";
                    DataGridEmployee.Columns[7].DataPropertyName = "GrName";
                    DataGridEmployee.Columns[7].Visible = false;

                    DataGridEmployee.Columns[8].Name = "ShiftId";
                    DataGridEmployee.Columns[8].HeaderText = "ShiftId";
                    DataGridEmployee.Columns[8].DataPropertyName = "ShiftId";
                    DataGridEmployee.Columns[8].Visible = false;

                    DataGridEmployee.Columns[9].Name = "EmpTypeId";
                    DataGridEmployee.Columns[9].HeaderText = "EmpTypeId";
                    DataGridEmployee.Columns[9].DataPropertyName = "EmpTyp";
                    DataGridEmployee.Columns[9].Visible = false;

                    DataGridEmployee.Columns[10].Name = "FName";
                    DataGridEmployee.Columns[10].HeaderText = "FatherName";
                    DataGridEmployee.Columns[10].DataPropertyName = "FName";
                    DataGridEmployee.Columns[10].Width = 160;

                    DataGridEmployee.Columns[11].Name = "DOB";
                    DataGridEmployee.Columns[11].HeaderText = "DOB";
                    DataGridEmployee.Columns[11].DataPropertyName = "DOB";
                    DataGridEmployee.Columns[11].Visible = false;

                    DataGridEmployee.Columns[12].Name = "DOJ";
                    DataGridEmployee.Columns[12].HeaderText = "DOJ";
                    DataGridEmployee.Columns[12].DataPropertyName = "DOJ";
                    DataGridEmployee.Columns[12].Visible = false;

                    DataGridEmployee.Columns[13].Name = "Sex";
                    DataGridEmployee.Columns[13].HeaderText = "Sex";
                    DataGridEmployee.Columns[13].DataPropertyName = "Sex";
                    DataGridEmployee.Columns[13].Visible = false;

                    DataGridEmployee.Columns[14].Name = "EmpAddress";
                    DataGridEmployee.Columns[14].HeaderText = "EmpAddress";
                    DataGridEmployee.Columns[14].DataPropertyName = "Street";
                    DataGridEmployee.Columns[14].Visible = false;

                    DataGridEmployee.Columns[15].Name = "City";
                    DataGridEmployee.Columns[15].HeaderText = "City";
                    DataGridEmployee.Columns[15].DataPropertyName = "City";
                    DataGridEmployee.Columns[15].Visible = false;

                    DataGridEmployee.Columns[16].Name = "Phone";
                    DataGridEmployee.Columns[16].HeaderText = "Phone";
                    DataGridEmployee.Columns[16].DataPropertyName = "Phone";
                    DataGridEmployee.Columns[16].Visible = false;

                    DataGridEmployee.Columns[17].Name = "Information";
                    DataGridEmployee.Columns[17].HeaderText = "Information";
                    DataGridEmployee.Columns[17].DataPropertyName = "Information";
                    DataGridEmployee.Columns[17].Visible = false;

                    DataGridEmployee.Columns[18].Name = "Dispensary";
                    DataGridEmployee.Columns[18].HeaderText = "Dispensary";
                    DataGridEmployee.Columns[18].DataPropertyName = "Dispensary";
                    DataGridEmployee.Columns[18].Visible = false;

                    DataGridEmployee.Columns[19].Name = "WeekOff";
                    DataGridEmployee.Columns[19].HeaderText = "WeekOff";
                    DataGridEmployee.Columns[19].DataPropertyName = "woff";
                    DataGridEmployee.Columns[19].Visible = false;

                    DataGridEmployee.Columns[20].Name = "CL";
                    DataGridEmployee.Columns[20].HeaderText = "CL";
                    DataGridEmployee.Columns[20].DataPropertyName = "CL";
                    DataGridEmployee.Columns[20].Visible = false;

                    DataGridEmployee.Columns[21].Name = "EL";
                    DataGridEmployee.Columns[21].HeaderText = "EL";
                    DataGridEmployee.Columns[21].DataPropertyName = "EL";
                    DataGridEmployee.Columns[21].Visible = false;

                    DataGridEmployee.Columns[22].Name = "ESINo";
                    DataGridEmployee.Columns[22].HeaderText = "ESINo";
                    DataGridEmployee.Columns[22].DataPropertyName = "ESINo";
                    DataGridEmployee.Columns[22].Visible = false;

                    DataGridEmployee.Columns[23].Name = "PFNo";
                    DataGridEmployee.Columns[23].HeaderText = "PFNo";
                    DataGridEmployee.Columns[23].DataPropertyName = "EPFNo";
                    DataGridEmployee.Columns[23].Visible = false;

                    DataGridEmployee.Columns[24].Name = "UANNo";
                    DataGridEmployee.Columns[24].HeaderText = "UANNo";
                    DataGridEmployee.Columns[24].DataPropertyName = "UANNo";
                    DataGridEmployee.Columns[24].Visible = false;

                    DataGridEmployee.Columns[25].Name = "PANNo";
                    DataGridEmployee.Columns[25].HeaderText = "PANNo";
                    DataGridEmployee.Columns[25].DataPropertyName = "PANNo";
                    DataGridEmployee.Columns[25].Visible = false;

                    DataGridEmployee.Columns[26].Name = "AadharNo";
                    DataGridEmployee.Columns[26].HeaderText = "AadharNo";
                    DataGridEmployee.Columns[26].DataPropertyName = "AadharNo";
                    DataGridEmployee.Columns[26].Visible = false;

                    DataGridEmployee.Columns[27].Name = "empphoto";
                    DataGridEmployee.Columns[27].HeaderText = "empphoto";
                    DataGridEmployee.Columns[27].DataPropertyName = "empphoto";
                    DataGridEmployee.Columns[27].Visible = false;

                    DataGridEmployee.Columns[28].Name = "DesName";
                    DataGridEmployee.Columns[28].HeaderText = "DesName";
                    DataGridEmployee.Columns[28].DataPropertyName = "DesName";
                    DataGridEmployee.Columns[28].Visible = false;


                    DataGridEmployee.Columns[29].Name = "DeptName";
                    DataGridEmployee.Columns[29].HeaderText = "DeptName";
                    DataGridEmployee.Columns[29].DataPropertyName = "DeptName";
                    DataGridEmployee.Columns[29].Width = 150;

                    DataGridEmployee.Columns[30].Name = "Grade";
                    DataGridEmployee.Columns[30].HeaderText = "Grade";
                    DataGridEmployee.Columns[30].DataPropertyName = "GrName";
                    DataGridEmployee.Columns[30].Width = 150;

                    DataGridEmployee.Columns[31].Name = "TName";
                    DataGridEmployee.Columns[31].HeaderText = "TName";
                    DataGridEmployee.Columns[31].DataPropertyName = "TName";
                    DataGridEmployee.Columns[31].Visible = false;

                    DataGridEmployee.Columns[32].Name = "CardNo";
                    DataGridEmployee.Columns[32].HeaderText = "Unicode";
                    DataGridEmployee.Columns[32].DataPropertyName = "CardNo";
                    DataGridEmployee.Columns[32].Width = 100;

                    DataGridEmployee.Columns[33].Name = "emialtag";
                    DataGridEmployee.Columns[33].HeaderText = "emialtag";
                    DataGridEmployee.Columns[33].DataPropertyName = "emialtag";
                    DataGridEmployee.Columns[33].Visible = false;

                    DataGridEmployee.Columns[34].Name = "isappriser";
                    DataGridEmployee.Columns[34].HeaderText = "isappriser";
                    DataGridEmployee.Columns[34].DataPropertyName = "isappriser";
                    DataGridEmployee.Columns[34].Visible = false;

                    DataGridEmployee.Columns[35].Name = "appid";
                    DataGridEmployee.Columns[35].HeaderText = "appid";
                    DataGridEmployee.Columns[35].DataPropertyName = "appid";
                    DataGridEmployee.Columns[35].Visible = false;

                    DataGridEmployee.Columns[36].Name = "kname";
                    DataGridEmployee.Columns[36].HeaderText = "kname";
                    DataGridEmployee.Columns[36].DataPropertyName = "kname";
                    DataGridEmployee.Columns[36].Visible = false;

                    DataGridEmployee.Columns[37].Name = "acno";
                    DataGridEmployee.Columns[37].HeaderText = "acno";
                    DataGridEmployee.Columns[37].DataPropertyName = "acno";
                    DataGridEmployee.Columns[37].Visible = false;

                    DataGridEmployee.Columns[38].Name = "pmode";
                    DataGridEmployee.Columns[38].HeaderText = "pmode";
                    DataGridEmployee.Columns[38].DataPropertyName = "pmode";
                    DataGridEmployee.Columns[38].Visible = false;

                    DataGridEmployee.Columns[39].Name = "ptype";
                    DataGridEmployee.Columns[39].HeaderText = "ptype";
                    DataGridEmployee.Columns[39].DataPropertyName = "ptype";
                    DataGridEmployee.Columns[39].Visible = false;

                    DataGridEmployee.Columns[40].Name = "BllodGroup";
                    DataGridEmployee.Columns[40].HeaderText = "BllodGroup";
                    DataGridEmployee.Columns[40].DataPropertyName = "BllodGroup";
                    DataGridEmployee.Columns[40].Visible = false;

                    DataGridEmployee.Columns[41].Name = "Marritalstatus";
                    DataGridEmployee.Columns[41].HeaderText = "Marrital status";
                    DataGridEmployee.Columns[41].DataPropertyName = "Marritalstatus";
                    DataGridEmployee.Columns[41].Visible = false;

                    DataGridEmployee.Columns[42].Name = "Grosssalary";
                    DataGridEmployee.Columns[42].HeaderText = "Grosssalary";
                    DataGridEmployee.Columns[42].DataPropertyName = "Grosssalary";
                    DataGridEmployee.Columns[42].Visible = false;

                    bsp.DataSource = bs;

                    DataGridEmployee.DataSource = bsp;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                DataTable dtDes = ds.Tables[1];
                DataTable DtEmpType = ds.Tables[2];
                DataTable dtGrade = ds.Tables[3];
                DataTable dtShift = ds.Tables[4];
                DataTable dtpaygrp = ds.Tables[5];
                cmbDepartment.DataSource = null;
                cmbDepartment.DisplayMember = "DeptName";
                cmbDepartment.ValueMember = "DeptId";
                cmbDepartment.DataSource = dtDept;

                DataTable dt = dtDept;


                DataRow dataRow = dt.NewRow();
                dataRow["DeptName"] = "All";
                dataRow["DeptId"] = "0";
                dt.Rows.Add(dataRow);

                cmgfntbox.DataSource = null;
                cmgfntbox.DisplayMember = "DeptName";
                cmgfntbox.ValueMember = "DeptId";
                cmgfntbox.DataSource = dt;



                cmbDesinganation.DataSource = null;
                cmbDesinganation.DisplayMember = "DesName";
                cmbDesinganation.ValueMember = "DesId";
                cmbDesinganation.DataSource = dtDes;

                cmbCategory.DataSource = null;
                cmbCategory.DisplayMember = "GrName";
                cmbCategory.ValueMember = "GrId";
                cmbCategory.DataSource = dtGrade;

                cmbEmpType.DataSource = null;
                cmbEmpType.DisplayMember = "TSName";
                cmbEmpType.ValueMember = "TId";
                cmbEmpType.DataSource = DtEmpType;

                cmbShiftName.DataSource = null;
                cmbShiftName.DisplayMember = "ShiftName";
                cmbShiftName.ValueMember = "ShiftId";
                cmbShiftName.DataSource = dtShift;

                cmbpaygrp.DataSource = null;
                cmbpaygrp.DisplayMember = "PGName";
                cmbpaygrp.ValueMember = "uid";
                cmbpaygrp.DataSource = dtpaygrp;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            grFront.Visible = true;
            grBack.Visible = false;
            btnBack.Visible = false;
            btnView.Visible = true;
            btnExit.Visible = true;
        }
    }
}
