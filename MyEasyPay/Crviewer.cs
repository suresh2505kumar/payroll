﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace MyEasyPay
{
    public partial class Crviewer : Form
    {
        private CrystalDecisions.Windows.Forms.CrystalReportViewer Cryview;
        private System.ComponentModel.Container components = null;
        ReportDocument doc = new ReportDocument();

        public Crviewer()
        {
            InitializeComponent();
        }
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        private void CreateReport()
        {
        }

        private void Cryview_Load(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            if (Genclass.address == "Strength Report" || Genclass.address == "In Report" || Genclass.address == "Irregular Punch Report" || Genclass.address == "Worked Hour Report(< 8 Hrs)" || Genclass.address == "Daily Punch Report" || Genclass.address == "Late In Report" || Genclass.address == "Permission Report" || Genclass.address == "Lunch Break Report" || Genclass.address == "Early Out Report" || Genclass.address == "Absent List" || Genclass.address == "Over Time Report" || Genclass.address == "Muster Roll Report" || Genclass.address == "Over Time Muster Roll Report" || Genclass.address == "MusterRoll(1 to 16 days)")
            {
                string qur = "select * from tcblname ";
                Genclass.cmd = new SqlCommand(qur, conn);
                SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
                DataTable tap = new DataTable();
                aptr.Fill(tap);
                for (int i = 0; i < tap.Rows.Count; i++)
                {
                    string qur1 = "select distinct dt from " + tap.Rows[i]["tbl"].ToString() + "    where cast(dt as date)='" + Genclass.prtfrmdate + "'";
                    Genclass.cmd = new SqlCommand(qur1, conn);
                    SqlDataAdapter aptr1 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap1 = new DataTable();
                    aptr1.Fill(tap1);
                    if (tap1.Rows.Count > 0)
                    {
                        Genclass.tblname = tap.Rows[i]["tbl"].ToString();
                    }
                }
            }
            if (Genclass.address == "Employee List" || Genclass.address == "New Join Employee List")
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_Emp_list", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                DataSet ds = new DataSet();
                da.Fill(ds, "EmpRpt");
                doc.Load(Application.StartupPath + "\\CryEmp.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Deleted Employee List")
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_EmpDel_list", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                DataSet ds = new DataSet();
                da.Fill(ds, "EmpRpt");
                doc.Load(Application.StartupPath + "\\CryEmpDelEsi.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Employee PF/ESI Details")
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_Empesi_list", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                DataSet ds = new DataSet();
                da.Fill(ds, "EmpRpt");
                doc.Load(Application.StartupPath + "\\CryEmpDelEsi.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Employee Bank Account Details")
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_EmpACC_list", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                DataSet ds = new DataSet();
                da.Fill(ds, "EmpRpt");
                doc.Load(Application.StartupPath + "\\CryEmpAcc.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Employee Contact Details")
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_EmpContact_list", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                DataSet ds = new DataSet();
                da.Fill(ds, "EmpRpt");
                doc.Load(Application.StartupPath + "\\CryEmpCon.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Strength Report")
            {
                SqlDataAdapter da = new SqlDataAdapter("ATTABSTRACT_SP", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@Shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@Dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                SqlDataAdapter da1 = new SqlDataAdapter("SP_Strengthrpt", conn);
                da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                da1.Fill(ds, "StrengthRpt");
                doc.Load(Application.StartupPath + "\\CryStrengthRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "In Report" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_INRPT", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "InRpt");
                doc.Load(Application.StartupPath + "\\CryInRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "In Report" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_INRPT", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "InRpt");
                doc.Load(Application.StartupPath + "\\CryInRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Absent List" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_AbsentRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "InRpt");
                doc.Load(Application.StartupPath + "\\CryAbRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Absent List" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_AbsentRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "InRpt");
                doc.Load(Application.StartupPath + "\\CryAbRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Late In Report" && Genclass.ty == 6 || Genclass.address == "Early Out Report" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_LateInRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryLERpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Late In Report" && Genclass.ty != 6 || Genclass.address == "Early Out Report" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_LateInRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryLERpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Irregular Punch Report" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_InOutRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryInOutRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Irregular Punch Report" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_InOutRptAll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryInOutRpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Over Time Report" && Genclass.ty == 6 || Genclass.address == "Lunch Break Report" && Genclass.ty == 6 || Genclass.address == "Daily Punch Report" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_OvrTimeRptAllOrg", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                if (Genclass.address == "Over Time Report" || Genclass.address == "Daily Punch Report")
                {
                    da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                }
                else if (Genclass.address == "Lunch Break Report")
                {
                    da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = "Lunch Break > 30 Minutes Report for";
                }
                DataSet ds = new DataSet();
                da.Fill(ds, "Otrpt");
                doc.Load(Application.StartupPath + "\\CryOtrpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Over Time Report" && Genclass.ty != 6 || Genclass.address == "Lunch Break Report" && Genclass.ty != 6 || Genclass.address == "Daily Punch Report" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_OvrTimeRptAllOrg", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                if (Genclass.address == "Over Time Report" || Genclass.address == "Daily Punch Report")
                {
                    da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                }
                else if (Genclass.address == "Lunch Break Report")
                {
                    da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = "Lunch Break > 30 Minutes Report for";
                }
                DataSet ds = new DataSet();
                da.Fill(ds, "Otrpt");
                doc.Load(Application.StartupPath + "\\CryOtrpt.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Worked Hour Report (< 8 Hrs)" && Genclass.ty != 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_InOutRptAllOrg", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryWorkHrs.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Worked Hour Report (< 8 Hrs)" && Genclass.ty == 6)
            {
                SqlDataAdapter da = new SqlDataAdapter("SP_InOutRptAllOrg", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                da.SelectCommand.Parameters.Add("@shiftid", SqlDbType.Int).Value = Genclass.ty;
                da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = Genclass.address;
                da.SelectCommand.Parameters.Add("@TBL", SqlDbType.NVarChar).Value = Genclass.tblname;
                DataSet ds = new DataSet();
                da.Fill(ds, "LateinRpt");
                doc.Load(Application.StartupPath + "\\CryWorkHrs.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Muster Roll Report")
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_mustroll", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Mustrollrpt");
                doc.Load(Application.StartupPath + "\\CryMu.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Over Time Muster Roll Report")
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_mustrollOT", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Mustrollrpt");
                doc.Load(Application.StartupPath + "\\CryOverTimeMust.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "MusterRoll (1 to 16 days)")
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_mustrollfirsthalf", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Mustrollrpt");
                doc.Load(Application.StartupPath + "\\CryMustFirsthalf.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "MusterRoll (17 to 31 days)")
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_mustrollsec", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Mustrollrpt");
                doc.Load(Application.StartupPath + "\\CryMustSechalf.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }
            else if (Genclass.address == "Muster Roll Shift Report")
            {
                SqlDataAdapter da = new SqlDataAdapter("sp_mustrollShift", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.Add("@dt", SqlDbType.DateTime).Value = Genclass.prtfrmdate;
                DataSet ds = new DataSet();
                da.Fill(ds, "Mustrollrpt");
                doc.Load(Application.StartupPath + "\\CryMustShift.rpt");
                doc.SetDataSource(ds);
                Cryview.ReportSource = doc;
            }

           


            }

        private void Btnexit_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
