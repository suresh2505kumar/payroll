﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syncfusion.Data;
using System.Data.OleDb;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.WinForms.DataGridConverter.Events;
using Syncfusion.XlsIO;
using System.Security.Cryptography;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;
using System.Collections.Generic;

namespace MyEasyPay
{
    public partial class FrmEmpMuster : Form
    {
        public FrmEmpMuster()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        ExcelExportingOptions GridExcelExportingOptions = new ExcelExportingOptions();
        int SelectId;

        private void FrmEmpMuster_Load(object sender, EventArgs e)
        {
            SelectId = 1;
            lblFileName.Visible = false;
            DataTable dtBranch = new DataTable();
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;
            CmbBranch.SelectedIndex = 0;
            qur.Connection = conn;
            LoadMasters();
            Loadmonth();
            DataGridEmpPay.RowHeadersVisible = false;
            DataGridEmpPay.ReadOnly = false;
            Frmdt.Value = DateTime.Now;
            SelectId = 0;
        }

        protected void LoadMasters()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dtDept = ds.Tables[0];
                //DataTable dtDept = new DataTable();
                //if (GeneralParameters.Branch == "All")
                //{
                //    dtDept = dataTable;
                //}
                //else
                //{
                //    dtDept = dataTable.Select("DeptName='" + GeneralParameters.Branch + "'", null).CopyToDataTable();
                //}
                CmbDepartment.DataSource = null;
                CmbDepartment.DisplayMember = "DeptName";
                CmbDepartment.ValueMember = "DeptId";
                CmbDepartment.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                cmbmonth.DataSource = null;
                cmbmonth.DisplayMember = "PMonth";
                cmbmonth.ValueMember = "monid";
                cmbmonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void LoadPayDetails()
        {
            try
            {
                string date1 = "01-" + cmbmonth.Text;
                DateTime date = Convert.ToDateTime(date1);
                GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";

                SqlParameter[] sqlParameters = { 
                    new SqlParameter("@Dt", date.ToString("yyyy-MM-dd")), 
                    new SqlParameter("@TBL", GeneralParameters.TablName),
                    new SqlParameter("@BranchId", CmbBranch.SelectedValue)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                
                //SqlParameter[] sqlParameters = { new SqlParameter("@Monid", cmbmonth.SelectedValue),new SqlParameter("@BRANCHID",CmbBranch.SelectedValue) };
                //db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_MusterGen", sqlParameters, conn);

                loadaftersave();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void loadaftersave()
        {
            if (SelectId == 0)
            {
        
                if (cmbmonth.SelectedValue != null && CmbDepartment.Text == "All")
                {
                    string date1 = "01-" + cmbmonth.Text;
                    DateTime date = Convert.ToDateTime(date1);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";

                    SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", date.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL", GeneralParameters.TablName),
                    new SqlParameter("@BranchId", CmbBranch.SelectedValue)
                    };
                    DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                    DataTable tab1 = data.Tables[1];

                    DataGridEmpPay.AutoGenerateColumns = false;
                    DataGridEmpPay.Refresh();
                    DataGridEmpPay.DataSource = null;
                    DataGridEmpPay.Rows.Clear();
                    DataGridEmpPay.ColumnCount = 10;

                    DataGridEmpPay.Columns[0].Name = "Empcode";
                    DataGridEmpPay.Columns[0].HeaderText = "Empcode";
                    DataGridEmpPay.Columns[0].DataPropertyName = "EMPCODE";
                    DataGridEmpPay.Columns[0].Width = 80;

                    DataGridEmpPay.Columns[1].Name = "Empname";
                    DataGridEmpPay.Columns[1].HeaderText = "Employee Name";
                    DataGridEmpPay.Columns[1].DataPropertyName = "EmpName";
                    DataGridEmpPay.Columns[1].Width = 200;

                    DataGridEmpPay.Columns[2].Name = "Pre";
                    DataGridEmpPay.Columns[2].HeaderText = "Pre";
                    DataGridEmpPay.Columns[2].DataPropertyName = "PDAYS";
                    DataGridEmpPay.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridEmpPay.Columns[2].Width = 50;

                    DataGridEmpPay.Columns[3].Name = "LOP";
                    DataGridEmpPay.Columns[3].HeaderText = "LOP";
                    DataGridEmpPay.Columns[3].DataPropertyName = "LOP";
                    DataGridEmpPay.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridEmpPay.Columns[3].Width = 60;

                    DataGridEmpPay.Columns[4].Name = "CL";
                    DataGridEmpPay.Columns[4].HeaderText = "CL";
                    DataGridEmpPay.Columns[4].DataPropertyName = "CL";
                    DataGridEmpPay.Columns[4].Width = 60;
                    DataGridEmpPay.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[5].Name = "EL";
                    DataGridEmpPay.Columns[5].HeaderText = "EL";
                    DataGridEmpPay.Columns[5].DataPropertyName = "EL";
                    DataGridEmpPay.Columns[5].Width = 60;
                    DataGridEmpPay.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[6].Name = "OT";
                    DataGridEmpPay.Columns[6].HeaderText = "OT";
                    DataGridEmpPay.Columns[6].DataPropertyName = "OT";
                    DataGridEmpPay.Columns[6].Width = 60;
                    DataGridEmpPay.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


                    DataGridEmpPay.Columns[7].Name = "WO";
                    DataGridEmpPay.Columns[7].HeaderText = "WO";
                    DataGridEmpPay.Columns[7].DataPropertyName = "WO";
                    DataGridEmpPay.Columns[7].Width = 60;
                    DataGridEmpPay.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[8].Name = "NFH";
                    DataGridEmpPay.Columns[8].HeaderText = "NFH";
                    DataGridEmpPay.Columns[8].DataPropertyName = "NFH";
                    DataGridEmpPay.Columns[8].Width = 60;
                    DataGridEmpPay.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[9].Name = "TOTAL";
                    DataGridEmpPay.Columns[9].HeaderText = "TOTAL";
                    DataGridEmpPay.Columns[9].DataPropertyName = "TOTAL";
                    DataGridEmpPay.Columns[9].Width = 60;
                    DataGridEmpPay.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    bs.DataSource = tab1;
                    DataGridEmpPay.DataSource = bs;
                }
                else
                {
                    string date1 = "01-" + cmbmonth.Text;
                    DateTime date = Convert.ToDateTime(date1);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";

                    SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", date.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL", GeneralParameters.TablName),
                    new SqlParameter("@BranchId", CmbBranch.SelectedValue)
                    };
                    DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                    DataTable tab1 = data.Tables[1];
                    DataGridEmpPay.AutoGenerateColumns = false;
                    DataGridEmpPay.Refresh();
                    DataGridEmpPay.DataSource = null;
                    DataGridEmpPay.Rows.Clear();
                    DataGridEmpPay.ColumnCount = 10;

                    DataGridEmpPay.Columns[0].Name = "Empcode";
                    DataGridEmpPay.Columns[0].HeaderText = "Empcode";
                    DataGridEmpPay.Columns[0].DataPropertyName = "EMPCODE";
                    DataGridEmpPay.Columns[0].Width = 80;

                    DataGridEmpPay.Columns[1].Name = "Empname";
                    DataGridEmpPay.Columns[1].HeaderText = "Employee Name";
                    DataGridEmpPay.Columns[1].DataPropertyName = "EmpName";
                    DataGridEmpPay.Columns[1].Width = 200;

                    DataGridEmpPay.Columns[2].Name = "Pre";
                    DataGridEmpPay.Columns[2].HeaderText = "Pre";
                    DataGridEmpPay.Columns[2].DataPropertyName = "PDAYS";
                    DataGridEmpPay.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridEmpPay.Columns[2].Width = 50;

                    DataGridEmpPay.Columns[3].Name = "LOP";
                    DataGridEmpPay.Columns[3].HeaderText = "LOP";
                    DataGridEmpPay.Columns[3].DataPropertyName = "LOP";
                    DataGridEmpPay.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    DataGridEmpPay.Columns[3].Width = 60;

                    DataGridEmpPay.Columns[4].Name = "CL";
                    DataGridEmpPay.Columns[4].HeaderText = "CL";
                    DataGridEmpPay.Columns[4].DataPropertyName = "CL";
                    DataGridEmpPay.Columns[4].Width = 60;
                    DataGridEmpPay.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[5].Name = "EL";
                    DataGridEmpPay.Columns[5].HeaderText = "EL";
                    DataGridEmpPay.Columns[5].DataPropertyName = "EL";
                    DataGridEmpPay.Columns[5].Width = 60;
                    DataGridEmpPay.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[6].Name = "OT";
                    DataGridEmpPay.Columns[6].HeaderText = "OT";
                    DataGridEmpPay.Columns[6].DataPropertyName = "OT";
                    DataGridEmpPay.Columns[6].Width = 60;
                    DataGridEmpPay.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


                    DataGridEmpPay.Columns[7].Name = "WO";
                    DataGridEmpPay.Columns[7].HeaderText = "WO";
                    DataGridEmpPay.Columns[7].DataPropertyName = "WO";
                    DataGridEmpPay.Columns[7].Width = 60;
                    DataGridEmpPay.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[8].Name = "NFH";
                    DataGridEmpPay.Columns[8].HeaderText = "NFH";
                    DataGridEmpPay.Columns[8].DataPropertyName = "NFH";
                    DataGridEmpPay.Columns[8].Width = 60;
                    DataGridEmpPay.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                    DataGridEmpPay.Columns[9].Name = "TOTAL";
                    DataGridEmpPay.Columns[9].HeaderText = "TOTAL";
                    DataGridEmpPay.Columns[9].DataPropertyName = "TOTAL";
                    DataGridEmpPay.Columns[9].Width = 60;
                    DataGridEmpPay.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    bs.DataSource = tab1;
                    DataGridEmpPay.DataSource = bs;
                }
            }
            else
            {
                string date1 = "01-" + cmbmonth.Text;
                DateTime date = Convert.ToDateTime(date1);
                GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";

                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", date.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL", GeneralParameters.TablName),
                    new SqlParameter("@BranchId", CmbBranch.SelectedValue)
                    };
                DataSet data = db.GetDataWithParamMultiple(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                DataTable tab1 = data.Tables[1];

                DataGridEmpPay.AutoGenerateColumns = false;
                DataGridEmpPay.Refresh();
                DataGridEmpPay.DataSource = null;
                DataGridEmpPay.Rows.Clear();
                DataGridEmpPay.ColumnCount = 10;

                DataGridEmpPay.Columns[0].Name = "Empcode";
                DataGridEmpPay.Columns[0].HeaderText = "Empcode";
                DataGridEmpPay.Columns[0].DataPropertyName = "EMPCODE";
                DataGridEmpPay.Columns[0].Width = 80;

                DataGridEmpPay.Columns[1].Name = "Empname";
                DataGridEmpPay.Columns[1].HeaderText = "Employee Name";
                DataGridEmpPay.Columns[1].DataPropertyName = "EmpName";
                DataGridEmpPay.Columns[1].Width = 200;

                DataGridEmpPay.Columns[2].Name = "Pre";
                DataGridEmpPay.Columns[2].HeaderText = "Pre";
                DataGridEmpPay.Columns[2].DataPropertyName = "PDAYS";
                DataGridEmpPay.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[2].Width = 50;

                DataGridEmpPay.Columns[3].Name = "LOP";
                DataGridEmpPay.Columns[3].HeaderText = "LOP";
                DataGridEmpPay.Columns[3].DataPropertyName = "LOP";
                DataGridEmpPay.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[3].Width = 60;

                DataGridEmpPay.Columns[4].Name = "CL";
                DataGridEmpPay.Columns[4].HeaderText = "CL";
                DataGridEmpPay.Columns[4].DataPropertyName = "CL";
                DataGridEmpPay.Columns[4].Width = 60;
                DataGridEmpPay.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[5].Name = "EL";
                DataGridEmpPay.Columns[5].HeaderText = "EL";
                DataGridEmpPay.Columns[5].DataPropertyName = "EL";
                DataGridEmpPay.Columns[5].Width = 60;
                DataGridEmpPay.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[6].Name = "OT";
                DataGridEmpPay.Columns[6].HeaderText = "OT";
                DataGridEmpPay.Columns[6].DataPropertyName = "OT";
                DataGridEmpPay.Columns[6].Width = 60;
                DataGridEmpPay.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


                DataGridEmpPay.Columns[7].Name = "WO";
                DataGridEmpPay.Columns[7].HeaderText = "WO";
                DataGridEmpPay.Columns[7].DataPropertyName = "WO";
                DataGridEmpPay.Columns[7].Width = 60;
                DataGridEmpPay.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[8].Name = "NFH";
                DataGridEmpPay.Columns[8].HeaderText = "NFH";
                DataGridEmpPay.Columns[8].DataPropertyName = "NFH";
                DataGridEmpPay.Columns[8].Width = 60;
                DataGridEmpPay.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[9].Name = "TOTAL";
                DataGridEmpPay.Columns[9].HeaderText = "TOTAL";
                DataGridEmpPay.Columns[9].DataPropertyName = "TOTAL";
                DataGridEmpPay.Columns[9].Width = 60;
                DataGridEmpPay.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                bs.DataSource = tab1;
                DataGridEmpPay.DataSource = bs;
            }
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DataGridEmpPay_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridEmpPay.RowCount > 1)
            {
                if (DataGridEmpPay.CurrentRow.Cells[1].Value != null)
                {
                    if (DataGridEmpPay.CurrentRow.Cells[1].Value.ToString() != "0")
                    {
                        if (DataGridEmpPay.CurrentCell.ColumnIndex == 4 || DataGridEmpPay.CurrentCell.ColumnIndex == 2 || DataGridEmpPay.CurrentCell.ColumnIndex == 3 || DataGridEmpPay.CurrentCell.ColumnIndex == 5 || DataGridEmpPay.CurrentCell.ColumnIndex == 6 || DataGridEmpPay.CurrentCell.ColumnIndex == 7 || DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                        {

                            int PP; int TT;
                            string Query = "select Nod,msdt from paymonth Where MonId=" + cmbmonth.SelectedValue + "";
                            DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                            PP = Convert.ToInt32(dt.Rows[0]["Nod"].ToString());
                            TT = Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[2].Value.ToString()) + Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[3].Value.ToString()) + Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[4].Value.ToString()) + Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[5].Value.ToString()) + Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[7].Value.ToString()) + Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[7].Value.ToString());
                            if (PP < TT)
                            {
                                MessageBox.Show("Total days are out of the month days");
                                return;
                            }
                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@empcode",DataGridEmpPay.CurrentRow.Cells[0].Value.ToString()),
                                new SqlParameter("@monid",cmbmonth.SelectedValue),
                                new SqlParameter("@pre",DataGridEmpPay.CurrentRow.Cells[2].Value.ToString()),
                                new SqlParameter("@lop",DataGridEmpPay.CurrentRow.Cells[3].Value.ToString()),
                                new SqlParameter("@cl",DataGridEmpPay.CurrentRow.Cells[4].Value.ToString()),
                                new SqlParameter("@el",DataGridEmpPay.CurrentRow.Cells[5].Value.ToString()),
                                new SqlParameter("@ot",DataGridEmpPay.CurrentRow.Cells[6].Value.ToString()),
                                new SqlParameter("@wo",DataGridEmpPay.CurrentRow.Cells[7].Value.ToString()),
                                new SqlParameter("@nfh",DataGridEmpPay.CurrentRow.Cells[8].Value.ToString()),
                                new SqlParameter("@deptname",CmbDepartment.Text),
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_SaveMMuster", sqlParameters, conn);
                        }
                        loadaftersave();
                    }
                }
            }
        }

        private void DataGridEmpPay_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            DataGridEmpPay_CellValueChanged(sender, e);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (CmbDepartment.ValueMember != "" && CmbDepartment.DisplayMember != "" && cmbmonth.ValueMember != "" && cmbmonth.DisplayMember != "")
            {
                LoadPayDetails();
            }
        }

        private void Cmbmonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbmonth.SelectedIndex != -1)
                {
                    string Query = "select Nod,msdt from paymonth Where MonId=" + cmbmonth.SelectedValue + "";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    if (dt.Rows.Count > 0)
                    {
                        TxtWDays.Text = dt.Rows[0]["Nod"].ToString();
                        Frmdt.Text = dt.Rows[0]["msdt"].ToString();
                    }
                    else
                    {
                        TxtWDays.Text = "0";
                    }
                }
                loadaftersave();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridEmpPay_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter || e.KeyCode == Keys.F2)
                {
                    if (DataGridEmpPay.CurrentRow.Cells[0].Value.ToString() != "")
                    {
                        if (DataGridEmpPay.CurrentCell.ColumnIndex == 4)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[4];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 5)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[5];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 6)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[6];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 7)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[7];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                        else if (DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                        {
                            DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[8];
                            DataGridEmpPay.CurrentCell = cell;
                            DataGridEmpPay.BeginEdit(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridEmpPay_CellClick_2(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DataGridEmpPay.CurrentRow.Cells[0].Value.ToString() != "")
                {
                    if (DataGridEmpPay.CurrentCell.ColumnIndex == 4)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[4];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 2)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[2];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 3)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[3];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 5)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[5];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 6)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[6];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 7)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[7];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                    else if (DataGridEmpPay.CurrentCell.ColumnIndex == 8)
                    {
                        DataGridViewCell cell = DataGridEmpPay.CurrentRow.Cells[8];
                        DataGridEmpPay.CurrentCell = cell;
                        DataGridEmpPay.BeginEdit(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime date = Convert.ToDateTime(Frmdt.Text);
            GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
            SqlParameter[] sqlParameters = {
                new SqlParameter("@Month",date.Month),
                    new SqlParameter("@Year",date.Year),
                    new SqlParameter("@Deptid",CmbDepartment.SelectedValue),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                };
            DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_POST_WEEKOFF", sqlParameters, conn);
        }
        private ExcelExportingOptions ExcelExportingOptions1()
        {
            GridExcelExportingOptions.Exporting += GridExcelExportingOptions_Exporting1;
            GridExcelExportingOptions.CellExporting += GridExcelExportingOptions_CellExporting1;
            return GridExcelExportingOptions;
        }

        private void GridExcelExportingOptions_CellExporting1(object sender, DataGridCellExcelExportingEventArgs e)
        {
            e.Range.CellStyle.Font.Size = 12;
            e.Range.CellStyle.Font.FontName = "Segoe UI";
            if (e.CellType == ExportCellType.RecordCell)
            {
                if (e.ColumnName == "NET")
                {
                    if (double.TryParse(e.CellValue.ToString(), out double value))
                        e.Range.Number = value;
                    e.Handled = true;
                }
            }
        }
        private void GridExcelExportingOptions_Exporting1(object sender, DataGridExcelExportingEventArgs e)
        {
            if (e.CellType == ExportCellType.HeaderCell)
            {
                e.CellStyle.FontInfo.Size = 12;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.BackGroundColor = Color.LightGray;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.CellStyle.FontInfo.Bold = true;
                e.Handled = true;
            }
            else if (e.CellType == ExportCellType.GroupCaptionCell)
            {
                e.CellStyle.FontInfo.Size = 12;
                e.CellStyle.FontInfo.FontName = "Segoe UI";
                e.CellStyle.FontInfo.Bold = true;
                e.CellStyle.BackGroundColor = Color.LightSeaGreen;
                e.CellStyle.ForeGroundColor = Color.Black;
                e.Handled = true;
            }
        }

        private void BtnExcelExport_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
            app.Visible = true;
            worksheet = workbook.Sheets["Sheet1"];
            worksheet.Name = "Muster Generation Report";
            worksheet.Cells[1, 10] = "Muster Generation Report";
            worksheet.Cells[3, 1] = "EMPCODE";
            worksheet.Cells[3, 2] = "EMPNAME";
            worksheet.Cells[3, 3] = "Pre";
            worksheet.Cells[3, 4] = "LOP";
            worksheet.Cells[3, 5] = "CL";
            worksheet.Cells[3, 6] = "EL";
            worksheet.Cells[3, 7] = "OT";
            worksheet.Cells[3, 8] = "WO";
            worksheet.Cells[3, 9] = "NFH";
            worksheet.Cells[3, 10] = "TOTAL";
            Genclass.sum = 0; Genclass.sum1 = 0; Genclass.sum2 = 0; Genclass.sum3 = 0; Genclass.sum4 = 0;
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            for (int i = 0; i < DataGridEmpPay.Rows.Count - 1; i++)
            {
                for (int j = 0; j < DataGridEmpPay.Columns.Count; j++)
                {
                    worksheet.Cells[i + 4, j + 1] = DataGridEmpPay.Rows[i].Cells[j].Value.ToString();
                }
            }
        }

        private void DataGridEmpPay_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
            //dialog.ShowDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                lblFileName.Text = dialog.FileName;
                lblFileName.Visible = true;
                string extensionpath = Path.GetExtension(dialog.FileName);
                switch (extensionpath)
                {
                    case ".xls":
                        connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx":
                        connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                        break;
                }

                ImportEarndeduction(connstring, lblFileName.Text);
                updateAmtdetails();

                MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void updateAmtdetails()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@monid", cmbmonth.SelectedValue),
                    new SqlParameter("@BranchId", CmbBranch.SelectedValue)
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_uploadMUSTERIMPORT", sqlParameters, conn);
                loadaftersave();
            }
            catch
            {

            }
        }
        protected void ImportEarndeduction(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();

                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    DataTable dtexcelData = new DataTable();
                    new DataColumn("SLNO", typeof(Int16));
                    new DataColumn("EMPCODE", typeof(String));
                    new DataColumn("WORKED", typeof(float));
                    new DataColumn("CL", typeof(float));
                    new DataColumn("EL", typeof(float));
                    new DataColumn("LOP", typeof(float));
                    new DataColumn("NFH", typeof(float));
                    new DataColumn("WEEKOFF", typeof(float));

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    {
                        string query = "truncate table  MUSTERIMPORT ";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();
                        conn.Open();
                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.MUSTERIMPORT";
                            sqlbulkcopy.ColumnMappings.Add("SLNO", "SLNO");
                            sqlbulkcopy.ColumnMappings.Add("EMPCODE", "EMPCODE");
                            sqlbulkcopy.ColumnMappings.Add("WORKED", "WORKED");
                            sqlbulkcopy.ColumnMappings.Add("CL", "CL");
                            sqlbulkcopy.ColumnMappings.Add("EL", "EL");
                            sqlbulkcopy.ColumnMappings.Add("LOP", "LOP");
                            sqlbulkcopy.ColumnMappings.Add("NFH", "NFH");
                            sqlbulkcopy.ColumnMappings.Add("WEEKOFF", "WEEKOFF");
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmgfntbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectId == 0)
            {
                if(CmbDepartment.Text == "All")
                {
                    bs.RemoveFilter();
                }
                else
                {
                    bs.Filter = string.Format("DeptId in ({0})", CmbDepartment.SelectedValue);
                }
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != string.Empty)
                {
                    bs.Filter = string.Format("EmpCode Like '%{0}%' or EmpName Like '%{1}%' ", textBox1.Text, textBox1.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    SelectId = 1;
                    string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    System.Data.DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = "0";
                    dataRow[1] = "All";
                    dataTable.Rows.Add(dataRow);
                    CmbDepartment.DataSource = null;
                    CmbDepartment.DisplayMember = "DeptName";
                    CmbDepartment.ValueMember = "DeptId";
                    CmbDepartment.DataSource = dataTable;
                    loadaftersave();
                    SelectId = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void linklblSample_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Muster";

                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                Microsoft.Office.Interop.Excel.Range row5 = worksheet.Rows.Cells[1, 5];
                Microsoft.Office.Interop.Excel.Range row6 = worksheet.Rows.Cells[1, 6];
                Microsoft.Office.Interop.Excel.Range row7 = worksheet.Rows.Cells[1, 7];
                Microsoft.Office.Interop.Excel.Range row8 = worksheet.Rows.Cells[1, 8];
                row1.Value = "SLNO";
                row2.Value = "EMPCODE";
                row3.Value = "WORKED";
                row4.Value = "CL";
                row5.Value = "EL";
                row6.Value = "LOP";
                row7.Value = "NFH";
                row8.Value = "WEEKOFF";
                MessageBox.Show("Completed Successfully !", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void LinkDetail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                DataGridDet.DataSource = null;
                DataGridDet.AutoGenerateColumns = true;
                string date = "01-" + cmbmonth.Text;
                DateTime dte = Convert.ToDateTime(date);
                string Query = string.Empty;
                if(CmbDepartment.Text == "All")
                {
                    Query = @"	select		E.EMPCODE,E.EMPNAME,E.DEPTNAME,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,
            	            D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,
				            A.PDAYS,A.CL,A.EL,A.LW,A.LOP,A.HD,A.WO
                        	from		MusterRoll M
	                        INNER JOIN	VW_EMP E ON E.EMPCODE =M.EMPCODE
	                        LEFT JOIN	ATT1 A ON A.EMPCODE =M.EMPCODE AND A.DT =M.DT AND A.BRANCHID =M.BRANCHID
	                        WHERE M.DT ='" + dte.ToString("yyyy-MM-dd") + "' AND M.BRANCHID =" + CmbBranch.SelectedValue + " order by E.empcode";
                }
                else
                {
                    Query = @"	select		E.EMPCODE,E.EMPNAME,E.DEPTNAME,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,
            	            D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,D26,D27,D28,D29,D30,D31,
				            A.PDAYS,A.CL,A.EL,A.LW,A.LOP,A.HD,A.WO
                        	from		MusterRoll M
	                        INNER JOIN	VW_EMP E ON E.EMPCODE =M.EMPCODE
	                        LEFT JOIN	ATT1 A ON A.EMPCODE =M.EMPCODE AND A.DT =M.DT AND A.BRANCHID =M.BRANCHID
	                        WHERE E.DeptId =" + CmbDepartment.SelectedValue + " and M.DT ='" + dte.ToString("yyyy-MM-dd") + "' AND M.BRANCHID =" + CmbBranch.SelectedValue + " order by E.empcode";
                }
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                DataGridDet.DataSource = dataTable;
                DataGridDet.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                groupBox1.Visible = false;
                GrDetail.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            GrDetail.Visible = false;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
