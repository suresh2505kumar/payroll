﻿namespace MyEasyPay
{
    partial class FrmEmpMuster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.CmbDepartment = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.DataGridEmpPay = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbmonth = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtWDays = new System.Windows.Forms.TextBox();
            this.Frmdt = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.BtnExcelExport = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.linklblSample = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkDetail = new System.Windows.Forms.LinkLabel();
            this.GrDetail = new System.Windows.Forms.GroupBox();
            this.BtnBack = new System.Windows.Forms.Button();
            this.DataGridDet = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmpPay)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.GrDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDet)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(299, 21);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(86, 19);
            this.lblName.TabIndex = 20;
            this.lblName.Text = "Department";
            // 
            // CmbDepartment
            // 
            this.CmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDepartment.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbDepartment.FormattingEnabled = true;
            this.CmbDepartment.Location = new System.Drawing.Point(388, 18);
            this.CmbDepartment.Name = "CmbDepartment";
            this.CmbDepartment.Size = new System.Drawing.Size(223, 27);
            this.CmbDepartment.TabIndex = 19;
            this.CmbDepartment.SelectedIndexChanged += new System.EventHandler(this.cmgfntbox_SelectedIndexChanged);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(778, 552);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(76, 28);
            this.btnExit.TabIndex = 22;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // DataGridEmpPay
            // 
            this.DataGridEmpPay.BackgroundColor = System.Drawing.Color.White;
            this.DataGridEmpPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmpPay.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridEmpPay.Location = new System.Drawing.Point(6, 113);
            this.DataGridEmpPay.Name = "DataGridEmpPay";
            this.DataGridEmpPay.Size = new System.Drawing.Size(848, 437);
            this.DataGridEmpPay.TabIndex = 23;
            this.DataGridEmpPay.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridEmpPay_CellClick_2);
            this.DataGridEmpPay.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridEmpPay_CellContentClick);
            this.DataGridEmpPay.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridEmpPay_CellValueChanged_1);
            this.DataGridEmpPay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridEmpPay_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(29, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 19);
            this.label7.TabIndex = 25;
            this.label7.Text = "Month ";
            // 
            // cmbmonth
            // 
            this.cmbmonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbmonth.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbmonth.FormattingEnabled = true;
            this.cmbmonth.Location = new System.Drawing.Point(87, 52);
            this.cmbmonth.Name = "cmbmonth";
            this.cmbmonth.Size = new System.Drawing.Size(130, 27);
            this.cmbmonth.TabIndex = 26;
            this.cmbmonth.SelectedIndexChanged += new System.EventHandler(this.Cmbmonth_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(223, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(179, 28);
            this.button1.TabIndex = 27;
            this.button1.Text = "Muster Generations";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(625, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "Working Days";
            // 
            // TxtWDays
            // 
            this.TxtWDays.Enabled = false;
            this.TxtWDays.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWDays.Location = new System.Drawing.Point(726, 15);
            this.TxtWDays.Name = "TxtWDays";
            this.TxtWDays.Size = new System.Drawing.Size(100, 26);
            this.TxtWDays.TabIndex = 30;
            // 
            // Frmdt
            // 
            this.Frmdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frmdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Frmdt.Location = new System.Drawing.Point(87, 235);
            this.Frmdt.Name = "Frmdt";
            this.Frmdt.Size = new System.Drawing.Size(115, 26);
            this.Frmdt.TabIndex = 208;
            this.Frmdt.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(408, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 28);
            this.button2.TabIndex = 209;
            this.button2.Text = "Week Off";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnExcelExport
            // 
            this.BtnExcelExport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcelExport.Location = new System.Drawing.Point(6, 552);
            this.BtnExcelExport.Name = "BtnExcelExport";
            this.BtnExcelExport.Size = new System.Drawing.Size(170, 29);
            this.BtnExcelExport.TabIndex = 210;
            this.BtnExcelExport.Text = "Export to Excel";
            this.BtnExcelExport.UseVisualStyleBackColor = true;
            this.BtnExcelExport.Click += new System.EventHandler(this.BtnExcelExport_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(182, 553);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 28);
            this.button3.TabIndex = 211;
            this.button3.Text = "Import Muster ";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(50, 213);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(98, 19);
            this.lblFileName.TabIndex = 212;
            this.lblFileName.Text = "Working Days";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 85);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(420, 26);
            this.textBox1.TabIndex = 213;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 242;
            this.label4.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(69, 17);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(219, 26);
            this.CmbBranch.TabIndex = 241;
            this.CmbBranch.SelectedIndexChanged += new System.EventHandler(this.CmbBranch_SelectedIndexChanged);
            // 
            // linklblSample
            // 
            this.linklblSample.AutoSize = true;
            this.linklblSample.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linklblSample.Location = new System.Drawing.Point(297, 559);
            this.linklblSample.Name = "linklblSample";
            this.linklblSample.Size = new System.Drawing.Size(118, 18);
            this.linklblSample.TabIndex = 436;
            this.linklblSample.TabStop = true;
            this.linklblSample.Text = "Download Format";
            this.linklblSample.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linklblSample_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkDetail);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.linklblSample);
            this.groupBox1.Controls.Add(this.CmbDepartment);
            this.groupBox1.Controls.Add(this.BtnExcelExport);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.lblName);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.DataGridEmpPay);
            this.groupBox1.Controls.Add(this.lblFileName);
            this.groupBox1.Controls.Add(this.Frmdt);
            this.groupBox1.Controls.Add(this.CmbBranch);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.cmbmonth);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TxtWDays);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(1, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(863, 586);
            this.groupBox1.TabIndex = 437;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // linkDetail
            // 
            this.linkDetail.AutoSize = true;
            this.linkDetail.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkDetail.Location = new System.Drawing.Point(643, 559);
            this.linkDetail.Name = "linkDetail";
            this.linkDetail.Size = new System.Drawing.Size(98, 18);
            this.linkDetail.TabIndex = 437;
            this.linkDetail.TabStop = true;
            this.linkDetail.Text = "Detaild Report";
            this.linkDetail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkDetail_LinkClicked);
            // 
            // GrDetail
            // 
            this.GrDetail.Controls.Add(this.BtnBack);
            this.GrDetail.Controls.Add(this.DataGridDet);
            this.GrDetail.Location = new System.Drawing.Point(1, -1);
            this.GrDetail.Name = "GrDetail";
            this.GrDetail.Size = new System.Drawing.Size(863, 579);
            this.GrDetail.TabIndex = 438;
            this.GrDetail.TabStop = false;
            this.GrDetail.Visible = false;
            // 
            // BtnBack
            // 
            this.BtnBack.BackColor = System.Drawing.Color.White;
            this.BtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnBack.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBack.Location = new System.Drawing.Point(781, 550);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(76, 28);
            this.BtnBack.TabIndex = 25;
            this.BtnBack.Text = "Back";
            this.BtnBack.UseVisualStyleBackColor = false;
            this.BtnBack.Click += new System.EventHandler(this.Button4_Click);
            // 
            // DataGridDet
            // 
            this.DataGridDet.BackgroundColor = System.Drawing.Color.White;
            this.DataGridDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridDet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.DataGridDet.Location = new System.Drawing.Point(6, 19);
            this.DataGridDet.Name = "DataGridDet";
            this.DataGridDet.Size = new System.Drawing.Size(848, 526);
            this.DataGridDet.TabIndex = 24;
            // 
            // FrmEmpMuster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(864, 582);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GrDetail);
            this.Name = "FrmEmpMuster";
            this.Text = "Muster Details";
            this.Load += new System.EventHandler(this.FrmEmpMuster_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmpPay)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.GrDetail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridDet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox CmbDepartment;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView DataGridEmpPay;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbmonth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtWDays;
        internal System.Windows.Forms.DateTimePicker Frmdt;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button BtnExcelExport;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbBranch;
        private System.Windows.Forms.LinkLabel linklblSample;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.LinkLabel linkDetail;
        private System.Windows.Forms.GroupBox GrDetail;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.DataGridView DataGridDet;
    }
}