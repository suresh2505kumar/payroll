﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmDedEntry : Form
    {
        public FrmDedEntry()
        {
            InitializeComponent();
        }
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SQLDBHelper db = new SQLDBHelper();

        private void FrmDedEntry_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            Loadmonth();
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void TxtEmpCode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (TxtEmpCode.Text != string.Empty)
                {
                    string Query = "Select * from Emp_Mast Where EmpCode ='" + TxtEmpCode.Text + "'";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    if (dt.Rows.Count > 0)
                    {
                        txtEmpName.Text = dt.Rows[0]["EmpName"].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Invalid Employee Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void TxtEmpCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEmpName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
