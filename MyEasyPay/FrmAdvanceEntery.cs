﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public partial class FrmAdvanceEntery : Form
    {
        public FrmAdvanceEntery()
        {
            InitializeComponent();
        }
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();
        int EmpId = 0;
        private DataRow doc1;
        DataTable Docno = new DataTable();
        int Loadid = 0;
        int Uid = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmAdvance_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DataSource = null;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;
            DataGridPunch.RowHeadersVisible = false;
            GetEDuctEntry();
            LoadEarnDuctGrid();
            Uid = 0;
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }

        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";

                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;

                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "sp_getempdet", conn);
                    bsp.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    loadbal();
                    txtnead.Focus();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void loadbal()
        {
            string qur = @"select  isnull(c.lamount,0)-isnull(sum(b.advance),0) as Amount from emp_mast a  
                        inner join advance c on a.empid=c.empid inner join payrollm b on a.empid=b.empid  where a.empid=" + txtempname.Tag + " and c.Isclosed = 0 group by  c.lamount";
            SqlCommand cmd1 = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd1);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            if (tab.Rows.Count > 0)
            {
                txtoldbal.Text = Convert.ToString(tab.Rows[0]["Amount"].ToString());
            }
            else
            {
                txtoldbal.Text = "0";
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string qur = "select uid from (select max(uid) as uid from advance where empid=" + txtempname.Tag + ")tab where uid is not null ";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);
                if (tab.Rows.Count > 0)
                {
                    Uid = Convert.ToInt16(tab.Rows[0]["uid"].ToString());
                }
                SqlParameter[] parameters = {
                    new SqlParameter("@Uid",Uid),
                    new SqlParameter("@ldate", Convert.ToDateTime(addt.Text).ToString("yyyy-MM-dd")),
                    new SqlParameter("@ltype", "Loan"),
                    new SqlParameter("@Empid",txtempname.Tag),
                    new SqlParameter("@lamount", txtamount.Text),
                    new SqlParameter("@tenure","0"),
                    new SqlParameter("@tamount",txtamount.Text),
                    new SqlParameter("@paidamount","0"),
                    new SqlParameter("@isclosed","0")
                };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "sp_saveadvance", parameters, conn);
                MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtempcode.Text = string.Empty;
                txtempname.Text = string.Empty;
                txtamount.Text = string.Empty;
                txtnead.Text = string.Empty;
                txtoldbal.Text = string.Empty;
                txtemiamount.Text = string.Empty;
                Uid = 0;
                GetEDuctEntry();
                LoadEarnDuctGrid();
                txtempcode.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void GetEDuctEntry()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                dtmon = db.GetDataWithParam(CommandType.StoredProcedure, "SP_Get_Advance", para, conn);
                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void LoadEarnDuctGrid()
        {
            try
            {
                DataGridPunch.DataSource = null;
                DataGridPunch.AutoGenerateColumns = false;
                DataGridPunch.ColumnCount = 6;
                this.DataGridPunch.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.DataGridPunch.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                DataGridPunch.Columns[0].Name = "Uid";
                DataGridPunch.Columns[0].HeaderText = "Uid";
                DataGridPunch.Columns[0].DataPropertyName = "Uid";
                DataGridPunch.Columns[0].Visible = false;

                DataGridPunch.Columns[1].Name = "EmpId";
                DataGridPunch.Columns[1].HeaderText = "EmpId";
                DataGridPunch.Columns[1].DataPropertyName = "EmpId";
                DataGridPunch.Columns[1].Visible = false;

                DataGridPunch.Columns[2].Name = "EmpCode";
                DataGridPunch.Columns[2].HeaderText = "EmpCode";
                DataGridPunch.Columns[2].DataPropertyName = "EmpCode";
                DataGridPunch.Columns[2].Width = 100;

                DataGridPunch.Columns[3].Name = "EmpName";
                DataGridPunch.Columns[3].HeaderText = "EmpName";
                DataGridPunch.Columns[3].DataPropertyName = "EmpName";
                DataGridPunch.Columns[3].Width = 150;

                DataGridPunch.Columns[4].Name = "ldate";
                DataGridPunch.Columns[4].HeaderText = "Date";
                DataGridPunch.Columns[4].DataPropertyName = "ldate";
                DataGridPunch.Columns[4].Width = 100;

                DataGridPunch.Columns[5].Name = "Lamount";
                DataGridPunch.Columns[5].HeaderText = "Lamount";
                DataGridPunch.Columns[5].DataPropertyName = "Lamount";
                DataGridPunch.Columns[5].Width = 100;
                DataGridPunch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }

        private void txtnead_TextChanged(object sender, EventArgs e)
        {
            if (txtoldbal.Text != "" && txtnead.Text != "")
            {
                double yy = Convert.ToDouble(txtoldbal.Text) + Convert.ToDouble(txtnead.Text);
                txtamount.Text = yy.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridPunch_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "")
            {
                if (DataGridPunch.CurrentCell.ColumnIndex == 5)
                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[5];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }
            }
        }

        private void DataGridPunch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridPunch_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.RowCount > 1)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "0")
                    {
                        if (DataGridPunch.CurrentCell.ColumnIndex == 5)
                        {
                            conn.Close();
                            conn.Open();
                            string Query = "update advance set lamount=" + DataGridPunch.CurrentRow.Cells[5].Value.ToString() + " ,Tamount= " + DataGridPunch.CurrentRow.Cells[5].Value.ToString() + " where uid=" + DataGridPunch.CurrentRow.Cells[0].Value.ToString() + "";
                            SqlCommand cmd = new SqlCommand(Query, conn);
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    GetEDuctEntry();
                    LoadEarnDuctGrid();
                }
            }
        }

        private void DataGridPunch_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Delete)
                {
                    int index = DataGridPunch.SelectedCells[0].RowIndex;
                    int uid = Convert.ToInt32(DataGridPunch.Rows[index].Cells[0].Value.ToString());
                    DialogResult res = MessageBox.Show("Are you sure want to delete Advance entry for this Employee", "Infromation", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                    if (res == DialogResult.Yes)
                    {
                        string Query = "delete from advance Where uid =" + uid + "";
                        db.ExecuteNonQuery(CommandType.Text, Query, conn);
                        MessageBox.Show("Deleted Successfully", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetEDuctEntry();
                        LoadEarnDuctGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void DataGridPunch_DoubleClick(object sender, EventArgs e)
        {

        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    loadbal();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
