﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace MyEasyPay
{
    public partial class FrmEmpFinal : Form
    {

        public FrmEmpFinal()
        {
            InitializeComponent();
        }
      
        int EditId = 0;
        string connstring = string.Empty;
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();
        int EmpId = 0;
        private DataRow doc1;
        DataTable Docno = new DataTable();
        int Loadid = 0;
        int Uid = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {

        }
       private void FrmEmpFinal_Load(object sender, EventArgs e)
        {
           
            qur.Connection = conn;
            Uid = 0;
            GetEDuctEntry();
            LoadEarnDuctGrid();




        }

        protected void GetEDuctEntry()
        {
            try
            {
                SqlParameter[] para = {new SqlParameter("@branch", GeneralParameters.Branch) };
                dtmon = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "SP_GETEMPFINAL", para);
                //dtmon = db.GetDataWithoutParam(CommandType.StoredProcedure, "SP_Get_Earn_Duct", conn);
                bs.DataSource = dtmon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void LoadEarnDuctGrid()
        {
            try
            {
                DataGridPunch.DataSource = null;
                DataGridPunch.AutoGenerateColumns = false;
                DataGridPunch.ColumnCount = 10;
                this.DataGridPunch.DefaultCellStyle.Font = new Font("Calibri", 10);
                this.DataGridPunch.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 10, FontStyle.Bold);
                DataGridPunch.Columns[0].Name = "Uid";
                DataGridPunch.Columns[0].HeaderText = "Uid";
                DataGridPunch.Columns[0].DataPropertyName = "Uid";
                DataGridPunch.Columns[0].Visible = false;

                DataGridPunch.Columns[1].Name = "EmpId";
                DataGridPunch.Columns[1].HeaderText = "EmpId";
                DataGridPunch.Columns[1].DataPropertyName = "EmpId";
                DataGridPunch.Columns[1].Visible = false;

                DataGridPunch.Columns[2].Name = "EmpCode";
                DataGridPunch.Columns[2].HeaderText = "EmpCode";
                DataGridPunch.Columns[2].DataPropertyName = "EmpCode";
                DataGridPunch.Columns[2].Width = 100;

                DataGridPunch.Columns[3].Name = "EmpName";
                DataGridPunch.Columns[3].HeaderText = "EmpName";
                DataGridPunch.Columns[3].DataPropertyName = "EmpName";
                DataGridPunch.Columns[3].Width = 340;

                DataGridPunch.Columns[4].Name = "balsal";
                DataGridPunch.Columns[4].HeaderText = "balsal";
                DataGridPunch.Columns[4].DataPropertyName = "balsal";
                DataGridPunch.Columns[4].Visible = false;
                DataGridPunch.Columns[4].DefaultCellStyle.Format = "N2";
                DataGridPunch.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridPunch.Columns[5].Name = "cd";
                DataGridPunch.Columns[5].HeaderText = "cd";
                DataGridPunch.Columns[5].DataPropertyName = "cd";
                DataGridPunch.Columns[5].Visible = false;

                DataGridPunch.Columns[6].Name = "OP";
                DataGridPunch.Columns[6].HeaderText = "OP";
                DataGridPunch.Columns[6].DataPropertyName = "OP";
                DataGridPunch.Columns[6].Visible = false;

                DataGridPunch.Columns[7].Name = "TOTAL";
                DataGridPunch.Columns[7].HeaderText = "TOTAL";
                DataGridPunch.Columns[7].DataPropertyName = "TOTAL";
                DataGridPunch.Columns[7].Width = 100;
                DataGridPunch.Columns[8].Name = "RECDT";
                DataGridPunch.Columns[8].HeaderText = "RECDT";
                DataGridPunch.Columns[8].DataPropertyName = "RECDT";
                DataGridPunch.Columns[8].Visible = false;

                DataGridPunch.Columns[9].Name = "approved";
                DataGridPunch.Columns[9].HeaderText = "approved";
                DataGridPunch.Columns[9].DataPropertyName = "approved";
                DataGridPunch.Columns[9].Visible = false;
                DataGridPunch.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return; throw;
            }
        }
        protected void LoadPaymonth()
        {
          
        }
        protected void LoadComponents()
        {
           
        }
        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (cboassetname.Text == "")
            {

                MessageBox.Show("select  the Assetname");
                cboassetname.Focus();
                return;
            }

            if (txtbalsal.Text == "")
            {

                MessageBox.Show("Enter  the Amount");
                txtbalsal.Focus();
                return;
            }

            Genclass.assetname = "";



            string qur = "Select uid, empid, assetname + ' - ' + Qty   as  assetname from empassetM where empid =" + txtempname.Tag + " order by assetname";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable tab = new DataTable();
            apt.Fill(tab);
            for (int i = 0;i<tab.Rows.Count;i++)
            {

                if (i == 0)
                {
                    Genclass.assetname =  tab.Rows[i]["assetname"].ToString();
                }
                else
                {
                    Genclass.assetname = Genclass.assetname + " , " + tab.Rows[i]["assetname"].ToString();

                }

            }

            int sp;
            if (checkBox1.Checked == true)
            {
                sp = 1;

            }
            else
            {
                sp = 0;
            }


                SqlParameter[] parameters = {
                        new SqlParameter("@uid",Uid),
                        new SqlParameter("@Empid",txtempname.Tag),
                        new SqlParameter("@balsal", txtbalsal.Text),
                        new SqlParameter("@cd", txtcd.Text),
                        new SqlParameter("@op",   txtop.Text),
                        new SqlParameter("@total",  txttot.Text),
                        new SqlParameter("@RECDT", Convert.ToDateTime(dt.Text).ToString("yyyy-MM-dd")),
                        new SqlParameter("@approved", sp),

                        new SqlParameter("@assetnames", Genclass.assetname),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "SP_Save_EmpFinal", parameters, conn);
                    MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtempcode.Text = string.Empty;
                    txtempname.Text = string.Empty;
                    txtbalsal.Text = string.Empty;
                    txtcd.Text = string.Empty;
                    txtop.Text = string.Empty;
                    txttot.Text = string.Empty;
                    Uid = 0;
          
            LoadButton(3);
            grFront.Visible = true;
            GetEDuctEntry();
            LoadEarnDuctGrid();
          





        }
        public string Rupees(Int64 rup)
        {
            string result = "";
            Int64 res;
            if ((rup / 10000000) > 0)
            {
                res = rup / 10000000;
                rup = rup % 10000000;
                result = result + ' ' + RupeesToWords(res) + " Crore";
            }
            if ((rup / 100000) > 0)
            {
                res = rup / 100000;
                rup = rup % 100000;
                result = result + ' ' + RupeesToWords(res) + " Lakh";
            }
            if ((rup / 1000) > 0)
            {
                res = rup / 1000;
                rup = rup % 1000;
                result = result + ' ' + RupeesToWords(res) + " Thousand";
            }


            if ((rup / 100) > 0)
            {
                res = rup / 100;
                rup = rup % 100;
                if (rup > 0)
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred and";
                }
                else
                {
                    result = result + ' ' + RupeesToWords(res) + " Hundred";
                }
            }
            if ((rup % 10) >= 0)
            {
                res = rup % 100;
                result = result + " " + RupeesToWords(res);
            }
            result = "Rupees " + result + ' ' + "only";
            return result;
        }

        public string RupeesToWords(Int64 rup)
        {
            string result = "";
            if ((rup >= 1) && (rup <= 10))
            {
                if ((rup % 10) == 1) result = "One";
                if ((rup % 10) == 2) result = "Two";
                if ((rup % 10) == 3) result = "Three";
                if ((rup % 10) == 4) result = "Four";
                if ((rup % 10) == 5) result = "Five";
                if ((rup % 10) == 6) result = "Six";
                if ((rup % 10) == 7) result = "Seven";
                if ((rup % 10) == 8) result = "Eight";
                if ((rup % 10) == 9) result = "Nine";
                if ((rup % 10) == 0) result = "Ten";
            }
            if (rup > 9 && rup < 20)
            {
                if (rup == 11) result = "Eleven";
                if (rup == 12) result = "Twelve";
                if (rup == 13) result = "Thirteen";
                if (rup == 14) result = "Forteen";
                if (rup == 15) result = "Fifteen";
                if (rup == 16) result = "Sixteen";
                if (rup == 17) result = "Seventeen";
                if (rup == 18) result = "Eighteen";
                if (rup == 19) result = "Nineteen";
            }

            if (rup >= 20 && (rup / 10) == 2 && (rup % 10) == 0) result = "Twenty";
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) == 0) result = "Thirty";
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) == 0) result = "Forty";
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) == 0) result = "Fifty";
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) == 0) result = "Sixty";
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) == 0) result = "Seventy";
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) == 0) result = "Eighty";
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) == 0) result = "Ninty";

            if (rup > 20 && (rup / 10) == 2 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Twenty One";
                if ((rup % 10) == 2) result = "Twenty Two";
                if ((rup % 10) == 3) result = "Twenty Three";
                if ((rup % 10) == 4) result = "Twenty Four";
                if ((rup % 10) == 5) result = "Twenty Five";
                if ((rup % 10) == 6) result = "Twenty Six";
                if ((rup % 10) == 7) result = "Twenty Seven";
                if ((rup % 10) == 8) result = "Twenty Eight";
                if ((rup % 10) == 9) result = "Twenty Nine";
            }
            if (rup > 20 && (rup / 10) == 3 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Thirty One";
                if ((rup % 10) == 2) result = "Thirty Two";
                if ((rup % 10) == 3) result = "Thirty Three";
                if ((rup % 10) == 4) result = "Thirty Four";
                if ((rup % 10) == 5) result = "Thirty Five";
                if ((rup % 10) == 6) result = "Thirty Six";
                if ((rup % 10) == 7) result = "Thirty Seven";
                if ((rup % 10) == 8) result = "Thirty Eight";
                if ((rup % 10) == 9) result = "Thirty Nine";
            }
            if (rup > 20 && (rup / 10) == 4 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Forty One";
                if ((rup % 10) == 2) result = "Forty Two";
                if ((rup % 10) == 3) result = "Forty Three";
                if ((rup % 10) == 4) result = "Forty Four";
                if ((rup % 10) == 5) result = "Forty Five";
                if ((rup % 10) == 6) result = "Forty Six";
                if ((rup % 10) == 7) result = "Forty Seven";
                if ((rup % 10) == 8) result = "Forty Eight";
                if ((rup % 10) == 9) result = "Forty Nine";
            }
            if (rup > 20 && (rup / 10) == 5 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Fifty One";
                if ((rup % 10) == 2) result = "Fifty Two";
                if ((rup % 10) == 3) result = "Fifty Three";
                if ((rup % 10) == 4) result = "Fifty Four";
                if ((rup % 10) == 5) result = "Fifty Five";
                if ((rup % 10) == 6) result = "Fifty Six";
                if ((rup % 10) == 7) result = "Fifty Seven";
                if ((rup % 10) == 8) result = "Fifty Eight";
                if ((rup % 10) == 9) result = "Fifty Nine";
            }
            if (rup > 20 && (rup / 10) == 6 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Sixty One";
                if ((rup % 10) == 2) result = "Sixty Two";
                if ((rup % 10) == 3) result = "Sixty Three";
                if ((rup % 10) == 4) result = "Sixty Four";
                if ((rup % 10) == 5) result = "Sixty Five";
                if ((rup % 10) == 6) result = "Sixty Six";
                if ((rup % 10) == 7) result = "Sixty Seven";
                if ((rup % 10) == 8) result = "Sixty Eight";
                if ((rup % 10) == 9) result = "Sixty Nine";
            }
            if (rup > 20 && (rup / 10) == 7 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Seventy One";
                if ((rup % 10) == 2) result = "Seventy Two";
                if ((rup % 10) == 3) result = "Seventy Three";
                if ((rup % 10) == 4) result = "Seventy Four";
                if ((rup % 10) == 5) result = "Seventy Five";
                if ((rup % 10) == 6) result = "Seventy Six";
                if ((rup % 10) == 7) result = "Seventy Seven";
                if ((rup % 10) == 8) result = "Seventy Eight";
                if ((rup % 10) == 9) result = "Seventy Nine";
            }
            if (rup > 20 && (rup / 10) == 8 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Eighty One";
                if ((rup % 10) == 2) result = "Eighty Two";
                if ((rup % 10) == 3) result = "Eighty Three";
                if ((rup % 10) == 4) result = "Eighty Four";
                if ((rup % 10) == 5) result = "Eighty Five";
                if ((rup % 10) == 6) result = "Eighty Six";
                if ((rup % 10) == 7) result = "Eighty Seven";
                if ((rup % 10) == 8) result = "Eighty Eight";
                if ((rup % 10) == 9) result = "Eighty Nine";
            }
            if (rup > 20 && (rup / 10) == 9 && (rup % 10) != 0)
            {
                if ((rup % 10) == 1) result = "Ninty One";
                if ((rup % 10) == 2) result = "Ninty Two";
                if ((rup % 10) == 3) result = "Ninty Three";
                if ((rup % 10) == 4) result = "Ninty Four";
                if ((rup % 10) == 5) result = "Ninty Five";
                if ((rup % 10) == 6) result = "Ninty Six";
                if ((rup % 10) == 7) result = "Ninty Seven";
                if ((rup % 10) == 8) result = "Ninty Eight";
                if ((rup % 10) == 9) result = "Ninty Nine";
            }
            return result;
        }

        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }
        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {


       

        }

        private void HFIT_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void HFIT_EditingControlShowing_2(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
           
        }

        private void CmbLvTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            

            GetEDuctEntry();
            LoadEarnDuctGrid();
        }
       
        private void updateAmtdetails()
        {
          
           

        }
        protected void ImportEarndeduction(string ConnectionString, string Path)
        {
            
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtempname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {

          
        }
       
     

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillloadid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    loadasset();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    loadasset();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    loadasset();

                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void loadasset()
        {


            try
            {
                string qur = "Select uid, empid, assetname + ' - ' + Qty   as  assetname from empassetM where empid =" + txtempname.Tag +" order by assetname";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                cboassetname.DataSource = null;
                cboassetname.DataSource = tab;
                cboassetname.DisplayMember = "assetname";
                cboassetname.ValueMember = "uid";
        


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void cmbcom_SelectedIndexChanged(object sender, EventArgs e)
        {
          

        }

        private void DataGridPunch_KeyDown(object sender, KeyEventArgs e)
        {
           
        }

        private void DataGridPunch_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void DataGridPunch_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           
        }

        private void DataGridPunch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ChckShift_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void DataGridPunch_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void LoadButton(int id)
        {
            try
            {
                if (id == 1)//Add
                {
                    grFront.Visible = false;
                
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 2)//Edit
                {
                    grFront.Visible = false;
                 
                    btnAdd.Visible = false;
                    btnEdit.Visible = false;
                    btnDelete.Visible = false;
                    btnExit.Visible = false;
                    btnSave.Visible = true;
                    btnBack.Visible = true;
                }
                else if (id == 3)//Back
                {
                    grFront.Visible = true;
                  
                    btnAdd.Visible = true;
                    btnEdit.Visible = true;
                    btnDelete.Visible = true;
                    btnExit.Visible = true;
                    btnSave.Visible = false;
                    btnBack.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            LoadButton(1);
            EditId = 0;
            try
            {
                txtempcode.Text = string.Empty;
                txtempname.Text = string.Empty;
                txtbalsal.Text = string.Empty;
                txtcd.Text = string.Empty;
                txtop.Text = string.Empty;
                txttot.Text = string.Empty;
                checkBox1.Checked = false;
                Uid = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "delete from  NotoWords";
            qur.ExecuteNonQuery();
           
            Int64 NumVal = Convert.ToInt64(DataGridPunch.CurrentRow.Cells[7].Value);
            string Nw = Rupees(NumVal);

            qur.CommandText = "Insert into NotoWords values('" + Nw + "')";
            qur.ExecuteNonQuery();

            GeneralParameters.ReportId = 24;
            GeneralParameters.EmpId = DataGridPunch.CurrentRow.Cells[1].Value.ToString();
            FrmReportviewer frmReportviewer = new FrmReportviewer();

            FrmReportviewer crv = new FrmReportviewer
            {
                StartPosition = FormStartPosition.CenterScreen
            };
            crv.ShowDialog();


        }

        private void btnEdit_Click(object sender, EventArgs e)
        {

            if(DataGridPunch.Rows.Count==0)
            {

                MessageBox.Show("There is No Record Found");
                return;
            }
            txtempcode.Text = string.Empty;
            txtempname.Text = string.Empty;
            txtbalsal.Text = string.Empty;
            txtcd.Text = string.Empty;
            txtop.Text = string.Empty;
            txttot.Text = string.Empty;
            Uid = 0;

            int Id = DataGridPunch.SelectedCells[0].RowIndex;
            Uid = Convert.ToInt32(DataGridPunch.Rows[Id].Cells[0].Value.ToString());
            txtempname.Tag = DataGridPunch.Rows[Id].Cells[1].Value.ToString();
            txtempcode.Text = DataGridPunch.Rows[Id].Cells[2].Value.ToString();
            txtempname.Text = DataGridPunch.Rows[Id].Cells[3].Value.ToString();
            txtbalsal.Text = DataGridPunch.Rows[Id].Cells[4].Value.ToString();
            txtcd.Text = DataGridPunch.Rows[Id].Cells[5].Value.ToString();
            txtop.Text = DataGridPunch.Rows[Id].Cells[6].Value.ToString();
            txttot.Text = DataGridPunch.Rows[Id].Cells[7].Value.ToString();
            dt.Text = DataGridPunch.Rows[Id].Cells[8].Value.ToString();
            loadasset();
            if (DataGridPunch.Rows[Id].Cells[9].Value.ToString() == "1")
            {
                checkBox1.Checked = true;

            }
            else
            {
                checkBox1.Checked = false;

            }
           

            LoadButton(2);
    }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            LoadButton(3);
            grFront.Visible = true;
            GetEDuctEntry();
            LoadEarnDuctGrid();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        private void txtcd_TextChanged(object sender, EventArgs e)
        {
            if(txtcd.Text!="" && txtbalsal.Text!="")
            {
                double yy = Convert.ToDouble(txtcd.Text) + Convert.ToDouble(txtbalsal.Text);

                txttot.Text = yy.ToString();

            }
        }

        private void txtop_TextChanged(object sender, EventArgs e)
        {
            if (txtcd.Text != "" && txtbalsal.Text != "" && txtop.Text != "")
            {
                double yy = Convert.ToDouble(txtcd.Text) + Convert.ToDouble(txtbalsal.Text)+ Convert.ToDouble(txtop.Text);

                txttot.Text = yy.ToString();

            }
        }
    }
}
