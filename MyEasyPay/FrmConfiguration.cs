﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Management;
using System.Configuration;

namespace MyEasyPay
{
    public partial class FrmConfiguration : Form
    {
        public FrmConfiguration()
        {
            InitializeComponent();
        }

        string EnValue = string.Empty;
        private string key = "o7x8y6";
        string WebserverName;
        string WebserverUserName;
        string WebserverPassword;
        string WebserverDBName;
        new string CompanyName;
        SQLDBHelper db = new SQLDBHelper();
        private void FrmConfiguration_Load(object sender, EventArgs e)
        {
            string path;
            path = Path.Combine(Application.StartupPath, "configL.LCG");
            string[] readText = File.ReadAllLines(path);
            for (int i = 0; i < readText.Length; i++)
            {
                if (i == 0)
                {
                    EnValue = readText[i].ToString();
                    string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    WebserverName = txt;
                }
                if (i == 1)
                {
                    EnValue = readText[i].ToString();
                    string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    WebserverUserName = txt;
                }
                if (i == 2)
                {
                    EnValue = readText[i].ToString();
                    string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    WebserverPassword = txt;
                }
                if (i == 3)
                {
                    EnValue = readText[i].ToString();
                    string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    WebserverDBName = txt;
                }
                if (i == 4)
                {
                    EnValue = readText[i].ToString();
                    string txt = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                    CompanyName = txt;
                }
            }
            if(GeneralParameters.IsVerify == 1)
            {
                string path1;
                path1 = Path.Combine(Application.StartupPath, "Register.Reg");
                string[] readText1 = File.ReadAllLines(path);
                EnValue = string.Empty;
                EnValue = readText1[0].ToString();
                string productKey = EncryptionandDecryption.DecryptStringAES(EnValue, key);
                SqlConnection conn = new SqlConnection("Data Source=" + WebserverName + ";User Id=" + WebserverUserName + ";Password=" + WebserverPassword + ";Initial Catalog=" + WebserverDBName + "");
                string Query = "Select * from MepCompinfo Where ProductKey ='" + productKey + "'";
                SqlCommand sqlCommand = new SqlCommand(Query, conn);
                SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                DataTable dataTable = new DataTable();
                da.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    txtCompanyName.Text = dataTable.Rows[0]["CompanyName"].ToString();
                    txtCity.Text = dataTable.Rows[0]["City"].ToString();
                    txtPincode.Text = dataTable.Rows[0]["Pincode"].ToString();
                    string ProcessorId = GetProcessorId();
                    string HDDSerialNo = GetHDDSerialNo();
                    txtLicenseKey.Text = ProcessorId + "-" + HDDSerialNo;
                }
            }
           
        }

        private void BtnGetDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtProductKey.Text != string.Empty)
                {
                    SqlConnection conn = new SqlConnection("Data Source=" + WebserverName + ";User Id=" + WebserverUserName + ";Password=" + WebserverPassword + ";Initial Catalog=" + WebserverDBName + "");
                    string Query = "Select * from MepCompinfo Where ProductKey ='" + txtProductKey.Text + "'";
                    SqlCommand sqlCommand = new SqlCommand(Query, conn);
                    SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                    DataTable dataTable = new DataTable();
                    da.Fill(dataTable);
                    if(dataTable.Rows.Count > 0)
                    {
                        txtCompanyName.Text = dataTable.Rows[0]["CompanyName"].ToString();
                        txtCity.Text = dataTable.Rows[0]["City"].ToString();
                        txtPincode.Text = dataTable.Rows[0]["Pincode"].ToString();
                        string ProcessorId = GetProcessorId();
                        string HDDSerialNo = GetHDDSerialNo();
                        txtLicenseKey.Text = ProcessorId + "-" + HDDSerialNo;
                    }
                    else
                    {
                        MessageBox.Show("Enter Correct Productkey or Please contact Vendor", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtProductKey.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Enter Product Key", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtProductKey.Focus();
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string path;
                path = Path.Combine(Application.StartupPath, "Register.Reg");
                if (!File.Exists(path))
                {
                    FileStream fs1 = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    fs1.Close();
                }
                var fs = new FileStream(path, FileMode.Truncate);
                fs.Close();
                string encryptionLicenseKey = string.Empty;
                string encryptionProductKey = string.Empty;
                string encryptionCompanyName = string.Empty;
                string encryptionUserName = string.Empty;
                string encryptionCity = string.Empty;
                string encryptionPincode = string.Empty;
                if (txtProductKey.Text != string.Empty && txtCompanyName.Text != string.Empty && txtCity.Text != string.Empty && txtPincode.Text != string.Empty)
                {
                    EnValue = string.Empty;
                    EnValue = txtProductKey.Text;
                    encryptionProductKey = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionProductKey + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtCompanyName.Text;
                    encryptionCompanyName = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionCompanyName + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtCity.Text;
                    encryptionCity = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionCity + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtPincode.Text;
                    encryptionPincode = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionPincode + Environment.NewLine);

                    EnValue = string.Empty;
                    EnValue = txtLicenseKey.Text;
                    encryptionLicenseKey = EncryptionandDecryption.EncryptStringAES(EnValue, key);
                    File.AppendAllText(path, encryptionLicenseKey + Environment.NewLine);
                }
                string LVerifyDate = string.Empty;
                string ValidDate = string.Empty;
                string RegisterDate = string.Empty;

                EnValue = string.Empty;
                EnValue = DateTime.Now.Date.ToString("dd-MMM-yyyy");
                LVerifyDate = EncryptionandDecryption.EncryptStringAES(EnValue, key);

                EnValue = string.Empty;
                EnValue = DateTime.Now.Date.AddDays(364).ToString("dd-MMM-yyyy");
                ValidDate = EncryptionandDecryption.EncryptStringAES(EnValue, key);

                EnValue = string.Empty;
                EnValue = DateTime.Now.Date.ToString("dd-MMM-yyyy");
                RegisterDate = EncryptionandDecryption.EncryptStringAES(EnValue, key);

                SqlConnection conn = new SqlConnection("Data Source=" + WebserverName + ";User Id=" + WebserverUserName + ";Password=" + WebserverPassword + ";Initial Catalog=" + WebserverDBName + "");
                SqlConnection connLocal = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
                string query = "Update MepCompinfo Set RegDate='" + DateTime.Now.Date + "',LVerifyDate='" + DateTime.Now.Date + "',ValidDate='" + DateTime.Now.Date.AddDays(365) + "',LicenseKey='" + encryptionLicenseKey.ToString() + "' Where ProductKey='" + txtProductKey.Text + "'";
                string LQuery = "Insert into LicenseInfo Values ('" + encryptionCompanyName + "','" + encryptionProductKey + "','" + encryptionLicenseKey + "','" + RegisterDate + "','" + LVerifyDate + "','" + ValidDate + "')";
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlCommand commandLocal = new SqlCommand(LQuery, connLocal);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                connLocal.Open();
                commandLocal.ExecuteNonQuery();
                connLocal.Close();

                MessageBox.Show("Registered Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtProductKey.Text = string.Empty;
                txtCompanyName.Text = string.Empty;
                txtCity.Text = string.Empty;
                txtPincode.Text = string.Empty;
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        public static string GetProcessorId()
        {
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();
            String Id = String.Empty;
            foreach (ManagementObject mo in moc)
            {

                Id = mo.Properties["processorID"].Value.ToString();
                break;
            }
            return Id;

        }
        public static string GetHDDSerialNo()
        {
            ManagementClass mangnmt = new ManagementClass("Win32_LogicalDisk");
            ManagementObjectCollection mcol = mangnmt.GetInstances();
            string result = "";
            foreach (ManagementObject strt in mcol)
            {
                result += Convert.ToString(strt["VolumeSerialNumber"]);
            }
            return result;
        }

    }
}
