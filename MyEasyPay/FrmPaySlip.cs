﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmPaySlip : Form
    {
        public FrmPaySlip()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int SelectId = 0;

        private void FrmPaySlip_Load(object sender, EventArgs e)
        {
            SelectId = 1;
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);

            //string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            //var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            DataTable dataTable1 = dtBranch;
            if (GeneralParameters.BranchId == 0)
            {
                DataRow dataRow = dataTable1.NewRow();
                dataRow["Uid"] = 0;
                dataRow["BRANCHNAME"] = "All";
                dataTable1.Rows.Add(dataRow);
            }
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable1;
            LoadMasters();
            Loadmonth();
            SelectId = 0;
        }

        protected void Loadmonth()
        {
            try
            {
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "sp_getmonthid", conn);
                DataTable dtDept = ds.Tables[0];
                CmbMonth.DataSource = null;
                CmbMonth.DisplayMember = "PMonth";
                CmbMonth.ValueMember = "monid";
                CmbMonth.DataSource = dtDept;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        protected void LoadMasters()
        {
            try
            {
                SelectId = 1;
                DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
                DataTable dataTable = ds.Tables[0];
                DataTable dtDept = new DataTable();
                dtDept = dataTable;
                CmbDepartment.DataSource = null;
                CmbDepartment.DisplayMember = "DeptName";
                CmbDepartment.ValueMember = "DeptId";
                CmbDepartment.DataSource = dtDept;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GeneralParameters.Branch = CmbBranch.Text;
                if (CmbDepartment.Text != string.Empty || CmbDepartment.SelectedIndex != -1)
                {
                    if (SelectId == 0)
                    {
                        Loadgrid2();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Loadgrid2()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@deptid", CmbDepartment.SelectedValue),
                    new SqlParameter("@Branch",GeneralParameters.Branch)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GetEmp_mast", sqlParameters, conn);
                bs.DataSource = dataTable;
                DataGridEmployee.DataSource = null;
                DataGridEmployee.AutoGenerateColumns = false;
                DataGridEmployee.ColumnCount = 3;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();
                dataGridViewCheckBoxColumn.HeaderText = "Chck";
                dataGridViewCheckBoxColumn.Name = "Chck";
                dataGridViewCheckBoxColumn.Width = 50;
                DataGridEmployee.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridEmployee.Columns[1].Name = "EmpId";
                DataGridEmployee.Columns[1].HeaderText = "EmpId";
                DataGridEmployee.Columns[1].DataPropertyName = "EmpId";
                DataGridEmployee.Columns[1].Visible = false;

                DataGridEmployee.Columns[2].Name = "EmpCode";
                DataGridEmployee.Columns[2].HeaderText = "EmpCode";
                DataGridEmployee.Columns[2].DataPropertyName = "EmpCode";
                DataGridEmployee.Columns[2].Width = 100;

                DataGridEmployee.Columns[3].Name = "EmpName";
                DataGridEmployee.Columns[3].HeaderText = "Employe Name";
                DataGridEmployee.Columns[3].DataPropertyName = "EmpName";
                DataGridEmployee.Columns[3].Width = 200;
                DataGridEmployee.DataSource = bs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Txtsearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bs.Filter = string.Format("EmpName Like '%{0}%' or EmpCode Like '%{1}%'", Txtsearch.Text, Txtsearch.Text);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void ChckSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChckSelectAll.Checked == true)
                {
                    for (int i = 0; i < DataGridEmployee.RowCount; i++)
                    {
                        DataGridEmployee[0, i].Value = true;
                    }
                }
                else
                {
                    for (int i = 0; i < DataGridEmployee.RowCount; i++)
                    {
                        DataGridEmployee[0, i].Value = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                GeneralParameters.EmpId = string.Empty; GeneralParameters.Branch = CmbBranch.Text;
                foreach (DataGridViewRow dataGridRow in DataGridEmployee.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.EmpId == string.Empty)
                        {
                            GeneralParameters.EmpId = dataGridRow.Cells[1].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.EmpId = GeneralParameters.EmpId + "," + dataGridRow.Cells[1].Value.ToString();
                        }
                    }
                }

                if (CmbReportType.Text == "PaySlip")
                {
                    GeneralParameters.PayMonthId = Convert.ToInt32(CmbMonth.SelectedValue);
                    GeneralParameters.PaMonth = CmbMonth.Text;
                    GeneralParameters.ReportId = 13;
                    GeneralParameters.Branch = CmbBranch.Text;
                    GeneralParameters.ReportBranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                }
                else if (CmbReportType.Text == "Bank Statement")
                {
                    GeneralParameters.PayMonthId = Convert.ToInt32(CmbMonth.SelectedValue);
                    GeneralParameters.PaMonth = CmbMonth.Text;
                    GeneralParameters.ReportId = 14;
                    GeneralParameters.ReportBranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                }
                else if (CmbReportType.Text == "Cash Statement")
                {
                    GeneralParameters.PayMonthId = Convert.ToInt32(CmbMonth.SelectedValue);
                    GeneralParameters.PaMonth = CmbMonth.Text;
                    GeneralParameters.ReportId = 15;
                    GeneralParameters.ReportBranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                }
                else if (CmbReportType.Text == "Salary Register")
                {
                    SqlParameter[] sqlParameters = {
                          new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                          new SqlParameter("@Monid",CmbMonth.SelectedValue),
                    };
                    DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_SALARYREGISTER", sqlParameters, conn);
                    if (data.Rows.Count > 0)
                    {
                        GeneralParameters.PayMonthId = Convert.ToInt32(CmbMonth.SelectedValue);
                        GeneralParameters.PaMonth = CmbMonth.Text;
                        GeneralParameters.Department = CmbDepartment.Text;
                        GeneralParameters.ReportId = 16;
                        GeneralParameters.ReportBranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                        GeneralParameters.DeptId = Convert.ToInt32(CmbDepartment.SelectedValue);
                    }
                    else
                    {
                        MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                FrmReportviewer frmReportviewer = new FrmReportviewer
                {
                    StartPosition = FormStartPosition.CenterScreen
                };
                frmReportviewer.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    SelectId = 1;
                    string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    System.Data.DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = "0";
                    dataRow[1] = "All";
                    dataTable.Rows.Add(dataRow);
                    CmbDepartment.DataSource = null;
                    CmbDepartment.DisplayMember = "DeptName";
                    CmbDepartment.ValueMember = "DeptId";
                    CmbDepartment.DataSource = dataTable;
                    SelectId = 0;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }
    }
}
