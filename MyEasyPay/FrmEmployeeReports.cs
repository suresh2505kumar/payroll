﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace MyEasyPay
{
    public partial class FrmEmployeeReports : Form
    {
        public FrmEmployeeReports()
        {
            this.BackColor = Color.White; ;
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsc = new BindingSource();
        private void Titlep1()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
        }
        private void Titlep4()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
        }

        private void Titlep3()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
            DatagridCategory.Columns.Add(checkColumn);

        }
        private void Titlep2()
        {
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn
            {
                Name = "CHECK",
                HeaderText = "CHECK",
                Width = 55,
                ReadOnly = false,
                FillWeight = 10 //if the datagridview is resized (on form resize) the checkbox won't take up too much; value is relative to the other columns' fill values
            };
            DataGridDepartment.Columns.Add(checkColumn);


        }
        private void FrmEmployeeReports_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);

            //string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            //var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            DataTable dataTable = dtBranch;
            if (GeneralParameters.BranchId == 0)
            {
                DataRow dataRow = dataTable.NewRow();
                dataRow["Uid"] = 0;
                dataRow["BRANCHNAME"] = "All";
                dataTable.Rows.Add(dataRow);
            }
            Frmdt.Visible = true;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            if (GeneralParametrs.MenyKey == 5)
            {
                Genclass.Dtype = 1;
                this.Name = "Employee Reports";
                //cboop.Visible = false;
                //comboBox2.Visible = true;
                BtnAttendanceProcess.Visible = false;
                CmbTag.Visible = false;
                cboop.Visible = true;
            }
            else if (GeneralParametrs.MenyKey == 6 || GeneralParametrs.MenyKey == 0)
            {
                Genclass.Dtype = 2;
                this.Name = "Daily Reports";
                CmbTag.Visible = true;
                cboop.Visible = false;
            }

            DataGridDepartment.RowHeadersVisible = false;
            DatagridCategory.RowHeadersVisible = false;
            qur.Connection = conn;
            Frmdt.Value = DateTime.Now;
            Dept();
            if (Genclass.Dtype == 1)
            {
                Label1.Visible = false;
                Frmdt.Visible = false;
                label5.Visible = false;
                CmbTag.Visible = false;
                DataGridShift.Visible = false;
                ChckShift.Visible = false;
                BtnImport.Visible = false;
            }
            else
            {
                label2.Visible = false;
                cboop.Visible = false;
                Label1.Visible = true;
                Frmdt.Visible = true;
                label5.Visible = true;
                CmbTag.Visible = true;
                DataGridShift.Visible = true;
                ChckShift.Visible = true;
                BtnImport.Visible = true;
            }
            Shift();
            chckDepartment.Checked = false;
            dtpFromDate.Text = DateTime.Now.Date.ToString();
            DtpToTate.Text = DateTime.Now.Date.ToString();
        }

        public void Shift()
        {
            string qur = "select ShiftName, ShiftId,Branch from Shift_Mast ";
            SqlCommand cmd = new SqlCommand(qur, conn);
            SqlDataAdapter apt = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            apt.Fill(data);
            //DataTable tab = data.Select("Branch='" + GeneralParameters.Branch + "'", null).CopyToDataTable();
            DataGridShift.DataSource = null;
            DataGridShift.AutoGenerateColumns = false;
            DataGridShift.ColumnCount = 2;
            DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
            {
                HeaderText = "Chck",
                Name = "Chck",
                Width = 50
            };
            DataGridShift.Columns.Insert(0, dataGridViewCheckBoxColumn);

            DataGridShift.Columns[1].Name = "Shift";
            DataGridShift.Columns[1].HeaderText = "Shift";
            DataGridShift.Columns[1].DataPropertyName = "ShiftName";

            DataGridShift.Columns[2].Name = "Shift";
            DataGridShift.Columns[2].HeaderText = "Shift";
            DataGridShift.Columns[2].DataPropertyName = "ShiftId";
            DataGridShift.Columns[2].Visible = false;
            DataGridShift.DataSource = data;
        }
        private void DataGridDepartment_MouseClick(object sender, MouseEventArgs e)
        {

        }

        public void Dept()
        {
            try
            {
                DataTable dt = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetDept_Mast", conn);
                DataGridDepartment.DataSource = null;
                DataGridDepartment.AutoGenerateColumns = false;
                DataGridDepartment.ColumnCount = 2;

                DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                {
                    HeaderText = "Chck",
                    Name = "Chck",
                    Width = 50
                };
                DataGridDepartment.Columns.Insert(0, dataGridViewCheckBoxColumn);

                DataGridDepartment.Columns[1].Name = "Department";
                DataGridDepartment.Columns[1].HeaderText = "Department";
                DataGridDepartment.Columns[1].DataPropertyName = "DeptName";
                DataGridDepartment.Columns[1].Width = 190;

                DataGridDepartment.Columns[2].Name = "Department";
                DataGridDepartment.Columns[2].HeaderText = "Department";
                DataGridDepartment.Columns[2].DataPropertyName = "DeptId";
                DataGridDepartment.Columns[2].Visible = false;
                DataGridDepartment.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        public void Category()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[2].Value.ToString());
            string qur = "select distinct  c.TName as Categroy from emp_mast a inner join Dept_Mast b on a.DeptId=b.DeptId  inner join emp_type c on a.eg=c.TNAME left join tmpitid nn on a.deptid=nn.id ";
            Genclass.cmd = new SqlCommand(qur, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            DatagridCategory.AutoGenerateColumns = false;
            DatagridCategory.Refresh();
            DatagridCategory.DataSource = null;
            DatagridCategory.Rows.Clear();

            DatagridCategory.ColumnCount = tap.Columns.Count + 1;
            Genclass.i = 1;
            foreach (DataColumn column in tap.Columns)
            {
                DatagridCategory.Columns[Genclass.i].Name = column.ColumnName;
                DatagridCategory.Columns[Genclass.i].HeaderText = column.ColumnName;
                DatagridCategory.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                Genclass.i = Genclass.i + 1;
            }

            DatagridCategory.Columns[1].Width = 115;

            DatagridCategory.DataSource = tap;
        }
        public void Group()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[1].Value.ToString());
            string qur1 = "select grname as GroupName,Grid from Gr_Mast ";
            Genclass.cmd = new SqlCommand(qur1, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
        }

        public void Esip()
        {
            //int A = DataGridDepartment.SelectedCells[0].RowIndex;
            //int B = Convert.ToInt32(DataGridDepartment.Rows[A].Cells[1].Value.ToString());
            string qur = "select distinct type  from emp_type ";


            Genclass.cmd = new SqlCommand(qur, conn);

            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);
        }

        private void DataGridDepartment_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                GeneralParameters.BranchId = Convert.ToInt32(CmbBranch.SelectedValue);
                GeneralParameters.Branch = CmbBranch.Text;
                GeneralParameters.Department = string.Empty;
                GeneralParameters.Desingnation = string.Empty;
                GeneralParameters.Shift = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Department == string.Empty)
                        {
                            GeneralParameters.Department = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Department = GeneralParameters.Department + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

                foreach (DataGridViewRow dataGridRow in DatagridCategory.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Desingnation == string.Empty)
                        {
                            GeneralParameters.Desingnation = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Desingnation = GeneralParameters.Desingnation + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

                foreach (DataGridViewRow dataGridRow in DataGridShift.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (GeneralParameters.Shift == string.Empty)
                        {
                            GeneralParameters.Shift = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            GeneralParameters.Shift = GeneralParameters.Shift + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

                if (CmbTag.Visible == true)
                {
                    DailyReport();
                }
                else
                {
                    EmployeeReport();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void EmployeeReport()
        {
            try
            {
                if (cboop.Text == "Employee List")
                {
                    if (GeneralParameters.Department.Length > 2)
                    {
                        GeneralParameters.Department = "1000";

                    }
                    GeneralParameters.ReportId = 7;
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (cboop.Text == "Employee 3 Month Joining List")
                {
                    DateTime str9 = Convert.ToDateTime(Frmdt.Text);

                    int pp = str9.Month - 3;
                    int tt;
                    int yy = str9.Year;
                    if (pp < 0)
                    {
                        yy = (yy - 1);
                        tt = 12 + ((pp));
                    }
                    else
                    {
                        tt = pp;
                    }
                    //str9.Year = Convert.ToDateTime(yy);
                    string quy1 = " exec SP_PROC_GET2MONTHJOIEMPLIST  '" + tt + "','" + yy + "'";
                    Genclass.cmd = new SqlCommand(quy1, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr2.Fill(tap);
                    HFGP.AutoGenerateColumns = false;
                    HFGP.Refresh();
                    HFGP.DataSource = null;
                    HFGP.Rows.Clear();


                    HFGP.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;

                    foreach (DataColumn column in tap.Columns)
                    {
                        HFGP.Columns[Genclass.i].Name = column.ColumnName;
                        HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }

                    HFGP.ColumnCount = 6;

                    HFGP.Columns[0].Name = "EMPCODE";

                    HFGP.Columns[1].Name = "EMPLOYEE NAME";
                    HFGP.Columns[2].Name = "DESIGNATION";
                    HFGP.Columns[3].Name = "BRANCH";
                    HFGP.Columns[4].Name = "DOB";
                    HFGP.Columns[5].Name = "DOJ";

                    HFGP.Columns[0].Width = 150;
                    HFGP.Columns[1].Width = 150;
                    HFGP.Columns[2].Width = 200;
                    HFGP.Columns[3].Width = 200;
                    HFGP.Columns[4].Width = 200;
                    HFGP.Columns[5].Width = 200;

                    HFGP.DataSource = tap;
                    HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                    HFGP.Columns[5].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

                    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

                    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                    app.Visible = true;
                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet.Name = "EMPLOYEE JOINING DETAILS";
                    //worksheet.Cells[1, 10] = "SIMTA MANUFACTURING COMPANY";
                    worksheet.Cells[1, 6] = "3 MONTH EMPLOYEE JOINING DETAILS FROM  DATE '" + Frmdt.Text + "'  ";

                    worksheet.Cells[3, 1] = "EMPCODE";
                    worksheet.Cells[3, 2] = "EMPLOYEE NAME";
                    worksheet.Cells[3, 3] = "DESIGNATION";
                    worksheet.Cells[3, 4] = "BRANCH";
                    worksheet.Cells[3, 5] = "DOB";
                    worksheet.Cells[3, 6] = "DOJ";


                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A1", "F1");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A2", "F2");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A3", "F3");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);



                    worksheet.get_Range("A1", "F1").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    worksheet.get_Range("A2", "F2").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


                    worksheet.Range["A1:F1"].MergeCells = true;

                    worksheet.Range["A2:F2"].MergeCells = true;

                    //worksheet.Range["A3:W3"].MergeCells = true;

                    worksheet.Range["A3"].ColumnWidth = 15;
                    worksheet.Range["B3"].ColumnWidth = 15;
                    worksheet.Range["C3"].ColumnWidth = 18;
                    worksheet.Range["D3"].ColumnWidth = 15;
                    worksheet.Range["E3"].ColumnWidth = 15;

                    for (int i = 0; i < HFGP.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < HFGP.Columns.Count; j++)
                        {
                            if (HFGP.Rows[i].Cells[j].Value.ToString() != null)
                            {
                                worksheet.Cells[i + 5, j + 1] = HFGP.Rows[i].Cells[j].Value.ToString();
                            }
                        }
                    }

                }
                else if (cboop.Text == "Employee 6 Month Joining List")
                {
                    DateTime str9 = Convert.ToDateTime(Frmdt.Text);
                    int pp = str9.Month - 6;
                    int tt;
                    int yy = str9.Year;
                    if (pp < 0)
                    {


                        yy = (yy - 1);
                        tt = 12 + ((pp));
                    }
                    else
                    {
                        tt = pp;
                    }
                    //str9.Year = Convert.ToDateTime(yy);
                    string quy1 = " exec SP_PROC_GET2MONTHJOIEMPLIST  '" + tt + "','" + yy + "'";
                    Genclass.cmd = new SqlCommand(quy1, conn);
                    SqlDataAdapter aptr2 = new SqlDataAdapter(Genclass.cmd);
                    DataTable tap = new DataTable();
                    aptr2.Fill(tap);
                    HFGP.AutoGenerateColumns = false;
                    HFGP.Refresh();
                    HFGP.DataSource = null;
                    HFGP.Rows.Clear();


                    HFGP.ColumnCount = tap.Columns.Count;
                    Genclass.i = 0;

                    foreach (DataColumn column in tap.Columns)
                    {
                        HFGP.Columns[Genclass.i].Name = column.ColumnName;
                        HFGP.Columns[Genclass.i].HeaderText = column.ColumnName;
                        HFGP.Columns[Genclass.i].DataPropertyName = column.ColumnName;
                        Genclass.i = Genclass.i + 1;
                    }

                    HFGP.ColumnCount = 6;

                    HFGP.Columns[0].Name = "EMPCODE";

                    HFGP.Columns[1].Name = "EMPLOYEE NAME";
                    HFGP.Columns[2].Name = "DESIGNATION";
                    HFGP.Columns[3].Name = "BRANCH";
                    HFGP.Columns[4].Name = "DOB";
                    HFGP.Columns[5].Name = "DOJ";

                    HFGP.Columns[0].Width = 150;
                    HFGP.Columns[1].Width = 150;
                    HFGP.Columns[2].Width = 200;
                    HFGP.Columns[3].Width = 200;
                    HFGP.Columns[4].Width = 200;
                    HFGP.Columns[5].Width = 200;

                    HFGP.DataSource = tap;
                    HFGP.Columns[4].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";
                    HFGP.Columns[5].DefaultCellStyle.Format = "yyyy'/'MM'/'dd";


                    Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

                    Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

                    Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                    app.Visible = true;
                    worksheet = workbook.Sheets["Sheet1"];
                    worksheet.Name = "EMPLOYEE JOINING DETAILS";
                    //worksheet.Cells[1, 10] = "SIMTA MANUFACTURING COMPANY";
                    worksheet.Cells[1, 6] = "6 MONTH EMPLOYEE JOINING DETAILS FROM  DATE '" + Frmdt.Text + "'  ";





                    worksheet.Cells[3, 1] = "EMPCODE";
                    worksheet.Cells[3, 2] = "EMPLOYEE NAME";
                    worksheet.Cells[3, 3] = "DESIGNATION";
                    worksheet.Cells[3, 4] = "BRANCH";
                    worksheet.Cells[3, 5] = "DOB";
                    worksheet.Cells[3, 6] = "DOJ";


                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A1", "F1");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A2", "F2");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
                    range2.EntireRow.Font.Name = "Calibri";
                    range2.EntireRow.Font.Bold = true;
                    range2.EntireRow.Font.Size = 16;
                    range2 = worksheet.get_Range("A3", "F3");
                    range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);



                    worksheet.get_Range("A1", "F1").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    worksheet.get_Range("A2", "F2").Style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


                    worksheet.Range["A1:F1"].MergeCells = true;

                    worksheet.Range["A2:F2"].MergeCells = true;

                    //worksheet.Range["A3:W3"].MergeCells = true;

                    worksheet.Range["A3"].ColumnWidth = 15;
                    worksheet.Range["B3"].ColumnWidth = 15;
                    worksheet.Range["C3"].ColumnWidth = 18;
                    worksheet.Range["D3"].ColumnWidth = 15;
                    worksheet.Range["E3"].ColumnWidth = 15;

                    for (int i = 0; i < HFGP.Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < HFGP.Columns.Count; j++)
                        {
                            if (HFGP.Rows[i].Cells[j].Value.ToString() != null)
                            {
                                worksheet.Cells[i + 5, j + 1] = HFGP.Rows[i].Cells[j].Value.ToString();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void DailyReport()
        {
            try
            {
                if (CmbTag.Text == "Daily Punch Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 1;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "PP";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "EPF Detail Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    DataTable dt1 = new DataTable();
                    SqlParameter[] PayParameters1 = {

                               new SqlParameter("@monid",date.Month.ToString()),
                              new SqlParameter("@BRANCH",CmbBranch.Text),


                    };
                    dt1 = db.GetData(CommandType.StoredProcedure, "sp_getepfrpt", PayParameters1);

                    LoadDataTable12(dt1);
                    Loadepfexcel();

                }
                else if (CmbTag.Text == "EPF Report")
                {
                    int pp, MM;
                    string qur1 = "exec sp_GETDAYS '" + Convert.ToDateTime(Frmdt.Text).ToString("yyyy-MM-dd") + "'";
                    SqlCommand cmd = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);

                    pp = Convert.ToInt32(tab.Rows[0]["days"].ToString());
                    GeneralParameters.ReportDate = Frmdt.Text;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    DataTable dt1 = new DataTable();
                    string qur2 = "SELECT * FROM  PAYMONTH WHERE  MONTH(MSDT)='" + date.Month.ToString() + "'";
                    SqlCommand cmd1 = new SqlCommand(qur2, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);

                    MM = Convert.ToInt32(tab1.Rows[0]["MONID"].ToString());



                    SqlParameter[] PayParameters1 = {

                              new SqlParameter("@monid",MM),
                              new SqlParameter("@BRANCH",CmbBranch.Text),
                              new SqlParameter("@DAYS",pp),

                    };
                    dt1 = db.GetData(CommandType.StoredProcedure, "sp_getepfrpt1", PayParameters1);

                    LoadDataTable123(dt1);
                    Loadepfexcel12();

                }
                else if (CmbTag.Text == "ESI Report")
                {
                    int pp, MM;
                    string qur1 = "exec sp_GETDAYS '" + Convert.ToDateTime(Frmdt.Text).ToString("yyyy-MM-dd") + "'";
                    SqlCommand cmd = new SqlCommand(qur1, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    DataTable tab = new DataTable();
                    apt.Fill(tab);

                    pp = Convert.ToInt32(tab.Rows[0]["days"].ToString());
                    GeneralParameters.ReportDate = Frmdt.Text;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    DataTable dt1 = new DataTable();
                    string qur2 = "SELECT * FROM  PAYMONTH WHERE  MONTH(MSDT)='" + date.Month.ToString() + "'";
                    SqlCommand cmd1 = new SqlCommand(qur2, conn);
                    SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                    DataTable tab1 = new DataTable();
                    apt1.Fill(tab1);

                    MM = Convert.ToInt32(tab1.Rows[0]["MONID"].ToString());

                    SqlParameter[] PayParameters1 = {

                               new SqlParameter("@monid",MM),
                              new SqlParameter("@BRANCH",CmbBranch.Text),
                                new SqlParameter("@DAYS",pp),

                    };
                    dt1 = db.GetData(CommandType.StoredProcedure, "sp_getesirpt", PayParameters1);

                    LoadDataTable1234(dt1);
                    Loadepfexcel1();

                }
                else if (CmbTag.Text == "Irregular Punch Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 2;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "Ir";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Without Leave Employess List")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 21;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "p";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "With Leave Employees List")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 22;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.LVTAG = "p";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Left Employee List")
                {

                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 20;
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Absent List")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 2;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    GeneralParameters.Shift = "0";
                    GeneralParameters.LVTAG = "AA";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Strength Report")
                {
                    if (GeneralParameters.Shift.Length > 2)
                    {
                        GeneralParameters.Shift = "1000";
                    }
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 3;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "In Report")
                {
                    if (GeneralParameters.Shift.Length > 2)
                    {
                        MessageBox.Show("Can View Single Shift Only", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 4;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Worked Hour Report (< 8 Hrs)")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 5;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Lunch Break Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 6;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Manul Punch Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 10;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    //GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "Muster Roll Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 8;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }
                else if (CmbTag.Text == "MusterRoll (1 to 15 days)" || CmbTag.Text == "MusterRoll (16 to 31 days)")
                {
                    GeneralParameters.MusterTag = 2;
                    FrmMuster25B frmMB = new FrmMuster25B
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    frmMB.ShowDialog();
                }
                else if (CmbTag.Text == "Late In Report")
                {
                    GeneralParameters.ReportDate = Frmdt.Text;
                    GeneralParameters.ReportId = 9;
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";
                    FrmReportviewer crv = new FrmReportviewer
                    {
                        StartPosition = FormStartPosition.CenterScreen
                    };
                    crv.ShowDialog();
                }

                else if (CmbTag.Text == "Late Generations Report")
                {
                    DataTable dt = new DataTable();
                    DataTable dt1 = new DataTable();
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    GeneralParameters.TablName = "ATT_REG" + date.Month.ToString("00") + "";

                    SqlParameter[] PayParameters = {

                        new SqlParameter("@TBL",GeneralParameters.TablName),
                          new SqlParameter("@BRANCH",GeneralParameters.Branch),
                        new SqlParameter("@FDT",date.Month.ToString()),

                    };
                    dt = db.GetData(CommandType.StoredProcedure, "SP_GETLATESUMMARY", PayParameters);

                    LoadDataTable1(dt);
                    SqlParameter[] PayParameters1 = {

                        new SqlParameter("@TBL",GeneralParameters.TablName),
                              new SqlParameter("@BRANCH",GeneralParameters.Branch),
                                new SqlParameter("@FDT",date.Month.ToString()),

                    };
                    dt1 = db.GetData(CommandType.StoredProcedure, "SP_GETLATEDET", PayParameters1);

                    LoadDataTable(dt1);
                    Loadexcel();
                }
                else if (CmbTag.Text == "Leave Carry Over Report")
                {
                    DataTable dt1 = new DataTable();
                    DateTime date = Convert.ToDateTime(Frmdt.Text);
                    SqlParameter[] sqlParameters = {

                            new SqlParameter("@Monid",date.Month.ToString()),
                              new SqlParameter("@BRANCH",GeneralParameters.Branch)
                        };
                    //DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GETSALARYREGleavecarry", sqlParameters, conn);
                    dt1 = db.GetData(CommandType.StoredProcedure, "PROC_GETSALARYREGleavecarry", sqlParameters);
                    LoadDataTable2(dt1);
                    loadleavedetails();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void loadleavedetails()
        {
            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;







            worksheet = workbook.Sheets["Sheet1"];


            worksheet.Name = "LEAVE CARRY OVER REPORT";



            worksheet.Cells[1, 8] = "RAHOBOTH";

            worksheet.Cells[2, 8] = "LEAVE CARRY OVER REPORT";

            for (int x = 1; x < datas.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = datas.Columns[x - 1].HeaderText;
            }
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "H1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "H2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 30;
            worksheet.Range["B3"].ColumnWidth = 30;
            worksheet.Range["C3"].ColumnWidth = 30;
            worksheet.Range["D3"].ColumnWidth = 30;
            worksheet.Range["E3"].ColumnWidth = 30;
            worksheet.Range["F3"].ColumnWidth = 30;
            worksheet.Range["G3"].ColumnWidth = 30;
            worksheet.Range["H3"].ColumnWidth = 30;
            worksheet.Range["A1:H1"].MergeCells = true;
            worksheet.Range["A2:H2"].MergeCells = true;
            for (int i = 0; i < datas.Rows.Count - 1; i++)
            {
                for (int j = 0; j < datas.Columns.Count; j++)
                {

                    worksheet.Cells[i + 4, j + 1] = datas.Rows[i].Cells[j].Value.ToString();
                }
            }

        }
        protected void LoadDataTable2(DataTable dt1)
        {
            try
            {

                this.datas.DefaultCellStyle.Font = new Font("calibri", 10);
                this.datas.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                datas.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                datas.EnableHeadersVisualStyles = false;
                datas.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                datas.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                datas.RowHeadersVisible = false;
                datas.DataSource = null;

                bs.DataSource = dt1;
                datas.DataSource = bs;
                datas.Columns[0].Width = 100;
                datas.Columns[1].Width = 100;
                datas.Columns[2].Width = 100;
                datas.Columns[3].Width = 250;
                datas.Columns[4].Width = 250;
                datas.Columns[5].Width = 100;
                datas.Columns[6].Width = 100;
                datas.Columns[7].Width = 90;
                //RQGR.Columns[8].Width = 90;
                //RQGR.Columns[9].Width = 90;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadDataTable1(DataTable dt)
        {
            try
            {

                this.datas.DefaultCellStyle.Font = new Font("calibri", 10);
                this.datas.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                datas.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                datas.EnableHeadersVisualStyles = false;
                datas.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                datas.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                datas.RowHeadersVisible = false;
                datas.DataSource = null;

                bs.DataSource = dt;
                datas.DataSource = bs;
                datas.Columns[0].Width = 100;
                datas.Columns[1].Width = 100;
                datas.Columns[2].Width = 100;
                datas.Columns[3].Width = 250;
                datas.Columns[4].Width = 250;
                //datas.Columns[5].Width = 100;
                //datas.Columns[6].Width = 100;
                //RQGR.Columns[7].Width = 90;
                //RQGR.Columns[8].Width = 90;
                //RQGR.Columns[9].Width = 90;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadDataTable(DataTable dt1)
        {
            try
            {

                this.data1.DefaultCellStyle.Font = new Font("calibri", 10);
                this.data1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                data1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                data1.EnableHeadersVisualStyles = false;
                data1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                data1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                data1.RowHeadersVisible = false;
                data1.DataSource = null;

                bsc.DataSource = dt1;
                data1.DataSource = bsc;
                data1.Columns[0].Width = 100;
                data1.Columns[1].Width = 100;
                data1.Columns[2].Width = 100;
                data1.Columns[3].Width = 250;
                data1.Columns[4].Width = 250;
                data1.Columns[5].Width = 100;
                //data1.Columns[6].Width = 100;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void LoadDataTable123(DataTable dt1)
        {
            try
            {

                this.data1.DefaultCellStyle.Font = new Font("calibri", 10);
                this.data1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                data1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                data1.EnableHeadersVisualStyles = false;
                data1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                data1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                data1.RowHeadersVisible = false;
                data1.DataSource = null;

                bsc.DataSource = dt1;
                data1.DataSource = bsc;
                data1.Columns[0].Width = 100;
                data1.Columns[1].Width = 100;
                data1.Columns[2].Width = 100;
                data1.Columns[3].Width = 250;
                data1.Columns[4].Width = 250;
                data1.Columns[5].Width = 100;
                data1.Columns[6].Width = 100;
                data1.Columns[7].Width = 100;
                data1.Columns[8].Width = 250;
                data1.Columns[9].Width = 100;
                data1.Columns[10].Width = 100;
                data1.Columns[11].Width = 100;
                data1.Columns[12].Width = 100;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadDataTable1234(DataTable dt1)
        {
            try
            {

                this.data1.DefaultCellStyle.Font = new Font("calibri", 10);
                this.data1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                data1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                data1.EnableHeadersVisualStyles = false;
                data1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                data1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                data1.RowHeadersVisible = false;
                data1.DataSource = null;

                bsc.DataSource = dt1;
                data1.DataSource = bsc;
                data1.Columns[0].Width = 100;
                data1.Columns[1].Width = 100;
                data1.Columns[2].Width = 100;
                data1.Columns[3].Width = 250;
                data1.Columns[4].Width = 250;
                data1.Columns[5].Width = 100;
                data1.Columns[6].Width = 100;
                data1.Columns[7].Width = 100;
                data1.Columns[8].Width = 250;
                data1.Columns[9].Width = 100;



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadDataTable12(DataTable dt1)
        {
            try
            {

                this.data1.DefaultCellStyle.Font = new Font("calibri", 10);
                this.data1.ColumnHeadersDefaultCellStyle.Font = new Font("calibri", 10, FontStyle.Bold);
                data1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                data1.EnableHeadersVisualStyles = false;
                data1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                data1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                data1.RowHeadersVisible = false;
                data1.DataSource = null;

                bsc.DataSource = dt1;
                data1.DataSource = bsc;
                data1.Columns[0].Width = 100;
                data1.Columns[1].Width = 100;
                data1.Columns[2].Width = 100;
                data1.Columns[3].Width = 250;
                data1.Columns[4].Width = 250;
                data1.Columns[5].Width = 100;
                data1.Columns[6].Width = 100;
                data1.Columns[7].Width = 100;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void Loadexcel()
        {

            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;







            worksheet = workbook.Sheets["Sheet1"];


            worksheet.Name = "LATE IN SUMMARY REPORT";



            worksheet.Cells[1, 5] = "RAHOBOTH";

            worksheet.Cells[2, 5] = "LATE IN SUMMARY REPORT";

            for (int x = 1; x < datas.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = datas.Columns[x - 1].HeaderText;
            }
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "E1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "E2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 30;
            worksheet.Range["B3"].ColumnWidth = 30;
            worksheet.Range["C3"].ColumnWidth = 30;
            worksheet.Range["D3"].ColumnWidth = 30;
            worksheet.Range["E3"].ColumnWidth = 30;
            worksheet.Range["A1:E1"].MergeCells = true;
            worksheet.Range["A2:E2"].MergeCells = true;
            for (int i = 0; i < datas.Rows.Count; i++)
            {
                for (int j = 0; j < datas.Columns.Count; j++)
                {

                    worksheet.Cells[i + 4, j + 1] = datas.Rows[i].Cells[j].Value.ToString();
                }
            }

            worksheet = workbook.Sheets["Sheet2"];
            worksheet.Name = "LATE IN REPORT- DETAILED";






            worksheet.Cells[1, 7] = "RAHOBOTH";

            worksheet.Cells[2, 7] = "LATE IN REPORT- DETAILED";

            for (int x = 1; x < data1.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = data1.Columns[x - 1].HeaderText;
            }

            Microsoft.Office.Interop.Excel.Range range3 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range3.EntireRow.Font.Name = "Calibri";
            range3.EntireRow.Font.Bold = true;
            range3.EntireRow.Font.Size = 16;
            range3 = worksheet.get_Range("A1", "G1");
            range3.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range3.EntireRow.Font.Name = "Calibri";
            range3.EntireRow.Font.Bold = true;
            range3.EntireRow.Font.Size = 16;
            range3 = worksheet.get_Range("A2", "G2");
            range3.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 20;
            worksheet.Range["B3"].ColumnWidth = 30;
            worksheet.Range["C3"].ColumnWidth = 30;
            worksheet.Range["D3"].ColumnWidth = 30;
            worksheet.Range["E3"].ColumnWidth = 20;
            worksheet.Range["F3"].ColumnWidth = 20;
            worksheet.Range["G3"].ColumnWidth = 20;
            worksheet.Range["A1:G1"].MergeCells = true;
            worksheet.Range["A2:G2"].MergeCells = true;



            for (int i = 0; i < data1.Rows.Count; i++)
            {
                for (int j = 0; j < data1.Columns.Count; j++)
                {
                    worksheet.Cells[i + 6, j + 1] = data1.Rows[i].Cells[j].Value.ToString();
                }
            }


        }
        private void Buttnext1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chckDepartment.Checked == true)
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DataGridDepartment.RowCount; i++)
                {
                    DataGridDepartment[0, i].Value = false;
                }
            }
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (s.Checked == true)
            {
                for (int i = 0; i < DatagridCategory.RowCount; i++)
                {
                    DatagridCategory[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DatagridCategory.RowCount; i++)
                {
                    DatagridCategory[0, i].Value = false;
                }
            }
        }
        private void Loadepfexcel()
        {

            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;







            worksheet = workbook.Sheets["Sheet1"];


            worksheet.Name = "EPF REPORT";



            worksheet.Cells[1, 8] = "RAHOBOTH";

            worksheet.Cells[2, 8] = "EPF REPORT";

            for (int x = 1; x < datas.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = datas.Columns[x - 1].HeaderText;
            }
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "I1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "I2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 30;
            worksheet.Range["B3"].ColumnWidth = 30;
            worksheet.Range["C3"].ColumnWidth = 30;
            worksheet.Range["D3"].ColumnWidth = 30;
            worksheet.Range["E3"].ColumnWidth = 30;
            worksheet.Range["F3"].ColumnWidth = 30;
            worksheet.Range["G3"].ColumnWidth = 30;
            worksheet.Range["H3"].ColumnWidth = 30;
            worksheet.Range["I3"].ColumnWidth = 30;
            worksheet.Range["A1:I1"].MergeCells = true;
            worksheet.Range["A2:I2"].MergeCells = true;
            for (int i = 0; i < datas.Rows.Count; i++)
            {
                for (int j = 0; j < datas.Columns.Count; j++)
                {

                    worksheet.Cells[i + 4, j + 1] = datas.Rows[i].Cells[j].Value.ToString();
                }
            }
        }
        private void Loadepfexcel1()
        {
            Genclass.sum4 = 0; Genclass.sum2 = 0; Genclass.sum3 = 0; Genclass.sum1 = 0;
            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;







            worksheet = workbook.Sheets["Sheet1"];

            if (CmbTag.Text == "ESI Report")
            {
                worksheet.Name = "ESI REPORT";



                worksheet.Cells[1, 13] = "REHOBOTH";

                worksheet.Cells[2, 13] = "ESI REPORT";
            }
            else
            {
                worksheet.Name = "EPF REPORT";



                worksheet.Cells[1, 13] = "REHOBOTH";

                worksheet.Cells[2, 13] = "EPF REPORT";

            }

            for (int x = 1; x < data1.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = data1.Columns[x - 1].HeaderText;
            }
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "M1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "M2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 10;
            worksheet.Range["B3"].ColumnWidth = 10;
            worksheet.Range["C3"].ColumnWidth = 15;
            worksheet.Range["D3"].ColumnWidth = 20;
            worksheet.Range["E3"].ColumnWidth = 20;
            worksheet.Range["F3"].ColumnWidth = 10;
            worksheet.Range["G3"].ColumnWidth = 15;
            worksheet.Range["H3"].ColumnWidth = 20;
            worksheet.Range["I3"].ColumnWidth = 20;
            worksheet.Range["J3"].ColumnWidth = 10;
            worksheet.Range["K3"].ColumnWidth = 10;
            worksheet.Range["L3"].ColumnWidth = 10;
            worksheet.Range["M3"].ColumnWidth = 10;

            worksheet.Range["A1:M1"].MergeCells = true;
            worksheet.Range["A2:M2"].MergeCells = true;
            Genclass.sum = data1.Rows.Count;
            for (int i = 0; i < data1.Rows.Count; i++)
            {
                for (int j = 0; j < data1.Columns.Count; j++)
                {

                    worksheet.Cells[i + 4, j + 1] = data1.Rows[i].Cells[j].Value.ToString();

                    if (j == 6 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(data1.Rows[i].Cells[6].Value.ToString());
                    }
                    else if (j == 7 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(data1.Rows[i].Cells[7].Value.ToString());
                    }
                    else if (j == 8 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(data1.Rows[i].Cells[8].Value.ToString());
                    }
                    else if (j == 9 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(data1.Rows[i].Cells[9].Value.ToString());
                    }
                }

            }
            Genclass.sum = Genclass.sum + 4;
            worksheet.Cells[Genclass.sum, 6] = "TOTAL";
            worksheet.Cells[Genclass.sum, 7] = Genclass.sum1;
            worksheet.Cells[Genclass.sum, 8] = Genclass.sum2;
            worksheet.Cells[Genclass.sum, 9] = Genclass.sum3;
            worksheet.Cells[Genclass.sum, 10] = Genclass.sum4;
        }
        private void Loadepfexcel12()
        {
            Genclass.sum4 = 0; Genclass.sum2 = 0; Genclass.sum3 = 0; Genclass.sum1 = 0; Genclass.sum5 = 0; Genclass.sum6 = 0;
            DateTime str9 = Convert.ToDateTime(Frmdt.Text);
            Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);

            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            app.Visible = true;







            worksheet = workbook.Sheets["Sheet1"];

            if (CmbTag.Text == "ESI Report")
            {
                worksheet.Name = "ESI REPORT";



                worksheet.Cells[1, 13] = "REHOBOTH";

                worksheet.Cells[2, 13] = "ESI REPORT";
            }
            else
            {
                worksheet.Name = "EPF REPORT";



                worksheet.Cells[1, 13] = "REHOBOTH";

                worksheet.Cells[2, 13] = "EPF REPORT";

            }

            for (int x = 1; x < data1.Columns.Count + 1; x++)
            {
                worksheet.Cells[3, x] = data1.Columns[x - 1].HeaderText;
            }
            Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range;
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A1", "M1");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);
            range2.EntireRow.Font.Name = "Calibri";
            range2.EntireRow.Font.Bold = true;
            range2.EntireRow.Font.Size = 16;
            range2 = worksheet.get_Range("A2", "M2");
            range2.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);

            worksheet.Range["A3"].ColumnWidth = 10;
            worksheet.Range["B3"].ColumnWidth = 10;
            worksheet.Range["C3"].ColumnWidth = 15;
            worksheet.Range["D3"].ColumnWidth = 20;
            worksheet.Range["E3"].ColumnWidth = 20;
            worksheet.Range["F3"].ColumnWidth = 15;
            worksheet.Range["G3"].ColumnWidth = 15;
            worksheet.Range["H3"].ColumnWidth = 20;
            worksheet.Range["I3"].ColumnWidth = 20;
            worksheet.Range["J3"].ColumnWidth = 25;
            worksheet.Range["K3"].ColumnWidth = 25;
            worksheet.Range["L3"].ColumnWidth = 10;
            worksheet.Range["M3"].ColumnWidth = 10;

            worksheet.Range["A1:M1"].MergeCells = true;
            worksheet.Range["A2:M2"].MergeCells = true;
            Genclass.sum = data1.Rows.Count;
            for (int i = 0; i < data1.Rows.Count; i++)
            {
                for (int j = 0; j < data1.Columns.Count; j++)
                {

                    worksheet.Cells[i + 4, j + 1] = data1.Rows[i].Cells[j].Value.ToString();

                    if (j == 7 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum1 = Genclass.sum1 + Convert.ToDouble(data1.Rows[i].Cells[7].Value.ToString());
                    }
                    else if (j == 8 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum2 = Genclass.sum2 + Convert.ToDouble(data1.Rows[i].Cells[8].Value.ToString());
                    }
                    else if (j == 9 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum3 = Genclass.sum3 + Convert.ToDouble(data1.Rows[i].Cells[9].Value.ToString());
                    }
                    else if (j == 10 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum4 = Genclass.sum4 + Convert.ToDouble(data1.Rows[i].Cells[10].Value.ToString());
                    }
                    else if (j == 11 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum5 = Genclass.sum5 + Convert.ToDouble(data1.Rows[i].Cells[11].Value.ToString());
                    }
                    else if (j == 12 && data1.Rows[i].Cells[j].Value.ToString() != "")
                    {

                        Genclass.sum6 = Genclass.sum6 + Convert.ToDouble(data1.Rows[i].Cells[12].Value.ToString());
                    }
                }

            }
            Genclass.sum = Genclass.sum + 4;
            worksheet.Cells[Genclass.sum, 7] = "TOTAL";
            worksheet.Cells[Genclass.sum, 8] = Genclass.sum1;
            worksheet.Cells[Genclass.sum, 9] = Genclass.sum2;
            worksheet.Cells[Genclass.sum, 10] = Genclass.sum3;
            worksheet.Cells[Genclass.sum, 11] = Genclass.sum4;
            worksheet.Cells[Genclass.sum, 12] = Genclass.sum5;
            worksheet.Cells[Genclass.sum, 13] = Genclass.sum6;
        }
        private void DataGridDepartment_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            ch1 = (DataGridViewCheckBoxCell)DataGridDepartment.Rows[DataGridDepartment.CurrentRow.Index].Cells[0];

            if (ch1.Value == null)
            {
                ch1.Value = false;
            }
            switch (ch1.Value.ToString())
            {
                case "True":
                    {
                        ch1.Value = false;
                        break;
                    }
                case "False":
                    {
                        ch1.Value = true;
                        break;
                    }
            }
        }
        public void Allgrid()
        {
            try
            {
                string DeptId = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (DeptId == string.Empty)
                        {
                            DeptId = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            DeptId = DeptId + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid ";
            qur.ExecuteNonQuery();
            int i = 0;
            foreach (DataGridViewRow row in DataGridDepartment.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {

                    Genclass.Gbtxtid = Convert.ToInt16(DataGridDepartment.Rows[i].Cells[2].Value.ToString());

                    qur.CommandText = "delete from tmpitid where id=" + Genclass.Gbtxtid + "";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid values (" + Genclass.Gbtxtid + ")";
                    qur.ExecuteNonQuery();
                }
                i = i + 1;
            }
            DatagridCategory.Visible = true;
            s.Visible = true;
            Titlep3();
            Category();
            Titlep1();
            Group();
            if (Genclass.Dtype == 1)
            {
                Titlep4();
                Esip();
            }
        }
        public void Gridcategory()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid2 ";
            qur.ExecuteNonQuery();
            int k = 0;
            foreach (DataGridViewRow row in DatagridCategory.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {

                    Genclass.bomstr = Convert.ToString(DatagridCategory.Rows[k].Cells[1].Value.ToString());

                    qur.CommandText = "delete from tmpitid2 where category='" + Genclass.bomstr + "'";
                    qur.ExecuteNonQuery();
                    qur.CommandText = "insert into tmpitid2 values ('" + Genclass.bomstr + "')";
                    qur.ExecuteNonQuery();
                }
                k = k + 1;
            }
        }
        public void Gridgroup()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid1 ";
            qur.ExecuteNonQuery();
        }

        public void Gridpfesi()
        {
            conn.Close();
            conn.Open();
            qur.CommandText = "truncate  table tmpitid3 ";
            qur.ExecuteNonQuery();
        }

        private void DatagridCategory_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //    DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
            //    ch1 = (DataGridViewCheckBoxCell)DatagridCategory.Rows[DatagridCategory.CurrentRow.Index].Cells[0];

            //    if (ch1.Value == null)
            //    {
            //        ch1.Value = false;
            //    }
            //    switch (ch1.Value.ToString())
            //    {
            //        case "True":
            //            {
            //                ch1.Value = false;

            //                break;
            //            }
            //        case "False":
            //            {
            //                ch1.Value = true;
            //                //Where should I put the selected cell here?
            //                break;
            //            }
            //    }
            //    Gridcategory();
        }

        private void DatagridCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DatagridGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GrpboxFront_Enter(object sender, EventArgs e)
        {

        }

        private void BtnAttendanceProcess_Click(object sender, EventArgs e)
        {
            try
            {
                int Process = 0;
                DateTime dateTime = Convert.ToDateTime(dtpFromDate.Text);
                string check = "Select *from Att_Reg" + dateTime.Month.ToString("00") + " Where Dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "'";
                DataTable dt = db.GetDataWithoutParam(CommandType.Text, check, conn);
                if (dt.Rows.Count == 0)
                {
                    Process = 1;
                }
                else
                {
                    DialogResult result = MessageBox.Show("Already Attendance Processed for this date Do you Want Re process this date?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        Process = 1;
                    }
                    else
                    {
                        Process = 0;
                    }
                }
                if (Process == 1)
                {
                    string tablename = "ATT_REG" + Convert.ToDateTime(dtpFromDate.Text).Month.ToString("00") + "";
                    SqlParameter[] parameters = {
                        new SqlParameter("@FDt",Convert.ToDateTime(dtpFromDate.Text)),
                        new SqlParameter("@TDt",Convert.ToDateTime(DtpToTate.Text)),
                        new SqlParameter("@TBL",tablename),
                        new SqlParameter("@BranchId",CmbBranch.SelectedValue),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_ATTPROCESS", parameters, conn);
                    MessageBox.Show("Process Completed Successfully");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BtnShow_Click(object sender, EventArgs e)
        {
            try
            {
                DatagridCategory.DataSource = null;
                string DeptId = string.Empty;
                foreach (DataGridViewRow dataGridRow in DataGridDepartment.Rows)
                {
                    if (dataGridRow.Cells["Chck"].Value != null && (bool)dataGridRow.Cells["Chck"].Value)
                    {
                        if (DeptId == string.Empty)
                        {
                            DeptId = dataGridRow.Cells[2].Value.ToString();
                        }
                        else
                        {
                            DeptId = DeptId + "," + dataGridRow.Cells[2].Value.ToString();
                        }
                    }
                }
                if (DeptId != string.Empty)
                {
                    string Query = @"Select Distinct b.GrName,b.Grid from EMp_Mast a 
                                Inner join Gr_Mast b On a.GrId = b.Grid Where a.DeptId in (" + DeptId + ")";
                    DataTable dt = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    DatagridCategory.ColumnCount = 2;
                    DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn
                    {
                        HeaderText = "Chck",
                        Name = "Chck",
                        Width = 50
                    };
                    DatagridCategory.Columns.Insert(0, dataGridViewCheckBoxColumn);

                    DatagridCategory.Columns[1].Name = "GrName";
                    DatagridCategory.Columns[1].HeaderText = "Grade";
                    DatagridCategory.Columns[1].DataPropertyName = "GrName";
                    DatagridCategory.Columns[1].Width = 170;

                    DatagridCategory.Columns[2].Name = "Grid";
                    DatagridCategory.Columns[2].HeaderText = "Grid";
                    DatagridCategory.Columns[2].DataPropertyName = "Grid";
                    DatagridCategory.Columns[2].Visible = false;
                    DatagridCategory.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Information");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (ChckShift.Checked == true)
            {
                for (int i = 0; i < DataGridShift.RowCount; i++)
                {
                    DataGridShift[0, i].Value = true;
                }
            }
            else
            {
                for (int i = 0; i < DataGridShift.RowCount; i++)
                {
                    DataGridShift[0, i].Value = false;
                }
            }
        }

        private void CmbTag_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboop_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboop.Text == "Employee 3 Month Joining List" || cboop.Text == "Employee 6 Month Joining List")
            {
                Frmdt.Visible = true;
                Frmdt.Value = DateTime.Now;
                DateTime uu = Convert.ToDateTime(Frmdt.Text);
                Genclass.mon = Convert.ToInt32(uu.Month);
                Label1.Visible = true;
            }
            else
            {
                Frmdt.Visible = false;
                Label1.Visible = false;

            }

        }

        private void BtnImport_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connstretime"].ConnectionString);
                    DateTime FromDate = Convert.ToDateTime(dtpFromDate.Text);
                    DateTime ToDate = Convert.ToDateTime(DtpToTate.Text);
                    string TableName = "DEVICELOGS_" + FromDate.Month + "_" + FromDate.Year + "";
                    SqlParameter[] sqlParameters = {
                    new SqlParameter("@FDT",FromDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TDT",ToDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",TableName),
                };
                    DataTable dtexport = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_EXP_DATA", sqlParameters, sqlConnection);

                    DataTable dtexcelData = new DataTable();
                    dtexcelData.Columns.Add("Dt", typeof(DateTime));
                    dtexcelData.Columns.Add("Userid", typeof(int));
                    dtexcelData.Columns.Add("InT", typeof(DateTime));
                    dtexcelData.Columns.Add("OutT", typeof(DateTime));
                    for (int i = 0; i < dtexport.Rows.Count; i++)
                    {
                        DataRow dataRow = dtexcelData.NewRow();
                        dataRow["Dt"] = dtexport.Rows[i]["Dt"].ToString();
                        dataRow["Userid"] = dtexport.Rows[i]["Userid"].ToString();
                        if (string.IsNullOrEmpty(dtexport.Rows[i]["InT"].ToString()))
                        {
                            dataRow["InT"] = DBNull.Value;
                        }
                        else
                        {
                            dataRow["InT"] = dtexport.Rows[i]["InT"].ToString();
                        }
                        if (string.IsNullOrEmpty(dtexport.Rows[i]["OutT"].ToString()))
                        {
                            dataRow["OutT"] = DBNull.Value;
                        }
                        else
                        {
                            dataRow["OutT"] = dtexport.Rows[i]["OutT"].ToString();
                        }

                        dtexcelData.Rows.Add(dataRow);
                    }
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString))
                    {
                        string query = "truncate table  AttData ";
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(query, conn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        conn.Close();
                        conn.Open();
                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.AttData";
                            sqlbulkcopy.ColumnMappings.Add("Dt", "Dt");
                            sqlbulkcopy.ColumnMappings.Add("Userid", "Userid");
                            sqlbulkcopy.ColumnMappings.Add("InT", "InT");
                            sqlbulkcopy.ColumnMappings.Add("OutT", "OutT");
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();
                        }
                    }
                    MessageBox.Show("Imported Successfully", "Information");
                    BtnAttendanceProcess_Click(sender, e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.StackTrace);
                    return;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            groupBox1.Visible = false;
        }
    }
}
