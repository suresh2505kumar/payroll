﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public partial class FrmEmpPay : Form
    {
        public FrmEmpPay()
        {
            InitializeComponent();
        }
        string connstring = string.Empty;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        BindingSource bsp = new BindingSource();
        BindingSource bsOTEntry = new BindingSource();
        DataTable dtexcelData = new DataTable();
        BindingSource bsSalary = new BindingSource();

        decimal totalid = 0;
        int Fillid;
        string str1key;
        public int SelectId = 0;
        private void FrmEmpPay_Load(object sender, EventArgs e)
        {
            SelectId = 1;
            lblFileName.Visible = false;
            DataTable dtBranch = new DataTable();
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);
            DataTable dataTable = dtBranch;
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable;

            qur.Connection = conn;
            LoadMasters();
            DataGridEmpPay.RowHeadersVisible = false;
            Cmgfntbox_SelectedIndexChanged(sender, e);
            SelectId = 0;
        }
        protected void LoadMasters()
        {
            DataSet ds = db.GetDataWithoutParamMultiple(CommandType.StoredProcedure, "SP_GetMasters", conn);
            DataTable dataTable = ds.Tables[0];
            DataTable ft = ds.Tables[5];

            DataTable dtDept = new DataTable();
            if (GeneralParameters.Branch == "All")
            {
                dtDept = dataTable;
            }
            else
            {
                dtDept = dataTable;
            }
            CmbDepartment.DataSource = null;
            CmbDepartment.DisplayMember = "DeptName";
            CmbDepartment.ValueMember = "DeptId";
            CmbDepartment.DataSource = dtDept;

            cbogroup.DataSource = null;
            cbogroup.DisplayMember = "pgname";
            cbogroup.ValueMember = "uid";
            cbogroup.DataSource = ft;
        }

        private void Cmgfntbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectId == 0)
            {
                if (CmbDepartment.Text == "All")
                {
                    bsSalary.RemoveFilter();
                    bsSalary.ResetBindings(true);
                }
                else if (bsSalary.DataSource != null && CmbDepartment.SelectedIndex != -1)
                {
                    bsSalary.Filter = string.Format("DeptId =" + CmbDepartment.SelectedValue + "");
                }
            }

        }
        private void LoadPayDetails()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@BRANCHID",CmbBranch.SelectedValue)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Get_PayDetails", sqlParameters, conn);
                bsSalary.DataSource = null;
                bsSalary.DataSource = dataTable;
                DataGridEmpPay.DataSource = null;
                DataGridEmpPay.AutoGenerateColumns = true;
                DataGridEmpPay.DataSource = bsSalary;
                DataGridEmpPay.Columns[0].Visible = false;
                DataGridEmpPay.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
            //CalculateTotal();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        bool notlastColumn = true;
        private void DataGridEmpPay_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridEmpPay.RowCount > 1)
            {
                if (DataGridEmpPay.CurrentRow.Cells[1].Value != null)
                {
                    if (DataGridEmpPay.CurrentRow.Cells[1].Value.ToString() != "0" && totalid == 0)
                    {
                        if (DataGridEmpPay.CurrentCell.ColumnIndex == 3 || DataGridEmpPay.CurrentCell.ColumnIndex == 4)
                        {
                            int EMPID;
                            int EMPPAYID;
                            string CNAME;
                            decimal AMOUNT;

                            EMPID = Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[0].Value.ToString());
                            EMPPAYID = Convert.ToInt32(DataGridEmpPay.CurrentRow.Cells[6].Value.ToString());
                            AMOUNT = Convert.ToDecimal(DataGridEmpPay.CurrentRow.Cells[e.ColumnIndex].Value.ToString());
                            CNAME = DataGridEmpPay.Columns[e.ColumnIndex].Name.ToString();

                            SqlParameter[] sqlParameters = {
                                new SqlParameter("@EMPID",EMPID),
                                new SqlParameter("@EMPPAYID",EMPPAYID),
                                new SqlParameter("@CNAME",CNAME),
                                new SqlParameter("@AMOUNT",AMOUNT)
                            };
                            db.ExecuteNonQuery(CommandType.StoredProcedure, "UPDATE_PayDetails", sqlParameters, conn);
                            CalculateTotal();
                            if (e.ColumnIndex != 10)
                            {
                                if (e.ColumnIndex == DataGridEmpPay.ColumnCount - 1)
                                {
                                    KeyEventArgs forKeyDown = new KeyEventArgs(Keys.Enter);
                                    notlastColumn = false;
                                    DataGridEmpPay_KeyDown(DataGridEmpPay, forKeyDown);
                                }
                                else
                                {
                                    SendKeys.Send("{up}");
                                    SendKeys.Send("{right}");
                                }
                            }
                            else
                            {
                                SendKeys.Send("{tab}");
                            }
                        }
                    }
                }
            }
        }

        private void DataGridEmpPay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (e.KeyCode == Keys.Enter && notlastColumn) //if not last column move to next
                {
                    if (DataGridEmpPay.CurrentCell.ColumnIndex == 10)
                    {
                        SendKeys.Send("{tab}");
                    }
                    else
                    {
                        SendKeys.Send("{up}");
                        SendKeys.Send("{right}");
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    SendKeys.Send("{tab}");//go to first column
                    notlastColumn = true;
                }
            }
        }

        protected void CalculateTotal()
        {
            try
            {
                totalid = 1;
                decimal basic = 0;
                decimal OA = 0;
                decimal GROSS = 0;
                for (int i = 0; i < DataGridEmpPay.Rows.Count; i++)
                {
                    basic = 0;
                    OA = 0;
                    GROSS = 0;
                    basic = Convert.ToDecimal(DataGridEmpPay.Rows[i].Cells[3].Value.ToString());
                    OA = Convert.ToDecimal(DataGridEmpPay.Rows[i].Cells[4].Value.ToString());
                    GROSS = basic + OA;
                    DataGridEmpPay.Rows[i].Cells[5].Value = GROSS;
                }
                totalid = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtempcode.Text = String.Empty;
            txtempname.Text = String.Empty;
            txtgross.Text = String.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtempcode.Text != string.Empty && txtempname.Text != string.Empty && txtgross.Text != string.Empty)
                {
                    SqlParameter[] parameters = {
                        new SqlParameter("@EMPID",txtempname.Tag),
                        new SqlParameter("@PGUID",cbogroup.SelectedValue),
                        new SqlParameter("@GROSS", txtgross.Text),
                        new SqlParameter("@DT",Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd")),
                    };
                    db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_POSTGROSSSALARY", parameters, conn);
                    MessageBox.Show("Saved Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtempcode.Text = string.Empty;
                    txtempname.Text = string.Empty;
                    txtgross.Text = string.Empty;

                    LoadPayDetails();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtempname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempcode);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    LoadPayDetails();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@BranchId", CmbBranch.SelectedValue) };
                    dt = db.GetDataWithParam(CommandType.StoredProcedure, "sp_getempdet", para, conn);
                    bsp.DataSource = dt;

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    LoadPayDetails();
                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    //txtmillid.Text = DataGridCommon.Rows[Index].Cells[4].Value.ToString();
                    LoadPayDetails();
                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            Point loc = FindLocation(txtempname);
            grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbogroup_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtgross_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != string.Empty)
                {
                    bsSalary.Filter = string.Format("EmpCode Like '%{0}%' or EmpName Like '%{1}%' ", textBox1.Text, textBox1.Text);
                }
                else
                {
                    bsSalary.RemoveFilter();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information");
                return;
            }
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            LoadPayDetails();
        }

        private void loadgrid1()
        {

            try
            {
                SqlParameter[] sqlParameters = {
                        new SqlParameter("@BRANCHID",CmbBranch.SelectedValue),
                    };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Get_PayDetails", sqlParameters, conn);
                DataGridEmpPay.AutoGenerateColumns = false;
                DataGridEmpPay.DataSource = null;
                DataGridEmpPay.ColumnCount = 12;
                DataGridEmpPay.Columns[0].Name = "EmpId";
                DataGridEmpPay.Columns[0].HeaderText = "EmpId";
                DataGridEmpPay.Columns[0].DataPropertyName = "EMPID";
                DataGridEmpPay.Columns[0].Visible = false;

                DataGridEmpPay.Columns[1].Name = "Empcode";
                DataGridEmpPay.Columns[1].HeaderText = "Empcode";
                DataGridEmpPay.Columns[1].DataPropertyName = "EMPCODE";
                DataGridEmpPay.Columns[1].Width = 80;

                DataGridEmpPay.Columns[2].Name = "Empname";
                DataGridEmpPay.Columns[2].HeaderText = "Employee Name";
                DataGridEmpPay.Columns[2].DataPropertyName = "EMPNAME";
                DataGridEmpPay.Columns[2].Width = 150;

                DataGridEmpPay.Columns[3].Name = "basic";
                DataGridEmpPay.Columns[3].HeaderText = "Basic";
                DataGridEmpPay.Columns[3].DataPropertyName = "BASIC";
                DataGridEmpPay.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[3].Width = 80;

                DataGridEmpPay.Columns[4].Name = "DA";
                DataGridEmpPay.Columns[4].HeaderText = "DA";
                DataGridEmpPay.Columns[4].DataPropertyName = "DA";
                DataGridEmpPay.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[4].Width = 80;

                DataGridEmpPay.Columns[5].Name = "HRA";
                DataGridEmpPay.Columns[5].HeaderText = "HRA";
                DataGridEmpPay.Columns[5].DataPropertyName = "HRA";
                DataGridEmpPay.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[5].Width = 80;

                DataGridEmpPay.Columns[6].Name = "CA";
                DataGridEmpPay.Columns[6].HeaderText = "CA";
                DataGridEmpPay.Columns[6].DataPropertyName = "CA";
                DataGridEmpPay.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[7].Name = "MA";
                DataGridEmpPay.Columns[7].HeaderText = "MA";
                DataGridEmpPay.Columns[7].DataPropertyName = "MA";
                DataGridEmpPay.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[7].Width = 80;

                DataGridEmpPay.Columns[8].Name = "TA";
                DataGridEmpPay.Columns[8].HeaderText = "TA";
                DataGridEmpPay.Columns[8].DataPropertyName = "TA";
                DataGridEmpPay.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[8].Width = 80;

                DataGridEmpPay.Columns[9].Name = "UNIA";
                DataGridEmpPay.Columns[9].HeaderText = "UNIA";
                DataGridEmpPay.Columns[9].DataPropertyName = "UNIA";
                DataGridEmpPay.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridEmpPay.Columns[9].Width = 80;

                DataGridEmpPay.Columns[10].Name = "GROSS";
                DataGridEmpPay.Columns[10].HeaderText = "GROSS";
                DataGridEmpPay.Columns[10].DataPropertyName = "GROSS";
                DataGridEmpPay.Columns[10].Width = 80;
                DataGridEmpPay.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridEmpPay.Columns[11].Name = "EMPPAYID";
                DataGridEmpPay.Columns[11].HeaderText = "EMPPAYID";
                DataGridEmpPay.Columns[11].DataPropertyName = "EMPPAYID";
                DataGridEmpPay.Columns[11].Visible = false;
                bs.DataSource = dataTable;
                DataGridEmpPay.DataSource = bs;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
            CalculateTotal();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "Excel|*.xls|All Excel|*.xlsx";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    lblFileName.Text = dialog.FileName;
                    lblFileName.Visible = false;
                    string extensionpath = Path.GetExtension(dialog.FileName);
                    switch (extensionpath)
                    {
                        case ".xls":
                            connstring = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx":
                            connstring = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                            break;
                    }
                    ImportEarndeduction(connstring, lblFileName.Text);
                    
                    MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void ImportEarndeduction(string ConnectionString, string Path)
        {
            try
            {
                connstring = string.Format(ConnectionString, Path);
                using (OleDbConnection excel_Conn = new OleDbConnection(connstring))
                {
                    excel_Conn.Open();
                    string sheet1 = excel_Conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    new DataColumn("SLNO", typeof(int));
                    new DataColumn("EMPCODE", typeof(string));
                    new DataColumn("GROSS", typeof(float));
                    new DataColumn("PAYGROUP", typeof(string));

                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_Conn))
                    {
                        oda.Fill(dtexcelData);
                    }
                    excel_Conn.Close();
                    string connString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString))
                    {
                        string query = "truncate table  ImportGross ";
                        db.ExecuteNonQuery(CommandType.Text, query, conn);
                        using (SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(conn))
                        {
                            sqlbulkcopy.DestinationTableName = "dbo.ImportGross";
                            sqlbulkcopy.ColumnMappings.Add("SLNO", "SLNO");
                            sqlbulkcopy.ColumnMappings.Add("EMPCODE", "EMPCODE");
                            sqlbulkcopy.ColumnMappings.Add("GROSS", "GROSS");
                            sqlbulkcopy.ColumnMappings.Add("PAYGROUP", "PAYGROUP");
                            sqlbulkcopy.BulkCopyTimeout = 600;
                            conn.Open();
                            sqlbulkcopy.WriteToServer(dtexcelData);
                            conn.Close();
                        }
                    }
                    DataTable table = db.GetDataWithoutParam(CommandType.StoredProcedure, "Proc_GetImportSalary", conn);
                    DataGridSalImport.DataSource = null;
                    DataGridSalImport.DataSource = table;
                    GrSal.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void LinklblSample_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                app.Visible = true;
                worksheet = workbook.Sheets["Sheet1"];
                worksheet = workbook.ActiveSheet;
                worksheet.Name = "Salary";

                Microsoft.Office.Interop.Excel.Range row1 = worksheet.Rows.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range row2 = worksheet.Rows.Cells[1, 2];
                Microsoft.Office.Interop.Excel.Range row3 = worksheet.Rows.Cells[1, 3];
                Microsoft.Office.Interop.Excel.Range row4 = worksheet.Rows.Cells[1, 4];
                row1.Value = "SLNO";
                row2.Value = "EMPCODE";
                row3.Value = "GROSS";
                row4.Value = "PAYGROUP";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] sqlParameters = { new SqlParameter("@INCRDT", DateTime.Now.Date) };
                db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_IMPORTSALARY", sqlParameters, conn);
                MessageBox.Show("Employee Imported Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadPayDetails();
                GrSal.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            GrSal.Visible = false;
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    if (CmbBranch.Text != "All")
                    {
                        string Query = "Select distinct a.DeptId,b.DeptName from Emp_mast a Inner join Dept_Mast b On a.DeptId = b.DeptId Where BranchId = " + CmbBranch.SelectedValue + " Order by b.DeptName";
                        DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = "0";
                        dataRow[1] = "All";
                        dataTable.Rows.Add(dataRow);
                        CmbDepartment.DataSource = null;
                        CmbDepartment.DisplayMember = "DeptName";
                        CmbDepartment.ValueMember = "DeptId";
                        CmbDepartment.DataSource = dataTable;
                    }
                }
                LoadPayDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
