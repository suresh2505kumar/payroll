﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MyEasyPay
{
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public DataTable GetDataWithParam(CommandType cmdType, string ProcedureName, SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetDataWithoutParam(CommandType cmdType, string ProcedureName)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet GetDataWithoutParamMultiple(CommandType cmdType, string ProcedureName)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = ProcedureName,
                    Connection = conn
                };
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para,int Outpara)
        {
            int Id = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                Id = Convert.ToInt32(para[Outpara].Value.ToString());//Solved
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
        public int ExecuteNonQuery(CommandType cmdType, string CommandText, SqlParameter[] para)
        {
            int Id = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = cmdType,
                    CommandText = CommandText
                };
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                Id = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                throw ex;
            }
            return Id;
        }
    }
}
