﻿namespace MyEasyPay
{
    partial class FrmEmpAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpAsset));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.label27 = new System.Windows.Forms.Label();
            this.txtctype = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.DataGridPunch = new System.Windows.Forms.DataGridView();
            this.CmbMonth = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbcom = new System.Windows.Forms.ComboBox();
            this.BtnOk = new System.Windows.Forms.Button();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtempcode = new System.Windows.Forms.TextBox();
            this.txtempname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtqty = new System.Windows.Forms.TextBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtassetname = new System.Windows.Forms.TextBox();
            this.issdt = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.recdt = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.ChckShift = new System.Windows.Forms.CheckBox();
            this.txtremarks = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnExcelExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPunch)).BeginInit();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpDOB
            // 
            this.dtpDOB.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(158, 251);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(111, 27);
            this.dtpDOB.TabIndex = 7;
            this.dtpDOB.ValueChanged += new System.EventHandler(this.dtpDOB_ValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(112, 252);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 19);
            this.label27.TabIndex = 57;
            this.label27.Text = "Date";
            // 
            // txtctype
            // 
            this.txtctype.Location = new System.Drawing.Point(303, 374);
            this.txtctype.MaxLength = 6;
            this.txtctype.Name = "txtctype";
            this.txtctype.Size = new System.Drawing.Size(120, 20);
            this.txtctype.TabIndex = 61;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(847, 485);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 36);
            this.button1.TabIndex = 62;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DataGridPunch
            // 
            this.DataGridPunch.BackgroundColor = System.Drawing.Color.White;
            this.DataGridPunch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridPunch.Location = new System.Drawing.Point(12, 104);
            this.DataGridPunch.Name = "DataGridPunch";
            this.DataGridPunch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DataGridPunch.Size = new System.Drawing.Size(905, 378);
            this.DataGridPunch.TabIndex = 401;
            this.DataGridPunch.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellClick_1);
            this.DataGridPunch.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridPunch_CellContentClick);
            this.DataGridPunch.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.HFIT_CellValueChanged_1);
            this.DataGridPunch.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.HFIT_EditingControlShowing_2);
            this.DataGridPunch.DoubleClick += new System.EventHandler(this.DataGridPunch_DoubleClick);
            this.DataGridPunch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridPunch_KeyDown);
            this.DataGridPunch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridPunch_MouseClick);
            this.DataGridPunch.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.DataGridPunch_MouseDoubleClick);
            // 
            // CmbMonth
            // 
            this.CmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMonth.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbMonth.FormattingEnabled = true;
            this.CmbMonth.Location = new System.Drawing.Point(306, 387);
            this.CmbMonth.Name = "CmbMonth";
            this.CmbMonth.Size = new System.Drawing.Size(145, 26);
            this.CmbMonth.TabIndex = 402;
            this.CmbMonth.SelectedIndexChanged += new System.EventHandler(this.CmbLvTag_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(253, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 19);
            this.label1.TabIndex = 403;
            this.label1.Text = "Month";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(523, 368);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(274, 28);
            this.button2.TabIndex = 404;
            this.button2.Text = "Import Datas";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(469, 390);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 19);
            this.label2.TabIndex = 406;
            this.label2.Text = "Component";
            // 
            // cmbcom
            // 
            this.cmbcom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcom.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbcom.FormattingEnabled = true;
            this.cmbcom.Location = new System.Drawing.Point(558, 387);
            this.cmbcom.Name = "cmbcom";
            this.cmbcom.Size = new System.Drawing.Size(250, 26);
            this.cmbcom.TabIndex = 405;
            this.cmbcom.SelectedIndexChanged += new System.EventHandler(this.cmbcom_SelectedIndexChanged);
            // 
            // BtnOk
            // 
            this.BtnOk.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnOk.Location = new System.Drawing.Point(875, 36);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(42, 28);
            this.BtnOk.TabIndex = 413;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(11, 114);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(459, 214);
            this.grSearch.TabIndex = 418;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(366, 179);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(71, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(5, 179);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(70, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(7, 5);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(430, 168);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCommon_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 19);
            this.label3.TabIndex = 417;
            this.label3.Text = "Emp Code";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 19);
            this.label4.TabIndex = 415;
            this.label4.Text = "Employee Name";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtempcode
            // 
            this.txtempcode.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempcode.Location = new System.Drawing.Point(11, 38);
            this.txtempcode.Name = "txtempcode";
            this.txtempcode.Size = new System.Drawing.Size(111, 27);
            this.txtempcode.TabIndex = 416;
            this.txtempcode.Click += new System.EventHandler(this.txtempcode_Click);
            this.txtempcode.TextChanged += new System.EventHandler(this.txtempcode_TextChanged);
            this.txtempcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtempcode_KeyDown);
            // 
            // txtempname
            // 
            this.txtempname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtempname.Location = new System.Drawing.Point(123, 38);
            this.txtempname.Name = "txtempname";
            this.txtempname.Size = new System.Drawing.Size(236, 27);
            this.txtempname.TabIndex = 414;
            this.txtempname.Click += new System.EventHandler(this.txtempname_Click);
            this.txtempname.TextChanged += new System.EventHandler(this.txtempname_TextChanged);
            this.txtempname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtempname_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(591, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 19);
            this.label5.TabIndex = 420;
            this.label5.Text = "Qty";
            // 
            // txtqty
            // 
            this.txtqty.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqty.Location = new System.Drawing.Point(588, 38);
            this.txtqty.Name = "txtqty";
            this.txtqty.Size = new System.Drawing.Size(72, 27);
            this.txtqty.TabIndex = 419;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(244, 387);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(53, 20);
            this.txtSearch.TabIndex = 421;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileName.Location = new System.Drawing.Point(20, 493);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(0, 19);
            this.lblFileName.TabIndex = 422;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(361, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 19);
            this.label6.TabIndex = 424;
            this.label6.Text = "Asset Name";
            // 
            // txtassetname
            // 
            this.txtassetname.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtassetname.Location = new System.Drawing.Point(360, 38);
            this.txtassetname.Name = "txtassetname";
            this.txtassetname.Size = new System.Drawing.Size(226, 27);
            this.txtassetname.TabIndex = 423;
            // 
            // issdt
            // 
            this.issdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.issdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.issdt.Location = new System.Drawing.Point(666, 39);
            this.issdt.Name = "issdt";
            this.issdt.Size = new System.Drawing.Size(93, 26);
            this.issdt.TabIndex = 426;
            this.issdt.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(666, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 18);
            this.label7.TabIndex = 425;
            this.label7.Text = "IssueDate";
            // 
            // recdt
            // 
            this.recdt.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.recdt.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.recdt.Location = new System.Drawing.Point(765, 39);
            this.recdt.Name = "recdt";
            this.recdt.Size = new System.Drawing.Size(93, 26);
            this.recdt.TabIndex = 428;
            this.recdt.Value = new System.DateTime(2017, 3, 25, 16, 16, 31, 0);
            this.recdt.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(765, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 18);
            this.label8.TabIndex = 427;
            this.label8.Text = "ReceiptDate";
            this.label8.Visible = false;
            // 
            // ChckShift
            // 
            this.ChckShift.AutoSize = true;
            this.ChckShift.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChckShift.Location = new System.Drawing.Point(765, 41);
            this.ChckShift.Name = "ChckShift";
            this.ChckShift.Size = new System.Drawing.Size(103, 22);
            this.ChckShift.TabIndex = 429;
            this.ChckShift.Text = "ReceiptDate";
            this.ChckShift.UseVisualStyleBackColor = true;
            this.ChckShift.CheckedChanged += new System.EventHandler(this.ChckShift_CheckedChanged);
            // 
            // txtremarks
            // 
            this.txtremarks.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtremarks.Location = new System.Drawing.Point(123, 71);
            this.txtremarks.Name = "txtremarks";
            this.txtremarks.Size = new System.Drawing.Size(735, 27);
            this.txtremarks.TabIndex = 430;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(52, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 19);
            this.label9.TabIndex = 431;
            this.label9.Text = "Remarks";
            // 
            // BtnExcelExport
            // 
            this.BtnExcelExport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcelExport.Location = new System.Drawing.Point(11, 485);
            this.BtnExcelExport.Name = "BtnExcelExport";
            this.BtnExcelExport.Size = new System.Drawing.Size(175, 34);
            this.BtnExcelExport.TabIndex = 432;
            this.BtnExcelExport.Text = "Export to Excel";
            this.BtnExcelExport.UseVisualStyleBackColor = true;
            this.BtnExcelExport.Click += new System.EventHandler(this.BtnExcelExport_Click);
            // 
            // FrmEmpAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(917, 518);
            this.Controls.Add(this.BtnExcelExport);
            this.Controls.Add(this.ChckShift);
            this.Controls.Add(this.recdt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.issdt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtassetname);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.grSearch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtqty);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtempcode);
            this.Controls.Add(this.txtempname);
            this.Controls.Add(this.BtnOk);
            this.Controls.Add(this.DataGridPunch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(this.txtctype);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CmbMonth);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbcom);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtremarks);
            this.Name = "FrmEmpAsset";
            this.Text = "Employee Assets";
            this.Load += new System.EventHandler(this.FrmEmpAsset_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridPunch)).EndInit();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtctype;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DataGridPunch;
        private System.Windows.Forms.ComboBox CmbMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbcom;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtempcode;
        private System.Windows.Forms.TextBox txtempname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtqty;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtassetname;
        internal System.Windows.Forms.DateTimePicker issdt;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.DateTimePicker recdt;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox ChckShift;
        private System.Windows.Forms.TextBox txtremarks;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button BtnExcelExport;
    }
}