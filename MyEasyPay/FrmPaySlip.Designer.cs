﻿namespace MyEasyPay
{
    partial class FrmPaySlip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrPaySlip = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.Txtsearch = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.ChckSelectAll = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CmbReportType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CmbDepartment = new System.Windows.Forms.ComboBox();
            this.DataGridEmployee = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbMonth = new System.Windows.Forms.ComboBox();
            this.GrPaySlip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).BeginInit();
            this.SuspendLayout();
            // 
            // GrPaySlip
            // 
            this.GrPaySlip.Controls.Add(this.label4);
            this.GrPaySlip.Controls.Add(this.CmbBranch);
            this.GrPaySlip.Controls.Add(this.Txtsearch);
            this.GrPaySlip.Controls.Add(this.btnExit);
            this.GrPaySlip.Controls.Add(this.btnPrint);
            this.GrPaySlip.Controls.Add(this.ChckSelectAll);
            this.GrPaySlip.Controls.Add(this.label3);
            this.GrPaySlip.Controls.Add(this.CmbReportType);
            this.GrPaySlip.Controls.Add(this.label2);
            this.GrPaySlip.Controls.Add(this.CmbDepartment);
            this.GrPaySlip.Controls.Add(this.DataGridEmployee);
            this.GrPaySlip.Controls.Add(this.label1);
            this.GrPaySlip.Controls.Add(this.CmbMonth);
            this.GrPaySlip.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrPaySlip.Location = new System.Drawing.Point(9, 7);
            this.GrPaySlip.Name = "GrPaySlip";
            this.GrPaySlip.Size = new System.Drawing.Size(701, 456);
            this.GrPaySlip.TabIndex = 0;
            this.GrPaySlip.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(49, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 244;
            this.label4.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(102, 82);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(213, 26);
            this.CmbBranch.TabIndex = 243;
            this.CmbBranch.SelectedIndexChanged += new System.EventHandler(this.CmbBranch_SelectedIndexChanged);
            // 
            // Txtsearch
            // 
            this.Txtsearch.Location = new System.Drawing.Point(345, 21);
            this.Txtsearch.Name = "Txtsearch";
            this.Txtsearch.Size = new System.Drawing.Size(346, 26);
            this.Txtsearch.TabIndex = 10;
            this.Txtsearch.TextChanged += new System.EventHandler(this.Txtsearch_TextChanged);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(222, 351);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 30);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(115, 351);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 30);
            this.btnPrint.TabIndex = 8;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // ChckSelectAll
            // 
            this.ChckSelectAll.AutoSize = true;
            this.ChckSelectAll.Location = new System.Drawing.Point(347, 414);
            this.ChckSelectAll.Name = "ChckSelectAll";
            this.ChckSelectAll.Size = new System.Drawing.Size(77, 22);
            this.ChckSelectAll.TabIndex = 7;
            this.ChckSelectAll.Text = "Slect All";
            this.ChckSelectAll.UseVisualStyleBackColor = true;
            this.ChckSelectAll.CheckedChanged += new System.EventHandler(this.ChckSelectAll_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Report Type";
            // 
            // CmbReportType
            // 
            this.CmbReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbReportType.FormattingEnabled = true;
            this.CmbReportType.Items.AddRange(new object[] {
            "PaySlip",
            "Salary Register",
            "Bank Statement",
            "Cash Statement"});
            this.CmbReportType.Location = new System.Drawing.Point(102, 191);
            this.CmbReportType.Name = "CmbReportType";
            this.CmbReportType.Size = new System.Drawing.Size(213, 26);
            this.CmbReportType.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "Department";
            // 
            // CmbDepartment
            // 
            this.CmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbDepartment.FormattingEnabled = true;
            this.CmbDepartment.Location = new System.Drawing.Point(102, 131);
            this.CmbDepartment.Name = "CmbDepartment";
            this.CmbDepartment.Size = new System.Drawing.Size(213, 26);
            this.CmbDepartment.TabIndex = 3;
            this.CmbDepartment.SelectedIndexChanged += new System.EventHandler(this.CmbDepartment_SelectedIndexChanged);
            // 
            // DataGridEmployee
            // 
            this.DataGridEmployee.AllowUserToAddRows = false;
            this.DataGridEmployee.BackgroundColor = System.Drawing.Color.White;
            this.DataGridEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridEmployee.Location = new System.Drawing.Point(345, 49);
            this.DataGridEmployee.Name = "DataGridEmployee";
            this.DataGridEmployee.RowHeadersVisible = false;
            this.DataGridEmployee.Size = new System.Drawing.Size(346, 359);
            this.DataGridEmployee.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Month";
            // 
            // CmbMonth
            // 
            this.CmbMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbMonth.FormattingEnabled = true;
            this.CmbMonth.Location = new System.Drawing.Point(102, 38);
            this.CmbMonth.Name = "CmbMonth";
            this.CmbMonth.Size = new System.Drawing.Size(213, 26);
            this.CmbMonth.TabIndex = 0;
            // 
            // FrmPaySlip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(721, 479);
            this.Controls.Add(this.GrPaySlip);
            this.Name = "FrmPaySlip";
            this.Text = "FrmPaySlip";
            this.Load += new System.EventHandler(this.FrmPaySlip_Load);
            this.GrPaySlip.ResumeLayout(false);
            this.GrPaySlip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridEmployee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrPaySlip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CmbReportType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CmbDepartment;
        private System.Windows.Forms.DataGridView DataGridEmployee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbMonth;
        private System.Windows.Forms.CheckBox ChckSelectAll;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.TextBox Txtsearch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CmbBranch;
    }
}