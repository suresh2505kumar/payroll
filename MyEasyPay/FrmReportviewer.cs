﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace MyEasyPay
{
    public partial class FrmReportviewer : Form
    {
        public FrmReportviewer()
        {
            InitializeComponent();
        }
        SQLDBHelper db = new SQLDBHelper();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);

        private void FrmReportviewer_Load(object sender, EventArgs e)
        {
            if (GeneralParameters.ReportId == 1)
            {
                PerformanceReport();
            }
            else if (GeneralParameters.ReportId == 2)
            {
                IregularReport();
            }
            else if (GeneralParameters.ReportId == 3)
            {
                StrengthReport();
            }
            else if (GeneralParameters.ReportId == 4)
            {
                InReport();
            }
            else if (GeneralParameters.ReportId == 5)
            {
                WorkesHoursReport();
            }
            else if (GeneralParameters.ReportId == 6)
            {
                LunchBreakReport();
            }
            else if (GeneralParameters.ReportId == 7)
            {
                EmployeeList();
            }
            else if (GeneralParameters.ReportId == 8)
            {
                MusterRoll();
            }
            else if (GeneralParameters.ReportId == 10)
            {
                ManulapuchReport();
            }
            else if (GeneralParameters.ReportId == 9)
            {
                LateInReport();
            }
            else if (GeneralParameters.ReportId == 11)
            {
                ManulapuchReport();
            }
            else if (GeneralParameters.ReportId == 12)
            {
                LateInReport();
            }
            else if (GeneralParameters.ReportId == 13)
            {
                PrintPaySlip();
            }
            else if (GeneralParameters.ReportId == 14)
            {
                BankStatement();
            }
            else if (GeneralParameters.ReportId == 15)
            {
                CashStatement();
            }
            else if (GeneralParameters.ReportId == 20)
            {
                LeftEmployeeList();
            }
            else if (GeneralParameters.ReportId == 21)
            {
                Withoutleavelist();
            }
            else if (GeneralParameters.ReportId == 22)
            {
                Withleavelist();
            }
            else if (GeneralParameters.ReportId == 23)
            {
                LOADEMPDET();
            }
            else if (GeneralParameters.ReportId == 24)
            {
                loadempfinalrpt();
            }
            else if (GeneralParameters.ReportId == 16)
            {
                SalaryRegister();
            }

        }

        private void SalaryRegister()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                       new SqlParameter("@BRANCHID",GeneralParameters.ReportBranchId),
                       new SqlParameter("@Monid",GeneralParameters.PayMonthId),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_SALARYREGISTER", sqlParameters, conn);

                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("UniCode", typeof(int));
                dataTable.Columns.Add("EmployeeCode", typeof(string));
                dataTable.Columns.Add("EmpName", typeof(string));
                dataTable.Columns.Add("PayDays", typeof(decimal));
                dataTable.Columns.Add("WorkedDays", typeof(decimal));
                dataTable.Columns.Add("Basic", typeof(decimal));
                dataTable.Columns.Add("DA", typeof(decimal));
                dataTable.Columns.Add("HRA", typeof(decimal));
                dataTable.Columns.Add("CCA", typeof(decimal));
                dataTable.Columns.Add("Conv", typeof(decimal));
                dataTable.Columns.Add("EDU", typeof(decimal));
                dataTable.Columns.Add("Unif", typeof(decimal));
                dataTable.Columns.Add("Medical", typeof(decimal));
                dataTable.Columns.Add("Gross", typeof(decimal));
                dataTable.Columns.Add("PF", typeof(decimal));
                dataTable.Columns.Add("ESI", typeof(decimal));
                dataTable.Columns.Add("ADV", typeof(decimal));
                dataTable.Columns.Add("IT", typeof(decimal));
                dataTable.Columns.Add("Ins", typeof(decimal));
                dataTable.Columns.Add("LWF", typeof(decimal));
                dataTable.Columns.Add("SWF", typeof(decimal));
                dataTable.Columns.Add("D1", typeof(decimal));
                dataTable.Columns.Add("D2", typeof(decimal));
                dataTable.Columns.Add("D3", typeof(decimal));
                dataTable.Columns.Add("OTH", typeof(decimal));
                dataTable.Columns.Add("CPT", typeof(decimal));
                dataTable.Columns.Add("OneDay", typeof(decimal));
                dataTable.Columns.Add("Net", typeof(decimal));

                if (data.Rows.Count > 0)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        decimal MA = 0;
                        decimal PF = 0;
                        decimal ESI = 0;
                        decimal ADV = 0;
                        decimal IT = 0;
                        decimal Ins = 0;
                        decimal LWF = 0;
                        decimal SWf = 0;
                        decimal D1 = 0;
                        decimal D2 = 0;
                        decimal D3 = 0;
                        decimal CPT = 0;
                        if (string.IsNullOrEmpty(data.Rows[i]["MA"].ToString()))
                        {
                            MA = 0;
                        }
                        else
                        {
                            MA = Convert.ToDecimal(data.Rows[i]["MA"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["EPF"].ToString()))
                        {
                            PF = 0;
                        }
                        else
                        {
                            PF = Convert.ToDecimal(data.Rows[i]["EPF"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["ESI"].ToString()))
                        {
                            ESI = 0;
                        }
                        else
                        {
                            ESI = Convert.ToDecimal(data.Rows[i]["ESI"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["ADVANCE"].ToString()))
                        {
                            ADV = 0;
                        }
                        else
                        {
                            ADV = Convert.ToDecimal(data.Rows[i]["ADVANCE"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["IT"].ToString()))
                        {
                            IT = 0;
                        }
                        else
                        {
                            IT = Convert.ToDecimal(data.Rows[i]["IT"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["INSUR"].ToString()))
                        {
                            Ins = 0;
                        }
                        else
                        {
                            Ins = Convert.ToDecimal(data.Rows[i]["INSUR"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["LWF"].ToString()))
                        {
                            LWF = 0;
                        }
                        else
                        {
                            LWF = Convert.ToDecimal(data.Rows[i]["LWF"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["SWF"].ToString()))
                        {
                            SWf = 0;
                        }
                        else
                        {
                            SWf = Convert.ToDecimal(data.Rows[i]["SWF"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["Ded1"].ToString()))
                        {
                            D1 = 0;
                        }
                        else
                        {
                            D1 = Convert.ToDecimal(data.Rows[i]["Ded1"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["Ded2"].ToString()))
                        {
                            D2 = 0;
                        }
                        else
                        {
                            D2 = Convert.ToDecimal(data.Rows[i]["Ded2"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["Ded3"].ToString()))
                        {
                            D3 = 0;
                        }
                        else
                        {
                            D3 = Convert.ToDecimal(data.Rows[i]["Ded3"].ToString());
                        }
                        if (string.IsNullOrEmpty(data.Rows[i]["P.TAX"].ToString()))
                        {
                            CPT = 0;
                        }
                        else
                        {
                            CPT = Convert.ToDecimal(data.Rows[i]["P.TAX"].ToString());
                        }
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = data.Rows[i]["CardNo"].ToString();
                        dataRow[1] = data.Rows[i]["EMPCODE"].ToString();
                        dataRow[2] = data.Rows[i]["EMPNAME"].ToString();
                        dataRow[3] = data.Rows[i]["PAIDDAYS"].ToString();
                        dataRow[4] = data.Rows[i]["WKDDAYS"].ToString();
                        dataRow[5] = data.Rows[i]["Basic"].ToString();
                        dataRow[6] = data.Rows[i]["DA"].ToString();
                        dataRow[7] = data.Rows[i]["HRA"].ToString();
                        dataRow[8] = data.Rows[i]["CA"].ToString();
                        dataRow[9] = data.Rows[i]["TA"].ToString();
                        dataRow[10] = data.Rows[i]["EDUA"].ToString();
                        dataRow[11] = data.Rows[i]["UNIA"].ToString();
                        dataRow[12] = MA;
                        dataRow[13] = data.Rows[i]["GROSS"].ToString();
                        dataRow[14] = PF;
                        dataRow[15] = ESI;
                        dataRow[16] = ADV;
                        dataRow[17] = IT;
                        dataRow[18] = Ins;
                        dataRow[19] = LWF;
                        dataRow[20] = SWf;
                        dataRow[21] = D1;
                        dataRow[22] = D2;
                        dataRow[23] = D3;
                        dataRow[24] = "0";
                        dataRow[25] = CPT;
                        dataRow[26] = "0";
                        dataRow[27] = data.Rows[i]["NET"].ToString();
                        dataTable.Rows.Add(dataRow);
                    }
                    DataTable dtSummary = new DataTable();
                    dtSummary.Columns.Add("Basic", typeof(decimal));
                    dtSummary.Columns.Add("FDA", typeof(decimal));
                    dtSummary.Columns.Add("HRA", typeof(decimal));
                    dtSummary.Columns.Add("CCA", typeof(decimal));
                    dtSummary.Columns.Add("Conveyance", typeof(decimal));
                    dtSummary.Columns.Add("Education", typeof(decimal));
                    dtSummary.Columns.Add("Uniform", typeof(decimal));
                    dtSummary.Columns.Add("Medical", typeof(decimal));
                    dtSummary.Columns.Add("PF", typeof(decimal));
                    dtSummary.Columns.Add("ESI", typeof(decimal));
                    dtSummary.Columns.Add("SalaryAdvance", typeof(decimal));
                    dtSummary.Columns.Add("IncomeTax", typeof(decimal));
                    dtSummary.Columns.Add("Insurance", typeof(decimal));
                    dtSummary.Columns.Add("LWF", typeof(decimal));
                    dtSummary.Columns.Add("SWF", typeof(decimal));
                    dtSummary.Columns.Add("Deduction1", typeof(decimal));
                    dtSummary.Columns.Add("Deduction2", typeof(decimal));
                    dtSummary.Columns.Add("Others", typeof(decimal));
                    dtSummary.Columns.Add("CropTax", typeof(decimal));
                    dtSummary.Columns.Add("1DaySalary", typeof(decimal));
                    dtSummary.Columns.Add("Earnings", typeof(decimal));
                    dtSummary.Columns.Add("Deduction", typeof(decimal));
                    dtSummary.Columns.Add("NettPay", typeof(decimal));


                    decimal PfS = Convert.ToDecimal(dataTable.Compute("SUM(PF)", string.Empty));
                    decimal ESIS = Convert.ToDecimal(dataTable.Compute("SUM(ESI)", string.Empty));
                    decimal ADVS = Convert.ToDecimal(dataTable.Compute("SUM(ADV)", string.Empty));
                    decimal ITS = Convert.ToDecimal(dataTable.Compute("SUM(IT)", string.Empty));
                    decimal INSS = Convert.ToDecimal(dataTable.Compute("SUM(Ins)", string.Empty));
                    decimal LWFS = Convert.ToDecimal(dataTable.Compute("SUM(LWF)", string.Empty));
                    decimal SWFS = Convert.ToDecimal(dataTable.Compute("SUM(SWF)", string.Empty));
                    decimal D1S = Convert.ToDecimal(dataTable.Compute("SUM(D1)", string.Empty));
                    decimal D2S = Convert.ToDecimal(dataTable.Compute("SUM(D2)", string.Empty));
                    decimal CPTS = Convert.ToDecimal(dataTable.Compute("SUM(CPT)", string.Empty));
                    decimal OnedayS = Convert.ToDecimal(dataTable.Compute("SUM(OneDay)", string.Empty));
                    decimal Others = Convert.ToDecimal(dataTable.Compute("SUM(OTH)", string.Empty));

                    decimal Deduction = PfS + ESIS + ADVS + ITS + INSS + LWFS + SWFS + D1S + D2S + CPTS + OnedayS;

                    decimal Basic = Convert.ToDecimal(dataTable.Compute("SUM(Basic)", string.Empty));
                    decimal DA = Convert.ToDecimal(dataTable.Compute("SUM(DA)", string.Empty));
                    decimal HRA = Convert.ToDecimal(dataTable.Compute("SUM(HRA)", string.Empty));
                    decimal CCA = Convert.ToDecimal(dataTable.Compute("SUM(CCA)", string.Empty));
                    decimal Conv = Convert.ToDecimal(dataTable.Compute("SUM(Conv)", string.Empty));
                    decimal EDU = Convert.ToDecimal(dataTable.Compute("SUM(EDU)", string.Empty));
                    decimal Unif = Convert.ToDecimal(dataTable.Compute("SUM(Unif)", string.Empty));
                    decimal Medical = Convert.ToDecimal(dataTable.Compute("SUM(Medical)", string.Empty));

                    decimal Earnings = Basic + DA + HRA + CCA + Conv + EDU + Unif + Medical;


                    DataRow row = dtSummary.NewRow();
                    row["Basic"] = Basic;
                    row["FDA"] = DA;
                    row["HRA"] = HRA;
                    row["CCA"] = CCA;
                    row["Conveyance"] = Conv;
                    row["Education"] = EDU;
                    row["Uniform"] = Unif;
                    row["Medical"] = Medical;
                    row["PF"] = PfS;
                    row["ESI"] = ESIS;
                    row["SalaryAdvance"] = ADVS;
                    row["IncomeTax"] = ITS;
                    row["Insurance"] = INSS;
                    row["LWF"] = LWFS;
                    row["SWF"] = SWFS;
                    row["Others"] = Others;
                    row["Deduction1"] = D1S;
                    row["Deduction2"] = D2S;
                    row["CropTax"] = CPTS;
                    row["1DaySalary"] = OnedayS;
                    row["Earnings"] = Earnings;
                    row["Deduction"] = Deduction;
                    row["NettPay"] = Earnings - Deduction;
                    dtSummary.Rows.Add(row);

                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CrySalaryRegister.rpt");
                    doc.Subreports["CrySalarySummary.rpt"].SetDataSource(dtSummary);
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                    textObject.Text = "Working Days : 26  - Salary Statement for : " + GeneralParameters.PaMonth + " of Branch : " + GeneralParameters.Branch + " Department : " + GeneralParameters.Department + "";
                    doc.SetDataSource(dataTable);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void loadempfinalrpt()
        {
            try
            {

                SqlParameter[] sqlParameters = {
                                        new SqlParameter("@EmpId",GeneralParameters.EmpId),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GETEMPFINALCLEARFORM", sqlParameters, conn);
                string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryEmpFinal.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompanyName"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void Withoutleavelist()
        {
            try
            {
                //GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                int pp, MM;
                DateTime dt1 = Convert.ToDateTime(GeneralParameters.ReportDate);
                string dte = "01-" + dt1.Month.ToString("00") + "-" + dt1.Year;
                DateTime fromdate = Convert.ToDateTime(dte);
                string qur1 = "exec sp_GETDAYS '" + Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd") + "'";
                SqlCommand cmd = new SqlCommand(qur1, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                pp = Convert.ToInt32(tab.Rows[0]["days"].ToString());


                string qur2 = "SELECT * FROM  PAYMONTH WHERE  MONTH(MSDT)='" + dt1.Month.ToString() + "'";
                SqlCommand cmd1 = new SqlCommand(qur2, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

                MM = Convert.ToInt32(tab1.Rows[0]["MONID"].ToString());


                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt",MM ),
                    new SqlParameter("@DEPTID",  GeneralParameters.Department),
                    new SqlParameter("@days",  pp),
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GETWITHOUTLEAVE", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryWithoutL.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void Withleavelist()
        {
            try
            {
                //GeneralParameters.Department = "(" + GeneralParameters.Department + ")";
                int pp, MM;
                DateTime dt1 = Convert.ToDateTime(GeneralParameters.ReportDate);
                string dte = "01-" + dt1.Month.ToString("00") + "-" + dt1.Year;
                DateTime fromdate = Convert.ToDateTime(dte);
                string qur1 = "exec sp_GETDAYS '" + Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd") + "'";
                SqlCommand cmd = new SqlCommand(qur1, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                DataTable tab = new DataTable();
                apt.Fill(tab);

                pp = Convert.ToInt32(tab.Rows[0]["days"].ToString());

                string qur2 = "SELECT * FROM  PAYMONTH WHERE  MONTH(MSDT)='" + dt1.Month.ToString() + "'";
                SqlCommand cmd1 = new SqlCommand(qur2, conn);
                SqlDataAdapter apt1 = new SqlDataAdapter(cmd1);
                DataTable tab1 = new DataTable();
                apt1.Fill(tab1);

                MM = Convert.ToInt32(tab1.Rows[0]["MONID"].ToString());


                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt", MM),

                    new SqlParameter("@DEPTID",  GeneralParameters.Department),
                    new SqlParameter("@days",  pp),
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_GETWITHLEAVE", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                    DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryWithL.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void CashStatement()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Branch",GeneralParameters.Branch),
                    new SqlParameter("@MonID",GeneralParameters.PayMonthId),
                    new SqlParameter("@PMode","Cash"),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_BankCashSatement", sqlParameters, conn);
                string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryBankCash.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompanyName"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject txtbranch = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                    txtbranch.Text = "Cash Statement";
                    TextObject textaddress = (TextObject)doc.ReportDefinition.ReportObjects["txtMonth"];
                    textaddress.Text = GeneralParameters.PaMonth;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void BankStatement()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Branch",GeneralParameters.Branch),
                    new SqlParameter("@MonID",GeneralParameters.PayMonthId),
                    new SqlParameter("@PMode","Bank"),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_BankCashSatement", sqlParameters, conn);
                string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryBankCash.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompanyName"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject txtbranch = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                    txtbranch.Text = "Bank Statement";
                    TextObject textaddress = (TextObject)doc.ReportDefinition.ReportObjects["txtMonth"];
                    textaddress.Text = GeneralParameters.PaMonth;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        private void PrintPaySlip()
        {
            try
            {
                GeneralParameters.EmpId = "In (" + GeneralParameters.EmpId + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@BRANCHID",GeneralParameters.ReportBranchId),
                    new SqlParameter("@Monid",GeneralParameters.PayMonthId),
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_SALARYSLIP", sqlParameters, conn);
                string Query = "Select * from Company Where Branch = '" + GeneralParameters.Branch + "'";
                DataTable dataTable = db.GetDataWithoutParam(CommandType.Text, Query, conn);

                SqlParameter[] sqlParameters1 = {
                    new SqlParameter("@BRANCHID",GeneralParameters.ReportBranchId)
                };
                DataTable dataTableSal = db.GetDataWithParam(CommandType.StoredProcedure, "Get_PayDetails", sqlParameters1, conn);

                data.Columns.Add("FBasic", typeof(decimal));
                data.Columns.Add("FHRA", typeof(decimal));
                data.Columns.Add("FDA", typeof(decimal));
                data.Columns.Add("FCA", typeof(decimal));
                data.Columns.Add("FEDUA", typeof(decimal));
                data.Columns.Add("FMA", typeof(decimal));
                data.Columns.Add("FCONV", typeof(decimal));
                data.Columns.Add("FUNIF", typeof(decimal));
                data.Columns.Add("FGROSS", typeof(decimal));

                for (int i = 0; i < dataTableSal.Rows.Count; i++)
                {
                    string Empcode = dataTableSal.Rows[i]["EMPCODE"].ToString();
                    foreach (DataRow dr in data.Rows) // search whole table
                    {
                        if (dr["EMPCODE"].ToString() == Empcode) // if id==2
                        {
                            decimal FBasic = 0;
                            decimal FHRA = 0;
                            decimal FDA = 0;
                            decimal FCA = 0;
                            decimal FEDUA = 0;
                            decimal FMA = 0;
                            decimal FCONV = 0;
                            decimal FUNIF = 0;
                            decimal FGROSS = 0;
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["Basic"].ToString()))
                            {
                                FBasic = 0;
                            }
                            else
                            {
                                FBasic = Convert.ToDecimal(dataTableSal.Rows[i]["Basic"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["HRA"].ToString()))
                            {
                                FHRA = 0;
                            }
                            else
                            {
                                FHRA = Convert.ToDecimal(dataTableSal.Rows[i]["HRA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["CA"].ToString()))
                            {
                                FCA = 0;
                            }
                            else
                            {
                                FCA = Convert.ToDecimal(dataTableSal.Rows[i]["CA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["DA"].ToString()))
                            {
                                FDA = 0;
                            }
                            else
                            {
                                FDA = Convert.ToDecimal(dataTableSal.Rows[i]["DA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["EDUA"].ToString()))
                            {
                                FEDUA = 0;
                            }
                            else
                            {
                                FEDUA = Convert.ToDecimal(dataTableSal.Rows[i]["EDUA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["MA"].ToString()))
                            {
                                FMA = 0;
                            }
                            else
                            {
                                FMA = Convert.ToDecimal(dataTableSal.Rows[i]["MA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["TA"].ToString()))
                            {
                                FCONV = 0;
                            }
                            else
                            {
                                FCONV = Convert.ToDecimal(dataTableSal.Rows[i]["TA"].ToString());
                            }
                            if (string.IsNullOrEmpty(dataTableSal.Rows[i]["UNIA"].ToString()))
                            {
                                FUNIF = 0;
                            }
                            else
                            {
                                FUNIF = Convert.ToDecimal(dataTableSal.Rows[i]["UNIA"].ToString());
                            }
                            FGROSS = FBasic + FHRA + FDA + FCA + FEDUA + FMA + FCONV + FUNIF;
                            dr["FBasic"] = FBasic;
                            dr["FHRA"] = FHRA;
                            dr["FDA"] = FDA;
                            dr["FCA"] = FCA;
                            dr["FEDUA"] = FEDUA;
                            dr["FMA"] = FMA;
                            dr["FCONV"] = FCONV;
                            dr["FUNIF"] = FUNIF;
                            dr["FGROSS"] = FGROSS;
                        }
                    }
                }

                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPaySlipNew.rpt");
                    TextObject textaddress = (TextObject)doc.ReportDefinition.ReportObjects["txtMonth"];
                    textaddress.Text = GeneralParameters.PaMonth;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }


        private void MusterRoll()
        {
            try
            {
                DateTime dt = Convert.ToDateTime(GeneralParameters.ReportDate);
                string dte = "01-" + dt.Month.ToString("00") + "-" + dt.Year;
                DateTime fromdate = Convert.ToDateTime(dte);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt",fromdate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@Branch",GeneralParameters.Branch),
                    new SqlParameter("@TBL",GeneralParameters.TablName)
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_MusterRoll", sqlParameters, conn);
                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryMu.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void EmployeeList()
        {
            try
            {
                if (GeneralParameters.Shift == "1000")
                {
                    GeneralParameters.Shift = "1000";
                }
                else
                {
                    GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                }
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@Branchid",GeneralParameters.BranchId)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_RPT_Emplist", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryEmp.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void LeftEmployeeList()
        {
            try
            {
                DateTime dt1 = Convert.ToDateTime(GeneralParameters.ReportDate);
                string dte = "01-" + dt1.Month.ToString("00") + "-" + dt1.Year;
                DateTime fromdate = Convert.ToDateTime(dte);
                if (GeneralParameters.Shift == "1000")
                {
                    GeneralParameters.Shift = "1000";
                }
                else
                {
                    GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                }
                SqlParameter[] sqlParameters = {
                     new SqlParameter("@DEPTID",GeneralParameters.Department),new SqlParameter("@MON",fromdate.Month.ToString())};
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_LeftEmp_list", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryLeftEmp.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void PerformanceReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyyMMdd")),
                    new SqlParameter("@TBLNAME",GeneralParameters.TablName),
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@LVTAG",GeneralParameters.LVTAG),
                    new SqlParameter("@SHIFT",GeneralParameters.Shift),
                    new SqlParameter("@BRANCHID",GeneralParameters.BranchId)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_InOut", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPerformance.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject txtRptTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                    txtRptTitle.Text = "Punch Report";
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void ManulapuchReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@DEPTID",GeneralParameters.Branch),

                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "SP_Manualpunch", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryManPunch.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void IregularReport()
        {
            try
            {
                GeneralParameters.Department = "In (" + GeneralParameters.Department + ")";
                GeneralParameters.Desingnation = "In (" + GeneralParameters.Desingnation + ")";
                GeneralParameters.Shift = "In (" + GeneralParameters.Shift + ")";
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyyMMdd")),
                    new SqlParameter("@TBLNAME",GeneralParameters.TablName),
                    new SqlParameter("@DEPTID",GeneralParameters.Department),
                    new SqlParameter("@LVTAG",GeneralParameters.LVTAG),
                    new SqlParameter("@SHIFT",GeneralParameters.Shift),
                    new SqlParameter("@BRANCHID",GeneralParameters.BranchId)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_InOut", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryPerformance.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    if (GeneralParameters.LVTAG == "AA")
                    {
                        TextObject txtRptTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                        txtRptTitle.Text = "Absent Report";
                    }
                    else
                    {
                        TextObject txtRptTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                        txtRptTitle.Text = "Iregular Report";
                    }

                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        protected void StrengthReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                    new SqlParameter("@BRANCHID",GeneralParameters.BranchId)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_RPT_ATTABSTRACT", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryStrengthRpt.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void LateInReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@Dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@BRANCHID",GeneralParameters.BranchId)
                };
                DataTable data = db.GetDataWithParam(CommandType.StoredProcedure, "PROC_GetLateEntries", sqlParameters, conn);
                if (data.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryLate.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    doc.SetDataSource(data);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void InReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                    new SqlParameter("@Branchid",GeneralParameters.BranchId)
                };
                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_INRPT", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryInReport.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject txtRptTitle = (TextObject)doc.ReportDefinition.ReportObjects["txtRptTitle"];
                    txtRptTitle.Text = "IN Report";
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        protected void WorkesHoursReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Branchid",GeneralParameters.BranchId)
                };

                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_WorkedHoursLessthan8", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryWorkedHours.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }
        private void LOADEMPDET()
        {

            ReportDocument doc = new ReportDocument();

            SqlDataAdapter da = new SqlDataAdapter("SP_GETEMPMASTPRINT", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@empid", SqlDbType.Int).Value = Genclass.cat;
            DataTable ds = new DataTable();
            da.Fill(ds);
            DataTable dt = new DataTable();

            dt.Columns.Add("Empname", typeof(string));
            dt.Columns.Add("empcode", typeof(string));
            dt.Columns.Add("cardno", typeof(string));
            dt.Columns.Add("deptname", typeof(string));
            dt.Columns.Add("desname", typeof(string));
            dt.Columns.Add("fname", typeof(string));
            dt.Columns.Add("dob", typeof(DateTime));
            dt.Columns.Add("doj", typeof(DateTime));
            dt.Columns.Add("sex", typeof(string));
            dt.Columns.Add("street", typeof(string));
            dt.Columns.Add("city", typeof(string));
            dt.Columns.Add("phone", typeof(string));
            dt.Columns.Add("information", typeof(string));
            dt.Columns.Add("grname", typeof(string));
            dt.Columns.Add("Tsname", typeof(string));
            dt.Columns.Add("shiftname", typeof(string));
            dt.Columns.Add("cl", typeof(decimal));
            dt.Columns.Add("el", typeof(decimal));
            dt.Columns.Add("esino", typeof(string));
            dt.Columns.Add("epfno", typeof(string));
            dt.Columns.Add("uanno", typeof(string));
            dt.Columns.Add("panno", typeof(string));
            dt.Columns.Add("aadharno", typeof(string));
            dt.Columns.Add("acno", typeof(string));
            dt.Columns.Add("emailtag", typeof(string));
            dt.Columns.Add("Appap", typeof(string));
            dt.Columns.Add("appraiser", typeof(string));
            dt.Columns.Add("Eg", typeof(string));
            dt.Columns.Add("woff", typeof(string));
            dt.Columns.Add("pmode", typeof(string));
            dt.Columns.Add("empid", typeof(int));

            dt.Columns.Add("empphoto", typeof(byte[]));
            dt.Columns.Add("amount", typeof(decimal));
            if (ds.Rows.Count != 0)
            {

                dt.Rows.Clear();
                DataRow row = dt.NewRow();
                row["Empname"] = ds.Rows[0]["Empname"].ToString();
                row["empcode"] = ds.Rows[0]["empcode"].ToString();
                row["cardno"] = ds.Rows[0]["cardno"].ToString();
                row["deptname"] = ds.Rows[0]["deptname"].ToString();
                row["desname"] = ds.Rows[0]["desname"].ToString();
                row["fname"] = ds.Rows[0]["fname"].ToString();
                row["dob"] = Convert.ToDateTime(ds.Rows[0]["dob"].ToString()).ToString("dd-MMM-yyyy");
                row["doj"] = Convert.ToDateTime(ds.Rows[0]["doj"].ToString()).ToString("dd-MMM-yyyy");
                row["sex"] = ds.Rows[0]["sex"].ToString();
                row["street"] = ds.Rows[0]["street"].ToString();
                row["city"] = ds.Rows[0]["city"].ToString();
                row["phone"] = ds.Rows[0]["phone"].ToString();
                row["information"] = ds.Rows[0]["information"].ToString();
                row["grname"] = ds.Rows[0]["grname"].ToString();
                row["Tsname"] = ds.Rows[0]["Tsname"].ToString();
                row["shiftname"] = ds.Rows[0]["shiftname"].ToString();
                row["cl"] = ds.Rows[0]["cl"].ToString();
                row["el"] = ds.Rows[0]["el"].ToString();
                row["esino"] = ds.Rows[0]["esino"].ToString();
                row["epfno"] = ds.Rows[0]["epfno"].ToString();
                row["uanno"] = ds.Rows[0]["uanno"].ToString();
                row["panno"] = ds.Rows[0]["panno"].ToString();
                row["aadharno"] = ds.Rows[0]["aadharno"].ToString();
                row["acno"] = ds.Rows[0]["acno"].ToString();
                row["emailtag"] = ds.Rows[0]["emailtag"].ToString();
                row["Appap"] = ds.Rows[0]["Appap"].ToString();
                row["appraiser"] = ds.Rows[0]["appraiser"].ToString();
                row["Eg"] = ds.Rows[0]["Eg"].ToString();
                row["woff"] = ds.Rows[0]["woff"].ToString();
                row["pmode"] = ds.Rows[0]["pmode"].ToString();
                row["empid"] = ds.Rows[0]["empid"].ToString();

                row["empphoto"] = ds.Rows[0]["empphoto"];
                row["amount"] = ds.Rows[0]["amount"];

                dt.Rows.Add(row);
                doc.Load(Application.StartupPath + "\\EmpDet.rpt");
                TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["Text1"];
                textObject.Text = GeneralParameters.Branch;
                //doc.Load(ReportPath + "\\CryEmpprt.rpt");

                doc.SetDataSource(dt);
                CryReportviewer.ReportSource = doc;


            }

        }
        protected void LunchBreakReport()
        {
            try
            {
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@dt",Convert.ToDateTime(GeneralParameters.ReportDate).ToString("yyyy-MM-dd")),
                    new SqlParameter("@TBL",GeneralParameters.TablName),
                    new SqlParameter("@Shiftid",GeneralParameters.Shift),
                    new SqlParameter("@Branchid",GeneralParameters.BranchId)
                };

                DataTable dt = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_LunchBreak", sqlParameters, conn);
                if (dt.Rows.Count > 0)
                {
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\Reports\\CryLunchBreak.rpt");
                    TextObject textObject = (TextObject)doc.ReportDefinition.ReportObjects["txtCompany"];
                    textObject.Text = GeneralParameters.Branch;
                    TextObject textdate = (TextObject)doc.ReportDefinition.ReportObjects["txtDate"];
                    textdate.Text = Convert.ToDateTime(GeneralParameters.ReportDate).ToString("dd-MMM-yyyy");
                    doc.SetDataSource(dt);
                    CryReportviewer.ReportSource = doc;
                }
                else
                {
                    MessageBox.Show("No Data Found", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.StackTrace);
                return;
            }
        }

        private void CryReportviewer_Load(object sender, EventArgs e)
        {

        }
    }
}
