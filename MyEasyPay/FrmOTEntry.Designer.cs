﻿namespace MyEasyPay
{
    partial class FrmOTEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmOTEntry));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GrBack = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnImport = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbBranch = new System.Windows.Forms.ComboBox();
            this.grSearch = new System.Windows.Forms.Panel();
            this.button18 = new System.Windows.Forms.Button();
            this.btnHide = new System.Windows.Forms.Button();
            this.DataGridCommon = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.txtApprovedOT = new System.Windows.Forms.TextBox();
            this.BtnExt = new System.Windows.Forms.Button();
            this.BtnOk = new System.Windows.Forms.Button();
            this.DataGridOT = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DtpDate = new System.Windows.Forms.DateTimePicker();
            this.TxtActualOT = new System.Windows.Forms.TextBox();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.TxtEmpCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnMaster = new System.Windows.Forms.Button();
            this.lblFileName = new System.Windows.Forms.Label();
            this.GrBack.SuspendLayout();
            this.grSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOT)).BeginInit();
            this.SuspendLayout();
            // 
            // GrBack
            // 
            this.GrBack.Controls.Add(this.label7);
            this.GrBack.Controls.Add(this.BtnImport);
            this.GrBack.Controls.Add(this.label6);
            this.GrBack.Controls.Add(this.CmbBranch);
            this.GrBack.Controls.Add(this.grSearch);
            this.GrBack.Controls.Add(this.label5);
            this.GrBack.Controls.Add(this.txtApprovedOT);
            this.GrBack.Controls.Add(this.BtnExt);
            this.GrBack.Controls.Add(this.BtnOk);
            this.GrBack.Controls.Add(this.DataGridOT);
            this.GrBack.Controls.Add(this.label4);
            this.GrBack.Controls.Add(this.label3);
            this.GrBack.Controls.Add(this.label2);
            this.GrBack.Controls.Add(this.DtpDate);
            this.GrBack.Controls.Add(this.TxtActualOT);
            this.GrBack.Controls.Add(this.txtEmpName);
            this.GrBack.Controls.Add(this.TxtEmpCode);
            this.GrBack.Controls.Add(this.label1);
            this.GrBack.Controls.Add(this.BtnMaster);
            this.GrBack.Controls.Add(this.lblFileName);
            this.GrBack.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrBack.Location = new System.Drawing.Point(9, 0);
            this.GrBack.Name = "GrBack";
            this.GrBack.Size = new System.Drawing.Size(593, 498);
            this.GrBack.TabIndex = 0;
            this.GrBack.TabStop = false;
            this.GrBack.Enter += new System.EventHandler(this.GrBack_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(484, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 18);
            this.label7.TabIndex = 424;
            this.label7.Text = "Enter OT in Min";
            // 
            // BtnImport
            // 
            this.BtnImport.BackColor = System.Drawing.Color.White;
            this.BtnImport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnImport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImport.Location = new System.Drawing.Point(10, 466);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(114, 28);
            this.BtnImport.TabIndex = 422;
            this.BtnImport.Text = "Import OT";
            this.BtnImport.UseVisualStyleBackColor = false;
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(189, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 18);
            this.label6.TabIndex = 421;
            this.label6.Text = "Branch";
            // 
            // CmbBranch
            // 
            this.CmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbBranch.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmbBranch.FormattingEnabled = true;
            this.CmbBranch.Location = new System.Drawing.Point(241, 15);
            this.CmbBranch.Name = "CmbBranch";
            this.CmbBranch.Size = new System.Drawing.Size(196, 26);
            this.CmbBranch.TabIndex = 0;
            this.CmbBranch.SelectedIndexChanged += new System.EventHandler(this.CmbBranch_SelectedIndexChanged);
            // 
            // grSearch
            // 
            this.grSearch.BackColor = System.Drawing.Color.White;
            this.grSearch.Controls.Add(this.button18);
            this.grSearch.Controls.Add(this.btnHide);
            this.grSearch.Controls.Add(this.DataGridCommon);
            this.grSearch.Location = new System.Drawing.Point(10, 105);
            this.grSearch.Name = "grSearch";
            this.grSearch.Size = new System.Drawing.Size(369, 274);
            this.grSearch.TabIndex = 419;
            this.grSearch.Visible = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button18.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.button18.Location = new System.Drawing.Point(294, 243);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(71, 28);
            this.button18.TabIndex = 394;
            this.button18.Text = "Select";
            this.button18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button18.UseVisualStyleBackColor = false;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // btnHide
            // 
            this.btnHide.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnHide.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHide.Image = ((System.Drawing.Image)(resources.GetObject("btnHide.Image")));
            this.btnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHide.Location = new System.Drawing.Point(5, 243);
            this.btnHide.Name = "btnHide";
            this.btnHide.Size = new System.Drawing.Size(70, 27);
            this.btnHide.TabIndex = 393;
            this.btnHide.Text = "Close";
            this.btnHide.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHide.UseVisualStyleBackColor = false;
            this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
            // 
            // DataGridCommon
            // 
            this.DataGridCommon.AllowUserToAddRows = false;
            this.DataGridCommon.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.DataGridCommon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCommon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.Location = new System.Drawing.Point(7, 5);
            this.DataGridCommon.Name = "DataGridCommon";
            this.DataGridCommon.ReadOnly = true;
            this.DataGridCommon.RowHeadersVisible = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DataGridCommon.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCommon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCommon.Size = new System.Drawing.Size(358, 236);
            this.DataGridCommon.TabIndex = 0;
            this.DataGridCommon.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridCommon_CellMouseDoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(454, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 18);
            this.label5.TabIndex = 15;
            this.label5.Text = "Approved OT ";
            // 
            // txtApprovedOT
            // 
            this.txtApprovedOT.Location = new System.Drawing.Point(454, 77);
            this.txtApprovedOT.Name = "txtApprovedOT";
            this.txtApprovedOT.Size = new System.Drawing.Size(86, 26);
            this.txtApprovedOT.TabIndex = 5;
            // 
            // BtnExt
            // 
            this.BtnExt.Location = new System.Drawing.Point(507, 467);
            this.BtnExt.Name = "BtnExt";
            this.BtnExt.Size = new System.Drawing.Size(75, 26);
            this.BtnExt.TabIndex = 12;
            this.BtnExt.Text = "Exit";
            this.BtnExt.UseVisualStyleBackColor = true;
            this.BtnExt.Click += new System.EventHandler(this.BtnExt_Click);
            // 
            // BtnOk
            // 
            this.BtnOk.Location = new System.Drawing.Point(541, 76);
            this.BtnOk.Name = "BtnOk";
            this.BtnOk.Size = new System.Drawing.Size(42, 28);
            this.BtnOk.TabIndex = 6;
            this.BtnOk.Text = "Ok";
            this.BtnOk.UseVisualStyleBackColor = true;
            this.BtnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // DataGridOT
            // 
            this.DataGridOT.AllowUserToAddRows = false;
            this.DataGridOT.BackgroundColor = System.Drawing.Color.White;
            this.DataGridOT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridOT.Location = new System.Drawing.Point(10, 105);
            this.DataGridOT.Name = "DataGridOT";
            this.DataGridOT.ReadOnly = true;
            this.DataGridOT.RowHeadersVisible = false;
            this.DataGridOT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridOT.Size = new System.Drawing.Size(572, 360);
            this.DataGridOT.TabIndex = 9;
            this.DataGridOT.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridOT_CellContentClick);
            this.DataGridOT.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridOT_CellMouseDoubleClick);
            this.DataGridOT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridOT_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(351, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Actual OT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(117, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "Emp Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Emp Code";
            // 
            // DtpDate
            // 
            this.DtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtpDate.Location = new System.Drawing.Point(62, 16);
            this.DtpDate.Name = "DtpDate";
            this.DtpDate.Size = new System.Drawing.Size(120, 26);
            this.DtpDate.TabIndex = 5;
            this.DtpDate.ValueChanged += new System.EventHandler(this.DtpDate_ValueChanged);
            // 
            // TxtActualOT
            // 
            this.TxtActualOT.Location = new System.Drawing.Point(351, 77);
            this.TxtActualOT.Name = "TxtActualOT";
            this.TxtActualOT.Size = new System.Drawing.Size(102, 26);
            this.TxtActualOT.TabIndex = 4;
            // 
            // txtEmpName
            // 
            this.txtEmpName.Location = new System.Drawing.Point(115, 77);
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(234, 26);
            this.txtEmpName.TabIndex = 3;
            this.txtEmpName.Click += new System.EventHandler(this.txtEmpName_Click);
            this.txtEmpName.TextChanged += new System.EventHandler(this.TxtEmpName_TextChanged);
            this.txtEmpName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmpName_KeyDown);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.Location = new System.Drawing.Point(10, 77);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Size = new System.Drawing.Size(104, 26);
            this.TxtEmpCode.TabIndex = 2;
            this.TxtEmpCode.Click += new System.EventHandler(this.TxtEmpCode_Click);
            this.TxtEmpCode.TextChanged += new System.EventHandler(this.TxtEmpCode_TextChanged);
            this.TxtEmpCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxtEmpCode_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date";
            // 
            // BtnMaster
            // 
            this.BtnMaster.Location = new System.Drawing.Point(213, 376);
            this.BtnMaster.Name = "BtnMaster";
            this.BtnMaster.Size = new System.Drawing.Size(104, 27);
            this.BtnMaster.TabIndex = 13;
            this.BtnMaster.Text = "Get OT Details";
            this.BtnMaster.UseVisualStyleBackColor = true;
            this.BtnMaster.Click += new System.EventHandler(this.BtnMaster_Click);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(210, 422);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(46, 18);
            this.lblFileName.TabIndex = 423;
            this.lblFileName.Text = "label7";
            this.lblFileName.Visible = false;
            // 
            // FrmOTEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(605, 504);
            this.Controls.Add(this.GrBack);
            this.MaximizeBox = false;
            this.Name = "FrmOTEntry";
            this.Text = "OT Entry";
            this.Load += new System.EventHandler(this.FrmOTEntry_Load);
            this.GrBack.ResumeLayout(false);
            this.GrBack.PerformLayout();
            this.grSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCommon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridOT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrBack;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker DtpDate;
        private System.Windows.Forms.TextBox TxtActualOT;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.TextBox TxtEmpCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DataGridOT;
        private System.Windows.Forms.Button BtnOk;
        private System.Windows.Forms.Button BtnExt;
        private System.Windows.Forms.Button BtnMaster;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtApprovedOT;
        private System.Windows.Forms.Panel grSearch;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button btnHide;
        private System.Windows.Forms.DataGridView DataGridCommon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CmbBranch;
        private System.Windows.Forms.Button BtnImport;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label7;
    }
}