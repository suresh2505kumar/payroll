﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Configuration;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace MyEasyPay
{
    public partial class FrmPun : Form
    {

        public FrmPun()
        {
            InitializeComponent();
        }
        DataGridViewComboBoxCell cbxComboRow6;
        ComboBox cbxRow5;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        SqlCommand qur = new SqlCommand();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        int EmpId = 0;
        private DataRow doc1;
        DataTable Docno = new DataTable();
        int Loadid = 0;

        BindingSource bsOTEntry = new BindingSource();
        DataTable dtmon = new DataTable();

        int Uid = 0;
        int Fillid;
        BindingSource bsp = new BindingSource();
        string str1key;
        public int SelectId = 0;
        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            DtpTodate_ValueChanged(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            pnlmiss.Visible = true;



        }


        private void loadgg()
        {
            Titlep();
            DateTime dateTime = Convert.ToDateTime(dtpFromDate.Text);
            string quy = "Select  c.empid,a.empcode,c.empname,convert(nvarchar(5),a.in1,108) InTime," +
                "convert(nvarchar(5),a.Out1,108) as Breakin,convert(nvarchar(5),a.in2,108) as Breakout," +
                "convert(nvarchar(5),a.Out2,108) as Outtime,a.Lvtag " +
                "from Att_Reg" + dateTime.Month.ToString("00") + "  a  " +
                "inner join emp_mast c on a.empcode=c.empcode " +
                "Where a.Dt ='" + dateTime.Date.ToString("yyyy-MM-dd") + "' and c.Active =1 and LvTag ='" + CmbLvTag.Text + "'";
            Genclass.cmd = new SqlCommand(quy, conn);
            SqlDataAdapter aptr = new SqlDataAdapter(Genclass.cmd);
            DataTable tap = new DataTable();
            aptr.Fill(tap);

            for (int k = 0; k < tap.Rows.Count; k++)
            {
                var index = DataGridPunch.Rows.Add();
                DataGridPunch.Rows[index].Cells[0].Value = tap.Rows[k]["empid"].ToString();
                DataGridPunch.Rows[index].Cells[1].Value = tap.Rows[k]["empcode"].ToString();
                DataGridPunch.Rows[index].Cells[2].Value = tap.Rows[k]["empname"].ToString();
                DataGridPunch.Rows[index].Cells[3].Value = tap.Rows[k]["InTime"].ToString();
                DataGridPunch.Rows[index].Cells[4].Value = tap.Rows[k]["Breakin"].ToString();
                DataGridPunch.Rows[index].Cells[5].Value = tap.Rows[k]["Breakout"].ToString();
                DataGridPunch.Rows[index].Cells[6].Value = tap.Rows[k]["Outtime"].ToString();
                DataGridPunch.Rows[index].Cells[7].Value = tap.Rows[k]["lvtag"].ToString();
            }

        }

        private void Titlep()
        {
            DataGridPunch.AutoGenerateColumns = false;
            DataGridPunch.DataSource = null;
            DataGridPunch.ColumnCount = 8;

            DataGridPunch.Columns[0].Name = "EmpId";
            DataGridPunch.Columns[1].Name = "EmpCode";
            DataGridPunch.Columns[2].Name = "EmpName";
            DataGridPunch.Columns[3].Name = "Grade";
            DataGridPunch.Columns[4].Name = "Date";
            DataGridPunch.Columns[5].Name = "In Time";
            DataGridPunch.Columns[6].Name = "Out Time";
            DataGridPunch.Columns[7].Name = "Status";

            DataGridPunch.Columns[0].Visible = false;
            DataGridPunch.Columns[1].Width = 100;
            DataGridPunch.Columns[2].Width = 150;
            DataGridPunch.Columns[3].Width = 90;
            DataGridPunch.Columns[4].Width = 90;
            DataGridPunch.Columns[5].Width = 70;
            DataGridPunch.Columns[6].Width = 70;
            DataGridPunch.Columns[7].Width = 100;
            DataGridPunch.Columns[4].DefaultCellStyle.Format = "dd-MMM-yyyy";
        }
        private void FrmPun_Load(object sender, EventArgs e)
        {
            GeneralParameters.BranchId = Convert.ToInt32(ConfigurationManager.AppSettings["BranchId"].ToString());
            Loadid = 1;
            DataGridPunch.RowHeadersVisible = false;
            qur.Connection = conn;
            Titlep();
            DataTable dataTable = Ctype();
            CmbLvTag.DataSource = null;
            CmbLvTag.DisplayMember = "SName";
            CmbLvTag.ValueMember = "LvId";
            CmbLvTag.DataSource = dataTable;
         
            pnlmiss.Visible = false;
            DataTable dtBranch = new DataTable();
            SqlParameter[] sqlParameters = { new SqlParameter("@BranchId", GeneralParameters.BranchId) };
            dtBranch = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_GetBranch", sqlParameters, conn);

            //string Branch1 = ConfigurationManager.AppSettings["Branch"].ToString();
            //var BranchA1 = new List<string>(ConfigurationManager.AppSettings["MultiBranch1"].Split(new char[] { ';' }));
            DataTable dataTable1 = dtBranch;
            if (GeneralParameters.BranchId == 0)
            {
                DataRow dataRow = dataTable1.NewRow();
                dataRow["Uid"] = 0;
                dataRow["BRANCHNAME"] = "All";
                dataTable1.Rows.Add(dataRow);
            }
            CmbBranch.DisplayMember = "BRANCHNAME";
            CmbBranch.ValueMember = "Uid";
            CmbBranch.DataSource = dataTable1;
            Loadid = 0;
        }

        private void HFIT_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "")
            {
                if (DataGridPunch.CurrentCell.ColumnIndex == 3)

                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[3];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }

                else if (DataGridPunch.CurrentCell.ColumnIndex == 6)

                {
                    DataGridViewCell cell = DataGridPunch.CurrentRow.Cells[6];
                    DataGridPunch.CurrentCell = cell;
                    DataGridPunch.BeginEdit(true);
                }
                else if (DataGridPunch.CurrentCell.ColumnIndex == 7)
                {
                    DataGridViewComboBoxCell l_objGridDropbox = new DataGridViewComboBoxCell();
                    if (DataGridPunch.CurrentRow.Cells[7].Value == null || DataGridPunch.CurrentRow.Cells[7].Value.ToString() != "")
                    {
                        DataTable nm = Ctype();
                        DataGridPunch[e.ColumnIndex, e.RowIndex] = l_objGridDropbox;
                        l_objGridDropbox.DataSource = nm; // Bind combobox with datasource.  
                        l_objGridDropbox.DisplayMember = "sname";
                        l_objGridDropbox.ValueMember = "sname";
                    }
                }
            }
        }

        private void HFIT_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (DataGridPunch.RowCount > 1)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    if (DataGridPunch.CurrentRow.Cells[0].Value.ToString() != "0")
                    {
                        if (DataGridPunch.CurrentCell.ColumnIndex == 5 || DataGridPunch.CurrentCell.ColumnIndex == 6 || DataGridPunch.CurrentCell.ColumnIndex == 7)
                        {
                            if (string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[5].Value.ToString()) || string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[6].Value.ToString()) || string.IsNullOrEmpty(DataGridPunch.CurrentRow.Cells[7].Value.ToString()))
                            {
                                MessageBox.Show("Status and IN OUT ccan't empty", "Infrmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            if (DataGridPunch.CurrentRow.Cells[5].Value.ToString() != string.Empty && DataGridPunch.CurrentRow.Cells[6].Value.ToString() != string.Empty && DataGridPunch.CurrentRow.Cells[7].Value.ToString() != "Ir")
                            {
                                DateTime dateTime = Convert.ToDateTime(dtpFromDate.Text);
                                string tablename = "ATT_REG" + dateTime.Month.ToString("00") + "";
                                //string Query = "update Att_Reg" + dateTime.Month.ToString("00") + " set  in1='" + DataGridPunch.CurrentRow.Cells[3].Value.ToString() + "' ,Out2='" + DataGridPunch.CurrentRow.Cells[6].Value.ToString() + "' ,lvtag='" + DataGridPunch.CurrentRow.Cells[7].Value.ToString() + "'  where  empid=" + DataGridPunch.CurrentRow.Cells[0].Value.ToString() + "  AND DT='" + Convert.ToDateTime(DataGridPunch.CurrentRow.Cells[4].Value.ToString()).ToString("yyyy-MM-dd") + "'";
                                //db.ExecuteNonQuery(CommandType.Text, Query, conn);
                                SqlParameter[] sqlParameters = {
                                    new SqlParameter("@empid", DataGridPunch.CurrentRow.Cells[0].Value.ToString()),
                                    new SqlParameter("@empcode",DataGridPunch.CurrentRow.Cells[1].Value.ToString()),
                                    new SqlParameter("@empname",DataGridPunch.CurrentRow.Cells[2].Value.ToString()),
                                    new SqlParameter("@intime",DataGridPunch.CurrentRow.Cells[5].Value.ToString()),
                                    new SqlParameter("@breakin",DBNull.Value),
                                    new SqlParameter("@breakout",DBNull.Value),
                                    new SqlParameter("@outtime",DataGridPunch.CurrentRow.Cells[6].Value.ToString()),
                                    new SqlParameter("@lvtag",DataGridPunch.CurrentRow.Cells[7].Value.ToString()),
                                    new SqlParameter("@dt",Convert.ToDateTime(DataGridPunch.CurrentRow.Cells[4].Value.ToString()).ToString("yyyy-MM-dd")),
                                    new SqlParameter("@DISPENSARY",CmbBranch.Text)
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "Proc_Pundetlogfile", sqlParameters, conn);

                                SqlParameter[] sqlParametersPunchDet = {
                                    new SqlParameter("@EMPCODE",DataGridPunch.CurrentRow.Cells[1].Value.ToString()),
                                    new SqlParameter("@TBL",tablename),
                                    new SqlParameter("@DT",Convert.ToDateTime(DataGridPunch.CurrentRow.Cells[4].Value.ToString()).ToString("yyyy-MM-dd")),
                                    new SqlParameter("@IN",DataGridPunch.CurrentRow.Cells[5].Value.ToString()),
                                    new SqlParameter("@OUT",DataGridPunch.CurrentRow.Cells[6].Value.ToString()),
                                };
                                db.ExecuteNonQuery(CommandType.StoredProcedure, "PROC_UPDATE_EMPPUNCH", sqlParametersPunchDet, conn);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Status can't set Ir", "Infrmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }
        }

        private void HFIT_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private DataTable Ctype()
        {
            DataTable tab = new DataTable();
            if (Loadid == 0)
            {
                if (DataGridPunch.CurrentRow.Cells[0].Value != null)
                {
                    string qur = "select sname,lvid from leave Order by LvId";
                    SqlCommand cmd = new SqlCommand(qur, conn);
                    SqlDataAdapter apt = new SqlDataAdapter(cmd);
                    apt.Fill(tab);
                    tab = tab.Select("sname in ('Ir','PP')", null).CopyToDataTable();
                }
            }
            else
            {
                string qur = "select sname,lvid from leave Order by LvId";
                SqlCommand cmd = new SqlCommand(qur, conn);
                SqlDataAdapter apt = new SqlDataAdapter(cmd);
                apt.Fill(tab);
                tab = tab.Select("sname in ('Ir','PP')", null).CopyToDataTable();
            }
            return tab;


        }
        private void HFIT_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }
        private void cbxItemCode_SelectionChangeCommitted(object sender, EventArgs e)
        {


            this.DataGridPunch.CurrentRow.Cells[7].Value = this.cbxRow5.Text;

        }

        private void HFIT_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (DataGridPunch.CurrentCell.ColumnIndex == 7)
            {

                if (DataGridPunch.CurrentCell.ColumnIndex == DataGridPunch.CurrentRow.Cells[7].ColumnIndex)
                {



                    if (e.Control is ComboBox)
                    {
                        cbxRow5 = e.Control as ComboBox;
                        cbxRow5.SelectionChangeCommitted -= new EventHandler(cbxItemCode_SelectionChangeCommitted);
                        cbxRow5.SelectionChangeCommitted += new EventHandler(cbxItemCode_SelectionChangeCommitted);
                    }
                }
            }
        }

        private void HFIT_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellClick(sender, e);
        }

        private void HFIT_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            HFIT_CellValueChanged(sender, e);
        }

        private void HFIT_EditingControlShowing_2(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            HFIT_EditingControlShowing_1(sender, e);
        }

        private void CmbLvTag_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Loadid == 0)
            {
                DataGridPunch.Rows.Clear();
                loadgg();

            }
        }

        private void DataGridPunch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pnlmiss_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtempcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empcode LIKE '%{0}%' ", txtempcode.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_TextChanged(object sender, EventArgs e)
        {

            try
            {
                if (SelectId == 0)

                {
                    bsp.Filter = string.Format("empname LIKE '%{0}%' ", txtempname.Text);



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempcode_Click(object sender, EventArgs e)
        {
            Genclass.type = 1;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            //Point loc = FindLocation(txtempcode);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }

        private void txtempcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtempname_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F10)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyCode == Keys.F2)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;

                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                    grSearch.Visible = false;
                    SelectId = 0;
                }
                else if (e.KeyCode == Keys.Escape)
                {
                    grSearch.Visible = false;
                }
                else if (e.KeyValue == 40)
                {
                    DataGridCommon.Select();
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    txtempcode_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private Point FindLocation(Control ctrl)
        {
            if (ctrl.Parent is Form)
                return ctrl.Location;
            else
            {
                Point p = FindLocation(ctrl.Parent);
                p.X += ctrl.Location.X;
                p.Y += ctrl.Location.Y;
                return p;
            }
        }
        protected DataTable getParty()
        {
            DataTable dt = new DataTable();
            try
            {
                if (Genclass.type == 1)
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@branch", GeneralParameters.Branch) };
                    dt = db.getDataWithParameterCmdType(CommandType.StoredProcedure, "sp_getempdet", para);
                    bsp.DataSource = dt;

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtempname_Click(object sender, EventArgs e)
        {
            Genclass.type = 2;
            DataTable dt = getParty();
            bsp.DataSource = dt;
            FillGrid2(dt, 1);
            //Point loc = FindLocation(txtempname);
            //grSearch.Location = new Point(loc.X, loc.Y + 20);
            grSearch.Visible = true;
        }
        protected void FillGrid2(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                DataGridCommon.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                DataGridCommon.DefaultCellStyle.WrapMode = DataGridViewTriState.True;


                DataGridCommon.ColumnCount = 3;
                DataGridCommon.Columns[0].Name = "empid";
                DataGridCommon.Columns[0].HeaderText = "empid";
                DataGridCommon.Columns[0].DataPropertyName = "empid";
                DataGridCommon.Columns[1].Name = "empcode";
                DataGridCommon.Columns[1].HeaderText = "empcode";
                DataGridCommon.Columns[1].DataPropertyName = "empcode";
                DataGridCommon.Columns[1].Width = 100;
                DataGridCommon.Columns[2].Name = "empname";
                DataGridCommon.Columns[2].HeaderText = "Employee Name";
                DataGridCommon.Columns[2].DataPropertyName = "empname";
                DataGridCommon.Columns[2].Width = 250;
                DataGridCommon.DataSource = bsp;
                DataGridCommon.Columns[0].Visible = false;




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Genclass.type == 1 || Genclass.type == 2)
                {
                    txtempcode.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtempname.Text = DataGridCommon.Rows[Index].Cells[2].Value.ToString();
                    txtempname.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();

                }


                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            qur.Connection = conn;
            conn.Close();
            conn.Open();


            DateTime dateTime = Convert.ToDateTime(dtpFromDate.Text);
            GeneralParameters.TablName = "ATT_REG" + dateTime.Month.ToString("00") + "";
            qur.CommandText = "PROC_ATTPROCESSSINGLE    '" + dateTime.Date.ToString("yyyy-MM-dd") + " ', '" + GeneralParameters.TablName + "','" + GeneralParameters.Branch + "','" + txtempcode.Text + "' ,'" + txtempname.Tag + "'";
            qur.ExecuteNonQuery();
            pnlmiss.Visible = false;

        }

        private void DtpTodate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DateTime fromDate = Convert.ToDateTime(dtpFromDate.Text);
                DateTime toDtae = Convert.ToDateTime(DtpTodate.Text);
                SqlParameter[] sqlParameters = {
                    new SqlParameter("@TBL","ATT_REG"+fromDate.Month.ToString("00")),
                    new SqlParameter("@Fromdate",fromDate.ToString("yyyy-MM-dd")),
                    new SqlParameter("@ToDate",toDtae.ToString("yyyy-MM-dd")),
                    new SqlParameter("@BranchId",CmbBranch.SelectedValue)
                };
                DataTable dataTable = db.GetDataWithParam(CommandType.StoredProcedure, "Proc_getIrregularPunch", sqlParameters, conn);
                DataGridPunch.Rows.Clear();
                if (dataTable.Rows.Count > 0)
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        int Index = DataGridPunch.Rows[0].Index;
                        DataGridViewRow row = (DataGridViewRow)DataGridPunch.Rows[Index].Clone();
                        row.Cells[0].Value = dataTable.Rows[i]["empid"].ToString();
                        row.Cells[1].Value = dataTable.Rows[i]["empcode"].ToString();
                        row.Cells[2].Value = dataTable.Rows[i]["empname"].ToString();
                        row.Cells[3].Value = dataTable.Rows[i]["GrName"].ToString();
                        row.Cells[4].Value = Convert.ToDateTime(dataTable.Rows[i]["dt"].ToString()).ToString("dd-MMM-yyyy");
                        row.Cells[5].Value = dataTable.Rows[i]["InTime"].ToString();
                        row.Cells[6].Value = dataTable.Rows[i]["Outtime"].ToString();
                        row.Cells[7].Value = dataTable.Rows[i]["Lvtag"].ToString();
                        DataGridPunch.Rows.Add(row);
                    }
                }
                else
                {
                    MessageBox.Show("No data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void CmbBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Loadid == 0)
            {
                DtpTodate_ValueChanged(sender, e);
            }
        }
    }
}